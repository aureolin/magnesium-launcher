package co.aureolin.labs.magnesium.fragment.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import com.android.vending.billing.IInAppBillingService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import co.aureolin.labs.magnesium.HomeActivity;
import co.aureolin.labs.magnesium.R;
import co.aureolin.labs.magnesium.SettingsActivity;
import co.aureolin.labs.magnesium.model.BaseItem;
import co.aureolin.labs.magnesium.util.Helper;
import co.aureolin.labs.magnesium.util.IabPurchaseStartedCallback;
import co.aureolin.labs.magnesium.util.StartIabPurchaseTask;

/**
 * Created by akinwale on 17/02/2016.
 */
public class IndexingAndSearchFragment extends Fragment {

    public static final String DELETE_KEY = "IS.DII";

    public static final String WSR_KEY = "IS.WSR";

    public static final String ISR_KEY = "IS.ISR";

    public static final String IWSR_PURCHASED_KEY = "IS.WSRP";

    public static final String SKU_WEB_SEARCH_RESULTS = "mg_toggle_wsr";

    public static final int DEFAULT_INCLUDE_WEB_SEARCH_RESULTS_VALUE = 1;

    public static final int DEFAULT_DELETE_MONTHS = 3;

    public static final String DEFAULT_INCLUDE_SEARCH_RESULTS = "2,3,4,5,6,8,9,21,22,23,51,61";

    private IInAppBillingService iabService;

    private boolean autoUpdating;

    private EditText numMonthsInput;

    private CheckBox webSearchResultsCheckbox;

    private View wsrAnnotation;

    private CheckBox appsCheckbox;

    private CheckBox callLogsCheckbox;

    private CheckBox contactsCheckbox;

    private CheckBox emailsCheckbox;

    private CheckBox filesCheckbox;

    private CheckBox rssCheckbox;

    private CheckBox smsCheckbox;

    private CheckBox socialMediaCheckbox;

    private StartIabPurchaseTask purchaseTask;

    private boolean startPurchaseInProgress;

    public static IndexingAndSearchFragment newInstance() {
        return new IndexingAndSearchFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SettingsActivity activity = (SettingsActivity) getContext();
        iabService = activity.getIabService();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_indexing_and_search, container, false);

        numMonthsInput = (EditText) rootView.findViewById(R.id.is_delete_items_older_than_input);
        numMonthsInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!autoUpdating) {
                    saveSettings();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        webSearchResultsCheckbox = (CheckBox) rootView.findViewById(R.id.is_include_checkbox_web_search_results);
        webSearchResultsCheckbox.setClickable(true);
        wsrAnnotation = rootView.findViewById(R.id.is_enable_web_search_annotation);

        //webSearchResultsCheckbox.setOnTouchListener(purchaseOnTouchListener);
        wsrAnnotation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if (!webSearchResultsCheckbox.isEnabled() || wsrAnnotation.getVisibility() == View.VISIBLE) {
                if (startPurchaseInProgress) {
                    return;
                }

                startPurchaseInProgress = true;
                if (purchaseTask == null || purchaseTask.getStatus() == AsyncTask.Status.FINISHED) {
                    purchaseTask = new StartIabPurchaseTask(SKU_WEB_SEARCH_RESULTS, iabService, getContext(), new IabPurchaseStartedCallback() {
                        @Override
                        public void onIabPurchaseStarted(Bundle result) {
                            startPurchaseInProgress = false;
                            purchaseTask = null;
                        }
                    });
                    purchaseTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            }
            }
        });

        appsCheckbox = (CheckBox) rootView.findViewById(R.id.is_include_checkbox_apps);
        callLogsCheckbox = (CheckBox) rootView.findViewById(R.id.is_include_checkbox_call_logs);
        contactsCheckbox = (CheckBox) rootView.findViewById(R.id.is_include_checkbox_contacts);
        emailsCheckbox = (CheckBox) rootView.findViewById(R.id.is_include_checkbox_email);
        filesCheckbox = (CheckBox) rootView.findViewById(R.id.is_include_checkbox_files);
        rssCheckbox = (CheckBox) rootView.findViewById(R.id.is_include_checkbox_rss);
        smsCheckbox = (CheckBox) rootView.findViewById(R.id.is_include_checkbox_sms);
        socialMediaCheckbox = (CheckBox) rootView.findViewById(R.id.is_include_checkbox_social_media);
        CheckBox[] checkBoxes = getCheckboxesArray();
        for (int i = 0; i < checkBoxes.length; i++) {
            checkBoxes[i].setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    saveSettings();
                }
            });
        }

        initFromSettings();
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        checkOwnedItems();
    }

    @Override
    public void onPause() {
        saveSettings();
        super.onPause();
    }

    private void initFromSettings() {
        SharedPreferences preferences = getContext().getSharedPreferences(HomeActivity.COMMON_SHARED_PREF_NAME, Context.MODE_PRIVATE);
        boolean proOwned = (preferences.getInt(GeneralSettingsFragment.PRO_OWNED_KEY, 0) == 1);
        boolean wsrPurchased = (preferences.getInt(IWSR_PURCHASED_KEY, 0) == 1);
        if (proOwned || wsrPurchased) {
            wsrAnnotation.setVisibility(View.GONE);
            webSearchResultsCheckbox.setEnabled(true);
        }

        int includeWsr = preferences.getInt(WSR_KEY, DEFAULT_INCLUDE_WEB_SEARCH_RESULTS_VALUE);
        int numDeleteMonths = preferences.getInt(DELETE_KEY, DEFAULT_DELETE_MONTHS);
        String includeSr = preferences.getString(ISR_KEY, DEFAULT_INCLUDE_SEARCH_RESULTS);

        webSearchResultsCheckbox.setChecked(includeWsr == 1);

        autoUpdating = true;
        numMonthsInput.setText(String.valueOf(numDeleteMonths));
        autoUpdating = false;

        if (Helper.isEmpty(includeSr)) {
            includeSr = DEFAULT_INCLUDE_SEARCH_RESULTS;
        }
        List<String> includeSearchResults = Arrays.asList(includeSr.split(","));
        appsCheckbox.setChecked(includeSearchResults.contains(String.valueOf(BaseItem.ITEM_TYPE_APP)));
        callLogsCheckbox.setChecked(includeSearchResults.contains(String.valueOf(BaseItem.ITEM_TYPE_CALL_LOG)));
        contactsCheckbox.setChecked(includeSearchResults.contains(String.valueOf(BaseItem.ITEM_TYPE_CONTACT))
            || includeSearchResults.contains(String.valueOf(BaseItem.ITEM_TYPE_CONTACT_EMAIL))
            || includeSearchResults.contains(String.valueOf(BaseItem.ITEM_TYPE_CONTACT_NUMBER)));
        emailsCheckbox.setChecked(includeSearchResults.contains(String.valueOf(BaseItem.ITEM_TYPE_EMAIL_MESSAGE)));
        filesCheckbox.setChecked(includeSearchResults.contains(String.valueOf(BaseItem.ITEM_TYPE_FILE)));
        rssCheckbox.setChecked(includeSearchResults.contains(String.valueOf(BaseItem.ITEM_TYPE_RSS_FEED_ITEM)));
        smsCheckbox.setChecked(includeSearchResults.contains(String.valueOf(BaseItem.ITEM_TYPE_SMS_MESSAGE)));
        socialMediaCheckbox.setChecked(includeSearchResults.contains(String.valueOf(BaseItem.ITEM_TYPE_FACEBOOK_POST))
            || includeSearchResults.contains(String.valueOf(BaseItem.ITEM_TYPE_INSTAGRAM_POST))
            || includeSearchResults.contains(String.valueOf(BaseItem.ITEM_TYPE_TWITTER_POST)));
    }

    private void saveSettings() {
        final int includeWsr = webSearchResultsCheckbox.isChecked() ? 1 : 0;
        int numDeleteMonths = Helper.parseInt(numMonthsInput.getText().toString());
        if (numDeleteMonths < 1) {
            numDeleteMonths = DEFAULT_DELETE_MONTHS;
            if (numMonthsInput != null) {
                autoUpdating = true;
                numMonthsInput.setText(String.valueOf(numDeleteMonths));
                autoUpdating = false;
            }
        }
        final int numMonths = numDeleteMonths;

        final List<String> searchResultIncludes = new ArrayList<String>();
        if (appsCheckbox.isChecked()) {
            searchResultIncludes.add(String.valueOf(BaseItem.ITEM_TYPE_APP));
        }
        if (callLogsCheckbox.isChecked()) {
            searchResultIncludes.add(String.valueOf(BaseItem.ITEM_TYPE_CALL_LOG));
        }
        if (contactsCheckbox.isChecked()) {
            searchResultIncludes.add(String.valueOf(BaseItem.ITEM_TYPE_CONTACT));
            searchResultIncludes.add(String.valueOf(BaseItem.ITEM_TYPE_CONTACT_NUMBER));
            searchResultIncludes.add(String.valueOf(BaseItem.ITEM_TYPE_CONTACT_EMAIL));
        }
        if (emailsCheckbox.isChecked()) {
            searchResultIncludes.add(String.valueOf(BaseItem.ITEM_TYPE_EMAIL_MESSAGE));
        }
        if (filesCheckbox.isChecked()) {
            searchResultIncludes.add(String.valueOf(BaseItem.ITEM_TYPE_FILE));
        }
        if (rssCheckbox.isChecked()) {
            searchResultIncludes.add(String.valueOf(BaseItem.ITEM_TYPE_RSS_FEED_ITEM));
        }
        if (smsCheckbox.isChecked()) {
            searchResultIncludes.add(String.valueOf(BaseItem.ITEM_TYPE_SMS_MESSAGE));
        }
        if (socialMediaCheckbox.isChecked()) {
            searchResultIncludes.add(String.valueOf(BaseItem.ITEM_TYPE_FACEBOOK_POST));
            searchResultIncludes.add(String.valueOf(BaseItem.ITEM_TYPE_INSTAGRAM_POST));
            searchResultIncludes.add(String.valueOf(BaseItem.ITEM_TYPE_TWITTER_POST));
        }

        (new AsyncTask<Void, Void, Void>() {
            protected Void doInBackground(Void... params) {
                Context context = getContext();
                if (context != null) {
                    SharedPreferences preferences = context.getSharedPreferences(HomeActivity.COMMON_SHARED_PREF_NAME, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putInt(WSR_KEY, includeWsr);
                    editor.putInt(DELETE_KEY, numMonths);
                    editor.putString(ISR_KEY,
                            searchResultIncludes.size() == 0 ? DEFAULT_INCLUDE_SEARCH_RESULTS :
                                    Helper.getCharacterSeparatedValues(",", searchResultIncludes));
                    editor.commit();
                }
                return null;
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private CheckBox[] getCheckboxesArray() {
        return new CheckBox[] {
            webSearchResultsCheckbox,
            appsCheckbox,
            callLogsCheckbox,
            contactsCheckbox,
            emailsCheckbox,
            filesCheckbox,
            rssCheckbox,
            smsCheckbox,
            socialMediaCheckbox
        };
    }

    public void onWsrPurchased() {
        if (wsrAnnotation != null) {
            wsrAnnotation.setVisibility(View.GONE);
        }
        if (webSearchResultsCheckbox != null) {
            webSearchResultsCheckbox.setEnabled(true);
        }
        (new AsyncTask<Void, Void, Void>() {
            protected Void doInBackground(Void... params) {
                Context context = getContext();
                if (context != null) {
                    SharedPreferences preferences = context.getSharedPreferences(HomeActivity.COMMON_SHARED_PREF_NAME, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putInt(IWSR_PURCHASED_KEY, 1);
                    editor.commit();
                }
                return null;
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        Helper.showToast("You can enable or disable the option to include web search results. Thank you for your purchase.", getContext());
    }

    private void checkOwnedItems() {
        if (iabService != null) {
            (new AsyncTask<Void, Void, Bundle>() {
                protected Bundle doInBackground(Void... params) {
                    try {
                        Context context = getContext();
                        if (context != null) {
                            Bundle ownedItems = iabService.getPurchases(3, context.getPackageName(), "inapp", null);
                            return ownedItems;
                        }
                    } catch (RemoteException ex) {

                    }
                    return null;
                }

                protected void onPostExecute(Bundle ownedItems) {
                    if (ownedItems == null) {
                        Helper.showToast("Your in-app purchases could not be verified at this time. Please try again later.", getContext());
                        return;
                    }

                    int response = ownedItems.getInt("RESPONSE_CODE");
                    if (response != 0) {
                        Helper.showToast("The request to verify your in-app purchases failed. Please try again later.", getContext());
                        return;
                    }

                    ArrayList<String> ownedSkus = ownedItems.getStringArrayList("INAPP_PURCHASE_ITEM_LIST");
                    if (ownedSkus.contains(GeneralSettingsFragment.SKU_PRO) || ownedSkus.contains(SKU_WEB_SEARCH_RESULTS)) {
                        if (wsrAnnotation != null) {
                            wsrAnnotation.setVisibility(View.GONE);
                        }
                        if (webSearchResultsCheckbox != null) {
                            webSearchResultsCheckbox.setEnabled(true);
                        }

                        Context context = getContext();
                        if (context != null) {
                            final SharedPreferences preferences =
                                    context.getSharedPreferences(HomeActivity.COMMON_SHARED_PREF_NAME, Context.MODE_PRIVATE);
                            int wsrPurchased = preferences.getInt(IWSR_PURCHASED_KEY, 0);
                            if (wsrPurchased == 0) {
                                (new AsyncTask<Void, Void, Void>() {
                                    protected Void doInBackground(Void... params) {
                                        SharedPreferences.Editor editor = preferences.edit();
                                        editor.putInt(IWSR_PURCHASED_KEY, 1);
                                        editor.commit();
                                        return null;
                                    }
                                }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            }
                        }
                    } else {
                        if (wsrAnnotation != null) {
                            wsrAnnotation.setVisibility(View.VISIBLE);
                        }
                        if (webSearchResultsCheckbox != null) {
                            webSearchResultsCheckbox.setEnabled(false);
                        }
                    }
                }

            }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }
}
