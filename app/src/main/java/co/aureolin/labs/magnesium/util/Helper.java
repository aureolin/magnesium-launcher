package co.aureolin.labs.magnesium.util;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.fasterxml.jackson.databind.JsonNode;

import java.io.Closeable;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import co.aureolin.labs.magnesium.HomeActivity;
import co.aureolin.labs.magnesium.model.BaseItem;
import co.aureolin.labs.magnesium.model.SocialMediaAccount;

/**
 * Created by akinwale on 08/02/2016.
 */
public final class Helper {

    public static final String URI_REGEX_PATTERN = "((([A-Za-z]{3,9}:(?:\\/\\/)?)(?:[-;:&=\\+\\$,\\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\\+\\$,\\w]+@)[A-Za-z0-9.-]+)((?:\\/[\\+~%\\/.\\w-_]*)?\\??(?:[-\\+=&;%@.\\w_]*)#?(?:[\\w]*))?)";

    public static final Map<String, Typeface> fontCache = new HashMap<String, Typeface>();

    public static final Map<String, String> fontFriendlyNameMap = new HashMap<String, String>();
    static {
        fontFriendlyNameMap.put("normal", "Neris-Light.otf");
        fontFriendlyNameMap.put("italic", "Neris-LightItalic.otf");
        fontFriendlyNameMap.put("bold", "Neris-SemiBold.otf");
    }

    public static final Map<String, String> settingDisplayValuesMap = new HashMap<String, String>();
    static {
        Helper.settingDisplayValuesMap.put(HomeActivity.APP_DOCK_POSITION_NONE, "Do not show the app dock");
        Helper.settingDisplayValuesMap.put(HomeActivity.APP_DOCK_POSITION_APP_LIST, "Show app dock in the app drawer only");
        Helper.settingDisplayValuesMap.put(HomeActivity.APP_DOCK_POSITION_GLOBAL, "Show app dock globally");

        Helper.settingDisplayValuesMap.put(HomeActivity.VALUE_APP_DRAWER, "App Drawer");
        Helper.settingDisplayValuesMap.put(HomeActivity.VALUE_FEED, "Universal Feed");
    }

    public static final String GOOGLE_OAUTH_CLIENT_ID = "723579097881-an5k8kdkqpoo30mf4aa4qeqtdsbfteu3.apps.googleusercontent.com";

    public static final String GOOGLE_OAUTH_CLIENT_SECRET = "rNC9snnEaYjtacdTqeYv0Hpt";

    public static final String MAGNESIUM_OAUTH_URL_PREFIX = "https://magnesium.labs.aureolin.co/oauth";

    public static final String GMAIL_OAUTH_CODE_URL_FORMAT =
        "https://accounts.google.com/o/oauth2/v2/auth?response_type=code&client_id=%s&redirect_uri="
            + getFormattedUrlEncodedString(MAGNESIUM_OAUTH_URL_PREFIX) /*getFormattedUrlEncodedString("urn:ietf:wg:oauth:2.0:oob")*/
            + "&access_type=offline&prompt=consent&scope=" + getFormattedUrlEncodedString("https://mail.google.com/");

    public static final String GMAIL_PROFILE_URL = "https://www.googleapis.com/gmail/v1/users/me/profile";

    public static final String GMAIL_MESSAGE_LIST_URL = "https://www.googleapis.com/gmail/v1/users/me/messages";

    public static final String GMAIL_MESSAGE_URL_FORMAT = "https://www.googleapis.com/gmail/v1/users/me/messages/%s";

    public static final String GOOGLE_OAUTH_TOKEN_URL =
        "https://www.googleapis.com/oauth2/v3/token";

    public static final String GOOGLE_OAUTH_TOKEN_URL_FORMAT =
        "https://www.googleapis.com/oauth2/v3/token?code=%s&client_id=%s&client_secret=%s&redirect_uri=%s&grant_type=authorization_code";

    public static final String OAUTH_TOKEN_SECRET_KEY = "oauth_token_secret";

    public static final String OAUTH_TOKEN_KEY = "oauth_token";

    public static final String FACEBOOK_APP_ID = "1675367809413327";

    public static final String FACEBOOK_APP_SECRET = "b26f72650c8cc94a5b153b8317445c93";

    public static final String[] FACEBOOK_SCOPES = {
        "public_profile",
        "user_friends",
        "email",
        /*"user_about_me",
        "user_actions.books",
        "user_actions.fitness",
        "user_actions.music",
        "user_actions.news",
        "user_actions.video",
        "user_birthday",
        "user_education_history",*/
        /*"user_events",
        "user_games_activity",
        "user_hometown",
        "user_likes",
        /*"user_location",
        "user_managed_groups",
        "user_photos",*/
        "user_posts",
        /*"user_relationships",
        "user_relationship_details",
        "user_religion_politics",
        "user_tagged_places",
        "user_videos",
        "user_website",
        "user_work_history",
        "read_custom_friendlists",
        "read_page_mailboxes",
        "manage_pages",
        "publish_pages",
        "publish_actions",
        "rsvp_event",
        "pages_show_list",
        "pages_manage_cta"*/
    };

    public static final String TWITTER_API_KEY = "VkxfILE2vynNyFEKGW8ARu0FK";

    public static final String TWITTER_CREDENTIALS_URL = "https://api.twitter.com/1.1/account/verify_credentials.json";

    public static final String TWITTER_API_CONSUMER_SECRET = "svvmUs7qR3f6iZN9IOC5Of2CESoM5i1TvMCG8d5jDzU4oFZFjp";

    public static final String TWITTER_AUTHORIZE_URL = "https://api.twitter.com/oauth/authorize";

    public static final String TWITTER_POST_UPDATE_URL = "https://api.twitter.com/1.1/statuses/update.json";

    public static final String TWITTER_REQUEST_TOKEN_URL = "https://api.twitter.com/oauth/request_token";

    public static final String TWITTER_OAUTH_TOKEN_URL = "https://api.twitter.com/oauth/access_token";

    public static final String DOCX_MIME_TYPE = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";

    public static final String PDF_MIME_TYPE = "application/pdf";

    public static final SimpleDateFormat GLOBAL_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

    public static final SimpleDateFormat GMAIL_DATE_FORMAT = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss ZZZZ", Locale.ENGLISH);

    public static final SimpleDateFormat GMAIL_DATE_FORMAT_2 = new SimpleDateFormat("d MMM yyyy HH:mm:ss ZZZZ", Locale.ENGLISH);

    public static final SimpleDateFormat GMAIL_DATE_FORMAT_3 = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss zzz", Locale.ENGLISH);

    public static final SimpleDateFormat JSON_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

    public static final SimpleDateFormat ITEM_TIME_FORMAT_12H = new SimpleDateFormat("h:mm a");

    public static final SimpleDateFormat ITEM_TIME_FORMAT_24H = new SimpleDateFormat("HH:mm");

    public static final SimpleDateFormat ITEM_DATETIME_FORMAT_12H = new SimpleDateFormat("h:mm a\nd MMM yy");

    public static final SimpleDateFormat ITEM_DATETIME_FORMAT_24H = new SimpleDateFormat("HH:mm\nd MMM yy");

    public static final SimpleDateFormat ITEM_DATETIME_FORMAT_12H_NO_YEAR = new SimpleDateFormat("h:mm a\nd MMM");

    public static final SimpleDateFormat ITEM_DATETIME_FORMAT_24H_NO_YEAR = new SimpleDateFormat("HH:mm\nd MMM");

    public static final SimpleDateFormat TWITTER_DATE_FORMAT = new SimpleDateFormat("EEE MMM dd HH:mm:ss ZZZZ yyyy", Locale.ENGLISH);

    // 2015-12-06T11:47:57+0000
    public static final SimpleDateFormat FACEBOOK_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZZZZ");

    public static final String FACEBOOK_OAUTH_DIALOG_URL_FORMAT = "https://www.facebook.com/dialog/oauth?response_type=token&client_id=%s&redirect_uri=%s&scope=%s";

    public static final String FACEBOOK_OAUTH_REDIRECT_URL = "https://www.facebook.com/connect/login_success.html";

    //public static final String FACEBOOK_GRAPH_EXCHANGE_TOKEN_URL_FORMAT =
    //    "https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id=%s&client_secret=%s&fb_exchange_token=%s";

    public static final String FACEBOOK_GRAPH_ME_URL = "https://graph.facebook.com/v2.5/me";

    public static final String FACEBOOK_GRAPH_FEED_URL = "https://graph.facebook.com/v2.5/me/feed?fields=id,from,story,message,created_time";

    public static final String INSTAGRAM_OAUTH_AUTHORIZE_URL_FORMAT =
            "https://api.instagram.com/oauth/authorize/?client_id=%s&redirect_uri=%s&scope=%s&response_type=token";

    public static final String[] INSTAGRAM_SCOPES = {
        "basic", /*"public_content", "follower_list",*/ "comments", "relationships", "likes"
    };

    public static final String INSTAGRAM_CLIENT_ID = "05d2e78bf9be49219eb92fba92f106d9";

    public static final String INSTAGRAM_CLIENT_SECRET = "555477e0fddc4518954348cbc2edbd3e";

    public static final String INSTAGRAM_USER_SELF_URL_FORMAT = "https://api.instagram.com/v1/users/self/?access_token=%s";

    //public static final String INSTAGRAM_USER_MEDIA_RECENT_URL_FORMAT = "https://api.instagram.com/v1/users/self/media/liked/?access_token=%s";
    public static final String INSTAGRAM_USER_MEDIA_RECENT_URL_FORMAT = "https://api.instagram.com/v1/users/self/feed/?access_token=%s";

    public static final List<String> VALID_ACCOUNT_TYPES = Arrays.asList(new String[]{
        SocialMediaAccount.TYPE_FACEBOOK,
        SocialMediaAccount.TYPE_INSTAGRAM,
        SocialMediaAccount.TYPE_TWITTER});

    public static boolean isEmpty(String string) {
        return (string == null || string.trim().length() == 0);
    }

    public static boolean isInteger(String string) {
        if (string == null) {
            return false;
        }
        try {
            Integer.parseInt(string);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    public static void closeCloseable(Cursor cursor) {
        if (cursor != null) {
            try {
                cursor.close();
            } catch (Exception e) {
                // ignore and continue
            }
        }
    }

    public static void closeCloseable(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                // ignore and continue
            }
        }
    }

    public static int getStringLength(String value) {
        return (value != null) ? value.length() : 0;
    }

    public static void closeDb(SQLiteDatabase db) {
        if (db != null && db.isOpen()) {
            db.close();
        }
    }

    public static String getDateTimeString(Date dt, boolean is24HourFormat) {
        Calendar now = Calendar.getInstance();
        Calendar dtCal = Calendar.getInstance();
        dtCal.setTime(dt);
        boolean noYear = (now.get(Calendar.YEAR) == dtCal.get(Calendar.YEAR));
        boolean today = (now.get(Calendar.MONTH) == dtCal.get(Calendar.MONTH)
            && (now.get(Calendar.DAY_OF_MONTH) == dtCal.get(Calendar.DAY_OF_MONTH)) && noYear);

        SimpleDateFormat itemDateTimeFormat = null;
        if (today) {
            itemDateTimeFormat = is24HourFormat ? ITEM_TIME_FORMAT_24H : ITEM_TIME_FORMAT_12H;
        } else if (noYear) {
            itemDateTimeFormat = is24HourFormat ? ITEM_DATETIME_FORMAT_24H_NO_YEAR : ITEM_DATETIME_FORMAT_12H_NO_YEAR;
        } else {
            itemDateTimeFormat = is24HourFormat ? ITEM_DATETIME_FORMAT_24H : ITEM_DATETIME_FORMAT_12H;
        }
        return itemDateTimeFormat.format(dt).replace("AM", "am").replace("PM", "pm");
    }

    public static String getSimpleLocationName(String locationName) {
        if (locationName != null && locationName.indexOf(",") > -1) {
            return locationName.substring(0, locationName.indexOf(",")).trim();
        }
        return locationName;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static String getNodeValue(String nodeName, JsonNode parent) {
        JsonNode node = parent.get(nodeName);
        return (node != null && !node.isNull()) ? node.asText().trim() : null;
    }

    public static long getLongNodeValue(String nodeName, JsonNode parent) {
        JsonNode node = parent.get(nodeName);
        return (node != null && !node.isNull()) ? node.asLong() : -1;
    }

    public static boolean getBooleanNodeValue(String nodeName, JsonNode parent) {
        JsonNode node = parent.get(nodeName);
        return (node != null && !node.isNull()) ? node.asBoolean() : false;
    }

    public static Map<String, String> splitQuery(String query) {
        Map<String, String> queryPairs = new LinkedHashMap<>();
        String[] pairs = query.split("&");
        for (String pair : pairs) {
            int idx = pair.indexOf("=");
            try {
                queryPairs.put(URLDecoder.decode(pair.substring(0, idx), "utf-8"), URLDecoder.decode(pair.substring(idx + 1), "utf-8"));
            } catch (UnsupportedEncodingException ex) {
                // ignore
            }
        }
        return queryPairs;
    }

    public static String getCharacterSeparatedValues(String separator, List<String> valueList) {
        if (valueList == null || valueList.size() == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        String delim = "";
        for (int i = 0; i < valueList.size(); i++) {
            sb.append(delim).append(valueList.get(i));
            delim = separator;
        }
        return sb.toString();
    }

    public static int parseInt(String value) {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException ex) {
            return -1;
        }
    }

    public static long parseLong(String value) {
        try {
            return Long.parseLong(value);
        } catch (NumberFormatException ex) {
            return -1;
        }
    }

    public static String getUrlEncodedString(String value) {
        try {
            return URLEncoder.encode(value, "utf-8");
        } catch (UnsupportedEncodingException ex) {
            return value;
        }
    }

    public static String getFormattedUrlEncodedString(String value) {
        String newValue = getUrlEncodedString(value);
        newValue = newValue.replaceAll("%", "%%");
        return newValue;
    }

    public static String getPlainTwitterName(String name) {
        if (name.startsWith("@")) {
            return name.substring(1);
        }
        return name;
    }

    public static void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException ex) {
            // continue
        }
    }

    public static String getFacebookUrlWithAccessToken(String url, String accessToken) {
        return String.format((url.indexOf("?") > -1) ? "%s&access_token=%s" : "%s?access_token=%s",
            url,
            getUrlEncodedString(accessToken));
    }

    public static Typeface getTypeface(String friendlyName, Context context) {
        Typeface tf = fontCache.get(friendlyName);
        if (tf == null) {
            try {
                tf = Typeface.createFromAsset(
                        context.getAssets(), String.format("fonts/%s", fontFriendlyNameMap.get(friendlyName)));
            }
            catch (Exception ex) {
                return null;
            }
            fontCache.put(friendlyName, tf);
        }
        return tf;
    }

    public static void showSoftKeyboard(EditText editText, Context context) {
        if (context != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
        }

    }

    public static void clearEditTextFocus(EditText editText, Context context) {
        if (editText != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (editText.hasFocus()) {
                editText.clearFocus();
            }
            imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
        }
    }

    public static String encrypt(String value, String encryptionKey) {
        String result = null;
        try {
            byte[] key = encryptionKey.getBytes("UTF8");
            MessageDigest sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16);

            SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
            byte[] valueBytes = value.getBytes("UTF8");
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS7Padding", "BC");
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
            result = Base64.encodeToString(cipher.doFinal(valueBytes), Base64.DEFAULT);
        } catch (InvalidKeyException | UnsupportedEncodingException
                | NoSuchAlgorithmException | NoSuchProviderException | NoSuchPaddingException
                | BadPaddingException | IllegalBlockSizeException e) {
            throw new RuntimeException(e);
        }

        return result;
    }

    public static String decrypt(String value, String encryptionKey) {
        String result = null;
        try {
            byte[] key = encryptionKey.getBytes("UTF8");
            MessageDigest sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16);

            SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
            byte[] valueBytes = Base64.decode(value, Base64.DEFAULT);
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS7Padding", "BC"); // cipher is not thread safe
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
            result = new String(cipher.doFinal(valueBytes), Charset.forName("UTF8"));
        } catch (InvalidKeyException | UnsupportedEncodingException
                | NoSuchAlgorithmException | NoSuchProviderException | NoSuchPaddingException
                | BadPaddingException | IllegalBlockSizeException e) {
            throw new RuntimeException(e);
        }

        return result;
    }

    public static void showToast(int stringId, Context context) {
        showToast(stringId, context, Toast.LENGTH_SHORT);
    }

    public static void showToast(String message, Context context) {
        showToast(message, context, Toast.LENGTH_SHORT);
    }

    public static void showToast(int stringId, Context context, int length) {
        if (context != null) {
            Toast.makeText(context, stringId, length).show();
        }
    }

    public static void showToast(String message, Context context, int length) {
        if (context != null) {
            Toast.makeText(context, message, length).show();
        }
    }

    public static int fahrenheitToCelsius(int fahrenheit) {
        return (int) ((fahrenheit - 32.0) / 1.8);
    }

    public static boolean isImage(String filename) {
        return filename.endsWith(".jpg") || filename.endsWith(".jpeg")
                || filename.endsWith(".png") || filename.endsWith(".gif")
                || filename.endsWith(".bmp");
    }

    public static void sendBroadcast(String action, Context context) {
        Intent intent = new Intent(action);
        context.sendBroadcast(intent);
    }

    public static int getDipForDimension(int dimension, Context context) {
        if (context != null) {
            DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
            return Float.valueOf(dimension / displayMetrics.density).intValue();
        }
        return 0;
    }

    public static int getValueForDip(int value, Context context) {
        if (context != null) {
            return Float.valueOf(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                    value, context.getResources().getDisplayMetrics())).intValue();
        }
        return 0;
    }

    public static boolean isInIncludedResults(BaseItem item, List<String> includedResults) {
        return includedResults.contains(String.valueOf(item.getType()));
    }

    public static String getDisplayUrl(String url) {
        Uri uri = Uri.parse(url);
        return String.format("%s%s", uri.getHost(), uri.getPath());
    }

    public static String htmlise(String text) {
        String html = "";
        Pattern pattern = Pattern.compile(URI_REGEX_PATTERN);
        html = pattern.matcher(text).replaceAll("<a href=\"$0\">$0</a>");
        return html;
    }

    public static String getReadableTime(long diffSeconds, boolean withSeconds) {
        if (diffSeconds < 0) {
            return "";
        }

        int numDays = Long.valueOf(diffSeconds / (60 * 60 * 24)).intValue();
        diffSeconds = diffSeconds % (60 * 60 * 24);
        int numHours = Long.valueOf(diffSeconds / (60 * 60)).intValue();
        diffSeconds = diffSeconds % (60 * 60);
        int minutes = Long.valueOf(diffSeconds / 60).intValue();
        diffSeconds = diffSeconds % 60;

        if (numDays == 0 && numHours == 0 && minutes == 0 && withSeconds) {
            return String.format("%ds", diffSeconds);
        } else if (numDays == 0 && numHours == 0) {
            return (withSeconds && diffSeconds > 0) ?
                String.format("%dm %ds", minutes, diffSeconds)
                : String.format("%dm", minutes);
        } else if (numDays == 0 && numHours != 0) {
            return (withSeconds && diffSeconds > 0) ? String.format("%dh %dm %ds", numHours, minutes, diffSeconds)
                : ((minutes > 0) ? String.format("%dh %dm", numHours, minutes) : String.format("%dh", numHours));
        }

        return (withSeconds && diffSeconds > 0) ? String.format("%dd %dh %dm %ds", numDays, numHours, minutes, diffSeconds)
            : String.format("%dd %dh %dm", numDays, numHours, minutes);
    }

    public static boolean hasPermission(String permission, Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return (ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED);
        }
        return true;
    }

    public static boolean isTelephoneNumber(String value) {
        if (isEmpty(value)) {
            return false;
        }
        String numString = ((value.startsWith("+") || value.startsWith("*") || value.startsWith("#")) ? value.substring(1) : value);
        try {
            Long.parseLong(numString);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    public static boolean isUssd(String value) {
        if (isEmpty(value)) {
            return false;
        }
        boolean hasStartAndEnd = (value.startsWith("*") || value.startsWith("#")) && (value.endsWith("*") || value.endsWith("#"));
        if (hasStartAndEnd) {
            return (value.length() == 1) ? true : isTelephoneNumber(value.substring(1, value.length() - 1));
        }
        return false;
    }

    public static String getDisplayValueForSettingValue(String value) {
        if (value != null && value.indexOf("=") > -1) {
            // Handle key value pair
            String[] parts = value.split("=");
            return parts[0];
        }

        if (settingDisplayValuesMap.containsKey(value)) {
            return settingDisplayValuesMap.get(value);
        }

        return value;
    }

    public static String join(String delimiter, List<String> values) {
        StringBuilder sb = new StringBuilder();
        String delim = "";
        for (int i = 0; i < values.size(); i++) {
            sb.append(delim).append(values);
            delim = delimiter;
        }

        return sb.toString();
    }

    public static String base64Decode(String base64) {
        base64 = base64.replaceAll("-", "+").replaceAll("_", "/");
        try {
            return new String(Base64.decode(base64, Base64.NO_WRAP), "UTF-8");
        } catch (Exception ex) {
            return null;
        }
    }

    public static Bitmap trimBitmap(Bitmap bmp) {
        int imgHeight = bmp.getHeight();
        int imgWidth  = bmp.getWidth();

        int startWidth = 0;
        for(int x = 0; x < imgWidth; x++) {
            if (startWidth == 0) {
                for (int y = 0; y < imgHeight; y++) {
                    if (bmp.getPixel(x, y) != Color.TRANSPARENT) {
                        startWidth = x;
                        break;
                    }
                }
            } else break;
        }

        int endWidth  = 0;
        for (int x = imgWidth - 1; x >= 0; x--) {
            if (endWidth == 0) {
                for (int y = 0; y < imgHeight; y++) {
                    if (bmp.getPixel(x, y) != Color.TRANSPARENT) {
                        endWidth = x;
                        break;
                    }
                }
            } else break;
        }

        int startHeight = 0;
        for(int y = 0; y < imgHeight; y++) {
            if (startHeight == 0) {
                for (int x = 0; x < imgWidth; x++) {
                    if (bmp.getPixel(x, y) != Color.TRANSPARENT) {
                        startHeight = y;
                        break;
                    }
                }
            } else break;
        }

        int endHeight = 0;
        for(int y = imgHeight - 1; y >= 0; y--) {
            if (endHeight == 0 ) {
                for (int x = 0; x < imgWidth; x++) {
                    if (bmp.getPixel(x, y) != Color.TRANSPARENT) {
                        endHeight = y;
                        break;
                    }
                }
            } else break;
        }


        return Bitmap.createBitmap(
            bmp,
            startWidth,
            startHeight,
            (endWidth - startWidth) == 0 ? 1 : (endWidth - startWidth),
            (endHeight - startHeight) == 0 ? 1 : (endHeight - startHeight)
        );
    }
}