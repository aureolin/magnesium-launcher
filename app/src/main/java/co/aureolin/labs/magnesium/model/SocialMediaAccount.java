package co.aureolin.labs.magnesium.model;

import java.util.Date;

import co.aureolin.labs.magnesium.util.Helper;

/**
 * Created by akinwale on 11/02/2016.
 */
public class SocialMediaAccount {
    public static final String TYPE_FACEBOOK = "facebook";

    public static final String TYPE_GOOGLE_PLUS = "google+";

    public static final String TYPE_INSTAGRAM = "instagram";

    public static final String TYPE_TUMBLR = "tumblr";

    public static final String TYPE_TWITTER = "twitter";

    private long id;

    private String accountId;

    private String socialAccountType;

    private String displayName;

    private String oauthData;

    private Date lastSyncedOn;

    public SocialMediaAccount() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getSocialAccountType() {
        return socialAccountType;
    }

    public void setSocialAccountType(String socialAccountType) {
        this.socialAccountType = socialAccountType;
    }

    public String getEncryptedOauthData(String key) {
        return Helper.encrypt(oauthData, key);
    }

    public void setOauthDataFromEncryptedString(String encData, String key) {
        this.oauthData = Helper.decrypt(encData, key);
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getOauthData() {
        return oauthData;
    }

    public void setOauthData(String oauthData) {
        this.oauthData = oauthData;
    }

    public Date getLastSyncedOn() {
        return lastSyncedOn;
    }

    public void setLastSyncedOn(Date lastSyncedOn) {
        this.lastSyncedOn = lastSyncedOn;
    }
}
