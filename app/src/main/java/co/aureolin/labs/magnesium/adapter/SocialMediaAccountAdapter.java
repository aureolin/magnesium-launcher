package co.aureolin.labs.magnesium.adapter;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.net.SocketAddress;
import java.util.HashMap;
import java.util.Map;

import co.aureolin.labs.magnesium.R;
import co.aureolin.labs.magnesium.model.SocialMediaAccount;

/**
 * Created by akinwale on 11/02/2016.
 */
public class SocialMediaAccountAdapter extends ArrayAdapter<SocialMediaAccount> {
    private static final Map<String, Integer> AccountTypeDrawableMap = new HashMap<String, Integer>();
    static {
        AccountTypeDrawableMap.put(SocialMediaAccount.TYPE_FACEBOOK, R.drawable.ic_facebook);
        AccountTypeDrawableMap.put(SocialMediaAccount.TYPE_GOOGLE_PLUS, R.drawable.ic_google_plus);
        AccountTypeDrawableMap.put(SocialMediaAccount.TYPE_INSTAGRAM, R.drawable.ic_instagram);
        AccountTypeDrawableMap.put(SocialMediaAccount.TYPE_TUMBLR, R.drawable.ic_tumblr);
        AccountTypeDrawableMap.put(SocialMediaAccount.TYPE_TWITTER, R.drawable.ic_twitter);
    }

    public SocialMediaAccountAdapter(Context context) {
        super(context, 0);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_string_with_icon, null);
        }

        SocialMediaAccount item = getItem(position);

        ImageView itemIconView = (ImageView) convertView.findViewById(R.id.item_icon);
        itemIconView.setImageResource(AccountTypeDrawableMap.get(item.getSocialAccountType()));
        itemIconView.getDrawable().setColorFilter(new PorterDuffColorFilter(0xff2196F3, PorterDuff.Mode.SRC_ATOP));

        TextView itemTextView = (TextView) convertView.findViewById(R.id.item_text);
        itemTextView.setText(item.getDisplayName());

        return convertView;
    }
}
