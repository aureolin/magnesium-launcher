package co.aureolin.labs.magnesium.model;

import java.util.Date;

import co.aureolin.labs.magnesium.util.Helper;

/**
 * Created by akinwale on 14/02/2016.
 */
public class EmailAccount {

    public static final int CONNECTION_TYPE_CLEAR = 1;

    public static final int CONNECTION_TYPE_SECURE_TLS = 2;

    public static final int CONNECTION_TYPE_OAUTH = 3;

    private long id;

    private String emailAddress;

    private String username;

    private String password;

    private String hostName;

    private int port;

    private int connectionType;

    private Date lastSyncedOn;

    public EmailAccount() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEncryptedPassword(String key) {
        return Helper.encrypt(password, key);
    }

    public void setPasswordFromEncryptedString(String encPassword, String key) {
        this.password = Helper.decrypt(encPassword, key);
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getConnectionType() {
        return connectionType;
    }

    public void setConnectionType(int connectionType) {
        this.connectionType = connectionType;
    }

    public Date getLastSyncedOn() {
        return lastSyncedOn;
    }

    public void setLastSyncedOn(Date lastSyncedOn) {
        this.lastSyncedOn = lastSyncedOn;
    }
}
