package co.aureolin.labs.magnesium.fragment.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.nfc.FormatException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.vending.billing.IInAppBillingService;

import java.util.ArrayList;
import java.util.List;

import co.aureolin.labs.magnesium.HomeActivity;
import co.aureolin.labs.magnesium.R;
import co.aureolin.labs.magnesium.SettingsActivity;
import co.aureolin.labs.magnesium.adapter.SettingItemAdapter;
import co.aureolin.labs.magnesium.dialog.EmailAccountDialog;
import co.aureolin.labs.magnesium.dialog.NumberPickerDialog;
import co.aureolin.labs.magnesium.dialog.RssFeedDialog;
import co.aureolin.labs.magnesium.dialog.SocialMediaOauthDialog;
import co.aureolin.labs.magnesium.model.SettingItem;
import co.aureolin.labs.magnesium.util.Helper;
import co.aureolin.labs.magnesium.util.IabPurchaseStartedCallback;
import co.aureolin.labs.magnesium.util.StartIabPurchaseTask;
import co.aureolin.labs.magnesium.util.ValueListener;

/**
 * Created by akinwale on 18/02/2016.
 */
public class GeneralSettingsFragment extends Fragment {
    public static GeneralSettingsFragment newInstance() {
        return new GeneralSettingsFragment();
    }

    public static final int SETTING_INDEX_EMAIL_ACCOUNT = 1;

    public static final int SETTING_INDEX_SOCIAL_MEDIA_ACCOUNT = 2;

    public static final int SETTING_INDEX_RSS_FEED_LIMIT = 3;

    public static final int SETTING_INDEX_REMOVE_ADS = 4;

    public static final String SETTING_KEY_WRI = "GS.WRI";

    public static final String[] SETTINGS_KEYS = {
        "GS.1O2",
        "GS.COF",
        SETTING_KEY_WRI,
        "GS.EAL",
        "GS.SMAL",
        "GS.RAFL",
        "GS.RADS"
    };

    public static final String PRO_OWNED_KEY = "GS.PRO";

    public static final String[] SETTINGS_OWNED_KEYS = {
        null,
        null,
        null,
        "GS.EAL.P",
        "GS.SMAL.P",
        "GS.RAFL.P",
        "GS.RADS.P",
    };

    public static final String[] SETTINGS_NAMES = {
        "12- or 24-hour format",
        "Celsius or Fahrenheit",
        "Weather refresh interval",
        "Email account limit",
        "Social media account limit",
        "RSS / Atom feed limit",
        "Remove ads"
    };

    public static final String[] SETTINGS_DEFAULT_VALUES = {
        "12",
        "Celsius",
        "4 hours",
        String.valueOf(EmailAccountDialog.DEFAULT_EMAIL_ACCOUNT_LIMIT),
        String.valueOf(SocialMediaOauthDialog.DEFAULT_SOCIAL_ACCOUNT_LIMIT),
        String.valueOf(RssFeedDialog.DEFAULT_RSS_FEED_LIMIT),
        "No"
    };

    public static final String[] SETTINGS_PURCHASED_VALUES = {
        null,
        null,
        null,
        String.valueOf(EmailAccountDialog.MAX_EMAIL_ACCOUNT_LIMIT),
        String.valueOf(SocialMediaOauthDialog.MAX_SOCIAL_ACCOUNT_LIMIT),
        String.valueOf(RssFeedDialog.MAX_RSS_FEED_LIMIT),
        "Yes"
    };

    public static final String SKU_PRO = "mg_pro";

    public static final String[] SETTINGS_SKUS = {
        null,
        null,
        null,
        "mg_limit_email",
        "mg_limit_social",
        "mg_limit_rss",
        "mg_remove_ads"
    };

    public static final String[] SETTINGS_ANNOTATIONS = {
        null,
        null,
        "Tap to select how often you want the weather on the universal feed to be refreshed. You can set this to 0 if you do not want the weather to automatically refresh.",
        "Tap to increase the number of email accounts you can add to 10 for $0.99",
        "Tap to increase the number of social media accounts you can add to 3 of each account type for $0.99",
        "Tap to increase the number of RSS / Atom feeds you can add to 10 for $0.99",
        "Tap to remove ads for $2.99"
    };

    private static final int[] OWNED_ITEMS_INDEXES = { 3, 4, 5, 6 };

    private List<SettingItem> generalSettings;

    private SettingItemAdapter adapter;

    private ListView generalSettingsListView;

    private boolean startPurchaseInProgress;

    private StartIabPurchaseTask purchaseTask;

    private IInAppBillingService iabService;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        generalSettings = new ArrayList<SettingItem>();
        SettingsActivity activity = (SettingsActivity) getContext();
        iabService = activity.getIabService();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_general_settings, container, false);

        TextView goProDescView = (TextView) rootView.findViewById(R.id.go_pro_desc_text);
        goProDescView.setText(Html.fromHtml(getResources().getString(R.string.go_pro_desc)));

        rootView.findViewById(R.id.go_pro_price_text).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (startPurchaseInProgress) {
                    return;
                }

                startPurchaseInProgress = true;
                if (purchaseTask == null || purchaseTask.getStatus() == AsyncTask.Status.FINISHED) {
                    purchaseTask = new StartIabPurchaseTask(SKU_PRO, iabService, getContext(), new IabPurchaseStartedCallback() {
                        @Override
                        public void onIabPurchaseStarted(Bundle result) {
                            startPurchaseInProgress = false;
                            purchaseTask = null;
                        }
                    });
                    purchaseTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            }
        });

        generalSettingsListView = (ListView) rootView.findViewById(R.id.general_settings_list);
        generalSettingsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final SettingItem item = (SettingItem) parent.getItemAtPosition(position);
                String itemName = item.getName();
                String itemValue = item.getValue();
                boolean saveHandled = false;


                if (SETTINGS_NAMES[1].equals(itemName)) { // Celsius or Fahrenheit
                    item.setValue("Celsius".equals(itemValue) ? "Fahrenheit" : "Celsius");
                } else if (SETTINGS_NAMES[0].equals(itemName)) {
                    item.setValue("12".equals(itemValue) ? "24" : "12");
                } else if (SETTINGS_NAMES[2].equals(itemName)) {
                    // Show the dialog
                    saveHandled = true;
                    NumberPickerDialog dialog = NumberPickerDialog.newInstance(itemName, 0, 24, getNumHoursFromString(itemValue, 4),
                            "hour", "hours", new ValueListener() {
                                @Override
                                public void onValueChanged(int value) {
                                    if (item != null) {
                                        item.setValue(getHoursString(value));
                                    }
                                    if (adapter != null) {
                                        adapter.notifyDataSetChanged();
                                    }

                                    saveSettings();

                                    if (HomeActivity.thisActivity != null) {
                                        HomeActivity.thisActivity.setWeatherFrequencyChanged(true);
                                    }
                                }

                                @Override
                                public void onCompleted() {

                                }
                            });

                    dialog.show(getFragmentManager(), "NumberPickerDialog");
                } else if (position > 2) {
                    boolean isOwned = item.isOwned();
                    saveHandled = true;
                    if (!isOwned) {
                        if (startPurchaseInProgress) {
                            return;
                        }

                        startPurchaseInProgress = true;
                        if (purchaseTask == null || purchaseTask.getStatus() == AsyncTask.Status.FINISHED) {
                            purchaseTask = new StartIabPurchaseTask(item.getRequiredSku(), iabService, getContext(), new IabPurchaseStartedCallback() {
                                @Override
                                public void onIabPurchaseStarted(Bundle result) {
                                    startPurchaseInProgress = false;
                                    purchaseTask = null;
                                }
                            });
                            purchaseTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        }
                    }
                }

                if (!saveHandled) {
                    if (adapter != null) {
                        adapter.notifyDataSetChanged();
                    }
                    saveSettings();
                }
            }
        });

        return rootView;
    }

    public void onStart() {
        super.onStart();
        generalSettings.clear();
        initSettings();
        checkOwnedItems();
    }

    public void onResume() {
        super.onResume();
        Context context = getContext();
        if (context != null) {
            SharedPreferences preferences = context.getSharedPreferences(HomeActivity.COMMON_SHARED_PREF_NAME, Context.MODE_PRIVATE);
            boolean proOwned = (preferences.getInt(PRO_OWNED_KEY, 0) == 1);
            updateGoProContainer(proOwned);
        }
    }

    public void onPause() {
        saveSettings();
        super.onPause();
    }

    private void updateGoProContainer(boolean proOwned) {
        View view = getView();
        if (view != null) {
            View proContainer = view.findViewById(R.id.general_settings_pro_container);
            proContainer.setVisibility(proOwned ? View.GONE : View.VISIBLE);
        }
    }

    private void setSettingsPurchased(final int[] indexes, final boolean isPro) {
        for (int i = 0; i < indexes.length; i++) {
            int index = indexes[i];
            final String settingPurchasedValue = SETTINGS_PURCHASED_VALUES[index];

            adapter.getItem(index).setOwned(true);
            adapter.getItem(index).setValue(settingPurchasedValue);
            adapter.getItem(index).setShowAnnotation(false);
        }

        if (isPro) {
            updateGoProContainer(true);
        }

        (new AsyncTask<Void, Void, Void>() {
            protected Void doInBackground(Void... params) {
                Context context = getContext();
                if (context != null) {
                    SharedPreferences preferences = context.getSharedPreferences(HomeActivity.COMMON_SHARED_PREF_NAME, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();

                    for (int i = 0; i < indexes.length; i++) {
                        int index = i;
                        String settingKey = SETTINGS_KEYS[index];
                        String purchasedKey = SETTINGS_OWNED_KEYS[index];
                        String purchasedValue = SETTINGS_PURCHASED_VALUES[index];
                        editor.putInt(purchasedKey, 1);
                        editor.putString(settingKey, purchasedValue);
                    }

                    if (isPro) {
                        editor.putInt(PRO_OWNED_KEY, 1);
                        editor.putInt(IndexingAndSearchFragment.IWSR_PURCHASED_KEY, 1);
                    }

                    editor.commit();
                }
                return null;
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void onPurchased(String sku) {
        if (adapter != null) {
            int index = -1;
            for (int i = 0; i < adapter.getCount(); i++) {
                if (sku.equals(adapter.getItem(i).getRequiredSku())) {
                    index = i;
                    break;
                }
            }

            if (sku.equals(SKU_PRO)) {
                final int[] purchaseIndexes = OWNED_ITEMS_INDEXES;
                setSettingsPurchased(purchaseIndexes, true);
                Helper.showToast("Congratulations! You have successfully unlocked all Pro features. Thank you for your purchase.", getContext());
            } else if (index > -1) {
                setSettingsPurchased(new int[] { index }, false);
                Helper.showToast("Your transaction was successfully completed. Thank you for your purchase.", getContext());
            }

            adapter.notifyDataSetChanged();
        }
    }

    private static String getHoursString(int numHours) {
        return String.format("%d hour%s", numHours, (numHours == 1 ? "" : "s"));
    }

    private static int getNumHoursFromString(String value, int defaultValue) {
        int numHours = defaultValue;
        if (!Helper.isEmpty(value)) {
            String parts[] = value.split(" ");
            if (parts.length == 2) {
                try {
                    numHours = Integer.parseInt(parts[0]);
                } catch (NumberFormatException ex) {
                    numHours = defaultValue;
                }
            }
        }

        return numHours;
    }

    private void initSettings() {
        Context context = getContext();
        SharedPreferences preferences = null;
        boolean proOwned = false;
        if (context != null) {
            preferences = context.getSharedPreferences(HomeActivity.COMMON_SHARED_PREF_NAME, Context.MODE_PRIVATE);
            proOwned = (preferences.getInt(PRO_OWNED_KEY, 0) == 1);
        }

        for (int i = 0; i < SETTINGS_NAMES.length; i++) {
            SettingItem item = new SettingItem();
            item.setName(SETTINGS_NAMES[i]);
            item.setAnnotation(SETTINGS_ANNOTATIONS[i]);
            item.setRequiredSku(SETTINGS_SKUS[i]);

            String settingKey = SETTINGS_KEYS[i];
            String ownedKey = SETTINGS_OWNED_KEYS[i];
            if (context != null && preferences != null) {
                if (SETTING_KEY_WRI.equals(settingKey)) {
                    item.setValue( getHoursString(preferences.getInt(settingKey, 4)) );
                    item.setShowAnnotation(true);
                } else {
                    item.setValue(preferences.getString(settingKey, SETTINGS_DEFAULT_VALUES[i]));
                }

                if (Helper.isEmpty(item.getValue())) {
                    item.setValue(SETTINGS_DEFAULT_VALUES[i]);
                }

                if (i <= 2) {
                    item.setOwned(true);
                } else {
                    boolean owned = (proOwned || preferences.getInt(ownedKey, 0) == 1);
                    item.setOwned(owned);
                    if (owned) {
                        item.setValue(SETTINGS_PURCHASED_VALUES[i]);
                    }
                }
            } else {
                item.setValue(SETTINGS_DEFAULT_VALUES[i]);
                item.setOwned((i <= 2));
            }

            generalSettings.add(item);
        }

        adapter = new SettingItemAdapter(context);
        adapter.addAll(generalSettings);
        generalSettingsListView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void saveSettings() {
        if (adapter != null) {
            final List<SettingItem> items = new ArrayList<SettingItem>();
            for (int i = 0; i < adapter.getCount(); i++) {
                if (i > 2) {
                    break;
                }
                items.add(adapter.getItem(i));
            }

            (new AsyncTask<Void, Void, Void>() {
                protected Void doInBackground(Void... params) {
                    Context context = getContext();
                    if (context != null) {
                        SharedPreferences preferences = context.getSharedPreferences(HomeActivity.COMMON_SHARED_PREF_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        for (int i = 0; i < items.size(); i++) {
                            String value = items.get(i).getValue();
                            if (SETTING_KEY_WRI.equals(SETTINGS_KEYS[i])) {
                                int numHours = getNumHoursFromString(value, 4);
                                editor.putInt(SETTINGS_KEYS[i], numHours);
                            } else {
                                editor.putString(SETTINGS_KEYS[i], value);
                            }
                        }
                        editor.commit();
                    }
                    return null;
                }
            }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    private void checkOwnedItems() {
        if (iabService != null) {
            (new AsyncTask<Void, Void, Bundle>() {
                protected Bundle doInBackground(Void... params) {
                    try {
                        Context context = getContext();
                        if (context != null) {
                            Bundle ownedItems = iabService.getPurchases(3, context.getPackageName(), "inapp", null);
                            return ownedItems;
                        }
                    } catch (RemoteException ex) {

                    }
                    return null;
                }

                protected void onPostExecute(Bundle ownedItems) {
                    if (ownedItems == null) {
                        Helper.showToast("Your in-app purchases could not be verified at this time. Please try again later.", getContext());
                        return;
                    }

                    int response = ownedItems.getInt("RESPONSE_CODE");
                    if (response != 0) {
                        Helper.showToast("The request to verify your in-app purchases failed. Please try again later.", getContext());
                        return;
                    }

                    ArrayList<String> ownedSkus = ownedItems.getStringArrayList("INAPP_PURCHASE_ITEM_LIST");
                    boolean proOwned = ownedSkus.contains(SKU_PRO);
                    if (proOwned) {
                        setSettingsPurchased(OWNED_ITEMS_INDEXES, true);
                    } else {
                        for (int i = 0; i < SETTINGS_SKUS.length; i++) {
                            String checkSku = SETTINGS_SKUS[i];
                            if (Helper.isEmpty(checkSku)) {
                                continue;
                            }

                            Context context = getContext();
                            int purchased = 0;
                            final String purchasedKey = SETTINGS_OWNED_KEYS[i];
                            final String valueKey = SETTINGS_KEYS[i];
                            final SharedPreferences preferences = context.getSharedPreferences(HomeActivity.COMMON_SHARED_PREF_NAME, Context.MODE_PRIVATE);
                            if (proOwned || ownedSkus.contains(checkSku)) {
                                adapter.getItem(i).setOwned(true);
                                purchased = 1;
                            } else {
                                adapter.getItem(i).setOwned(false);
                            }

                            final int purchasedValue = purchased;
                            final String settingPurchasedValue = SETTINGS_PURCHASED_VALUES[i];
                            final String settingDefaultValue = SETTINGS_DEFAULT_VALUES[i];
                            if (context != null) {
                                (new AsyncTask<Void, Void, Void>() {
                                    protected Void doInBackground(Void... params) {
                                        SharedPreferences.Editor editor = preferences.edit();
                                        editor.putInt(purchasedKey, purchasedValue);
                                        editor.putString(valueKey, (purchasedValue == 1) ? settingPurchasedValue : settingDefaultValue);
                                        editor.commit();
                                        return null;
                                    }
                                }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            }
                        }
                    }

                    adapter.notifyDataSetChanged();
                }
            }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }
}