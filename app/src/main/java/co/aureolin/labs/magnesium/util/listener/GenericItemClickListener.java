package co.aureolin.labs.magnesium.util.listener;

import android.view.View;

/**
 * Created by akinwale on 11/09/2016.
 */
public interface GenericItemClickListener {
    void onItemClick(int position, View v);

    void onItemLongClick(int position, View v);
}
