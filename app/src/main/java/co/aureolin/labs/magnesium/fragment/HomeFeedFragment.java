package co.aureolin.labs.magnesium.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.telephony.SmsManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.webkit.ConsoleMessage;
import android.webkit.CookieManager;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.smaato.soma.AdDownloaderInterface;
import com.smaato.soma.AdListenerInterface;
import com.smaato.soma.ErrorCode;
import com.smaato.soma.ReceivedBannerInterface;
import com.smaato.soma.exception.AdReceiveFailed;
import com.smaato.soma.internal.nativead.BannerNativeAd;
import com.smaato.soma.nativead.NativeAd;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import co.aureolin.labs.magnesium.EmailMessageActivity;
import co.aureolin.labs.magnesium.HomeActivity;
import co.aureolin.labs.magnesium.R;
import co.aureolin.labs.magnesium.adapter.FeedItemListAdapter;
import co.aureolin.labs.magnesium.dialog.SocialMediaOauthDialog;
import co.aureolin.labs.magnesium.fragment.settings.GeneralSettingsFragment;
import co.aureolin.labs.magnesium.fragment.settings.IndexingAndSearchFragment;
import co.aureolin.labs.magnesium.model.BaseItem;
import co.aureolin.labs.magnesium.model.FeedItem;
import co.aureolin.labs.magnesium.model.IndexedItem;
import co.aureolin.labs.magnesium.model.NavigationDrawerItem;
import co.aureolin.labs.magnesium.model.OauthData;
import co.aureolin.labs.magnesium.model.SocialMediaAccount;
import co.aureolin.labs.magnesium.model.WeatherCondition;
import co.aureolin.labs.magnesium.util.Helper;
import co.aureolin.labs.magnesium.util.Http;
import co.aureolin.labs.magnesium.util.IndexedItemSearchAdapterLoader;
import co.aureolin.labs.magnesium.util.NativeAdControlInterface;
import co.aureolin.labs.magnesium.util.SmsMessageSender;
import co.aureolin.labs.magnesium.util.WebSearchAdapterLoader;
import co.aureolin.labs.magnesium.util.db.MagnesiumDbContext;
import co.aureolin.labs.magnesium.util.listener.GenericItemClickListener;
import co.aureolin.labs.magnesium.widget.HorizontalDividerItemDecoration;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.basic.DefaultOAuthConsumer;
import oauth.signpost.http.HttpParameters;

/**
 * Created by akinwale on 07/02/2016.
 */
public class HomeFeedFragment extends Fragment {
    private NativeAd nativeAd;

    private static final int MAX_LOCATION_TRIES = 3;

    private static final String INSTAGRAM_SHARED_DATA_PATTERN = "window\\._sharedData = (.*?);<\\/script>";

    private static final Map<String, Integer> WeatherConditionNameIconMap = new HashMap<String, Integer>();
    static {
        WeatherConditionNameIconMap.put("Haze", R.drawable.ic_weather_fog_white_48dp);
        WeatherConditionNameIconMap.put("Cloudy", R.drawable.ic_weather_cloudy_white_48dp);
        WeatherConditionNameIconMap.put("Partly Cloudy", R.drawable.ic_weather_partlycloudy_white_48dp);
        WeatherConditionNameIconMap.put("Mostly Cloudy", R.drawable.ic_weather_cloudy_white_48dp);
        WeatherConditionNameIconMap.put("Fair", R.drawable.ic_weather_sunny_white_48dp);
        WeatherConditionNameIconMap.put("Light Snow", R.drawable.ic_weather_snowy_white_48dp);
        WeatherConditionNameIconMap.put("Heavy Snow", R.drawable.ic_weather_snowy_white_48dp);
        WeatherConditionNameIconMap.put("Light Rain", R.drawable.ic_weather_rainy_white_48dp);
        WeatherConditionNameIconMap.put("Heavy Rain", R.drawable.ic_weather_rainy_white_48dp);
        WeatherConditionNameIconMap.put("Scattered Showers", R.drawable.ic_weather_rainy_white_48dp);
        WeatherConditionNameIconMap.put("Showers", R.drawable.ic_weather_rainy_white_48dp);
        WeatherConditionNameIconMap.put("Thunderstorms", R.drawable.ic_weather_rainy_white_48dp);
    }

    private int numLocationTries;

    private CoordinatorLayout coordinatorLayout;

    private static final String TAG = HomeFeedFragment.class.getCanonicalName();

    private static final Paint paint = new Paint();

    private static final int ONE_HOUR_MILLIS = 3600000;

    private static final int HALF_HOUR_MILLIS = 1800000;

    private static final int MAX_INSTAGRAM_ITEMS_PER_LOAD = 100;

    private static final String GEOCODE_URL_FORMAT = "https://maps.googleapis.com/maps/api/geocode/json?latlng=%s,%s";

    private static final String WEATHER_CONDITION_QUERY_FORMAT = "select item.condition from weather.forecast where woeid=\"%s\"";

    private static final String WEATHER_CONDITION_KEY = "Weather";

    private static final String CACHED_LOCATION_KEY = "LastKnownLocation";

    private static final String WOE_ID_QUERY_FORMAT = "select woeid from geo.places where text=\"%s\"";

    private static final String YQL_QUERY_URL_FORMAT = "https://query.yahooapis.com/v1/public/yql?q=%s&format=json";

    private static final String INSTAGRAM_BASE_ITEM_LINK = "https://www.instagram.com/p";

    private static final String INSTAGRAM_QUERY_URL = "https://www.instagram.com/query/";

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("d MMMM yyyy");

    private static final SimpleDateFormat TIME_FORMAT_12H = new SimpleDateFormat("h:mm a");

    private static final SimpleDateFormat TIME_FORMAT_24H = new SimpleDateFormat("HH:mm");

    private static final SimpleDateFormat DAY_OF_WEEK_FORMAT = new SimpleDateFormat("EEEE");

    public static Location LastKnownLocation;

    private LocationListener locationListener;

    private LocationManager locationManager;

    private MagnesiumDbContext dbContext;

    private boolean isFahrenheit;

    private boolean is24HourFormat;

    private String currentLocationName;

    private FeedItemListAdapter homeFeedAdapter;

    private boolean deleteInProgress;

    private static ObjectMapper objectMapper = new ObjectMapper();

    private static String userAgent;

    private TextView dayOfWeekView;

    private TextView dateView;

    private TextView timeView;

    private TextView weatherTempView;

    private TextView weatherConditionTextView;

    private TextView weatherLocationNameView;

    private ImageView weatherIconView;

    private View weatherLocationIconView;

    private RecyclerView homeFeedList;

    private SwipeRefreshLayout swipeRefreshLayout;

    private EditText searchInput;

    private ProgressBar searchProgress;

    private View clearSearchInput;

    private TextView feedEmptyView;

    private TextView noResultsView;

    private Date lastRemoteLoadTime;

    private Date lastInstagramLoadTime;

    private long instagramMaxLocalTs;

    private Date locationLoadStartTime;

    private WebView instagramWebView;

    private String instagramCsrfToken;

    private String lastInstagramWvUrl;

    private boolean instagramWvLoginInitialised;

    private int instagramWvScriptExecCount;

    private SocialMediaAccount currentInstagramAccount;

    private int currentNumInstagramItems;

    private int currentInstagramIndex;

    private boolean instagramShouldFetchMore;

    private boolean instagramQueryInProgress;

    private String instagramNextCursor;

    private HashMap<String, String> processedInstagramCodes = new HashMap<String, String>();

    private List<SocialMediaAccount> instagramAccountsQueue = new ArrayList<SocialMediaAccount>();

    private boolean localFeedLoaded;

    private boolean remoteFeedLoaded;

    private static HashMap<String, String> locationsWoeIdCache = new HashMap<String, String>();

    private static final String SETTING_KEY_WOE_ID_CACHE = "LocationsCache";

    private Timer locationLoadTimer;

    private TimerTask locationLoadTimerTask;

    private ProgressBar loadProgress;

    private boolean webSearchInProgress;

    private GenericItemClickListener homeFeedItemClickListener;

    private String currentSmsMessageToSend;

    private String currentSmsRecipient;

    private static final String IG_QUERY_REF = "feed::show";

    private static final String IG_QUERY_Q =
            "ig_me() {\n" +
            "  feed {\n" +
            "    media.after(%s\n" +
            ", 12) {\n" +
            "      nodes {\n" +
            "        id,\n" +
            "        caption,\n" +
            "        code,\n" +
            "        comments.last(4) {\n" +
            "          count,\n" +
            "          nodes {\n" +
            "            id,\n" +
            "            created_at,\n" +
            "            text,\n" +
            "            user {\n" +
            "              id,\n" +
            "              profile_pic_url,\n" +
            "              username\n" +
            "            }\n" +
            "          },\n" +
            "          page_info\n" +
            "        },\n" +
            "        comments_disabled,\n" +
            "        date,\n" +
            "        dimensions {\n" +
            "          height,\n" +
            "          width\n" +
            "        },\n" +
            "        display_src,\n" +
            "        is_video,\n" +
            "        likes {\n" +
            "          count,\n" +
            "          nodes {\n" +
            "            user {\n" +
            "              id,\n" +
            "              profile_pic_url,\n" +
            "              username\n" +
            "            }\n" +
            "          },\n" +
            "          viewer_has_liked\n" +
            "        },\n" +
            "        location {\n" +
            "          id,\n" +
            "          has_public_page,\n" +
            "          name\n" +
            "        },\n" +
            "        owner {\n" +
            "          id,\n" +
            "          blocked_by_viewer,\n" +
            "          followed_by_viewer,\n" +
            "          full_name,\n" +
            "          has_blocked_viewer,\n" +
            "          is_private,\n" +
            "          profile_pic_url,\n" +
            "          requested_by_viewer,\n" +
            "          username\n" +
            "        },\n" +
            "        usertags {\n" +
            "          nodes {\n" +
            "            user {\n" +
            "              username\n" +
            "            },\n" +
            "            x,\n" +
            "            y\n" +
            "          }\n" +
            "        },\n" +
            "        video_url,\n" +
            "        video_views\n" +
            "      },\n" +
            "      page_info\n" +
            "    }\n" +
            "  },\n" +
            "  id,\n" +
            "  profile_pic_url,\n" +
            "  username\n" +
            "}";

    private static final String DATA_MARKET_ACCOUNT_KEY = "XZMVEsWsQ+F3hM1eQm3XXn0EHS5tUvx7kWjolT05jHU";

    private static final String WEB_SEARCH_URL_FORMAT = "https://api.datamarket.azure.com/Bing/Search/Composite?$top=20&$format=JSON&Sources=%%27web%%2bnews%%27&Query=%%27%s%%27";

    public static HomeFeedFragment newInstance() {
        return new HomeFeedFragment();
    }

    private static final int DELETE_ICON_DIMEN = 18;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_home_feed, container, false);

        Typeface normalTypeface = HomeActivity.getNormalTypeface(getContext());

        loadProgress = (ProgressBar) rootView.findViewById(R.id.home_feed_load_progress);
        coordinatorLayout = (CoordinatorLayout) rootView.findViewById(R.id.home_feed_coordinator);

        // Search input
        searchInput = (EditText) rootView.findViewById(R.id.home_feed_find_everything_input);
        searchInput.setTypeface(normalTypeface);

        searchProgress = (ProgressBar) rootView.findViewById(R.id.home_feed_find_everything_progress);
        clearSearchInput = rootView.findViewById(R.id.home_feed_clear_search);

        searchInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE
                        || actionId == EditorInfo.IME_ACTION_GO
                        || actionId == EditorInfo.IME_ACTION_SEARCH) {
                    Helper.clearEditTextFocus(searchInput, getContext());
                    return true;
                }

                return false;
            }
        });
        searchInput.addTextChangedListener(new TextWatcher() {
            private Timer delayTimer = new Timer();

            private final long DELAY = 1000; // Wait 1s after input before performing web search

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (homeFeedAdapter != null) {
                    final String text = searchInput.getText().toString();
                    int len = text.trim().length();
                    if (len > 0) {
                        homeFeedAdapter.filter(text);
                    } else {
                        homeFeedAdapter.filter(null);
                    }

                    searchProgress.setVisibility(len > 0 ? View.VISIBLE : View.GONE);
                    clearSearchInput.setVisibility(len > 0 ? View.VISIBLE : View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!webSearchInProgress) {
                    delayTimer.cancel();
                    delayTimer = new Timer();
                    delayTimer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            if (homeFeedAdapter != null) {
                                String currentConstraint = searchInput.getText().toString().trim();
                                if (!Helper.isUssd(currentConstraint)) {
                                    WebSearchAdapterLoader webSearchAdapterLoader = homeFeedAdapter.getWebSearchAdapterLoader();
                                    if (webSearchAdapterLoader != null && currentConstraint != null && currentConstraint.trim().length() >= 3) {
                                        HomeActivity activity = (HomeActivity) getContext();
                                        activity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                if (homeFeedAdapter.getItemCount() == 0 && searchProgress != null) {
                                                    searchProgress.setVisibility(View.VISIBLE);
                                                }
                                            }
                                        });
                                        webSearchAdapterLoader.searchWebItems(currentConstraint, homeFeedAdapter);
                                    }
                                }
                            }
                        }
                    }, DELAY);
                }
            }
        });

        clearSearchInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (searchInput.getText().length() > 0) {
                    searchInput.setText(null);
                }
                clearSearchInput.setVisibility(View.GONE);
                searchProgress.setVisibility(View.GONE);
            }
        });

        homeFeedItemClickListener = new GenericItemClickListener() {
            @Override
            public void onItemLongClick(int positin, View v) {

            }

            @Override
            public void onItemClick(int position, View v) {
                if (homeFeedAdapter != null) {
                    BaseItem item = (BaseItem) homeFeedAdapter.getItem(position);
                    if (item != null) {
                        if (!Helper.isEmpty(item.getUrl())) {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(item.getUrl()));

                            if (item instanceof IndexedItem) {
                                IndexedItem indexedItem = (IndexedItem) item;
                                if (!Helper.isEmpty(indexedItem.getMimeType())) {
                                    intent.setType(indexedItem.getMimeType());
                                }
                            }

                            try {
                                startActivity(intent);
                            } catch (ActivityNotFoundException ex) {
                                Helper.showToast("An activity to handle the selected item was not found.", getContext());
                            }
                        } else {
                            // Log.d("#HELP", "Checking item type.");
                            HomeActivity activity = (HomeActivity) getContext();
                            switch (item.getType()) {
                                case BaseItem.ITEM_TYPE_SMS_MESSAGE:
                                    FeedItem smsItem = null;
                                    if (item instanceof IndexedItem) {
                                        smsItem = ((IndexedItem) item).getFeedItem();
                                    } else {
                                        smsItem = (FeedItem) item;
                                    }

                                    boolean canOpen = false;
                                    if (smsItem != null) {
                                        String threadId = smsItem.getThreadId();
                                        if (!Helper.isEmpty(threadId)) {
                                            canOpen = true;
                                            Uri uri = Uri.parse(String.format("content://mms-sms/conversations/%s", threadId));
                                            Intent intent = new Intent(Intent.ACTION_VIEW, uri);

                                            try {
                                                startActivity(intent);
                                            } catch (ActivityNotFoundException ex) {
                                                Helper.showToast("The default text messaging app could not be opened.", getContext());
                                            }
                                        }
                                    }
                                    if (!canOpen) {
                                        Helper.showToast("Unable to open the message thread.", getContext());
                                    }
                                    break;

                                case BaseItem.ITEM_TYPE_EMAIL_MESSAGE:
                                    FeedItem emailItem = null;
                                    if (item instanceof IndexedItem) {
                                        emailItem = ((IndexedItem) item).getFeedItem();
                                    } else {
                                        emailItem = (FeedItem) item;
                                    }

                                    if (emailItem == null || emailItem.getId() < 0) {
                                        Helper.showToast("Unable to open the email message.", getContext());
                                    } else {
                                        Uri uri = Uri.parse(String.format("mgemail:%d", emailItem.getId()));
                                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        intent.setClassName("co.aureolin.labs.magnesium", EmailMessageActivity.class.getName());
                                        startActivity(intent);
                                    }
                                    break;

                                case BaseItem.ITEM_TYPE_APP:
                                    // getReferenceId returns packageName
                                    // Log.d("#HELP", "Launching app: " + item.getReferenceId());
                                    if (item instanceof IndexedItem) {
                                        IndexedItem indexedItem = (IndexedItem) item;
                                        if (activity != null && !Helper.isEmpty(item.getReferenceId())) {
                                            activity.launchApp(indexedItem.getReferenceId(), indexedItem.getMetaFieldName());
                                        }
                                    }
                                    break;
                            }
                        }
                    }
                }
            }
        };

        // Feed list
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.home_feed_swipe_refresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadHomeFeedItems(true, false);
            }
        });

        homeFeedList = (RecyclerView) rootView.findViewById(R.id.home_feed_list);
        final Context context = getContext();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        homeFeedList.addItemDecoration(new HorizontalDividerItemDecoration(context));
        homeFeedList.setLayoutManager(layoutManager);

        ItemTouchHelper.SimpleCallback homeFeedItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                if (viewHolder instanceof FeedItemListAdapter.ViewHolder) {
                    int type = ((FeedItemListAdapter.ViewHolder) viewHolder).getType();
                    if (type == BaseItem.ITEM_TYPE_AD || type == BaseItem.ITEM_TYPE_WEB_SEARCH_RESULT) {
                        return 0;
                    }
                }
                return super.getSwipeDirs(recyclerView, viewHolder);
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                final int position = viewHolder.getAdapterPosition();
                if (direction == ItemTouchHelper.RIGHT && homeFeedAdapter != null) {
                    final BaseItem removedItem = homeFeedAdapter.removeItem(position);

                    if (removedItem != null) {
                        homeFeedAdapter.setDeleteInProgress(true);
                        showUndoableSnackbarMessage("Item removed.", new Snackbar.Callback() {
                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                if (homeFeedAdapter.isDeleteInProgress()) {
                                    (new AsyncTask<Void, Void, Boolean>() {
                                        protected Boolean doInBackground(Void... params) {
                                            SQLiteDatabase db = dbContext.getWritableDatabase();
                                            FeedItem feedItem = null;
                                            try {
                                                if (removedItem instanceof FeedItem) {
                                                    feedItem = (FeedItem) removedItem;
                                                    feedItem.setArchived(true);
                                                    MagnesiumDbContext.update(feedItem, db);

                                                    return true;
                                                } else if (removedItem instanceof IndexedItem) {
                                                    IndexedItem indexedItem = (IndexedItem) removedItem;
                                                    feedItem = indexedItem.getFeedItem();

                                                    indexedItem.setArchived(true);
                                                    MagnesiumDbContext.update(indexedItem, db);

                                                    if (feedItem != null) {
                                                        feedItem.setArchived(true);
                                                        MagnesiumDbContext.update(feedItem, db);
                                                    }

                                                    return true;
                                                }
                                            } catch (Exception ex) {
                                                return false;
                                            }

                                            return false;
                                        }

                                        protected void onPostExecute(Boolean result) {
                                            if (result) {
                                                // Remove completed
                                            }
                                            homeFeedAdapter.setDeleteInProgress(false);
                                        }
                                    }).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
                                }
                            }
                        }, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                homeFeedAdapter.setDeleteInProgress(false);
                                homeFeedAdapter.restoreItem(position, removedItem);
                            }
                        });
                    }
                }
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                Bitmap icon;
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();

                    if (dX > 0) {
                        paint.setColor(Color.parseColor("#D32F2F"));
                        RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX, (float) itemView.getBottom());
                        c.drawRect(background, paint);

                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_delete_white_18dp);

                        int deleteIconDimenDp = Helper.getValueForDip(DELETE_ICON_DIMEN, context);
                        float left = (float) itemView.getLeft() + (deleteIconDimenDp * 2);
                        float top = (float) itemView.getTop() + ((height - deleteIconDimenDp) / 2);
                        float right = left + deleteIconDimenDp;
                        float bottom = top + deleteIconDimenDp;
                        RectF iconRect = new RectF(left, top, right, bottom);
                        c.clipRect(background);
                        c.drawBitmap(icon, null, iconRect, paint);
                    }
                }

                c.restore();
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(homeFeedItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(homeFeedList);


        /*homeFeedList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            BaseItem item = (BaseItem) parent.getItemAtPosition(position);
            if (item != null) {
                if (!Helper.isEmpty(item.getUrl())) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(item.getUrl()));

                    if (item instanceof IndexedItem) {
                        IndexedItem indexedItem = (IndexedItem) item;
                        if (!Helper.isEmpty(indexedItem.getMimeType())) {
                            intent.setType(indexedItem.getMimeType());
                        }
                    }

                    // Log.d("#HELP", "Trying to start intent.");
                    startActivity(intent);
                } else {
                    // Log.d("#HELP", "Checking item type.");
                    HomeActivity activity = (HomeActivity) getContext();
                    switch (item.getType()) {
                        case BaseItem.ITEM_TYPE_SMS_MESSAGE:
                            FeedItem smsItem = null;
                            if (item instanceof IndexedItem) {
                                smsItem = ((IndexedItem) item).getFeedItem();
                            } else {
                                smsItem = (FeedItem) item;
                            }

                            boolean canOpen = false;
                            if (smsItem != null) {
                                String threadId = smsItem.getThreadId();
                                if (!Helper.isEmpty(threadId)) {
                                    canOpen = true;
                                    Uri uri = Uri.parse(String.format("content://mms-sms/conversations/%s", threadId));
                                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                    startActivity(intent);
                                }
                            }
                            if (!canOpen) {
                                Helper.showToast("Unable to open the message thread.", getContext());
                            }
                            break;

                        case BaseItem.ITEM_TYPE_EMAIL_MESSAGE:
                            FeedItem emailItem = null;
                            if (item instanceof IndexedItem) {
                                emailItem = ((IndexedItem) item).getFeedItem();
                            } else {
                                emailItem = (FeedItem) item;
                            }

                            if (emailItem == null || emailItem.getId() < 0) {
                                Helper.showToast("Unable to open the email message.", getContext());
                            } else {
                                Uri uri = Uri.parse(String.format("mgemail:%d", emailItem.getId()));
                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.setClassName("co.aureolin.labs.magnesium", EmailMessageActivity.class.getName());
                                startActivity(intent);
                            }
                            break;

                        case BaseItem.ITEM_TYPE_APP:
                            // getReferenceId returns packageName
                            // Log.d("#HELP", "Launching app: " + item.getReferenceId());
                            if (item instanceof IndexedItem) {
                                IndexedItem indexedItem = (IndexedItem) item;
                                if (activity != null && !Helper.isEmpty(item.getReferenceId())) {
                                    activity.launchApp(indexedItem.getReferenceId(), indexedItem.getMetaFieldName());
                                }
                            }
                            break;
                    }
                }
            }
            }
        });*/

        feedEmptyView = (TextView) rootView.findViewById(R.id.home_feed_no_feed_items);
        noResultsView = (TextView) rootView.findViewById(R.id.home_feed_no_results);

        // Current date/time
        dayOfWeekView = (TextView) rootView.findViewById(R.id.home_feed_day_of_week);
        dateView = (TextView) rootView.findViewById(R.id.home_feed_date);
        timeView = (TextView) rootView.findViewById(R.id.home_feed_time);


        // Current Weather Condition
        weatherTempView = (TextView) rootView.findViewById(R.id.home_feed_weather_temp);
        weatherConditionTextView = (TextView) rootView.findViewById(R.id.home_feed_weather_condition_text);
        weatherLocationNameView = (TextView) rootView.findViewById(R.id.home_feed_weather_location);
        weatherIconView = (ImageView) rootView.findViewById(R.id.home_feed_weather_icon);
        weatherLocationIconView = rootView.findViewById(R.id.home_feed_weather_location_icon);

        TextView[] textViews = {
            feedEmptyView, noResultsView, dayOfWeekView, dateView, timeView,
            weatherTempView, weatherConditionTextView, weatherLocationNameView
        };
        for (int i = 0; i < textViews.length; i++) {
            textViews[i].setTypeface(normalTypeface);
        }

        instagramWebView = (WebView) rootView.findViewById(R.id.home_feed_instagram_webview);
        userAgent = instagramWebView.getSettings().getUserAgentString();
        instagramWebView.setWebChromeClient(new WebChromeClient() {
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                String message = consoleMessage.message();
                if (message.startsWith(">>MG:")) {
                    if (SocialMediaOauthDialog.INSTAGRAM_CLASSIC_LOGIN.equals(lastInstagramWvUrl) && message.contains("js not-logged-in")) {
                        if (!instagramWvLoginInitialised) {
                            // Do login stuff
                            instagramWvScriptExecCount++;
                            if (instagramWvScriptExecCount >= 1) {
                                instagramWvLoginInitialised = true;
                            }

                            instagramWebView.loadUrl(String.format(SocialMediaOauthDialog.INSTAGRAM_LOGIN_SCRIPT_FORMAT,
                               currentInstagramAccount.getAccountId(), currentInstagramAccount.getOauthData()));
                        } else {
                            Log.e(TAG, "Unable to authenticate to Instagram with username: " + currentInstagramAccount.getAccountId());
                        }
                    } else if (SocialMediaOauthDialog.INSTAGRAM_HOME.equals(lastInstagramWvUrl)
                            && message.contains("js logged-in")
                            && !instagramQueryInProgress) {
                        // Process Instagram URLs
                        // Parse the sharedData out for first load
                        final String pageData = message.substring(5);
                        (new AsyncTask<Void, Void, List<BaseItem>>() {
                            protected List<BaseItem> doInBackground(Void... params) {
                                List<BaseItem> feedItems = new ArrayList<BaseItem>();
                                instagramShouldFetchMore = false;

                                long lowestEntryTs = 0;
                                Pattern pattern = Pattern.compile(INSTAGRAM_SHARED_DATA_PATTERN, Pattern.DOTALL);
                                Matcher matcher = pattern.matcher(pageData);
                                if (matcher.find() && matcher.groupCount() == 1) {
                                    checkObjectMapper();
                                    SQLiteDatabase db = dbContext.getWritableDatabase();
                                    String sharedDataJson = matcher.group(1);
                                    try {
                                        JsonNode sharedDataNode = objectMapper.readTree(sharedDataJson);
                                        JsonNode mediaNode = sharedDataNode.get("entry_data").get("FeedPage").get(0).get("feed").get("media");
                                        JsonNode nodes = mediaNode.get("nodes");
                                        if (nodes.isArray()) {
                                            for (final JsonNode feedNode : nodes) {
                                                FeedItem feedItem = feedItemFromInstagramJsonNode(feedNode);
                                                if (feedItem != null) {
                                                    // TODO: Parse likes and comments as well
                                                    feedItem.setOwnerId(currentInstagramAccount.getId());
                                                    feedItems.add(feedItem);
                                                    lowestEntryTs = (lowestEntryTs == 0) ? feedItem.getEntryTime().getTime()
                                                            : Math.min(feedItem.getEntryTime().getTime(), lowestEntryTs);

                                                    String itemUrl = feedItem.getUrl();
                                                    if (!processedInstagramCodes.containsKey(itemUrl)) {
                                                        // Add to processed
                                                        processedInstagramCodes.put(itemUrl, itemUrl);
                                                        currentNumInstagramItems++;

                                                        // Persist the feed item
                                                        if (!MagnesiumDbContext.externalRefIdExists(feedItem, db)) {
                                                            MagnesiumDbContext.insert(feedItem, db);
                                                        }

                                                        Log.d("#HELP", "Inserting Instagram link: " + itemUrl);
                                                    }
                                                }
                                            }
                                        }

                                        instagramCsrfToken = Helper.getNodeValue("csrf_token", sharedDataNode.get("config"));

                                        JsonNode pageInfoNode = mediaNode.get("page_info");
                                        boolean hasMore = Helper.getBooleanNodeValue("has_next_page", pageInfoNode);
                                        instagramNextCursor = hasMore ? Helper.getNodeValue("end_cursor", pageInfoNode) : null;
                                        instagramShouldFetchMore = (hasMore &&
                                                ((instagramMaxLocalTs > 0 && lowestEntryTs > instagramMaxLocalTs)
                                                        || currentNumInstagramItems < MAX_INSTAGRAM_ITEMS_PER_LOAD));
                                    } catch (IOException | NullPointerException ex) {
                                        Log.e(TAG, "Cannot parse Instagram shared data", ex);
                                    }
                                }

                                return feedItems;
                            }

                            protected void onPostExecute(List<BaseItem> results) {
                                int currentItemType = 0;
                                HomeActivity activity = (HomeActivity) getActivity();
                                if (activity != null) {
                                    NavigationDrawerItem item = activity.getCurrentNavigationDrawerItem();
                                    if (item != null && item.getFeedItemType() > 0) {
                                        currentItemType = item.getFeedItemType();
                                    }
                                }

                                if (results != null && results.size() > 0) {
                                    List<BaseItem> resultsToAdd = new ArrayList<BaseItem>();
                                    if (currentItemType > 0) {
                                        for (int i = 0; i < results.size(); i++) {
                                            BaseItem item = results.get(i);
                                            if (item.getType() == currentItemType) {
                                                resultsToAdd.add(item);
                                            }
                                        }
                                    }

                                    homeFeedAdapter.addItemsToOriginalList(currentItemType > 0 ? resultsToAdd : results);
                                    notifyHomeFeedAdapterChanged();
                                }

                                lastInstagramLoadTime = new Date();
                                if (instagramShouldFetchMore) {
                                    processNextInstagramFeedItems();
                                } else {
                                    currentInstagramIndex++;
                                    processNextInstagramAccount();
                                }
                            }
                        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                }

                return true;
            }
        });
        instagramWebView.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                if (!url.startsWith("javascript:")) {
                    lastInstagramWvUrl = url;
                }

                boolean isInstagram = (SocialMediaOauthDialog.INSTAGRAM_CLASSIC_LOGIN.equals(lastInstagramWvUrl)
                        || SocialMediaOauthDialog.INSTAGRAM_HOME.equals(lastInstagramWvUrl));
                if (instagramWebView != null) {
                    if (isInstagram) {
                        instagramWebView.loadUrl("javascript:console.log('>>MG:' + document.getElementsByTagName('html')[0].outerHTML);");
                    }
                }
            }

            @Override
            @SuppressWarnings("deprecation")
            public WebResourceResponse shouldInterceptRequest(WebView view, final String url) {
                if (INSTAGRAM_QUERY_URL.equals(url)
                        && !Helper.isEmpty(instagramCsrfToken)
                        && !Helper.isEmpty(instagramNextCursor)) {
                    final Map<String, String> additionalHeaders = new HashMap<String, String>();
                    additionalHeaders.put("User-Agent", userAgent);
                    additionalHeaders.put("Referer", "https://www.instagram.com/");
                    additionalHeaders.put("X-CSRFToken", instagramCsrfToken);
                    additionalHeaders.put("X-Instagram-AJAX", "1");
                    additionalHeaders.put("X-Requested-With", "XMLHttpRequest");

                    (new AsyncTask<Void, Void, List<BaseItem>>() {
                        protected List<BaseItem> doInBackground(Void... params) {
                            List<BaseItem> feedItems = new ArrayList<BaseItem>();

                            CookieManager cookieManager = CookieManager.getInstance();
                            if (cookieManager != null) {
                                try {
                                    URL parsedUrl = new URL(url);
                                    String cookiesString = cookieManager.getCookie(url);

                                    StringBuilder sb = new StringBuilder();
                                    HttpParameters httpParams = new HttpParameters();
                                    httpParams.put("q", String.format(IG_QUERY_Q, instagramNextCursor));
                                    httpParams.put("ref", IG_QUERY_REF);
                                    String delim = "";

                                    for (String paramName : httpParams.keySet()) {
                                        String paramValue = Uri.encode(httpParams.getFirst(paramName));
                                        sb.append(delim).append(paramName).append("=").append(paramValue);
                                        delim = "&";
                                    }

                                    instagramShouldFetchMore = false;
                                    String response = Http.post(url, sb.toString(), cookiesString, additionalHeaders);

                                    checkObjectMapper();
                                    long lowestEntryTs = 0;
                                    SQLiteDatabase db = dbContext.getWritableDatabase();

                                    JsonNode responseNode = objectMapper.readTree(response);
                                    JsonNode mediaNode = responseNode.get("feed").get("media");
                                    JsonNode nodes = mediaNode.get("nodes");
                                    if (nodes.isArray()) {
                                        for (final JsonNode feedNode : nodes) {
                                            FeedItem feedItem = feedItemFromInstagramJsonNode(feedNode);
                                            if (feedItem != null) {
                                                // TODO: Parse likes and comments as well
                                                feedItem.setOwnerId(currentInstagramAccount.getId());
                                                feedItems.add(feedItem);
                                                lowestEntryTs = (lowestEntryTs == 0) ? feedItem.getEntryTime().getTime()
                                                        : Math.min(feedItem.getEntryTime().getTime(), lowestEntryTs);

                                                String itemUrl = feedItem.getUrl();
                                                if (!processedInstagramCodes.containsKey(itemUrl)) {
                                                    // Add to processed
                                                    processedInstagramCodes.put(itemUrl, itemUrl);
                                                    currentNumInstagramItems++;

                                                    // Persist the feed item
                                                    if (!MagnesiumDbContext.externalRefIdExists(feedItem, db)) {
                                                        MagnesiumDbContext.insert(feedItem, db);
                                                    }

                                                    Log.d("#HELP", "Inserting Instagram link: " + itemUrl);
                                                }
                                            }
                                        }
                                    }

                                    JsonNode pageInfoNode = mediaNode.get("page_info");
                                    boolean hasMore = Helper.getBooleanNodeValue("has_next_page", pageInfoNode);
                                    instagramNextCursor = hasMore ? Helper.getNodeValue("end_cursor", pageInfoNode) : null;
                                    instagramShouldFetchMore = (hasMore &&
                                            ((instagramMaxLocalTs > 0 && lowestEntryTs > instagramMaxLocalTs)
                                                    || currentNumInstagramItems < MAX_INSTAGRAM_ITEMS_PER_LOAD));
                                } catch (IOException | NullPointerException ex) {
                                    Log.e(TAG, "Cannot parse new Instagram query", ex);
                                }
                            }

                            return feedItems;
                        }

                        protected void onPostExecute(List<BaseItem> results) {
                            int currentItemType = 0;
                            HomeActivity activity = (HomeActivity) getActivity();
                            if (activity != null) {
                                NavigationDrawerItem item = activity.getCurrentNavigationDrawerItem();
                                if (item != null && item.getFeedItemType() > 0) {
                                    currentItemType = item.getFeedItemType();
                                }
                            }

                            if (results != null && results.size() > 0) {
                                List<BaseItem> resultsToAdd = new ArrayList<BaseItem>();
                                if (currentItemType > 0) {
                                    for (int i = 0; i < results.size(); i++) {
                                        BaseItem item = results.get(i);
                                        if (item.getType() == currentItemType) {
                                            resultsToAdd.add(item);
                                        }
                                    }
                                }

                                homeFeedAdapter.addItemsToOriginalList(currentItemType > 0 ? resultsToAdd : results);
                                notifyHomeFeedAdapterChanged();
                            }

                            lastInstagramLoadTime = new Date();

                            Log.d("#HELP", "InstagramShouldFetchMore=" + instagramShouldFetchMore + "; NumInstagramItems=" + currentNumInstagramItems);

                            if (instagramShouldFetchMore) {
                                processNextInstagramFeedItems();
                            } else {
                                currentInstagramIndex++;
                                processNextInstagramAccount();
                            }
                        }
                    }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
                return super.shouldInterceptRequest(view, url);
            }
        });
        WebSettings settings = instagramWebView.getSettings();
        settings.setJavaScriptEnabled(true);

        return rootView;
    }

    private void checkObjectMapper() {
        if (objectMapper == null) {
            objectMapper = new ObjectMapper();
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        final HomeActivity activity = (HomeActivity) getContext();
        SharedPreferences preferences = activity.getSharedPreferences(HomeActivity.COMMON_SHARED_PREF_NAME, Context.MODE_PRIVATE);
        is24HourFormat = "24".equals(preferences.getString(GeneralSettingsFragment.SETTINGS_KEYS[0], "12"));
        isFahrenheit = "Fahrenheit".equalsIgnoreCase(preferences.getString(GeneralSettingsFragment.SETTINGS_KEYS[1], "Celsius"));
        loadWoeIdCache(preferences);

        updateDateAndTime();
        loadCurrentWeatherCondition();

        dbContext = new MagnesiumDbContext(activity);

        boolean isNewAdapter = false;
        if (homeFeedAdapter == null) {
            initHomeFeedAdapter();
            isNewAdapter = true;
        }

        initAds();

        if (!isNewAdapter) {
            // Refresh view if not new adapter
            notifyHomeFeedAdapterChanged();
        }
    }

    private void notifyHomeFeedAdapterChanged() {
        if (homeFeedAdapter.isInReplyMode() || homeFeedAdapter.isDeleteInProgress()) {
            homeFeedAdapter.setPendingNotify(true);
        } else {
            homeFeedAdapter.notifyDataSetChanged();
            homeFeedAdapter.setPendingNotify(false);
        }
    }

    private void initAds() {
        Context context = getContext();

        if (context != null) {
            boolean removeAds = (HomeActivity.thisActivity != null && HomeActivity.thisActivity.canRemoveAds());
            NativeAdControl nativeAdControl = null;
            if (!removeAds) {
                nativeAd = new NativeAd(context);
                nativeAd.getAdSettings().setPublisherId(1100017976);
                nativeAd.getAdSettings().setAdspaceId(130083060);

                nativeAdControl = new NativeAdControl(nativeAd);
            }

            if (homeFeedAdapter != null) {
                homeFeedAdapter.setAdsEnabled(!removeAds);
                homeFeedAdapter.setNativeAd(!removeAds ? nativeAd : null);
                homeFeedAdapter.setNativeAdControlInterface(!removeAds ? nativeAdControl : null);

                if (removeAds) {
                    homeFeedAdapter.removeAllAds();
                }

                notifyHomeFeedAdapterChanged();
            }
        }
    }

    private LoadLocalFeedTask loadLocalFeedTask;

    private RefreshFeedFromRemoteSourcesTask refreshRemoteTask;

    public void loadHomeFeedItems(boolean refreshRemote, boolean clearPrevious) {
        if (loadLocalFeedTask == null || loadLocalFeedTask.getStatus() == AsyncTask.Status.FINISHED) {
            loadLocalFeedTask = new LoadLocalFeedTask(refreshRemote, clearPrevious);
            loadLocalFeedTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    private void refreshRemoteFeedItems() {
        if ((refreshRemoteTask == null || refreshRemoteTask.getStatus() == AsyncTask.Status.FINISHED)
                && isOnline()) {
            refreshRemoteTask = new RefreshFeedFromRemoteSourcesTask();
            refreshRemoteTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    private class LoadLocalFeedTask extends AsyncTask<Void, Void, List<BaseItem>> {
        private boolean refreshRemote;

        private boolean clearPrevious;

        protected void onPreExecute() {
            if (clearPrevious && homeFeedAdapter != null) {
                homeFeedAdapter.clearOriginalList();
                notifyHomeFeedAdapterChanged();
            }
            if (loadProgress != null && (homeFeedAdapter == null || homeFeedAdapter.getItemCount() == 0)) {
                loadProgress.setVisibility(View.VISIBLE);
            }
            if (feedEmptyView != null && feedEmptyView.getVisibility() == View.VISIBLE) {
                feedEmptyView.setVisibility(View.INVISIBLE);
            }
        }

        public LoadLocalFeedTask(boolean refreshRemote, boolean clearPrevious) {
            this.refreshRemote = refreshRemote;
            this.clearPrevious = clearPrevious;
        }

        @Override
        protected List<BaseItem> doInBackground(Void... params) {
            try {
                int itemType = 0;
                HomeActivity activity = (HomeActivity) getActivity();
                if (activity != null) {
                    NavigationDrawerItem item = activity.getCurrentNavigationDrawerItem();
                    if (item != null && item.getFeedItemType() > 0) {
                        itemType = item.getFeedItemType();
                    }
                }

                SQLiteDatabase db = dbContext.getReadableDatabase();
                return MagnesiumDbContext.getFeedItems(db, 200, itemType);
            } catch (Exception ex) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<BaseItem> results) {
            localFeedLoaded = true;
            if (clearPrevious && homeFeedAdapter != null) {
                homeFeedAdapter.clearOriginalList();
            }

            if (loadProgress != null) {
                loadProgress.setVisibility(View.INVISIBLE);
            }

            int currentItemType = 0;
            HomeActivity activity = (HomeActivity) getActivity();
            if (activity != null) {
                NavigationDrawerItem item = activity.getCurrentNavigationDrawerItem();
                if (item != null && item.getFeedItemType() > 0) {
                    currentItemType = item.getFeedItemType();
                }
            }

            if (results != null && results.size() > 0) {
                List<BaseItem> resultsToAdd = new ArrayList<BaseItem>();
                if (currentItemType > 0) {
                    for (int i = 0; i < results.size(); i++) {
                        BaseItem item = results.get(i);
                        if (item.getType() == currentItemType) {
                            resultsToAdd.add(item);
                        }
                    }
                }
                if (homeFeedAdapter != null) {
                    homeFeedAdapter.addItemsToOriginalList(currentItemType > 0 ? resultsToAdd : results);
                    notifyHomeFeedAdapterChanged();
                }
            }

            boolean hasFeedItems = (homeFeedAdapter != null && homeFeedAdapter.getItemCount() > 0);
            if (feedEmptyView != null) {
                feedEmptyView.setVisibility(hasFeedItems ? View.INVISIBLE : View.VISIBLE);
            }
            loadLocalFeedTask = null;

            if (refreshRemote) {
                refreshRemoteFeedItems();
            }
        }
    }

    private FeedItemListAdapter.SmsMessageSenderCallback currentSmsMessageSenderCallback;

    private void preSendCurrentSmsMessage() {
        HomeActivity activity = (HomeActivity) getContext();
        if (!Helper.hasPermission(Manifest.permission.SEND_SMS, activity)) {
            Log.d("#HELP", "Requesting ability to send SMS messages?");
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.SEND_SMS)) {
                Helper.showToast(R.string.rationale_send_sms_permission, activity, Toast.LENGTH_LONG);
            }
            ActivityCompat.requestPermissions(activity,
                    new String[] { Manifest.permission.SEND_SMS }, PERMISSION_REQUEST_SEND_SMS_MESSAGE);
        } else {
            sendCurrentSmsMessage();
        }
    }

    private void sendCurrentSmsMessage() {
        boolean succeeded = false;
        if (!Helper.isEmpty(currentSmsRecipient) && !Helper.isEmpty(currentSmsMessageToSend)) {
            try {
                SmsManager smsManager = SmsManager.getDefault();
                if (smsManager != null) {
                    if (currentSmsMessageToSend.length() <= 160) {
                        smsManager.sendTextMessage(currentSmsRecipient, null, currentSmsMessageToSend, null, null);
                    } else {
                        ArrayList<String> messageParts = smsManager.divideMessage(currentSmsMessageToSend);
                        smsManager.sendMultipartTextMessage(currentSmsRecipient, null, messageParts, null, null);
                    }
                    showSimpleSnackbarMessage(String.format("Message sent to %s", currentSmsRecipient));
                    succeeded = true;

                    currentSmsRecipient = null;
                    currentSmsMessageToSend = null;
                }
            } catch (SecurityException ex) {
                Helper.showToast("The message could not be sent due to insufficient permissions. Magnesium Launcher requires the ability to be able to send text messages.",
                        getContext());
            } catch (IllegalArgumentException ex) {
                Helper.showToast("Please type the message you would like to send.", getContext());
            }
        }

        if (currentSmsMessageSenderCallback != null) {
            currentSmsMessageSenderCallback.onSendComplete(succeeded);
            currentSmsMessageSenderCallback = null;
        }
    }

    public void initHomeFeedAdapter() {
        Context context = getContext();

        homeFeedAdapter = new FeedItemListAdapter(context);
        homeFeedAdapter.setItemClickListener(homeFeedItemClickListener);
        homeFeedAdapter.setNativeAd(nativeAd);
        homeFeedAdapter.setFeedRecyclerView(homeFeedList);
        homeFeedAdapter.setFeedEmptyView(feedEmptyView);
        homeFeedAdapter.setNoResultsView(noResultsView);
        homeFeedAdapter.setIs24HourFormat(is24HourFormat);
        homeFeedAdapter.setDbContext(dbContext);

        homeFeedAdapter.setSmsMessageSender(new SmsMessageSender() {
            @Override
            public void sendMessage(String recipient, String message, FeedItemListAdapter.SmsMessageSenderCallback callback) {
                currentSmsRecipient = recipient;
                currentSmsMessageToSend = message;
                currentSmsMessageSenderCallback = callback;
                preSendCurrentSmsMessage();
            }
        });

        homeFeedAdapter.setSearchAdapterLoader(new IndexedItemSearchAdapterLoader() {
            @Override
            public void searchForIndexedItems(final String filterString, final FeedItemListAdapter adapter) {
                (new AsyncTask<Void, Void, List<IndexedItem>>() {
                    @Override
                    protected List<IndexedItem> doInBackground(Void... params) {
                        List<IndexedItem> results = new ArrayList<IndexedItem>();
                        HomeActivity context = (HomeActivity) getContext();
                        if (context != null) {
                            SharedPreferences preferences = context.getSharedPreferences(HomeActivity.COMMON_SHARED_PREF_NAME, Context.MODE_PRIVATE);
                            String includeResults = preferences.getString(
                                IndexingAndSearchFragment.ISR_KEY, IndexingAndSearchFragment.DEFAULT_INCLUDE_SEARCH_RESULTS);

                            int itemType = 0;
                            NavigationDrawerItem item = context.getCurrentNavigationDrawerItem();
                            if (item != null && item.getFeedItemType() > 0) {
                                itemType = item.getFeedItemType();
                            }
                            if (itemType > 0) {
                                includeResults = String.valueOf(itemType);
                            }

                            SQLiteDatabase db = dbContext.getReadableDatabase();
                            results = MagnesiumDbContext.findIndexedItems(filterString, db, 200, includeResults);
                        }
                        return results;
                    }

                    @Override
                    protected void onPostExecute(List<IndexedItem> results) {
                        if (searchProgress != null) {
                            searchProgress.setVisibility(View.GONE);
                        }

                        if (adapter != null) {
                            adapter.addIndexedSearchResults(results, webSearchInProgress);
                            adapter.notifyDataSetChanged();
                        }
                    }
                }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });

        if (homeFeedList != null) {
            homeFeedList.setAdapter(homeFeedAdapter);
        }
    }

    private static final String TWITTER_HOME_TIMELINE_URL = "https://api.twitter.com/1.1/statuses/home_timeline.json";

    private static IndexedItem indexedItemFromWebSearchResult(JsonNode node) {
        IndexedItem item = new IndexedItem();
        item.setType(BaseItem.ITEM_TYPE_WEB_SEARCH_RESULT);
        item.setTitle(Helper.getNodeValue("Title", node));
        item.setMetaContent(Helper.getNodeValue("Description", node));
        item.setRefId(Helper.getNodeValue("DisplayUrl", node));
        item.setUrl(Helper.getNodeValue("Url", node));
        if (Helper.isEmpty(item.getRefId())) {
            item.setRefId(Helper.getDisplayUrl(item.getUrl()));
        }

        String date = Helper.getNodeValue("Date", node);
        if (!Helper.isEmpty(date)) {
            try {
                item.setCreatedOn(Helper.JSON_DATE_FORMAT.parse(date));
            } catch (ParseException ex) {

            }
        }

        return item;
    }

    private class RefreshFeedFromRemoteSourcesTask extends AsyncTask<Void, Void, List<BaseItem>> {
        @Override
        protected void onPreExecute() {
            checkObjectMapper();
        }
        @Override
        protected List<BaseItem> doInBackground(Void... params) {
            List<BaseItem> results = new ArrayList<BaseItem>();

            // Load twitter elements
            SQLiteDatabase db = dbContext.getWritableDatabase();
            try {
                List<OauthData> oauthDataList = MagnesiumDbContext.getOauthDataList("twitter", db);
                OAuthConsumer consumer = new DefaultOAuthConsumer(Helper.TWITTER_API_KEY, Helper.TWITTER_API_CONSUMER_SECRET);
                for (int i = 0; i < oauthDataList.size(); i++) {
                    //String sinceId = (homeFeedAdapter != null) ? twitterGetMaxSinceId(homeFeedAdapter.getItems()) : "0";
                    String twitterUrl = TWITTER_HOME_TIMELINE_URL + "?count=200";
                    String sinceId = MagnesiumDbContext.getMaxTwitterSinceId(db);
                    if (!Helper.isEmpty(sinceId) && !"0".equals(sinceId)) {
                        twitterUrl = twitterUrl + "&since_id=" + sinceId;
                    }
                    OauthData oauth = oauthDataList.get(i);
                    if (!oauth.isValid()) {
                        continue;
                    }
                    consumer.setTokenWithSecret(oauth.getToken(), oauth.getTokenSecret());
                    String json = Http.getWithConsumer(twitterUrl, consumer);
                    if (Helper.isEmpty(json)) {
                        continue;
                    }

                    JsonNode jsonRoot = objectMapper.readTree(json);
                    // Save the feed items
                    if (jsonRoot != null && jsonRoot.isArray()) {
                        for (int j = 0; j < jsonRoot.size(); j++) {
                            JsonNode twitterNode =  jsonRoot.get(j);
                            FeedItem feedItem = feedItemFromTwitterJsonNode(twitterNode);
                            if (feedItem == null) {
                                continue;
                            }
                            if (feedItem.getPosterId().equals(String.format("@%s", oauth.getSocialAccountId()))) {
                                continue;
                            }

                            feedItem.setOwnerId(oauth.getLocalAccountId());
                            results.add(feedItem);

                            // Persist the feed item
                            if (!MagnesiumDbContext.externalRefIdExists(feedItem, db)) {
                                MagnesiumDbContext.insert(feedItem, db);
                            }
                        }
                    }
                }
            } catch (IOException ex) {
                Log.e(TAG, "IOException occurred", ex);
            }

            // Load Facebook elements
            try {
                List<OauthData> oauthDataList = MagnesiumDbContext.getOauthDataList("facebook", db);
                for (int i = 0; i < oauthDataList.size(); i++) {
                    OauthData oauth = oauthDataList.get(i);
                    if (!oauth.isValid()) {
                        continue;
                    }

                    String accessToken = oauth.getToken();
                    String json = Http.get(Helper.getFacebookUrlWithAccessToken(Helper.FACEBOOK_GRAPH_FEED_URL, accessToken));
                    if (Helper.isEmpty(json)) {
                        continue;
                    }
                    JsonNode jsonRoot = objectMapper.readTree(json);
                    if (!jsonRoot.has("data")) {
                        continue;
                    }
                    JsonNode dataJson = jsonRoot.get("data");
                    if (dataJson != null && dataJson.isArray()) {
                        for (int j = 0; j < dataJson.size(); j++) {
                            JsonNode fbNode =  dataJson.get(j);
                            FeedItem feedItem = feedItemFromFacebookJsonNode(fbNode);
                            if (feedItem == null || oauth.getSocialAccountId().equals(feedItem.getPosterId())) {
                                continue;
                            }
                            feedItem.setOwnerId(oauth.getLocalAccountId());
                            results.add(feedItem);

                            // Persist the feed item
                            if (!MagnesiumDbContext.externalRefIdExists(feedItem, db)) {
                                MagnesiumDbContext.insert(feedItem, db);
                            }
                        }
                    }
                }
            } catch (IOException ex) {
                Log.e(TAG, "IOException occurred", ex);
            }

            // Load Instagram elements
            instagramAccountsQueue.clear();
            try {
                List<SocialMediaAccount> socialAccounts = MagnesiumDbContext.getInstagramAccounts(HomeActivity.thisActivity.getEncrytionKey(), db);
                for (int i = 0; i < socialAccounts.size(); i++) {
                    SocialMediaAccount instagramAccount = socialAccounts.get(i);
                    if (!Helper.isEmpty(instagramAccount.getAccountId())
                            && !Helper.isEmpty(instagramAccount.getOauthData())) {
                        instagramAccountsQueue.add(instagramAccount);
                    }
                    break;
                }
            } catch (Exception ex) {
                Log.e(TAG, "IOException occurred", ex);
            }

            return results;
        }

        @Override
        protected void onPostExecute(List<BaseItem> results) {
            int currentItemType = 0;
            HomeActivity activity = (HomeActivity) getActivity();
            if (activity != null) {
                NavigationDrawerItem item = activity.getCurrentNavigationDrawerItem();
                if (item != null && item.getFeedItemType() > 0) {
                    currentItemType = item.getFeedItemType();
                }
            }

            if (results != null && results.size() > 0) {
                List<BaseItem> resultsToAdd = new ArrayList<BaseItem>();
                if (currentItemType > 0) {
                    for (int i = 0; i < results.size(); i++) {
                        BaseItem item = results.get(i);
                        if (item.getType() == currentItemType) {
                            resultsToAdd.add(item);
                        }
                    }
                }
                homeFeedAdapter.addItemsToOriginalList(currentItemType > 0 ? resultsToAdd : results);
                notifyHomeFeedAdapterChanged();
            }

            if (swipeRefreshLayout != null) {
                swipeRefreshLayout.setRefreshing(false);
            }

            if (instagramAccountsQueue.size() > 0 &&
                    (lastInstagramLoadTime == null || new Date().getTime() > (lastInstagramLoadTime.getTime() + HALF_HOUR_MILLIS))) {
                currentInstagramIndex = 0;
                processNextInstagramAccount();
            }

            boolean hasFeedItems = (homeFeedAdapter != null && homeFeedAdapter.getItemCount() > 0);
            feedEmptyView.setVisibility(hasFeedItems ? View.INVISIBLE : View.VISIBLE);
        }
    }

    public static final String TWITTER_STATUS_URL_FORMAT = "https://twitter.com/%s/status/%s";

    public static final String FACEBOOK_POST_URL_FORMAT = "https://www.facebook.com/%s/posts/%s";

    private void processNextInstagramFeedItems() {
        if (instagramWebView != null) {
            instagramWebView.pageDown(true);
            instagramQueryInProgress = true;
        }
    }

    private void processNextInstagramAccount() {
        if (currentInstagramIndex > -1 && instagramAccountsQueue.size() > currentInstagramIndex) {
            SocialMediaAccount instagramAccount = instagramAccountsQueue.get(currentInstagramIndex);
            currentInstagramAccount = instagramAccount;

            currentNumInstagramItems = 0;
            processedInstagramCodes.clear();
            instagramShouldFetchMore = false;
            instagramNextCursor = null;
            instagramQueryInProgress = false;
            instagramCsrfToken = null;

            (new AsyncTask<Void, Void, Void>() {
                protected Void doInBackground(Void... params) {
                    SQLiteDatabase db = dbContext.getReadableDatabase();
                    instagramMaxLocalTs = MagnesiumDbContext.getLatestFeedItemTimestampForOwner(
                            BaseItem.ITEM_TYPE_INSTAGRAM_POST, currentInstagramAccount.getId(), db);
                    return null;
                }

                protected void onPostExecute(Void result) {
                    // Check the maximum date for the Instagram feed
                    CookieManager.getInstance().removeAllCookie();
                    if (instagramWebView != null) {
                        instagramWebView.loadUrl(SocialMediaOauthDialog.INSTAGRAM_CLASSIC_LOGIN);
                    }
                }
            }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    public static FeedItem feedItemFromTwitterJsonNode(JsonNode node) {
        boolean isRetweet = node.has("retweeted_status");
        JsonNode baseNode = (isRetweet) ? node.get("retweeted_status") : node;

        FeedItem feedItem = new FeedItem();
        feedItem.setType(BaseItem.ITEM_TYPE_TWITTER_POST);

        feedItem.setRetweet(isRetweet);
        if (isRetweet && node.has("user")) {
            JsonNode retweeterNode = node.get("user");
            feedItem.setRetweetedById(String.format("@%s", Helper.getNodeValue("screen_name", retweeterNode)));
            feedItem.setRetweetedBy(Helper.getNodeValue("name", retweeterNode));
        }

        feedItem.setExternalRefId(Helper.getNodeValue("id", baseNode));
        feedItem.setContent(Helper.getNodeValue("text", baseNode));
        feedItem.setSource(Helper.getNodeValue("source", baseNode));

        if (baseNode.has("user")) {
            JsonNode posterNode = baseNode.get("user");
            feedItem.setAuthor(Helper.getNodeValue("name", posterNode));
            feedItem.setPosterId(String.format("@%s", Helper.getNodeValue("screen_name", posterNode)));
            feedItem.setPosterImageUrl(Helper.getNodeValue("profile_image_url", posterNode));
        } else {
            return null;
        }

        feedItem.setLikeCount(Helper.parseInt(Helper.getNodeValue("favorite_count", baseNode)));
        feedItem.setShareCount(Helper.parseInt(Helper.getNodeValue("retweet_count", baseNode)));

        // TODO: Retweets count, Favorites count
        feedItem.setUrl(String.format(TWITTER_STATUS_URL_FORMAT,
                Helper.getPlainTwitterName(feedItem.getPosterId()), feedItem.getExternalRefId()));

        try {
            feedItem.setEntryTime(Helper.TWITTER_DATE_FORMAT.parse(Helper.getNodeValue("created_at", baseNode)));
        } catch (ParseException ex) {
            Log.e(TAG, "Error parsing twitter date format.");
        }

        return feedItem;
    }

    private static final String FACEBOOK_GRAPH_PROFILE_PHOTO_FORMAT = "http://graph.facebook.com/%s/picture?type=normal";

    public static FeedItem feedItemFromFacebookJsonNode(JsonNode node) {
        FeedItem feedItem = new FeedItem();
        feedItem.setType(BaseItem.ITEM_TYPE_FACEBOOK_POST);

        if (!node.has("from")) {
            return null;
        }
        JsonNode fromNode = node.get("from");
        feedItem.setPosterId(Helper.getNodeValue("id", fromNode));
        feedItem.setAuthor(Helper.getNodeValue("name", fromNode));

        String fullPostId = Helper.getNodeValue("id", node);
        feedItem.setExternalRefId(fullPostId);
        String[] idParts = fullPostId.split("_");
        if (idParts.length < 2) {
            return null;
        }
        feedItem.setUrl(String.format(FACEBOOK_POST_URL_FORMAT, idParts[0], idParts[1]));

        //feedItem.setAuthor("Akinwale Ariwodola"); // TODO: Temp
        if (node.has("message")) {
            feedItem.setContent(Helper.getNodeValue("message", node));
        } else if (node.has("story")) {
            feedItem.setContent(Helper.getNodeValue("story", node));
        }

        feedItem.setPosterImageUrl(String.format(FACEBOOK_GRAPH_PROFILE_PHOTO_FORMAT, feedItem.getPosterId()));
        try {
            feedItem.setEntryTime(Helper.FACEBOOK_DATE_FORMAT.parse(Helper.getNodeValue("created_time", node)));
        } catch (ParseException ex) {
            Log.e(TAG, "Error parsing facebook date format.");
        }

        return feedItem;
    }

    public static FeedItem feedItemFromInstagramJsonNode(JsonNode node) {
        FeedItem feedItem = new FeedItem();
        feedItem.setType(BaseItem.ITEM_TYPE_INSTAGRAM_POST);
        feedItem.setExternalRefId(Helper.getNodeValue("id", node));

        if (node.has("caption")) {
            feedItem.setContent(Helper.getNodeValue("caption", node));
        }

        if (node.has("owner")) {
            JsonNode userNode = node.get("owner");
            feedItem.setPosterId(Helper.getNodeValue("username", userNode));
            feedItem.setPosterImageUrl(Helper.getNodeValue("profile_pic_url", userNode));
            feedItem.setAuthor(Helper.getNodeValue("full_name", userNode));
            if (Helper.isEmpty(feedItem.getAuthor())) {
                feedItem.setAuthor(feedItem.getPosterId());
            }
        } else {
            return null;
        }

        boolean isVideo = Helper.getBooleanNodeValue("is_video", node);
        feedItem.setMediaType(isVideo? "video" : "image");
        if (isVideo) {
            feedItem.setMediaUrl(Helper.getNodeValue("video_url", node));
        }
        feedItem.setImageUrl(Helper.getNodeValue("display_src", node));

        String code = Helper.getNodeValue("code", node);
        feedItem.setUrl(String.format("%s/%s", INSTAGRAM_BASE_ITEM_LINK, code));

        long time = (Helper.parseLong(Helper.getNodeValue("date", node)) * 1000);
        feedItem.setEntryTime(new Date(time));

        return feedItem;
    }

    public static String twitterGetMaxSinceId(List<BaseItem> items) {
        long id = 0;
        for (int i = 0; i < items.size(); i++) {
            BaseItem item = items.get(i);
            if (item.getType() != BaseItem.ITEM_TYPE_TWITTER_POST) {
                continue;
            }
            id = Math.max(id, Helper.parseLong(item.getReferenceId()));
        }
        return String.valueOf(id);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (searchInput != null) {
            searchInput.clearFocus();
        }

        initAds();

        if (locationLoadTimerTask == null) {
            locationLoadTimerTask = new TimerTask() {
                @Override
                public void run() {
                    boolean cleared = false;
                    if (locationLoadStartTime != null) {
                        Date dt = new Date();
                        if ((dt.getTime() - locationLoadStartTime.getTime()) > 120000) {
                            clearLocationUpdates(); // Force clear after 2 minutes.
                            cleared = true;
                        }
                    } else {
                        // Force clear anyway, if there was no start time
                        clearLocationUpdates();
                        cleared = true;
                    }

                    if (cleared) {
                        if (locationLoadTimerTask != null) {
                            locationLoadTimerTask.cancel();
                            locationLoadTimerTask = null;
                        }
                        if (locationLoadTimer != null) {
                            locationLoadTimer.cancel();
                            locationLoadTimer = null;
                        }
                    }
                }
            };
            locationLoadTimer = new Timer();
            locationLoadTimer.scheduleAtFixedRate(locationLoadTimerTask, 60000, 60000); // Check every 60 seconds
        }

        HomeActivity activity = (HomeActivity) getContext();
        SharedPreferences preferences = activity.getSharedPreferences(HomeActivity.COMMON_SHARED_PREF_NAME, Context.MODE_PRIVATE);
        int includeWsr = preferences.getInt(IndexingAndSearchFragment.WSR_KEY, 1);
        boolean isWebSearchEnabled = (includeWsr == 1);
        searchInput.setHint(isWebSearchEnabled ? R.string.find_everything_placeholder : R.string.find_on_device_only_placeholder);

        is24HourFormat = "24".equals(preferences.getString(GeneralSettingsFragment.SETTINGS_KEYS[0], "12"));
        isFahrenheit = "Fahrenheit".equalsIgnoreCase(preferences.getString(GeneralSettingsFragment.SETTINGS_KEYS[1], "Celsius"));
        updateDateAndTime();
        displayWeatherCondition(getCachedWeatherCondition());

        if (homeFeedAdapter != null) {
            homeFeedAdapter.setWebSearchAdapterLoader(null);
            homeFeedAdapter.setIs24HourFormat(is24HourFormat);
            notifyHomeFeedAdapterChanged();
        }
        if (homeFeedAdapter != null && isWebSearchEnabled) {
            WebSearchAdapterLoader wsAdapterLoader = new WebSearchAdapterLoader() {
                @Override
                public void searchWebItems(final String filterString, final FeedItemListAdapter adapter) {
                (new AsyncTask<Void, Void, List<IndexedItem>>() {
                    @Override
                    protected void onPreExecute() {
                        checkObjectMapper();

                        if (!isOnline() || webSearchInProgress) {
                            cancel(true);
                            return;
                        }

                        HomeActivity activity = (HomeActivity) getContext();
                        if (activity != null) {
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (searchProgress != null) {
                                        searchProgress.setVisibility(View.VISIBLE);
                                    }
                                }
                            });
                        }

                        webSearchInProgress = true;
                    }

                    @Override
                    protected List<IndexedItem> doInBackground(Void... params) {
                        if (isCancelled()) {
                            return null;
                        }

                        List<IndexedItem> results = new ArrayList<IndexedItem>();
                        String url = String.format(WEB_SEARCH_URL_FORMAT, Helper.getUrlEncodedString(filterString));
                        String json = Http.getWithBasicAuth(url, DATA_MARKET_ACCOUNT_KEY, DATA_MARKET_ACCOUNT_KEY);
                        try {
                            JsonNode jsonNode = objectMapper.readTree(json);
                            if (jsonNode.has("d")) {
                                JsonNode dNode = jsonNode.get("d");
                                if (dNode.has("results")) {
                                    JsonNode resultsNode = dNode.get("results");
                                    JsonNode firstNode =  null;
                                    JsonNode currentBaseNode = null;

                                    if (resultsNode.isArray()) {
                                        firstNode = resultsNode.get(0);
                                    }
                                    if (firstNode != null && firstNode.has("News")) {
                                        currentBaseNode = firstNode.get("News");
                                        insertItemsFromSearchResultsNode(currentBaseNode, results);
                                    }
                                    if (firstNode != null && firstNode.has("Web")) {
                                        currentBaseNode = firstNode.get("Web");
                                        insertItemsFromSearchResultsNode(currentBaseNode, results);
                                    }
                                }
                            }
                        } catch (IOException ex) {
                            // Log and continue
                        }

                        Collections.sort(results, Collections.reverseOrder());
                        return results;
                    }

                    @Override
                    protected void onPostExecute(final List<IndexedItem> results) {
                        webSearchInProgress = false;

                        HomeActivity activity = (HomeActivity) getContext();
                        if (activity != null) {
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                if (searchProgress != null) {
                                    searchProgress.setVisibility(View.GONE);
                                }
                                if (results == null) {
                                    return;
                                }

                                if (adapter != null) {
                                    adapter.intersperseWebSearchResults(results);
                                    adapter.notifyDataSetChanged();
                                }
                                }
                            });
                        }
                    }
                }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            };

            homeFeedAdapter.setWebSearchAdapterLoader(wsAdapterLoader);
        }

        loadHomeFeedItems(true, false);
        //loadHomeFeedItems(false);
    }

    private void insertItemsFromSearchResultsNode(JsonNode currentBaseNode, List<IndexedItem> results) {
        if (currentBaseNode != null && currentBaseNode.isArray()) {
            for (int i = 0; i < currentBaseNode.size(); i++) {
                JsonNode thisNode = currentBaseNode.get(i);
                IndexedItem indexedItem = indexedItemFromWebSearchResult(thisNode);
                results.add(indexedItem);
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public void updateDateAndTime() {
        Date dt = new Date();
        if (dayOfWeekView != null) {
            dayOfWeekView.setText(DAY_OF_WEEK_FORMAT.format(dt));
        }
        if (dateView != null) {
            dateView.setText(DATE_FORMAT.format(dt));
        }
        if (timeView != null) {
            SimpleDateFormat timeFormat = is24HourFormat ? TIME_FORMAT_24H : TIME_FORMAT_12H;
            timeView.setText(timeFormat.format(dt).toLowerCase());
        }
    }

    private static final int PERMISSION_REQUEST_ACCESS_FINE_LOCATION = 201;

    private static final int PERMISSION_REQUEST_SEND_SMS_MESSAGE = 202;

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_ACCESS_FINE_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    detectCurrentLocationForWeather();
                } else {
                    Helper.showToast(R.string.denied_location_permission, getContext(), Toast.LENGTH_LONG);
                }
                break;

            case PERMISSION_REQUEST_SEND_SMS_MESSAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    sendCurrentSmsMessage();
                } else {
                    Helper.showToast(R.string.denied_send_sms_permission, getContext(), Toast.LENGTH_LONG);
                    if (currentSmsMessageSenderCallback != null) {
                        currentSmsMessageSenderCallback.onSendComplete(false);

                        currentSmsRecipient = null;
                        currentSmsMessageToSend = null;
                        currentSmsMessageSenderCallback = null;
                    }
                }
                break;
        }
    }

    public void refreshWeather() {
        boolean gpsEnabled = false;
        String cachedLocation = getCachedLocation();
        Context context = getContext();
        if (context != null) {
            LocationManager manager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
            gpsEnabled = manager.isProviderEnabled(LocationManager.GPS_PROVIDER) || manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
                || manager.isProviderEnabled(LocationManager.PASSIVE_PROVIDER);
        }

        if (gpsEnabled) {
            detectCurrentLocationForWeather();
        } else if (!Helper.isEmpty(cachedLocation)) {
            updateWeatherForLocationName(cachedLocation);
        }
    }

    private void detectCurrentLocationForWeather() {
        HomeActivity activity = (HomeActivity) getContext();
        if (activity == null) {
            return;
        }

        if (locationManager == null) {
            locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        }

        try {
            numLocationTries = 0;
            locationListener = new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    LastKnownLocation = location;
                    updateWeatherForLocation(location);
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            };

            boolean networkProvider = false;
            boolean gpsProvider = false;
            boolean passiveProvider = false;

            try {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 20000, 0, locationListener);
                networkProvider = true;
            } catch (IllegalArgumentException ex) {
                // continue
            }

            try {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 20000, 0, locationListener);
                gpsProvider = true;
            } catch (IllegalArgumentException ex) {
                // continue
            }

            try {
                locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, 20000, 0, locationListener);
                passiveProvider = true;
            } catch (IllegalArgumentException ex) {
                // continue
            }

            if (networkProvider || gpsProvider || passiveProvider) {
                // Cannot obtain user location
                locationLoadStartTime = new Date();
            }
        } catch (SecurityException ex) {
            Log.e("#HELP", "Security Exception man. Using cached location.", ex);
            // Try to check for cached location instead
            String cachedLocation = getCachedLocation();
            if (!Helper.isEmpty(cachedLocation)) {
                updateWeatherForLocationName(cachedLocation);
            }
        }
    }

    public void loadCurrentWeatherCondition() {
        WeatherCondition cachedCondition = getCachedWeatherCondition();
        if (cachedCondition == null
                || cachedCondition.getLastUpdated() == null
                || (new Date().getTime() - cachedCondition.getLastUpdated().getTime()) > (4 * ONE_HOUR_MILLIS)) {
            if (cachedCondition != null) {
                currentLocationName = cachedCondition.getFullLocationName();
            }

            if (Helper.isEmpty(currentLocationName)) {
                HomeActivity activity = (HomeActivity) getContext();
                if (!Helper.hasPermission(Manifest.permission.ACCESS_FINE_LOCATION, activity)) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_FINE_LOCATION)) {
                        Helper.showToast(R.string.rationale_location_permission, activity, Toast.LENGTH_LONG);
                    }
                    ActivityCompat.requestPermissions(activity,
                        new String[] { Manifest.permission.ACCESS_FINE_LOCATION }, PERMISSION_REQUEST_ACCESS_FINE_LOCATION);
                } else {
                    detectCurrentLocationForWeather();
                }
            }
            return;
        }

        if (cachedCondition != null) {
            displayWeatherCondition(cachedCondition);
        }
    }

    public void refreshWeatherCondition() {
        String locationName = getCachedLocation();
        if (Helper.isEmpty(locationName)) {
            HomeActivity activity = (HomeActivity) getContext();
            if (!Helper.hasPermission(Manifest.permission.ACCESS_FINE_LOCATION, activity)) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_FINE_LOCATION)) {
                    Helper.showToast(R.string.rationale_location_permission, activity, Toast.LENGTH_LONG);
                }
                ActivityCompat.requestPermissions(activity,
                        new String[] { Manifest.permission.ACCESS_FINE_LOCATION }, PERMISSION_REQUEST_ACCESS_FINE_LOCATION);
            } else {
                detectCurrentLocationForWeather();
            }
        }
    }

    private void displayWeatherCondition(WeatherCondition condition) {
        if (condition == null) {
            return;
        }

        int iconDrawable = WeatherConditionNameIconMap.containsKey(condition.getConditionText())
            ? WeatherConditionNameIconMap.get(condition.getConditionText()) : R.drawable.ic_weather_sunny_white_48dp;
        Calendar cal = Calendar.getInstance();
        int hourOfDay = cal.get(Calendar.HOUR_OF_DAY);
        if ((hourOfDay >= 0 && hourOfDay <= 6) || (hourOfDay >= 18 && hourOfDay <= 23)) {
            iconDrawable = R.drawable.ic_weather_night_white_48dp;
        }

        weatherIconView.setImageResource(iconDrawable);
        weatherConditionTextView.setText(condition.getConditionText());
        weatherTempView.setText((isFahrenheit ? condition.getTemperature() : condition.getCelsiusTemperature()) + "\u00B0");
        weatherLocationNameView.setText(Helper.getSimpleLocationName(condition.getFullLocationName()));
        weatherLocationIconView.setVisibility(View.VISIBLE);
    }

    private void clearLocationUpdates() {
        try {
            if (locationManager != null) {
                locationManager.removeUpdates(locationListener);
                locationListener = null;
            }
            locationManager = null;
        } catch (SecurityException ex) {
            // ignore
        }

        if (locationLoadTimer != null) {
            locationLoadTimer.cancel();
        }
        locationLoadTimer = null;
        locationLoadTimerTask = null;
    }

    private void cacheWeatherCondition(WeatherCondition condition) {
        HomeActivity activity = (HomeActivity) getContext();
        if (activity != null) {
            try {
                checkObjectMapper();

                SharedPreferences preferences = activity.getSharedPreferences(HomeActivity.COMMON_SHARED_PREF_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString(WEATHER_CONDITION_KEY, objectMapper.writeValueAsString(condition));
                editor.commit();
            } catch (JsonProcessingException ex) {
                // cannot cache. Ignore
            }
        }
    }

    private void saveCachedLocation(String locationName) {
        HomeActivity activity = (HomeActivity) getContext();
        if (activity != null) {
            SharedPreferences preferences = activity.getSharedPreferences(HomeActivity.COMMON_SHARED_PREF_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(CACHED_LOCATION_KEY, locationName);
            editor.commit();
        }
    }

    private String getCachedLocation() {
        HomeActivity activity = (HomeActivity) getContext();
        if (activity != null) {
            SharedPreferences preferences = activity.getSharedPreferences(HomeActivity.COMMON_SHARED_PREF_NAME, Context.MODE_PRIVATE);
            String locationName = preferences.getString(CACHED_LOCATION_KEY, null);
            return locationName;
        }
        return null;
    }

    private void loadWoeIdCache(SharedPreferences preferences) {
        TypeFactory typeFactory = objectMapper.getTypeFactory();
        MapType mapType = typeFactory.constructMapType(HashMap.class, String.class, String.class);
        try {
            locationsWoeIdCache.putAll((HashMap<String, String>) objectMapper.readValue(preferences.getString(SETTING_KEY_WOE_ID_CACHE, "{}"), mapType));
        } catch (IOException ex) {
            // Cannot read location woe ID cache
        }
    }

    private void commitWoeIdCache() {
        final HomeActivity activity = (HomeActivity) getContext();
        if (activity != null) {
            (new AsyncTask<Void, Void, Void>() {
                protected Void doInBackground(Void... params) {
                    if (activity != null) {
                        SharedPreferences preferences = activity.getSharedPreferences(HomeActivity.COMMON_SHARED_PREF_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();

                        try {
                            String json = objectMapper.writeValueAsString(locationsWoeIdCache);
                            editor.putString(SETTING_KEY_WOE_ID_CACHE, json);
                        } catch (JsonProcessingException ex) {
                            // Do nothing
                        }

                        editor.commit();
                    }

                    return null;
                }
            }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    private WeatherCondition getCachedWeatherCondition() {
        checkObjectMapper();

        HomeActivity activity = (HomeActivity) getContext();
        if (activity != null) {
            SharedPreferences preferences = activity.getSharedPreferences(HomeActivity.COMMON_SHARED_PREF_NAME, Context.MODE_PRIVATE);
            String wcJson = preferences.getString(WEATHER_CONDITION_KEY, null);
            try {
                if (!Helper.isEmpty(wcJson)) {
                    return objectMapper.readValue(wcJson, WeatherCondition.class);
                }
            } catch (IOException ex) {
                // cannot read cached weather condition. Ignore
                Log.e(TAG, "Exception occurred reading cached weather condition", ex);
            }
        }

        return null;
    }

    public boolean isOnline() {
        HomeActivity activity = (HomeActivity) getContext();
        return (activity != null && activity.isOnline());
    }

    public void updateWeatherForLocationName(final String locationName) {
        if (isOnline()) {
            (new AsyncTask<Void, Void, WeatherCondition>() {
                protected WeatherCondition doInBackground(Void... params) {
                    return loadWeatherConditionForLocationName(locationName);
                }

                protected void onPostExecute(WeatherCondition result) {
                    if (result != null) {
                        cacheWeatherCondition(result);
                        displayWeatherCondition(result);
                    }
                }
            }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    private WeatherCondition loadWeatherConditionForLocationName(String locationName) {
        if (Helper.isEmpty(locationName)) {
            // Try to fallback to cached location
            locationName = getCachedLocation();
            if (Helper.isEmpty(locationName)) {
                return null;
            }
        }

        try {
            boolean isNewWoeId = false;
            String woeId = locationsWoeIdCache.get(locationName);
            if (Helper.isEmpty(woeId)) {
                isNewWoeId = true;

                // If no local WOE ID, get Yahoo! WOE ID
                String woeIdQuery = String.format(WOE_ID_QUERY_FORMAT, locationName);
                String url = String.format(YQL_QUERY_URL_FORMAT, URLEncoder.encode(woeIdQuery, "utf-8"));
                String json = Http.get(url);

                JsonNode rootNode = objectMapper.readTree(json);
                JsonNode placeNode = rootNode.get("query").get("results").get("place");
                if (placeNode.getNodeType() == JsonNodeType.ARRAY) {
                    woeId = placeNode.get(0).get("woeid").asText();
                } else if (placeNode.getNodeType() == JsonNodeType.OBJECT) {
                    JsonNode woeIdNode = placeNode.get("woeId");
                    if (woeIdNode == null) {
                        woeIdNode = placeNode.get("woeid");
                    }
                    if (woeIdNode != null) {
                        woeId = woeIdNode.asText();
                    }
                }
            }

            if (!Helper.isEmpty(woeId)) {
                if (isNewWoeId) {
                    locationsWoeIdCache.put(locationName, woeId);
                    commitWoeIdCache();
                }

                String weatherQuery = String.format(WEATHER_CONDITION_QUERY_FORMAT, woeId);
                String url = String.format(YQL_QUERY_URL_FORMAT, URLEncoder.encode(weatherQuery, "utf-8"));
                String json = Http.get(url);
                JsonNode rootNode = objectMapper.readTree(json);

                if (rootNode != null
                        && rootNode.has("query")
                        && rootNode.get("query").has("results")
                        && rootNode.get("query").get("results").has("channel")
                        && rootNode.get("query").get("results").get("channel").has("item")) {
                    JsonNode conditionNode = rootNode.get("query").get("results").get("channel").get("item").get("condition");
                    return WeatherCondition.fromYqlJsonNode(conditionNode, locationName, woeId);
                }
            }
        } catch (Exception ex) {
            Log.e(TAG, "Could not process JSON", ex);
        }

        return null;
    }

    private void updateWeatherForLocation(final Location location) {
        clearLocationUpdates();

        if (isOnline()) {
            // Get current or last known location
            (new AsyncTask<Void, Void, WeatherCondition>() {
                protected WeatherCondition doInBackground(Void... params) {
                    HomeActivity activity = (HomeActivity) getContext();

                    JsonFactory factory = new JsonFactory();
                    ObjectMapper mapper = new ObjectMapper();
                    if (activity != null) {
                        try {
                            if (location != null) {
                                String url = String.format(GEOCODE_URL_FORMAT,
                                        String.valueOf(location.getLatitude()),
                                        String.valueOf(location.getLongitude()));
                                String json = Http.get(url);

                                JsonNode geocodeResult = mapper.readTree(json);
                                //Log.d("#GEOCODE", geocodeResult.toString());
                                JsonNode results = geocodeResult.get("results");
                                if (results != null && results.getNodeType() == JsonNodeType.ARRAY) {
                                    // Get the first result
                                    JsonNode resultNode = results.get(0);
                                    if (resultNode != null && resultNode.has("address_components")) {
                                        JsonNode addrComponentsNode = resultNode.get("address_components");
                                        String locationName = null;
                                        String country = null;
                                        if (addrComponentsNode != null && addrComponentsNode.getNodeType() == JsonNodeType.ARRAY) {
                                            String currentName = null;
                                            for (int i = 0; i < addrComponentsNode.size(); i++) {
                                                JsonNode node = addrComponentsNode.get(i);
                                                JsonNode nameNode = node.get("long_name");
                                                if (nameNode != null) {
                                                    currentName = nameNode.asText();
                                                }
                                                JsonNode types = node.get("types");
                                                if (types != null && types.getNodeType() == JsonNodeType.ARRAY) {
                                                    for (int j = 0; j < types.size(); j++) {
                                                        String type = types.get(j).asText();
                                                        if (("sublocality_level_1".equals(type) || "sublocality".equals(type)) && !Helper.isEmpty(currentName)) {
                                                            locationName = currentName;
                                                            break;
                                                        }
                                                        if (locationName == null && "locality".equals(type)) {
                                                            locationName = currentName;
                                                            break;
                                                        }
                                                        if ("country".equals(type)) {
                                                            country = currentName;
                                                            break;
                                                        }
                                                    }
                                                }
                                                if (!Helper.isEmpty(locationName) && !Helper.isEmpty(country)) {
                                                    break;
                                                }
                                            }

                                            currentLocationName = locationName;
                                            if (!Helper.isEmpty(country)) {
                                                currentLocationName = String.format("%s, %s", currentLocationName, country);
                                            }

                                            saveCachedLocation(currentLocationName);
                                        }
                                    }
                                }
                            }
                        } catch (IOException ex) {
                            return null;
                        }

                        return loadWeatherConditionForLocationName(currentLocationName);
                    }

                    return null;
                }

                @Override
                protected void onPostExecute(WeatherCondition result) {
                    if (result != null) {
                        cacheWeatherCondition(result);
                        displayWeatherCondition(result);
                    }
                }
            }).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
        }
    }

    public boolean isDeleteInProgress() {
        return deleteInProgress;
    }

    public void setDeleteInProgress(boolean deleteInProgress) {
        this.deleteInProgress = deleteInProgress;
    }

    public void showUndoableSnackbarMessage(String message, Snackbar.Callback dismissCallback, View.OnClickListener undoClickListener) {
        if (coordinatorLayout != null) {
            Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT).
                    setCallback(dismissCallback).
                    setAction(R.string.action_undo, undoClickListener).show();
        }
    }

    public void showSimpleSnackbarMessage(String message) {
        if (coordinatorLayout != null) {
            Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT).show();
        }
    }

    private class NativeAdControl implements NativeAdControlInterface, AdListenerInterface, NativeAd.NativeAdListener {

        private Date lastLoadTime;

        private int position;

        private NativeAd nad;

        private ImageView iconView;

        private TextView titleView;

        private TextView summaryView;

        private View parentView;

        private boolean nativeAdLoading;

        private boolean adLoaded;

        public NativeAdControl(NativeAd nad) {
            this.nad = nad;
            this.nad.setAdListener(this);
        }

        public void loadNativeAd(int position, ImageView iconView, TextView titleView, TextView summaryView, View parentView) {
            this.position = position;
            this.iconView = iconView;
            this.titleView = titleView;
            this.summaryView = summaryView;
            this.parentView = parentView;

            if (!nativeAdLoading/* && (lastLoadTime == null || (lastLoadTime != null && (new Date().getTime() - lastLoadTime.getTime()) > 30000))*/) {
                iconView.setImageBitmap(null);
                parentView.setPadding(0, 0, 0, 0);

                this.nad.asyncLoadPlainNativeAd(true, false, true, true, this);
                nativeAdLoading = true;
            }
        }

        public void onAdResponse(BannerNativeAd ad) {
            nativeAdLoading = false;
            lastLoadTime = new Date();

            Context context = getContext();
            int side = Helper.getValueForDip(8, context);
            int top = Helper.getValueForDip(12, context);

            iconView.setVisibility(View.VISIBLE);
            titleView.setVisibility(View.VISIBLE);
            summaryView.setVisibility(View.VISIBLE);

            if (ad != null && homeFeedAdapter != null) {
                parentView.setPadding(side, top, side, top);

                homeFeedAdapter.updateAdItem(ad.getTitle(), ad.getText(), ad.getIconImageUrl(), ad.getClickToActionUrl());
                homeFeedAdapter.notifyItemChanged(position);
            }

            adLoaded = true;
        }

        public void onError(ErrorCode errorCode, String message) {
            nativeAdLoading = false;
            lastLoadTime = new Date();

            iconView.setVisibility(View.GONE);
            titleView.setVisibility(View.GONE);
            summaryView.setVisibility(View.GONE);
            parentView.setPadding(0, 0, 0, 0);

            adLoaded = false;
        }

        @Override
        public void onReceiveAd(AdDownloaderInterface adDownloaderInterface, ReceivedBannerInterface receivedBannerInterface) throws AdReceiveFailed {
            nativeAdLoading = false;
            lastLoadTime = new Date();

        }

        public boolean isAdLoaded() {
            return adLoaded;
        }
    }
}