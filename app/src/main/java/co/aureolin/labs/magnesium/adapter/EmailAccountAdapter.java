package co.aureolin.labs.magnesium.adapter;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

import co.aureolin.labs.magnesium.R;
import co.aureolin.labs.magnesium.model.EmailAccount;

public class EmailAccountAdapter extends ArrayAdapter<EmailAccount> {
    public EmailAccountAdapter(Context context) {
        super(context, 0);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_string, null);
        }

        EmailAccount item = getItem(position);

        TextView itemTextView = (TextView) convertView.findViewById(R.id.item_text);
        itemTextView.setText(item.getEmailAddress());

        return convertView;
    }
}
