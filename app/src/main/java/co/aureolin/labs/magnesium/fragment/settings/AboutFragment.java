package co.aureolin.labs.magnesium.fragment.settings;

import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import co.aureolin.labs.magnesium.R;

/**
 * Created by akinwale on 17/02/2016.
 */
public class AboutFragment extends Fragment {
    public static AboutFragment newInstance() {
        return new AboutFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_about, container, false);

        try {
            ((TextView) rootView.findViewById(R.id.version_info)).setText(
                getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName
            );
        } catch (PackageManager.NameNotFoundException e) {

        }

        //((ImageView) rootView.findViewById(R.id.app_image)).setColorFilter(0xFF00838F, PorterDuff.Mode.SRC_ATOP);
        return rootView;
    }
}
