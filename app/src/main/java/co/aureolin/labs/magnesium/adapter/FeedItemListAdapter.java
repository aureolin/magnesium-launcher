package co.aureolin.labs.magnesium.adapter;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.NativeExpressAdView;
import com.smaato.soma.nativead.NativeAd;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.aureolin.labs.magnesium.HomeActivity;
import co.aureolin.labs.magnesium.R;
import co.aureolin.labs.magnesium.fragment.HomeFeedFragment;
import co.aureolin.labs.magnesium.fragment.settings.IndexingAndSearchFragment;
import co.aureolin.labs.magnesium.model.BaseItem;
import co.aureolin.labs.magnesium.model.FeedItem;
import co.aureolin.labs.magnesium.model.IndexedItem;
import co.aureolin.labs.magnesium.model.OauthData;
import co.aureolin.labs.magnesium.util.BitmapMemoryCache;
import co.aureolin.labs.magnesium.util.ContactsCache;
import co.aureolin.labs.magnesium.util.Helper;
import co.aureolin.labs.magnesium.util.Http;
import co.aureolin.labs.magnesium.util.IndexedItemSearchAdapterLoader;
import co.aureolin.labs.magnesium.util.NativeAdControlInterface;
import co.aureolin.labs.magnesium.util.SmsMessageSender;
import co.aureolin.labs.magnesium.util.WebSearchAdapterLoader;
import co.aureolin.labs.magnesium.util.db.MagnesiumDbContext;
import co.aureolin.labs.magnesium.util.listener.GenericItemClickListener;
import oauth.signpost.OAuth;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.basic.DefaultOAuthConsumer;
import oauth.signpost.http.HttpParameters;

/**
 * Created by akinwale on 11/09/2016.
 */
public class FeedItemListAdapter extends RecyclerView.Adapter<FeedItemListAdapter.ViewHolder> {

    private static final String TAG = FeedItemListAdapter.class.getCanonicalName();

    private static final int MAX_ITEMS = 500;

    private static final int SMAATO_VIEW_TYPE = -1000;

    private static final int ADMOB_VIEW_TYPE = -2000;

    private String currentConstraint;

    private HashMap<Long, TextWatcher> itemTextWatchers = new HashMap<Long, TextWatcher>();

    private boolean adsEnabled;

    private boolean adMobAdLoaded;

    private RecyclerView feedRecyclerView;

    private NativeAd nativeAd;

    private GenericItemClickListener itemClickListener;

    private static final String REF_ID_SMAATO = "smaato_feed_ad";

    private static final String REF_ID_ADMOB = "admob_feed_ad";

    private Object lock;

    private ObjectMapper objectMapper = new ObjectMapper();

    private Context context;

    private boolean adMobInitialRequestSent;

    private boolean twitterPostInProgress;

    private boolean smsSendInProgress;

    private SmsMessageSender smsMessageSender;

    private static final int TWITTER_CHARACTER_LIMIT = 140;

    private static final int SMS_CHARACTER_LIMIT = 160;

    private boolean inReplyMode;

    private boolean deleteInProgress;

    private boolean pendingNotify;

    private TextView feedEmptyView;

    private TextView noResultsView;

    private List<BaseItem> originalList;

    private List<BaseItem> currentList;

    private List<BaseItem> searchResultsList;

    private IndexedItemSearchAdapterLoader searchAdapterLoader;

    private WebSearchAdapterLoader webSearchAdapterLoader;

    private NativeAdControlInterface nativeAdControlInterface;

    private boolean is24HourFormat;

    private boolean nativeAdConfigured;

    private List<IndexedItem> currentExternalResults;

    private boolean filteringInProgress;

    private AdRequest adRequest;

    private MagnesiumDbContext dbContext;

    private boolean intersperseInProgress;

    private static final int DEFAULT_FILE_EXT_DRAWABLE = R.drawable.document325;
    private static Map<String, Integer> FileExtensionDrawableMap = new HashMap<String, Integer>();
    static {
        FileExtensionDrawableMap.put("docx", R.drawable.docx);
        FileExtensionDrawableMap.put("txt", R.drawable.txt2);
        FileExtensionDrawableMap.put("pdf", R.drawable.pdf19);
        FileExtensionDrawableMap.put("jpg", R.drawable.mountain35);
        FileExtensionDrawableMap.put("png", R.drawable.mountain35);
        FileExtensionDrawableMap.put("gif", R.drawable.mountain35);
        FileExtensionDrawableMap.put("jpeg", R.drawable.mountain35);
        FileExtensionDrawableMap.put("bmp", R.drawable.mountain35);
    }

    public FeedItemListAdapter(Context context) {
        this.context = context;
        lock = new Object();
        originalList = new ArrayList<BaseItem>();
        currentList = new ArrayList<BaseItem>();
        searchResultsList = new ArrayList<BaseItem>();
    }

    public void clearOriginalList() {
        setOriginalList(new ArrayList<BaseItem>());
    }

    public void setOriginalList(List<BaseItem> appItemList) {
        originalList = new ArrayList<BaseItem>(appItemList);
        if (originalList.size() > 0) {
            placeAdsInList(originalList);
        }
        currentList = new ArrayList<BaseItem>(originalList);
    }

    private void removeAdsFromList(List<BaseItem> list) {
        if (HomeActivity.thisActivity != null && !HomeActivity.thisActivity.canRemoveAds()) {
            FeedItem adItem = new FeedItem();
            adItem.setExternalRefId(REF_ID_SMAATO);
            int index = list.indexOf(adItem);
            if (index > -1) {
                list.remove(adItem);
            }

            adItem = new FeedItem();
            adItem.setExternalRefId(REF_ID_ADMOB);
            index = list.indexOf(adItem);
            if (index > -1) {
                list.remove(adItem);
            }
        }
    }

    public void removeAllAds() {
        synchronized (lock) {
            removeAdsFromList(currentList);
        }
        removeAdsFromList(originalList);
    }

    private void placeAdsInList(List<BaseItem> list) {
        /*if (HomeActivity.thisActivity != null && !HomeActivity.thisActivity.canRemoveAds()) {
            FeedItem adItem = new FeedItem();
            adItem.setExternalRefId(REF_ID_SMAATO);
            adItem.setType(BaseItem.ITEM_TYPE_AD);
            adItem.setAd(true);

            if (!list.contains(adItem)) {
                if (list.size() > 2) {
                    list.add(1, adItem);
                } else {
                    list.add(adItem);
                }
            }

            adItem = new FeedItem();
            adItem.setExternalRefId(REF_ID_ADMOB);
            adItem.setContent("visible");
            adItem.setType(BaseItem.ITEM_TYPE_AD);
            adItem.setAd(true);

            if (!list.contains(adItem)) {
                if (list.size() > 6) {
                    list.add(5, adItem);
                } else {
                    list.add(adItem);
                }
            }
        }*/
    }

    public void updateAdItem(String title, String text, String iconImageUrl, String url) {
        if (HomeActivity.thisActivity != null && !HomeActivity.thisActivity.canRemoveAds()) {
            synchronized (lock) {
                int index = -1;
                for (int i = 0; i < currentList.size(); i++) {
                    if (currentList.get(i).isAd()) {
                        index = i;
                        break;
                    }
                }

                if (index > -1) {
                    FeedItem adItem = (FeedItem) currentList.get(index);

                    adItem.setTitle(title);
                    adItem.setContent(text);
                    adItem.setPosterImageUrl(iconImageUrl);
                    adItem.setUrl(url);
                }
            }
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        int layoutResource = (viewType == ADMOB_VIEW_TYPE) ? R.layout.content_native_express_ad : R.layout.item_home_feed;
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(layoutResource, viewGroup, false);

        if (viewType == ADMOB_VIEW_TYPE) {
            final NativeExpressAdView adView = (NativeExpressAdView) view.findViewById(R.id.native_express_ad_view);
            if (adView != null) {
                adView.setAdListener(new AdListener() {
                    private void refreshAdItem(String state) {
                        FeedItem adMobItem = new FeedItem();
                        adMobItem.setExternalRefId(REF_ID_ADMOB);

                        int index = currentList.indexOf(adMobItem);
                        if (index > -1) {
                            synchronized (lock) {
                                ((FeedItem) currentList.get(index)).setContent(state);
                            }
                            notifyItemChanged(index);
                        }
                    }

                    @Override
                    public void onAdClosed() {
                        super.onAdClosed();
                    }

                    @Override
                    public void onAdFailedToLoad(int errorCode) {
                        super.onAdFailedToLoad(errorCode);
                        if (adMobInitialRequestSent && adMobAdLoaded) {
                            return;
                        }

                        refreshAdItem("gone");
                        adView.pause();

                        Handler handler = new android.os.Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                refreshAdItem("visible");
                                adView.resume();
                                adView.loadAd(adRequest);
                            }
                        }, 30000);
                    }

                    @Override
                    public void onAdLeftApplication() {
                        super.onAdLeftApplication();
                    }

                    @Override
                    public void onAdOpened() {
                        super.onAdOpened();
                    }

                    @Override
                    public void onAdLoaded() {
                        super.onAdLoaded();
                        adMobAdLoaded = true;
                    }
                });
            }
        }

        ViewHolder viewHolder = new ViewHolder(view, viewType);
        return viewHolder;
    }

    public void restoreItem(int position, BaseItem item) {
        synchronized (lock) {
            currentList.add(position, item);
            notifyItemInserted(position);

            //if (feedRecyclerView != null) {
                //feedRecyclerView.smoothScrollToPosition(position);
            //}

            originalList.add(item);
        }
    }

    public BaseItem removeItem(int position) {
        BaseItem item = null;
        synchronized (lock) {
            item = currentList.remove(position);
            if (item != null) {
                notifyItemRemoved(position);
                originalList.remove(item);
            }
        }

        return item;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int position) {
        BaseItem item = currentList.get(position);
        final int itemType = item.getType();
        viewHolder.setType(itemType);

        RelativeLayout.LayoutParams miLayoutParams = null;

        if (getItemViewType(position) != ADMOB_VIEW_TYPE) {
            viewHolder.titleView.setSingleLine(true);
            viewHolder.titleView.setTypeface(HomeActivity.getBoldTypeface(context));
            viewHolder.replyInputView.setTypeface(HomeActivity.getNormalTypeface(context));
            viewHolder.replyContainerView.setVisibility(View.GONE);

            TextView[] normalTextViews = {
                    viewHolder.subjectView, viewHolder.annotationView, viewHolder.subtitleView,
                    viewHolder.summaryView, viewHolder.sourceView,
                    viewHolder.displayUrlView, viewHolder.largeNameView, viewHolder.dateTimeView,
                    viewHolder.replyCharacterCountView
            };
            for (int i = 0; i < normalTextViews.length; i++) {
                normalTextViews[i].setTypeface(HomeActivity.getNormalTypeface(context));
            }

            viewHolder.itemActionsContainer.setVisibility(View.GONE);
            viewHolder.largeIconView.setVisibility(View.GONE);
            viewHolder.mediumIconView.setVisibility(View.INVISIBLE);
            viewHolder.actionIconView.setVisibility(View.GONE);
            viewHolder.largeImageView.setVisibility(View.GONE);

            viewHolder.actionIcon1.setVisibility(View.GONE);
            viewHolder.actionIcon2.setVisibility(View.GONE);
            viewHolder.actionIcon3.setVisibility(View.GONE);
            viewHolder.actionIcon4.setVisibility(View.GONE);
            viewHolder.actionIcon5.setVisibility(View.GONE);

            viewHolder.annotationView.setVisibility(View.GONE);
            viewHolder.subjectView.setVisibility(View.GONE);
            viewHolder.titleView.setVisibility(View.GONE);
            viewHolder.subtitleView.setVisibility(View.GONE);
            viewHolder.summaryView.setVisibility(View.GONE);
            viewHolder.sourceView.setVisibility(View.GONE);
            viewHolder.displayUrlView.setVisibility(View.GONE);

            viewHolder.largeNameView.setVisibility(View.GONE);
            viewHolder.dateTimeView.setVisibility(View.GONE);

            //if (!viewHolder.isActionHandlersSet()) {
                viewHolder.actionIcon1.setOnClickListener(new ItemActionHandler(context, 1, item, viewHolder));
                viewHolder.actionIcon2.setOnClickListener(new ItemActionHandler(context, 2, item, viewHolder));
                viewHolder.actionIcon3.setOnClickListener(new ItemActionHandler(context, 3, item, viewHolder));
            /*viewHolder.actionIcon4.setOnClickListener(new ItemActionHandler(getContext(), 4, item));
            viewHolder.actionIcon5.setOnClickListener(new ItemActionHandler(getContext(), 3, item));*/
                //viewHolder.setActionHandlersSet(true);
            //}

            miLayoutParams = (RelativeLayout.LayoutParams) viewHolder.mediumIconView.getLayoutParams();

            int side = Helper.getValueForDip(8, context);
            int top = Helper.getValueForDip(12, context);
            viewHolder.containerView.setPadding(side, top, side, top);
            viewHolder.containerView.setBackgroundColor(Color.TRANSPARENT);

            viewHolder.mediumIconView.setImageDrawable(null);
            viewHolder.largeImageView.setImageDrawable(null);
            viewHolder.largeIconView.setImageDrawable(null);

            miLayoutParams.topMargin = Helper.getValueForDip(4, context);

            //if (!viewHolder.isReplyLogicSetup()) {
            long watcherItemId = item.getId();
            if (itemTextWatchers.containsKey(watcherItemId)) {
                viewHolder.replyInputView.removeTextChangedListener(itemTextWatchers.remove(watcherItemId));
            }

            ReplyInputTextWatcher textWatcher = new ReplyInputTextWatcher(itemType, viewHolder);
            itemTextWatchers.put(watcherItemId, textWatcher);
            viewHolder.replyInputView.addTextChangedListener(textWatcher);
            //}

            //if (!viewHolder.isSendHandlersSet()
            //        && (itemType == BaseItem.ITEM_TYPE_SMS_MESSAGE || itemType == BaseItem.ITEM_TYPE_TWITTER_POST)) {
                viewHolder.replyButton.setOnClickListener(new SendActionHandler(context, item, viewHolder));
                //viewHolder.setSendHandlersSet(true);
            //}
        }

        switch (itemType) {
            case BaseItem.ITEM_TYPE_AD:
                if (getItemViewType(position) != ADMOB_VIEW_TYPE) {
                    if (nativeAd != null && !nativeAdConfigured) {
                        //nativeAd.setTitleView(viewHolder.largeNameView);
                        //nativeAd.setTextView(viewHolder.annotationView);
                        //nativeAd.setIconImageView(viewHolder.mediumIconView);
                        //nativeAd.setMainLayout((RelativeLayout) viewHolder.containerView);
                        nativeAdConfigured = true;
                    }

                    boolean titleIsEmpty = Helper.isEmpty(item.getTitle());
                    viewHolder.mediumIconView.setVisibility(titleIsEmpty ? View.GONE : View.VISIBLE);
                    viewHolder.titleView.setVisibility(titleIsEmpty ? View.GONE : View.VISIBLE);
                    viewHolder.summaryView.setVisibility(titleIsEmpty ? View.GONE : View.VISIBLE);

                    viewHolder.titleView.setText(item.getTitle());
                    viewHolder.summaryView.setText(((FeedItem) item).getContent());

                    String imageUrl = ((FeedItem) item).getPosterImageUrl();
                    if (!Helper.isEmpty(imageUrl) && (!HomeActivity.IMAGE_LOADING_TASKS.contains(imageUrl))) {
                        new HomeActivity.LoadImageUrlTask(context, viewHolder.mediumIconView, imageUrl).executeOnExecutor(
                                AsyncTask.THREAD_POOL_EXECUTOR, "admedia", ((FeedItem) item).getExternalRefId(), imageUrl);
                    }

                    boolean isAdLoaded = (nativeAdControlInterface != null && nativeAdControlInterface.isAdLoaded());
                    if (!isAdLoaded || titleIsEmpty) {
                        viewHolder.containerView.setPadding(0, 0, 0, 0);
                        if (nativeAdControlInterface != null) {
                            nativeAdControlInterface.loadNativeAd(position, viewHolder.mediumIconView, viewHolder.titleView, viewHolder.summaryView, viewHolder.containerView);
                        }
                    }
                } else {
                    if (adRequest == null) {
                        adRequest = new AdRequest.Builder().addTestDevice("06BE10C908578CA6FA211C13DE2147B8").build();
                    }

                    String content = ((FeedItem) item).getContent();
                    NativeExpressAdView adView = (NativeExpressAdView) viewHolder.containerView.findViewById(R.id.native_express_ad_view);
                    View adHolderView = viewHolder.containerView.findViewById(R.id.ad_container_view);
                    adHolderView.setVisibility("gone".equals(content) ? View.GONE : View.VISIBLE);
                    if (adView != null && !adMobInitialRequestSent) {
                        adView.loadAd(adRequest);
                        adMobInitialRequestSent = true;
                    }
                }
                break;

            case BaseItem.ITEM_TYPE_APP:
                String packageName = item.getReferenceId();
                viewHolder.titleView.setVisibility(View.VISIBLE);
                viewHolder.titleView.setText(item.getTitle());

                PackageManager pkgManager = context.getPackageManager();
                try {
                    Drawable d = pkgManager.getApplicationIcon(packageName);
                    viewHolder.mediumIconView.setImageDrawable(d);
                    viewHolder.mediumIconView.setVisibility(View.VISIBLE);
                } catch (PackageManager.NameNotFoundException ex) {
                    // continue
                }

                break;

            case BaseItem.ITEM_TYPE_CONTACT:
                if (item instanceof IndexedItem) {
                    IndexedItem indexedItem = (IndexedItem) item;
                    viewHolder.titleView.setVisibility(View.VISIBLE);
                    viewHolder.actionIconView.setVisibility(View.GONE);

                    viewHolder.mediumIconView.setImageResource(R.drawable.ic_person_outline_white_36dp);
                    viewHolder.mediumIconView.setVisibility(View.VISIBLE);

                    String contactId = indexedItem.getRefId();
                    viewHolder.titleView.setText(indexedItem.getMetaContent());

                    // TODO: Load contact avatar?
                }

                break;

            case BaseItem.ITEM_TYPE_CONTACT_NUMBER:
            case BaseItem.ITEM_TYPE_CONTACT_EMAIL:
                if (item instanceof IndexedItem) {
                    IndexedItem indexedItem = (IndexedItem) item;
                    String title = indexedItem.getTitle();

                    viewHolder.itemActionsContainer.setVisibility(View.VISIBLE);
                    viewHolder.titleView.setVisibility(Helper.isEmpty(title) ? View.GONE : View.VISIBLE);
                    viewHolder.largeNameView.setVisibility(View.VISIBLE);

                    boolean canBeAdded = Helper.isEmpty(item.getTitle());
                    viewHolder.actionIcon1.setImageResource(itemType == BaseItem.ITEM_TYPE_CONTACT_NUMBER ? R.drawable.ic_phone_white_24dp : R.drawable.ic_call_made_white_48dp);
                    viewHolder.actionIcon1.setVisibility(View.VISIBLE);

                    if (canBeAdded) {
                        if (itemType == BaseItem.ITEM_TYPE_CONTACT_EMAIL || Helper.isUssd(indexedItem.getValue())) {
                            viewHolder.actionIcon2.setImageResource(R.drawable.ic_person_add_white_24dp);
                            viewHolder.actionIcon2.setVisibility(View.VISIBLE);
                        } else {
                            viewHolder.actionIcon3.setImageResource(R.drawable.ic_person_add_white_24dp);
                            viewHolder.actionIcon3.setVisibility(View.VISIBLE);
                        }
                    }

                    if (itemType == BaseItem.ITEM_TYPE_CONTACT_NUMBER && !Helper.isUssd(indexedItem.getValue())) {
                        // Phone Number (Action 2 will be SMS)
                        viewHolder.actionIcon2.setImageResource(R.drawable.ic_message_white_24dp);
                        viewHolder.actionIcon2.setVisibility(View.VISIBLE);
                    }

                    String contactId = indexedItem.getRefId();
                    if (!Helper.isEmpty(title)) {
                        viewHolder.titleView.setText(title);
                    }
                    //indexedItem.getMetaFieldName()
                    viewHolder.largeNameView.setText(indexedItem.getMetaContent());
                    // TODO: Load contact avatar?
                }
                break;

            case BaseItem.ITEM_TYPE_FILE:
                viewHolder.largeImageView.setImageBitmap(null);

                String filename = item.getTitle();
                viewHolder.titleView.setVisibility(View.VISIBLE);
                viewHolder.titleView.setText(filename);

                (new AsyncTask<Object, Void, Integer>() {
                    private ImageView imageView;

                    private ImageView lgImageView;

                    private String itemFilename;

                    private String itemUrl;

                    private boolean isImage;

                    @Override
                    protected Integer doInBackground(Object... params) {
                        int drawable = DEFAULT_FILE_EXT_DRAWABLE;

                        itemFilename = (String) params[0];
                        itemUrl = (String) params[1];
                        imageView = (ImageView) params[2];
                        lgImageView = (ImageView) params[3];

                        if (itemFilename.endsWith(".pdf")) {
                            drawable = R.drawable.pdf19;
                        } else if (itemFilename.endsWith(".docx")) {
                            drawable = R.drawable.docx;
                        } else if (Helper.isImage(itemFilename)) {
                            drawable = R.drawable.mountain35;
                            isImage = true;
                        }

                        return drawable;
                    }

                    @Override
                    protected void onPostExecute(Integer result) {
                        if (result != null && imageView != null) {
                            imageView.setImageResource(result);
                            imageView.getDrawable().setColorFilter(new PorterDuffColorFilter(0xffdddddd, PorterDuff.Mode.SRC_ATOP));
                            imageView.setVisibility(View.VISIBLE);
                        }
                        if (isImage && lgImageView != null) {
                            new LoadImageFileTask(lgImageView).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, itemUrl);
                        }
                    }
                }).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, filename, item.getUrl(), viewHolder.mediumIconView, viewHolder.largeImageView);


                if (item instanceof IndexedItem) {
                    IndexedItem indexedItem = (IndexedItem) item;
                    String content = item.getValue();
                    String mimeType = indexedItem.getMimeType();

                    if (!Helper.isEmpty(content)) {
                        viewHolder.summaryView.setText(content);
                        viewHolder.summaryView.setVisibility(View.VISIBLE);
                    }
                }
                if (item.getTimestamp() != null) { // TODO: Change to ModifiedOn
                    viewHolder.dateTimeView.setText(Helper.getDateTimeString(item.getTimestamp(), is24HourFormat));
                    viewHolder.dateTimeView.setVisibility(View.VISIBLE);
                }

                break;

            case BaseItem.ITEM_TYPE_CALL_LOG:
                FeedItem callLogItem = null;
                if (item instanceof IndexedItem) {
                    callLogItem = ((IndexedItem) item).getFeedItem();
                } else if (item instanceof FeedItem) {
                    callLogItem = (FeedItem) item;
                }

                if (callLogItem != null) {
                    String number = callLogItem.getContent();
                    String plainNumber = number;
                    if (number != null && number.indexOf(' ') > -1) {
                        plainNumber = number.replaceAll(" ", "");
                    }
                    viewHolder.largeNameView.setText(number);
                    viewHolder.largeNameView.setVisibility(View.VISIBLE);
                    viewHolder.titleView.setText("");
                    ContactsCache.updateViewWithContactName(plainNumber, HomeActivity.COUNTRY_ISO_CODE, viewHolder.titleView, context, false);

                    viewHolder.itemActionsContainer.setVisibility(View.VISIBLE);

                    if (callLogItem.isVoicemail()) {
                        viewHolder.mediumIconView.setImageResource(R.drawable.ic_voicemail_white_24dp);
                    } else if (callLogItem.isMissed()) {
                        viewHolder.mediumIconView.setImageResource(R.drawable.ic_phone_missed_white_48dp);
                    } else {
                        viewHolder.mediumIconView.setImageResource(
                                (callLogItem.getDirection() == BaseItem.DIRECTION_INCOMING) ?
                                        R.drawable.ic_call_received_white_48dp : R.drawable.ic_call_made_white_48dp);
                    }
                    viewHolder.mediumIconView.setVisibility(View.VISIBLE);

                    long duration = callLogItem.getDuration();
                    viewHolder.sourceView.setText(duration > 0 ? Helper.getReadableTime(callLogItem.getDuration(), true) : "");
                    viewHolder.sourceView.setVisibility(duration > 0 ? View.VISIBLE : View.GONE);

                    boolean canBeAdded = Helper.isEmpty(viewHolder.titleView.getText().toString());
                    viewHolder.actionIcon1.setImageResource(R.drawable.ic_phone_white_24dp);
                    viewHolder.actionIcon1.setVisibility(View.VISIBLE);

                    if (canBeAdded) {
                        if (Helper.isUssd(number)) {
                            viewHolder.actionIcon2.setImageResource(R.drawable.ic_person_add_white_24dp);
                            viewHolder.actionIcon2.setVisibility(View.VISIBLE);
                        } else {
                            viewHolder.actionIcon3.setImageResource(R.drawable.ic_person_add_white_24dp);
                            viewHolder.actionIcon3.setVisibility(View.VISIBLE);
                        }
                    }

                    if (!Helper.isUssd(number)) {
                        // Phone Number (Action 2 will be SMS)
                        viewHolder.actionIcon2.setImageResource(R.drawable.ic_message_white_24dp);
                        viewHolder.actionIcon2.setVisibility(View.VISIBLE);
                    }
                }

                if (item.getTimestamp() != null) {
                    viewHolder.dateTimeView.setText(Helper.getDateTimeString(item.getTimestamp(), is24HourFormat));
                    viewHolder.dateTimeView.setVisibility(View.VISIBLE);
                }

                break;


            case BaseItem.ITEM_TYPE_TWITTER_POST:
                viewHolder.titleView.setVisibility(View.VISIBLE);
                viewHolder.titleView.setText(item.getAuthor());

                FeedItem twitterItem = null;
                if (item instanceof IndexedItem) {
                    twitterItem = ((IndexedItem) item).getFeedItem();
                } else if (item instanceof FeedItem) {
                    twitterItem = (FeedItem) item;
                }

                if (twitterItem != null) {
                    viewHolder.annotationView.setText(twitterItem.isRetweet() ? String.format("%s retweeted", twitterItem.getRetweetedBy()) : null);
                    viewHolder.annotationView.setVisibility(twitterItem.isRetweet() ? View.VISIBLE : View.GONE);
                    miLayoutParams.topMargin = twitterItem.isRetweet() ? Helper.getValueForDip(16, context) : Helper.getValueForDip(4, context);

                    viewHolder.subtitleView.setText(String.format(twitterItem.getPosterId()));
                    viewHolder.subtitleView.setVisibility(View.VISIBLE);

                    viewHolder.summaryView.setText(Html.fromHtml(Helper.htmlise(twitterItem.getContent())));
                    viewHolder.summaryView.setVisibility(View.VISIBLE);

                    String portraitKey = String.format("twitter_%s", twitterItem.getPosterId());
                    Bitmap cachedBitmap = BitmapMemoryCache.getBitmap(portraitKey);
                    if (cachedBitmap != null) {
                        viewHolder.mediumIconView.setVisibility(View.VISIBLE);
                        viewHolder.mediumIconView.setImageDrawable(HomeActivity.getRoundedDrawableForBitmap(cachedBitmap, context.getResources()));
                    } else {
                        if (!Helper.isEmpty(twitterItem.getPosterImageUrl())) {
                            if (!HomeActivity.IMAGE_LOADING_TASKS.contains(portraitKey)) {
                                new HomeActivity.LoadImageAvatarTask(
                                        context, viewHolder.mediumIconView, portraitKey, context.getResources()).executeOnExecutor(
                                        AsyncTask.THREAD_POOL_EXECUTOR, "twitter", twitterItem.getPosterId(), twitterItem.getPosterImageUrl());
                            } else {
                                if (!HomeActivity.PENDING_IMAGE_VIEWS.containsKey(portraitKey)) {
                                    HomeActivity.PENDING_IMAGE_VIEWS.put(portraitKey, new ArrayList<ImageView>());
                                }
                                HomeActivity.PENDING_IMAGE_VIEWS.get(portraitKey).add(viewHolder.mediumIconView);
                            }
                        }
                    }

                    viewHolder.sourceView.setText(Html.fromHtml(twitterItem.getDisplaySource()));
                    viewHolder.sourceView.setVisibility(View.VISIBLE);
                }
                if (item.getTimestamp() != null) { // TODO: Change to ModifiedOn
                    viewHolder.dateTimeView.setText(Helper.getDateTimeString(item.getTimestamp(), is24HourFormat));
                    viewHolder.dateTimeView.setVisibility(View.VISIBLE);
                }
                // Actions : Reply / Retweet (Retweet count) / Like.Favorite (count) / Share

                viewHolder.itemActionsContainer.setVisibility(View.VISIBLE);
                viewHolder.actionIcon1.setImageResource(R.drawable.ic_reply_white_24dp);
                viewHolder.actionIcon1.setVisibility(View.VISIBLE);

                break;

            case BaseItem.ITEM_TYPE_FACEBOOK_POST:
                viewHolder.titleView.setVisibility(View.VISIBLE);
                viewHolder.titleView.setText(item.getAuthor());

                FeedItem fbItem = null;
                if (item instanceof IndexedItem) {
                    fbItem = ((IndexedItem) item).getFeedItem();
                } else if (item instanceof FeedItem) {
                    fbItem = (FeedItem) item;
                }

                if (fbItem != null) {
                    viewHolder.summaryView.setText(fbItem.getContent());
                    viewHolder.summaryView.setVisibility(View.VISIBLE);

                    String portraitKey = String.format("facebook_%s", fbItem.getPosterId());
                    Bitmap cachedBitmap = BitmapMemoryCache.getBitmap(portraitKey);
                    if (cachedBitmap != null) {
                        viewHolder.mediumIconView.setVisibility(View.VISIBLE);
                        viewHolder.mediumIconView.setImageDrawable(HomeActivity.getRoundedDrawableForBitmap(cachedBitmap, context.getResources()));
                    } else {
                        if (!Helper.isEmpty(fbItem.getPosterImageUrl())) {
                            if (!HomeActivity.IMAGE_LOADING_TASKS.contains(portraitKey)) {
                                new HomeActivity.LoadImageAvatarTask(
                                        context, viewHolder.mediumIconView, portraitKey, context.getResources()).executeOnExecutor(
                                        AsyncTask.THREAD_POOL_EXECUTOR, "facebook", fbItem.getPosterId(), fbItem.getPosterImageUrl());
                            } else {
                                if (!HomeActivity.PENDING_IMAGE_VIEWS.containsKey(portraitKey)) {
                                    HomeActivity.PENDING_IMAGE_VIEWS.put(portraitKey, new ArrayList<ImageView>());
                                }
                                HomeActivity.PENDING_IMAGE_VIEWS.get(portraitKey).add(viewHolder.mediumIconView);
                            }
                        }
                    }
                }
                if (item.getTimestamp() != null) {
                    viewHolder.dateTimeView.setText(Helper.getDateTimeString(item.getTimestamp(), is24HourFormat));
                    viewHolder.dateTimeView.setVisibility(View.VISIBLE);
                }
                // Actions : Comment / Like.Favorite (count) / Share

                break;

            case BaseItem.ITEM_TYPE_INSTAGRAM_POST:
                viewHolder.titleView.setText(item.getAuthor());
                viewHolder.titleView.setVisibility(View.VISIBLE);

                FeedItem igItem = null;
                if (item instanceof IndexedItem) {
                    igItem = ((IndexedItem) item).getFeedItem();
                } else if (item instanceof FeedItem) {
                    igItem = (FeedItem) item;
                }

                if (igItem != null) {
                    viewHolder.summaryView.setText(igItem.getContent());
                    viewHolder.summaryView.setVisibility(View.VISIBLE);

                    String portraitKey = String.format("instagram_%s", igItem.getPosterId());
                    Bitmap cachedBitmap = BitmapMemoryCache.getBitmap(portraitKey);
                    if (cachedBitmap != null) {
                        viewHolder.mediumIconView.setVisibility(View.VISIBLE);
                        viewHolder.mediumIconView.setImageDrawable(HomeActivity.getRoundedDrawableForBitmap(cachedBitmap, context.getResources()));
                    } else if (!Helper.isEmpty(igItem.getPosterImageUrl())) {
                        if (!HomeActivity.IMAGE_LOADING_TASKS.contains(portraitKey)) {
                            new HomeActivity.LoadImageAvatarTask(
                                    context, viewHolder.mediumIconView, portraitKey, context.getResources()).executeOnExecutor(
                                    AsyncTask.THREAD_POOL_EXECUTOR, "instagram", igItem.getPosterId(), igItem.getPosterImageUrl());
                        } else {
                            if (!HomeActivity.PENDING_IMAGE_VIEWS.containsKey(portraitKey)) {
                                HomeActivity.PENDING_IMAGE_VIEWS.put(portraitKey, new ArrayList<ImageView>());
                            }
                            HomeActivity.PENDING_IMAGE_VIEWS.get(portraitKey).add(viewHolder.mediumIconView);
                        }
                    }

                    if (!Helper.isEmpty(igItem.getImageUrl())) {
                        String igImageKey = String.format("igmedia_%s", igItem.getExternalRefId());
                        Bitmap cachedMedia = BitmapMemoryCache.getBitmap(igImageKey);

                        viewHolder.largeImageView.setImageBitmap(null);
                        viewHolder.largeImageView.setVisibility(View.VISIBLE);

                        if (cachedMedia != null) {
                            viewHolder.largeImageView.setImageBitmap(cachedMedia);
                        } else {
                            if (!HomeActivity.IMAGE_LOADING_TASKS.contains(igImageKey)) {
                                new HomeActivity.LoadImageUrlTask(context, viewHolder.largeImageView, igImageKey).executeOnExecutor(
                                        AsyncTask.THREAD_POOL_EXECUTOR,
                                        "igmedia", igItem.getExternalRefId(), igItem.getImageUrl());
                            }
                        }
                    }
                }
                if (item.getTimestamp() != null) {
                    viewHolder.dateTimeView.setText(Helper.getDateTimeString(item.getTimestamp(), is24HourFormat));
                    viewHolder.dateTimeView.setVisibility(View.VISIBLE);
                }
                // Actions : Comment / Like.Favorite (count) / Share

                break;

            case BaseItem.ITEM_TYPE_EMAIL_MESSAGE:
                viewHolder.titleView.setVisibility(View.VISIBLE);
                viewHolder.titleView.setText(item.getAuthor());

                viewHolder.mediumIconView.setImageResource(R.drawable.ic_email_white_24dp);
                viewHolder.mediumIconView.setVisibility(View.VISIBLE);

                FeedItem msgItem = null;
                if (item instanceof IndexedItem) {
                    msgItem = ((IndexedItem) item).getFeedItem();
                } else if (item instanceof FeedItem) {
                    msgItem = (FeedItem) item;
                }

                if (msgItem != null) {
                    if (Helper.isEmpty(item.getAuthor())) {
                        viewHolder.titleView.setText(msgItem.getPosterId());
                    }

                    viewHolder.subjectView.setText(msgItem.getTitle());
                    viewHolder.subjectView.setVisibility(View.VISIBLE);

                    viewHolder.summaryView.setText(msgItem.getContent());
                    viewHolder.summaryView.setVisibility(View.VISIBLE);
                }

                if (item.getTimestamp() != null) {
                    viewHolder.dateTimeView.setText(Helper.getDateTimeString(item.getTimestamp(), is24HourFormat));
                    viewHolder.dateTimeView.setVisibility(View.VISIBLE);
                }

                break;

            case BaseItem.ITEM_TYPE_RSS_FEED_ITEM:
                viewHolder.titleView.setText(item.getTitle());
                viewHolder.titleView.setSingleLine(false);
                viewHolder.titleView.setVisibility(View.VISIBLE);

                viewHolder.mediumIconView.setImageResource(R.drawable.ic_rss_feed_white_24dp);
                viewHolder.mediumIconView.setVisibility(View.VISIBLE);

                FeedItem rssFeedItem = null;
                if (item instanceof IndexedItem) {
                    rssFeedItem = ((IndexedItem) item).getFeedItem();
                } else if (item instanceof FeedItem) {
                    rssFeedItem = (FeedItem) item;
                }

                if (rssFeedItem != null) {
                    viewHolder.subjectView.setText(rssFeedItem.getAuthor());
                    viewHolder.subjectView.setVisibility(Helper.isEmpty(rssFeedItem.getAuthor()) ? View.GONE : View.VISIBLE);

                    viewHolder.summaryView.setText(rssFeedItem.getContent());
                    viewHolder.summaryView.setVisibility(View.VISIBLE);

                    viewHolder.sourceView.setText(rssFeedItem.getSource());
                    viewHolder.sourceView.setVisibility(View.VISIBLE);
                }

                if (item.getTimestamp() != null) {
                    viewHolder.dateTimeView.setText(Helper.getDateTimeString(item.getTimestamp(), is24HourFormat));
                    viewHolder.dateTimeView.setVisibility(View.VISIBLE);
                }

                break;

            case BaseItem.ITEM_TYPE_SMS_MESSAGE:
                viewHolder.mediumIconView.setImageResource(R.drawable.ic_sms_white_24dp);
                viewHolder.mediumIconView.setVisibility(View.VISIBLE);

                FeedItem smsItem = null;
                if (item instanceof IndexedItem) {
                    smsItem = ((IndexedItem) item).getFeedItem();
                } else if (item instanceof FeedItem) {
                    smsItem = (FeedItem) item;
                }

                viewHolder.itemActionsContainer.setVisibility(View.VISIBLE);
                viewHolder.actionIcon1.setImageResource(R.drawable.ic_reply_white_24dp);
                viewHolder.actionIcon1.setVisibility(View.VISIBLE);

                if (smsItem != null) {
                    String from = smsItem.getPosterId();
                    viewHolder.titleView.setText(from);
                    viewHolder.titleView.setVisibility(View.VISIBLE);

                    if (Helper.isTelephoneNumber(from)) {
                        ContactsCache.updateViewWithContactName(from, HomeActivity.COUNTRY_ISO_CODE, viewHolder.titleView, context, true);
                    }

                    viewHolder.summaryView.setText(smsItem.getContent());
                    viewHolder.summaryView.setVisibility(View.VISIBLE);
                }

                if (item.getTimestamp() != null) {
                    viewHolder.dateTimeView.setText(Helper.getDateTimeString(item.getTimestamp(), is24HourFormat));
                    viewHolder.dateTimeView.setVisibility(View.VISIBLE);
                }

                break;

            case BaseItem.ITEM_TYPE_WEB_SEARCH_RESULT:
                viewHolder.mediumIconView.setVisibility(View.GONE);

                viewHolder.titleView.setText(item.getTitle());
                viewHolder.titleView.setSingleLine(false);
                viewHolder.titleView.setVisibility(View.VISIBLE);

                if (item instanceof IndexedItem) {
                    IndexedItem searchResult = (IndexedItem) item;
                    viewHolder.summaryView.setText(searchResult.getMetaContent());
                    viewHolder.summaryView.setVisibility(View.VISIBLE);

                    viewHolder.displayUrlView.setText(searchResult.getRefId());
                    viewHolder.displayUrlView.setVisibility(View.VISIBLE);
                }

                if (item.getTimestamp() != null) {
                    viewHolder.dateTimeView.setText(Helper.getDateTimeString(item.getTimestamp(), is24HourFormat));
                    viewHolder.dateTimeView.setVisibility(View.VISIBLE);
                }

                break;
        }
    }

    @Override
    public int getItemCount() {
        return currentList != null ? currentList.size() : 0;
    }

    public BaseItem getItem(int position) {
        synchronized (lock) {
            return (currentList != null && currentList.size() > position) ? currentList.get(position) : null;
        }
    }

    public void setSearchAdapterLoader(IndexedItemSearchAdapterLoader searchAdapterLoader) {
        this.searchAdapterLoader = searchAdapterLoader;
    }

    public WebSearchAdapterLoader getWebSearchAdapterLoader() {
        return webSearchAdapterLoader;
    }

    public void setWebSearchAdapterLoader(WebSearchAdapterLoader webSearchAdapterLoader) {
        this.webSearchAdapterLoader = webSearchAdapterLoader;
    }

    public void setFeedEmptyView(TextView feedEmptyView) {
        this.feedEmptyView = feedEmptyView;
    }

    public void setAdsEnabled(boolean adsEnabled) {
        this.adsEnabled = adsEnabled;
        this.nativeAdConfigured = false;
    }

    public void setNoResultsView(TextView noResultsView) {
        this.noResultsView = noResultsView;
    }

    public void setNativeAd(NativeAd nativeAd) {
        this.nativeAd = nativeAd;
    }

    public void setFeedRecyclerView(RecyclerView feedRecyclerView) {
        this.feedRecyclerView = feedRecyclerView;
    }

    public void setNativeAdControlInterface(NativeAdControlInterface nativeAdControlInterface) {
        this.nativeAdControlInterface = nativeAdControlInterface;
    }

    private void resetScrollPosition() {
        if (feedRecyclerView != null) {
            feedRecyclerView.scrollToPosition(0);
        }
    }

    public boolean is24HourFormat() {
        return is24HourFormat;
    }

    public void setIs24HourFormat(boolean is24HourFormat) {
        this.is24HourFormat = is24HourFormat;
    }

    public void addItemsToOriginalList(List<BaseItem> appItemList) {
        removeAdsFromList(originalList);

        for (int i = 0; i < appItemList.size(); i++) {
            BaseItem item = appItemList.get(i);
            if (!originalList.contains(item)) {
                originalList.add(item);
            }
        }
        Collections.sort(originalList, Collections.reverseOrder());
        if (originalList.size() > 0) {
            placeAdsInList(originalList);
        }

        // Trim originalList to 500 items
        if (originalList.size() > MAX_ITEMS) {
            synchronized (lock) {
                for (int i = MAX_ITEMS; i < originalList.size(); i++) {
                    originalList.remove(i);
                }
            }
        }

        if (currentConstraint == null) {
            synchronized (lock) {
                currentList = new ArrayList<BaseItem>(originalList);
            }
        }
    }

    private boolean itemMatchesFilterString(BaseItem item, String filterString) {
        String lcFilterString = filterString.toLowerCase();
        if (getLcString(item.getValue()).equals(lcFilterString)) {
            return true;
        }

        return (getLcString(item.getTitle()).contains(lcFilterString)
                || getLcString(item.getAuthor()).contains(lcFilterString)
                || getLcString(item.getRecipients()).contains(lcFilterString)
                || getLcString(item.getUrl()).contains(lcFilterString)
                || getLcString(item.getReplyTo()).contains(lcFilterString));
    }

    public void filter(final String constraint) {
        if (filteringInProgress) {
            return;
        }

        (new AsyncTask<Void, Void, List<BaseItem>>() {
            protected void onPreExecute() {
                filteringInProgress = true;
            }

            public List<BaseItem> doInBackground(Void... params) {
                List<BaseItem> filteredList = new ArrayList<BaseItem>();

                if (Helper.isEmpty(constraint)) {
                    int idxToRemove = -1;
                    for (int i = 0; i < originalList.size(); i++) {
                        if (originalList.get(i).isPlaceholder()) {
                            idxToRemove = i;
                            break;
                        }
                    }
                    if (idxToRemove > -1) {
                        originalList.remove(idxToRemove);
                    }

                    filteredList = new ArrayList<BaseItem>(originalList);
                } else {
                    SharedPreferences preferences = context.getSharedPreferences(HomeActivity.COMMON_SHARED_PREF_NAME, Context.MODE_PRIVATE);
                    String includedResultsString = preferences.getString(IndexingAndSearchFragment.ISR_KEY, IndexingAndSearchFragment.DEFAULT_INCLUDE_SEARCH_RESULTS);
                    List<String> includedResults = Arrays.asList(includedResultsString.split(","));

                    String filterStr = constraint.toString().trim();
                    boolean exactMatchFound = false;
                    for (int i = 0; i < originalList.size(); i++) {
                        BaseItem thisItem = originalList.get(i);
                        if (thisItem.isAd()) {
                            filteredList.add(thisItem);
                        } else if (itemMatchesFilterString(thisItem, filterStr) && Helper.isInIncludedResults(thisItem, includedResults)) {
                            filteredList.add(thisItem);
                        }
                        if (itemExactMatchesFilterString(thisItem, filterStr) && Helper.isInIncludedResults(thisItem, includedResults)) {
                            exactMatchFound = true;
                        }
                    }

                    if (!exactMatchFound) {
                        // Determine the type of input
                        IndexedItem placeholderItem = new IndexedItem();
                        placeholderItem.setMetaContent(filterStr);
                        placeholderItem.setPlaceholder(true);
                        if (Helper.isTelephoneNumber(filterStr) || Helper.isUssd(filterStr)) {
                            placeholderItem.setType(BaseItem.ITEM_TYPE_CONTACT_NUMBER);
                            placeholderItem.setMetaFieldName("number");
                            placeholderItem.setUrl(String.format("tel:%s", filterStr));
                            placeholderItem.setCreatedOn(new Date(0));
                            filteredList.add(placeholderItem);
                        }
                    }
                }

                Collections.sort(filteredList, Collections.reverseOrder());
                removeAdsFromList(filteredList);
                return filteredList;
            }

            @Override
            protected void onPostExecute(List<BaseItem> results) {
                currentConstraint = getConstraintFromSequence(constraint);
                synchronized (lock) {
                    currentList = results;
                    placeAdsInList(currentList);
                }
                resetScrollPosition();

                if (searchAdapterLoader != null && constraint != null && !Helper.isEmpty(constraint.toString())) {
                    searchAdapterLoader.searchForIndexedItems(currentConstraint, FeedItemListAdapter.this);
                } else {
                    updateFeedEmptyView(currentConstraint);
                    updateNoResultsView(currentConstraint);
                }

                filteringInProgress = false;
                notifyDataSetChanged();
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void intersperseWebSearchResults(List<IndexedItem> results) {
        synchronized (lock) {
            for (int i = 0, j = 4; i < results.size(); i++, j += 5) {
                IndexedItem item = results.get(i);
                if (currentList != null) {
                    if (j < currentList.size()) {
                        currentList.add(j, item);
                    } else {
                        currentList.add(item);
                    }
                }
            }
        }

        resetScrollPosition();
        updateNoResultsView(currentConstraint);
        updateFeedEmptyView(currentConstraint);
    }

    public void setDbContext(MagnesiumDbContext dbContext) {
        this.dbContext = dbContext;
    }

    private void purgeExternalResultsFromCurrentList() {
        if (currentExternalResults != null && currentList != null) {
            List<Integer> idxsToRemove = new ArrayList<Integer>();
            synchronized (lock) {
                for (int i = 0; i < currentExternalResults.size(); i++) {
                    IndexedItem ext = currentExternalResults.get(i);
                    if (currentList.contains(ext)) {
                        idxsToRemove.add(currentList.indexOf(ext));
                    }
                }

                for (int i = 0; i < idxsToRemove.size(); i++) {
                    currentList.remove(idxsToRemove.get(i));
                }
            }
        }
    }

    public void addIndexedSearchResults(List<IndexedItem> results, boolean webSearchInProgress) {
        purgeExternalResultsFromCurrentList();

        currentExternalResults = new ArrayList<IndexedItem>(results);
        synchronized (lock) {
            removeAdsFromList(currentList);

            for (int i = 0; i < currentExternalResults.size(); i++) {
                BaseItem result = currentExternalResults.get(i);
                if (!currentList.contains(result)) {
                    currentList.add(result); // TODO: only add all to current list if current filter is set
                }
            }

            Collections.sort(currentList, Collections.reverseOrder());
            placeAdsInList(currentList);

            if (Helper.isTelephoneNumber(currentConstraint) || Helper.isUssd(currentConstraint)) {
                int placeholderIdx = getPlaceholderIndexFromList(currentList);
                if (placeholderIdx > -1) {
                    boolean exactMatchFound = false;
                    for (int i = 0; i < currentList.size(); i++) {
                        BaseItem item = currentList.get(i);
                        if (item.isPlaceholder() || item.getType() != BaseItem.ITEM_TYPE_CONTACT_NUMBER) {
                            continue;
                        }
                        exactMatchFound = itemExactMatchesFilterString(item, currentConstraint);
                    }
                    if (exactMatchFound) {
                        currentList.remove(placeholderIdx);
                    }
                }
            }
        }

        resetScrollPosition();

        if ((currentConstraint != null && currentConstraint.trim().length() < 3)
                || webSearchAdapterLoader == null || Helper.isUssd(currentConstraint)) {
            updateNoResultsView(currentConstraint);
            updateFeedEmptyView(currentConstraint);
        }
    }

    private int getPlaceholderIndexFromList(List<BaseItem> items) {
        for (int i = 0; i < items.size(); i++) {
            BaseItem item = items.get(i);
            if (item.isPlaceholder()) {
                return i;
            }
        }
        return -1;
    }

    private boolean itemExactMatchesFilterString(BaseItem item, String filterString) {
        String lcFilterString = filterString.toLowerCase();
        if (getLcString(item.getValue()).equals(lcFilterString)) {
            return true;
        }
        return getLcString(item.getTitle()).equals(filterString.toLowerCase());
    }

    private void updateNoResultsView(String constraint) {
        int currentListSize = currentList.size();
        for (int i = 0; i < currentList.size(); i++) {
            if (currentList.get(i).isAd()) {
                currentListSize--;
            }
        }
        boolean hasResults = (!Helper.isEmpty(constraint) && currentListSize > 0) || constraint == null;
        if (noResultsView != null) {
            noResultsView.setText(String.format("No item matching \"%s\" was found.", constraint));
            noResultsView.setVisibility(hasResults ? View.INVISIBLE : View.VISIBLE);
        }
    }

    private void updateFeedEmptyView(String constraint) {
        boolean feedEmpty =
                feedEmptyView != null && constraint == null && originalList.size() == 0 && currentList.size() == 0;
        if (feedEmptyView != null) {
            feedEmptyView.setVisibility(feedEmpty ? View.VISIBLE : View.INVISIBLE);
        }
    }

    private String getConstraintFromSequence(CharSequence constraint) {
        return (constraint != null) ? constraint.toString() : null;
    }

    private static String getLcString(String value) {
        if (value != null) {
            return value.trim().toLowerCase();
        }
        return "";
    }

    public SmsMessageSender getSmsMessageSender() {
        return smsMessageSender;
    }

    public void setSmsMessageSender(SmsMessageSender smsMessageSender) {
        this.smsMessageSender = smsMessageSender;
    }

    public List<BaseItem> getItems() {
        return currentList;
    }

    public GenericItemClickListener getItemClickListener() {
        return itemClickListener;
    }

    public void setItemClickListener(GenericItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private int type;
        protected View containerView;
        protected View itemActionsContainer;

        private boolean replyLogicSetup;
        private boolean actionHandlersSet;
        private boolean sendHandlersSet;

        protected ImageView actionIcon1;
        protected ImageView actionIcon2;
        protected ImageView actionIcon3;
        protected ImageView actionIcon4;
        protected ImageView actionIcon5;
        protected ImageView largeImageView;
        protected ImageView largeIconView;
        protected ImageView mediumIconView;
        protected ImageView actionIconView;

        protected TextView annotationView;
        protected TextView dateTimeView;
        protected TextView displayUrlView;
        protected TextView largeNameView;
        protected TextView sourceView;
        protected TextView subjectView;
        protected TextView subtitleView;
        protected TextView summaryView;
        protected TextView titleView;

        // Reply Views
        protected ProgressBar sendProgress;
        protected View replyContainerView;
        protected View closeReplyView;
        protected EditText replyInputView;
        protected TextView replyCharacterCountView;
        protected FloatingActionButton replyButton;

        public ViewHolder(final View containerView, int viewType) {
            super(containerView);
            this.containerView = containerView;
            this.type = type;

            if (viewType != ADMOB_VIEW_TYPE) {
                this.itemActionsContainer = containerView.findViewById(R.id.item_actions_container);

                this.actionIcon1 = (ImageView) containerView.findViewById(R.id.item_action_1);
                this.actionIcon2 = (ImageView) containerView.findViewById(R.id.item_action_2);
                this.actionIcon3 = (ImageView) containerView.findViewById(R.id.item_action_3);
                this.actionIcon4 = (ImageView) containerView.findViewById(R.id.item_action_4);
                this.actionIcon5 = (ImageView) containerView.findViewById(R.id.item_action_5);
                this.actionIconView = (ImageView) containerView.findViewById(R.id.item_action_icon);

                this.largeImageView = (ImageView) containerView.findViewById(R.id.item_image_large);
                this.largeIconView = (ImageView) containerView.findViewById(R.id.item_large_icon);
                this.mediumIconView = (ImageView) containerView.findViewById(R.id.item_medium_icon);

                this.dateTimeView = (TextView) containerView.findViewById(R.id.item_date_time);
                this.annotationView = (TextView) containerView.findViewById(R.id.item_annotation);
                this.displayUrlView = (TextView) containerView.findViewById(R.id.item_display_url);
                this.largeNameView = (TextView) containerView.findViewById(R.id.item_name_large);
                this.sourceView = (TextView) containerView.findViewById(R.id.item_source);
                this.subjectView = (TextView) containerView.findViewById(R.id.item_subject);
                this.subtitleView = (TextView) containerView.findViewById(R.id.item_subtitle);
                this.summaryView = (TextView) containerView.findViewById(R.id.item_summary);
                this.titleView = (TextView) containerView.findViewById(R.id.item_title);

                this.sendProgress = (ProgressBar) containerView.findViewById(R.id.item_reply_send_progress);
                this.replyContainerView = containerView.findViewById(R.id.item_reply_container);
                this.closeReplyView = containerView.findViewById(R.id.item_reply_close_container);
                this.replyInputView = (EditText) containerView.findViewById(R.id.item_reply_input);
                this.replyCharacterCountView = (TextView) containerView.findViewById(R.id.item_reply_character_count);
                this.replyButton = (FloatingActionButton) containerView.findViewById(R.id.item_reply_send_button);

                if (itemClickListener != null) {
                    containerView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (itemClickListener != null) {
                                itemClickListener.onItemClick(getAdapterPosition(), view);
                            }
                        }
                    });
                }

                closeReplyView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (sendProgress.getVisibility() == View.VISIBLE) {
                            return;
                        }

                        collapseReplyContainer(ViewHolder.this, false);
                    }
                });
            }
        }

        public void setType(int type) {
            this.type = type;
        }

        public int getType() {
            return type;
        }

        public boolean isReplyLogicSetup() {
            return replyLogicSetup;
        }

        public void setReplyLogicSetup(boolean replyLogicSetup) {
            this.replyLogicSetup = replyLogicSetup;
        }

        public boolean isActionHandlersSet() {
            return actionHandlersSet;
        }

        public void setActionHandlersSet(boolean actionHandlersSet) {
            this.actionHandlersSet = actionHandlersSet;
        }

        public boolean isSendHandlersSet() {
            return sendHandlersSet;
        }

        public void setSendHandlersSet(boolean sendHandlersSet) {
            this.sendHandlersSet = sendHandlersSet;
        }
    }

    private static void updateCharacterCountForText(String text, int itemType, TextView charCountView) {
        int characterCount = text.length();
        String displayText = String.valueOf(characterCount);
        switch (itemType) {
            case BaseItem.ITEM_TYPE_TWITTER_POST:
                displayText = String.format("%d / %d", characterCount, TWITTER_CHARACTER_LIMIT);
                break;
            case BaseItem.ITEM_TYPE_SMS_MESSAGE:
            case BaseItem.ITEM_TYPE_CONTACT_NUMBER:
            case BaseItem.ITEM_TYPE_CONTACT:
            case BaseItem.ITEM_TYPE_CALL_LOG:
                int numTexts = 1;
                if (characterCount > SMS_CHARACTER_LIMIT) {
                    numTexts = Double.valueOf(Math.ceil((double) characterCount / (double) SMS_CHARACTER_LIMIT)).intValue();
                }

                displayText = String.format("%d / %d%s", characterCount, SMS_CHARACTER_LIMIT, (numTexts > 1) ? " (" + numTexts + ")" : "");
                break;
        }

        charCountView.setText(displayText);
    }

    private class ReplyInputTextWatcher implements TextWatcher {
        private int itemType;

        private ViewHolder viewHolder;

        public ReplyInputTextWatcher(int itemType, ViewHolder viewHolder) {
            this.itemType = itemType;
            this.viewHolder = viewHolder;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            updateCharacterCountForText(editable.toString(), itemType, viewHolder.replyCharacterCountView);
        }
    }

    private class SendActionHandler implements View.OnClickListener {
        private Context context;
        private BaseItem item;
        private ViewHolder viewHolder;

        public SendActionHandler(Context context, BaseItem item, ViewHolder viewHolder) {
            this.context = context;
            this.viewHolder = viewHolder;
            this.item = item;
        }

        @Override
        public void onClick(View v) {
            if (item == null) {
                return;
            }
            handleSendAction();
        }

        private void handleSendAction() {
            if (viewHolder.replyInputView.getText().length() == 0) {
                Helper.showToast("Please type the message that you would like to send.", context);
                return;
            }

            if (item.getType() == BaseItem.ITEM_TYPE_TWITTER_POST && !HomeActivity.thisActivity.isOnline()) {
                Helper.showToast("The tweet cannot be posted because you do not have an active Internet connection.", context, Toast.LENGTH_SHORT);
                return;
            }

            if (item.getType() == BaseItem.ITEM_TYPE_TWITTER_POST) {
                final String text = viewHolder.replyInputView.getText().toString();

                // Async task to post to twitter
                (new AsyncTask<Void, Void, Boolean>() {
                    @Override
                    protected void onPreExecute() {
                        viewHolder.closeReplyView.setVisibility(View.GONE);
                        viewHolder.sendProgress.setVisibility(View.VISIBLE);
                    }

                    @Override
                    protected Boolean doInBackground(Void... params) {
                        try {
                            FeedItem twitterItem = (item instanceof FeedItem) ? (FeedItem) item : ((IndexedItem) item).getFeedItem();
                            long ownerId = twitterItem.getOwnerId();
                            SQLiteDatabase db = dbContext.getReadableDatabase();
                            List<OauthData> oauthDataList = MagnesiumDbContext.getOauthDataList("twitter", db);
                            OauthData oauthData = null;
                            for (int i = 0; i < oauthDataList.size(); i++) {
                                oauthData = oauthDataList.get(i);
                                if (oauthData.isValid() && oauthData.getLocalAccountId() == ownerId) {
                                    break;
                                }
                            }

                            if (oauthData != null) {
                                HttpParameters httpParams = new HttpParameters();
                                httpParams.put("status", text);
                                if (!Helper.isEmpty(twitterItem.getExternalRefId())) {
                                    httpParams.put("in_reply_to_status_id", twitterItem.getExternalRefId());
                                }
                                if (HomeFeedFragment.LastKnownLocation != null) {
                                    httpParams.put("lat", String.valueOf(HomeFeedFragment.LastKnownLocation.getLatitude()));
                                    httpParams.put("long", String.valueOf(HomeFeedFragment.LastKnownLocation.getLongitude()));
                                    httpParams.put("display_coordinates", "true");
                                }

                                HttpParameters doubleEncodedParams = new HttpParameters();

                                StringBuilder sb = new StringBuilder();
                                String delim = "";
                                for (String paramName : httpParams.keySet()) {
                                    String paramValue = OAuth.percentEncode(httpParams.getFirst(paramName));
                                    sb.append(delim).append(paramName).append("=").append(paramValue);
                                    doubleEncodedParams.put(paramName, paramValue);
                                    delim = "&";
                                }

                                OAuthConsumer consumer = new DefaultOAuthConsumer(Helper.TWITTER_API_KEY, Helper.TWITTER_API_CONSUMER_SECRET);
                                consumer.setTokenWithSecret(oauthData.getToken(), oauthData.getTokenSecret());
                                doubleEncodedParams.put("realm", Helper.TWITTER_POST_UPDATE_URL);
                                consumer.setAdditionalParameters(doubleEncodedParams);

                                String response = Http.postWithConsumer(Helper.TWITTER_POST_UPDATE_URL, sb.toString(), "application/x-www-form-urlencoded", consumer);
                                if (Helper.isEmpty(response)) {
                                    return false;
                                }

                                JsonNode jsonNode = objectMapper.readTree(response);
                                if (!jsonNode.has("created_at")) {
                                    return false;
                                }

                                return true;
                            }
                        } catch (Exception ex) {
                            return false;
                        }

                        return false;
                    }

                    @Override
                    protected void onPostExecute(Boolean result) {
                        viewHolder.closeReplyView.setVisibility(View.VISIBLE);
                        viewHolder.sendProgress.setVisibility(View.GONE);
                        if (result) {
                            String message = "Tweet posted.";
                            if (HomeActivity.thisActivity != null && HomeActivity.thisActivity.getHomeFeedFragment() != null) {
                                HomeActivity.thisActivity.getHomeFeedFragment().showSimpleSnackbarMessage(message);
                            } else {
                                Helper.showToast(message, context);
                            }
                            collapseReplyContainer(viewHolder, true);
                        } else {
                            Helper.showToast("An error occurred while trying to post to Twitter. Please try again later.", context);
                        }
                        twitterPostInProgress = false;
                    }
                }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                // Send SMS message
                // Check permissions first
                if (smsMessageSender != null) {
                    if (smsSendInProgress) {
                        return;
                    }

                    FeedItem feedItem = (item instanceof FeedItem) ? ((FeedItem) item) : ((IndexedItem) item).getFeedItem();
                    final String to = (item.getType() == BaseItem.ITEM_TYPE_SMS_MESSAGE) ? feedItem.getPosterId() : feedItem.getValue();
                    final String message = viewHolder.replyInputView.getText().toString();

                    viewHolder.closeReplyView.setVisibility(View.GONE);
                    viewHolder.sendProgress.setVisibility(View.VISIBLE);

                    smsSendInProgress = true;
                    smsMessageSender.sendMessage(to, message, new SmsMessageSenderCallback() {
                        @Override
                        public void onSendComplete(boolean succeeded) {
                            viewHolder.closeReplyView.setVisibility(View.VISIBLE);
                            viewHolder.sendProgress.setVisibility(View.GONE);
                            smsSendInProgress = false;

                            if (succeeded) {
                                collapseReplyContainer(viewHolder, true);
                            } else {
                                // Launch the intent for sending SMS instead
                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", to, null));
                                intent.putExtra("sms_body", message);
                                if (context != null) {
                                    try {
                                        context.startActivity(intent);
                                    } catch (ActivityNotFoundException ex) {
                                        Log.e(TAG, "SMS intent launch failed.", ex);
                                    }
                                }
                            }
                        }
                    });
                }
            }
        }
    }

    public static Animation createScaleAnimationForReplyContainer(final float startScale, final float endScale) {
        Animation animation = new ScaleAnimation(1, 1, startScale, endScale, Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0);
        animation.setDuration(100);
        return animation;
    }

    private void expandReplyContainer(final ViewHolder viewHolder, final int itemType) {
        if (viewHolder.replyContainerView.getVisibility() != View.VISIBLE) {

            Animation animation = createScaleAnimationForReplyContainer(0, 1);
            animation.setFillAfter(true);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    inReplyMode = true;
                    viewHolder.replyContainerView.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    updateCharacterCountForText(viewHolder.replyInputView.getText().toString(), itemType, viewHolder.replyCharacterCountView);

                    viewHolder.replyInputView.requestFocus();
                    viewHolder.replyInputView.setSelection(viewHolder.replyInputView.getText().length());
                    Helper.showSoftKeyboard(viewHolder.replyInputView, context);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            viewHolder.replyContainerView.clearAnimation();
            viewHolder.replyContainerView.startAnimation(animation);
        }
    }

    private void collapseReplyContainer(final ViewHolder viewHolder, final boolean clearInput) {
        if (viewHolder.replyContainerView.getVisibility() == View.VISIBLE) {
            Animation animation = createScaleAnimationForReplyContainer(1, 0);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    inReplyMode = false;
                    viewHolder.replyContainerView.setVisibility(View.GONE);

                    if (clearInput) {
                        viewHolder.replyInputView.setText(null);
                    }

                    Helper.clearEditTextFocus(viewHolder.replyInputView, context);

                    if (pendingNotify) {
                        notifyDataSetChanged();
                        pendingNotify = false;
                    }
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            viewHolder.replyContainerView.clearAnimation();
            viewHolder.replyContainerView.startAnimation(animation);
        }
    }

    public interface SmsMessageSenderCallback {
        void onSendComplete(boolean succeeded);
    }

    private class ItemActionHandler implements View.OnClickListener {
        private Context context;
        private BaseItem item;
        private int actionIndex;
        private ViewHolder viewHolder;

        public ItemActionHandler(Context context, int actionIndex, BaseItem item, ViewHolder viewHolder) {
            this.context = context;
            this.actionIndex = actionIndex;
            this.viewHolder = viewHolder;
            this.item = item;
        }
        @Override
        public void onClick(View v) {
            if (item == null) {
                return;
            }
            handleItemAction();
        }

        private void handleItemAction() {
            int itemType = item.getType();
            Intent intent = null;
            switch (actionIndex) {
                case 1:
                    // Primary action
                    switch (itemType) {
                        default:
                            break;
                        case BaseItem.ITEM_TYPE_CONTACT_NUMBER: // Phone Dial action
                        case BaseItem.ITEM_TYPE_CALL_LOG:
                            intent = new Intent(Intent.ACTION_CALL, Uri.parse(item.getUrl()));
                            break;
                        case BaseItem.ITEM_TYPE_CONTACT_EMAIL: // Send Email action
                            intent = new Intent(Intent.ACTION_SEND, Uri.parse(item.getUrl()));
                            intent.setType("message/rfc822");
                            break;
                        case BaseItem.ITEM_TYPE_SMS_MESSAGE: // Reply to SMS message
                        case BaseItem.ITEM_TYPE_TWITTER_POST: // Reply to Twitter post
                            String prevText = viewHolder.replyInputView.getText().toString();
                            String initialText = "";
                            if (itemType == BaseItem.ITEM_TYPE_TWITTER_POST) {
                                FeedItem twitterItem = null;
                                if (item instanceof IndexedItem) {
                                    twitterItem = ((IndexedItem) item).getFeedItem();
                                } else if (item instanceof FeedItem) {
                                    twitterItem = (FeedItem) item;
                                }

                                if (twitterItem != null) {
                                    initialText = String.format("%s ", twitterItem.getPosterId());
                                }
                            }
                            if (prevText.length() == 0 || !prevText.startsWith(initialText)) {
                                viewHolder.replyInputView.setText(initialText);
                            }

                            expandReplyContainer(viewHolder, itemType);
                            break;
                    }
                    break;

                case 2:
                    // Secondary action
                    switch (itemType) {
                        default:
                            break;
                        case BaseItem.ITEM_TYPE_CONTACT_NUMBER:
                        case BaseItem.ITEM_TYPE_CONTACT_EMAIL: // Add Contact action
                        case BaseItem.ITEM_TYPE_CONTACT:
                        case BaseItem.ITEM_TYPE_CALL_LOG:
                            if (itemType == BaseItem.ITEM_TYPE_CONTACT_EMAIL || Helper.isUssd(item.getValue())) {
                                intent = new Intent(Intent.ACTION_INSERT);
                                intent.setType(ContactsContract.Contacts.CONTENT_TYPE);

                                intent.putExtra(itemType == BaseItem.ITEM_TYPE_CONTACT_NUMBER ?
                                        ContactsContract.Intents.Insert.PHONE :
                                        ContactsContract.Intents.Insert.EMAIL, item.getValue());
                            } else {
                                // Send SMS. TODO: Local/inline SMS handling
                                expandReplyContainer(viewHolder, itemType);
                                //intent = new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", item.getValue(), null));
                            }
                            break;
                    }
                    break;

                case 3:
                    // Tertiary action
                    switch (itemType) {
                        default:
                            break;
                        case BaseItem.ITEM_TYPE_CONTACT_NUMBER:
                        case BaseItem.ITEM_TYPE_CALL_LOG:
                            intent = new Intent(Intent.ACTION_INSERT);
                            intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
                            intent.putExtra(ContactsContract.Intents.Insert.PHONE, item.getValue());
                            break;
                    }
                    break;

            }

            if (intent != null && context != null) {
                try {
                    context.startActivity(intent);
                } catch (ActivityNotFoundException ex) {
                    Toast.makeText(context, context.getString(R.string.cannot_handle_action), Toast.LENGTH_SHORT);
                }
            }
        }
    }

    public boolean isPendingNotify() {
        return pendingNotify;
    }

    public void setPendingNotify(boolean pendingNotify) {
        this.pendingNotify = pendingNotify;
    }

    public boolean isDeleteInProgress() {
        return deleteInProgress;
    }

    public void setDeleteInProgress(boolean deleteInProgress) {
        this.deleteInProgress = deleteInProgress;
    }

    public boolean isInReplyMode() {
        return inReplyMode;
    }

    public void setInReplyMode(boolean inReplyMode) {
        this.inReplyMode = inReplyMode;
    }

    @Override
    public int getItemViewType(int position) {
        BaseItem item = null;
        synchronized (lock) {
            item = (currentList.size() > position) ? currentList.get(position) : null;
        }

        if (item != null && item.getType() == BaseItem.ITEM_TYPE_AD) {
            return REF_ID_SMAATO.equals(((FeedItem) item).getExternalRefId()) ? SMAATO_VIEW_TYPE : ADMOB_VIEW_TYPE;
        }

        return position;
    }

    private class LoadImageFileTask extends AsyncTask<String, Void, Bitmap> {
        private ImageView imageView;

        public LoadImageFileTask(ImageView imageView) {
            this.imageView = imageView;
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            if (params.length == 0 || Helper.isEmpty(params[0])) {
                return null;
            }
            File file = new File(Uri.parse(params[0]).getPath());
            if (file.exists()) {
                final BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(file.getAbsolutePath(), options);

                // Calculate inSampleSize
                options.inSampleSize = Helper.calculateInSampleSize(options, 256, 192);

                // Decode bitmap with inSampleSize set
                options.inJustDecodeBounds = false;
                Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), options);
                return bitmap;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if (result != null && imageView != null) {
                imageView.setImageBitmap(result);
            }
            imageView.setVisibility((result != null) ? View.VISIBLE : View.GONE);
        }
    }
}
