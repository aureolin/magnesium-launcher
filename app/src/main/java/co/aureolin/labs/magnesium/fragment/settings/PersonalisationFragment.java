package co.aureolin.labs.magnesium.fragment.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import co.aureolin.labs.magnesium.HomeActivity;
import co.aureolin.labs.magnesium.R;
import co.aureolin.labs.magnesium.SettingsActivity;
import co.aureolin.labs.magnesium.adapter.SettingItemAdapter;
import co.aureolin.labs.magnesium.dialog.IconPackSelectionDialog;
import co.aureolin.labs.magnesium.dialog.SliderValueSelectorDialog;
import co.aureolin.labs.magnesium.model.IconPack;
import co.aureolin.labs.magnesium.model.SettingItem;
import co.aureolin.labs.magnesium.util.Helper;
import co.aureolin.labs.magnesium.util.ValueListener;
import co.aureolin.labs.magnesium.util.handler.IconPackSelectedHandler;

/**
 * Created by akinwale on 26/02/2016.
 */
public class PersonalisationFragment extends Fragment {
    private List<SettingItem> personalisationSettings;

    private SettingItemAdapter adapter;

    private ListView personalisationListView;

    public static final String SETTING_NAME_APP_DOCK_DISPLAY = "App Dock Display";

    public static final String SETTING_NAME_PREFERRED_HOME_SCREEN = "Preferred Home Screen";

    public static final String SETTING_NAME_HIDDEN_APPS = "Hidden Apps";

    public static final String SETTING_NAME_ICON_PACK = "Icon Pack";

    public static final String SETTING_NAME_BACKGROUND_TRANSPARENCY = "Background Transparency";

    public static final String SETTING_NAME_DOCK_TRANSPARENCY = "App Dock Background Transparency";

    public static final String HIDDEN_APPS_KEY = "HiddenApps";

    public static final String[] SETTINGS_KEYS = {
        HomeActivity.SETTING_KEY_APP_DOCK_POSITION,
        HomeActivity.SETTING_KEY_APP_DOCK_TRANSPARENCY,
        HomeActivity.SETTING_KEY_BACKGROUND_TRANSPARENCY,
        HIDDEN_APPS_KEY,
        HomeActivity.SETTING_KEY_ICON_PACK,
        HomeActivity.SETTING_KEY_PREFERRED_HOME_SCREEN,
    };

    public static final String[] SETTINGS_NAMES = {
        SETTING_NAME_APP_DOCK_DISPLAY,
        SETTING_NAME_DOCK_TRANSPARENCY,
        SETTING_NAME_BACKGROUND_TRANSPARENCY,
        SETTING_NAME_HIDDEN_APPS,
        SETTING_NAME_ICON_PACK,
        SETTING_NAME_PREFERRED_HOME_SCREEN,
    };

    public static final String[] SETTINGS_DEFAULT_VALUES = {
        HomeActivity.APP_DOCK_POSITION_APP_LIST,
        "66%",
        "40%",
        "0",
        HomeActivity.DEFAULT_VALUE_ICON_PACK_NONE,
        HomeActivity.VALUE_APP_DRAWER,
    };

    public static final String[] SETTINGS_ANNOTATIONS = {
        null,
        null,
        "Tap to change the transparency of the launcher's background layer. Set this to 100% if you want your wallpaper to be completely visible.",
        null,
        null,
        "Tap to change the screen to be displayed when you press your device's home button",
    };

    public static PersonalisationFragment newInstance() {
        return new PersonalisationFragment();
    }

    public static float percentageStringToFloat(String value) {
        return Float.parseFloat(value.replace("%", "")) / 100;
    }

    public static int percentageStringToInt(String value) {
        return Integer.parseInt(value.replace("%", ""));
    }

    public static String valueToPercentageString(float value) {
        return String.format("%d%%", Float.valueOf(value * 100).intValue());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        personalisationSettings = new ArrayList<SettingItem>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_personalisation, container, false);

        personalisationListView = (ListView) rootView.findViewById(R.id.personalisation_settings_list);
        personalisationListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final SettingItem item = (SettingItem) parent.getItemAtPosition(position);
                final String itemName = item.getName();

                if (SETTING_NAME_HIDDEN_APPS.equals(itemName)) {
                    if (SettingsActivity.CurrentActivity != null) {
                        SettingsActivity.CurrentActivity.setCurrentFragment(itemName, HiddenAppsFragment.newInstance());
                    }

                    return;
                }

                if (SETTING_NAME_BACKGROUND_TRANSPARENCY.equals(itemName) || SETTING_NAME_DOCK_TRANSPARENCY.equals(itemName)) {
                    int currentValue = percentageStringToInt(item.getValue());
                    SliderValueSelectorDialog dialog = SliderValueSelectorDialog.newInstance(
                            itemName, 100, currentValue, "%", new ValueListener() {
                                @Override
                                public void onValueChanged(int value) {
                                    float newValue = Float.valueOf(value) / 100.0f;

                                    if (item != null) {
                                        item.setValue(valueToPercentageString(newValue));
                                    }
                                    if (adapter != null) {
                                        adapter.notifyDataSetChanged();
                                    }

                                    if (HomeActivity.thisActivity != null) {
                                        if (SETTING_NAME_BACKGROUND_TRANSPARENCY.equals(itemName)) {
                                            HomeActivity.thisActivity.setBackgroundTransparencyChanged(true);
                                        } else if (SETTING_NAME_DOCK_TRANSPARENCY.equals(itemName)) {
                                            HomeActivity.thisActivity.setDockBackgroundTransparencyChanged(true);
                                        }
                                    }
                                }

                                @Override
                                public void onCompleted() {
                                    saveSettings();
                                }
                            }
                    );

                    dialog.show(getFragmentManager(), "SliderValueSelectorDialog");

                    return;
                }

                if (SETTING_NAME_APP_DOCK_DISPLAY.equals(itemName)) {
                    String itemValue = item.getValue();
                    String newValue = null;
                    if (HomeActivity.APP_DOCK_POSITION_NONE.equals(itemValue)) {
                        newValue = HomeActivity.APP_DOCK_POSITION_APP_LIST;
                    } else if (HomeActivity.APP_DOCK_POSITION_GLOBAL.equals(itemValue)) {
                        newValue = HomeActivity.APP_DOCK_POSITION_NONE;
                    } else {
                        newValue = HomeActivity.APP_DOCK_POSITION_GLOBAL;
                    }
                    item.setValue(newValue);
                } else if (SETTING_NAME_PREFERRED_HOME_SCREEN.equals(itemName)) {
                    String itemValue = item.getValue();
                    item.setValue(HomeActivity.VALUE_APP_DRAWER.equals(itemValue) ? HomeActivity.VALUE_FEED : HomeActivity.VALUE_APP_DRAWER);
                } else if (SETTING_NAME_ICON_PACK.equals(itemName)) {
                    // Show icon pack selection dialog
                    IconPackSelectionDialog dialog = new IconPackSelectionDialog();
                    dialog.setIconPackSelectedHandler(new IconPackSelectedHandler() {
                        @Override
                        public void onIconPackSelected(IconPack iconPack) {
                            item.setValue(iconPack.getSettingValue());
                            if (adapter != null) {
                                adapter.notifyDataSetChanged();
                            }
                            saveSettings();
                        }
                    });
                    dialog.show(getFragmentManager(), "IconPackSelectionDialog");
                }

                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
                saveSettings();
            }
        });

        return rootView;
    }

    public void onStart() {
        super.onStart();
        personalisationSettings.clear();
        initSettings();
    }

    public void onPause() {
        saveSettings();
        super.onPause();
    }

    private void initSettings() {
        Context context = getContext();
        if (context != null) {
            SharedPreferences preferences = context.getSharedPreferences(HomeActivity.COMMON_SHARED_PREF_NAME, Context.MODE_PRIVATE);
            for (int i = 0; i < SETTINGS_NAMES.length; i++) {
                SettingItem item = new SettingItem();
                item.setName(SETTINGS_NAMES[i]);
                item.setValue(SETTINGS_DEFAULT_VALUES[i]);
                item.setAnnotation(SETTINGS_ANNOTATIONS[i]);
                item.setOwned(true);

                String itemName = item.getName();
                if (SETTING_NAME_PREFERRED_HOME_SCREEN.equals(itemName) || SETTING_NAME_BACKGROUND_TRANSPARENCY.equals(itemName)) {
                    item.setShowAnnotation(true);
                }

                String settingKey = SETTINGS_KEYS[i];
                if (HIDDEN_APPS_KEY.equals(settingKey)) {
                    item.setValue( String.valueOf(preferences.getStringSet(settingKey, new HashSet<String>()).size()) );
                } else if (HomeActivity.SETTING_KEY_BACKGROUND_TRANSPARENCY.equals(settingKey)) {
                    float value = preferences.getFloat(settingKey, HomeActivity.DEFAULT_BACKGROUND_TRANSPARENCY);
                    item.setValue(valueToPercentageString(value));
                } else if (HomeActivity.SETTING_KEY_APP_DOCK_TRANSPARENCY.equals(settingKey)) {
                    float value = preferences.getFloat(settingKey, HomeActivity.DEFAULT_DOCK_TRANSPARENCY);
                    item.setValue(valueToPercentageString(value));
                } else {
                    item.setValue(preferences.getString(settingKey, SETTINGS_DEFAULT_VALUES[i]));
                }

                if (Helper.isEmpty(item.getValue())) {
                    item.setValue(SETTINGS_DEFAULT_VALUES[i]);
                }

                personalisationSettings.add(item);
            }

            adapter = new SettingItemAdapter(context);
            adapter.addAll(personalisationSettings);
            personalisationListView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }

    private void saveSettings() {
        if (adapter != null) {
            final List<SettingItem> items = new ArrayList<SettingItem>();
            for (int i = 0; i < adapter.getCount(); i++) {
                items.add(adapter.getItem(i));
            }

            (new AsyncTask<Void, Void, Void>() {
                protected Void doInBackground(Void... params) {
                    Context context = getContext();
                    if (context != null) {
                        SharedPreferences preferences = context.getSharedPreferences(HomeActivity.COMMON_SHARED_PREF_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        for (int i = 0; i < items.size() && i < SETTINGS_KEYS.length; i++) {
                            if (HIDDEN_APPS_KEY.equals(SETTINGS_KEYS[i])) {
                                continue;
                            }

                            if (HomeActivity.SETTING_KEY_BACKGROUND_TRANSPARENCY.equals(SETTINGS_KEYS[i])
                                    || HomeActivity.SETTING_KEY_APP_DOCK_TRANSPARENCY.equals(SETTINGS_KEYS[i])) {
                                editor.putFloat(SETTINGS_KEYS[i], percentageStringToFloat(items.get(i).getValue()));
                            } else {
                                editor.putString(SETTINGS_KEYS[i], items.get(i).getValue());
                            }
                        }
                        editor.commit();
                    }
                    return null;
                }
            }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }
}
