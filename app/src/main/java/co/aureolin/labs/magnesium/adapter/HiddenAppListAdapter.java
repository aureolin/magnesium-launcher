package co.aureolin.labs.magnesium.adapter;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import co.aureolin.labs.magnesium.R;
import co.aureolin.labs.magnesium.fragment.AppListFragment;
import co.aureolin.labs.magnesium.model.AppItem;
import co.aureolin.labs.magnesium.util.listener.GenericItemClickListener;

/**
 * Created by akinwale on 11/09/2016.
 */
public class HiddenAppListAdapter extends RecyclerView.Adapter<HiddenAppListAdapter.ViewHolder>  {

    private Context context;

    private List<AppItem> items;

    private GenericItemClickListener itemClickListener;

    public HiddenAppListAdapter(Context context, List<AppItem> items) {
        this.items = items;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_hidden_app_list, viewGroup, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        AppItem item = items.get(i);

        // Load the App Icon
        String fullActivityName = item.getFullActivityName();
        if (AppListFragment.AppIconCache.containsKey(fullActivityName)) {
            viewHolder.iconView.setImageDrawable(AppListFragment.AppIconCache.get(fullActivityName));
        } else {
            new AppListFragment.LoadAppIconTask(item, viewHolder.iconView, context).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }

        viewHolder.titleView.setText(item.getName());
        viewHolder.checkBox.setChecked(item.isHidden());
    }

    @Override
    public int getItemCount() {
        return (items != null ? items.size() : 0);
    }

    public void setItems(List<AppItem> items) {
        this.items = items;
    }

    public AppItem getItem(int position) {
        return (items != null && items.size() > position) ? items.get(position) : null;
    }

    public void setItemClickListener(GenericItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        protected ImageView iconView;
        protected TextView titleView;
        protected CheckBox checkBox;

        public ViewHolder(final View containerView) {
            super(containerView);

            this.iconView = (ImageView) containerView.findViewById(R.id.item_hidden_app_list_image);
            this.titleView = (TextView) containerView.findViewById(R.id.item_hidden_app_list_title);
            this.checkBox = (CheckBox) containerView.findViewById(R.id.item_hidden_app_list_checkbox);

            containerView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (itemClickListener != null) {
                        itemClickListener.onItemClick(getAdapterPosition(), containerView);
                    }
                }
            });
            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (itemClickListener != null) {
                        itemClickListener.onItemClick(getAdapterPosition(), view);
                    }
                }
            });
        }
    }
}
