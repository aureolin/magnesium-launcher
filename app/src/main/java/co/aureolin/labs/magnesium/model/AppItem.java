package co.aureolin.labs.magnesium.model;

import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;

import java.util.Collections;
import java.util.List;

public class AppItem implements Comparable<AppItem> {
    private int rank;

    private int displayOrder;

    private boolean folder;

    private boolean hidden;

    private String uniqueId;

    private String name;

    private String packageName;

    private String activityName;

    private Drawable icon;

    private boolean placeholder;

    private boolean selected;

    private boolean systemApp;

    private ActivityInfo activityInfo;

    private List<AppItem> folderItems;

    public AppItem() {

    }

    public int getNumFolderItems() {
        return folderItems != null ? folderItems.size() : 0;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public boolean isPlaceholder() {
        return placeholder;
    }

    public void setPlaceholder(boolean placeholder) {
        this.placeholder = placeholder;
    }

    public int getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isSystemApp() {
        return systemApp;
    }

    public void setSystemApp(boolean systemApp) {
        this.systemApp = systemApp;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public ActivityInfo getActivityInfo() {
        return activityInfo;
    }

    public void setActivityInfo(ActivityInfo activityInfo) {
        this.activityInfo = activityInfo;
    }

    public String getFullActivityName() {
        return String.format("%s.%s", packageName, activityInfo != null ? activityInfo.name : "");
    }

    public boolean isFolder() {
        return folder;
    }

    public void setFolder(boolean folder) {
        this.folder = folder;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public List<AppItem> getFolderItems() {
        return folderItems;
    }

    public int indexOfFolderItem(AppItem appItem) {
        return (folderItems != null && folderItems.size() > 0) ? folderItems.indexOf(appItem) : -1;
    }

    public void setFolderItems(List<AppItem> folderItems) {
        this.folderItems = folderItems;
        if (this.folderItems != null) {
            Collections.sort(this.folderItems);
        }
    }

    public boolean equals(Object o) {
        if (!(o instanceof AppItem)) {
            return false;
        }

        AppItem item = (AppItem) o;
        if (folder && item.isFolder()) {
            return (uniqueId != null) && uniqueId.equals(item.getUniqueId());
        }

        return ( (getFullActivityName() != null) && getFullActivityName().equals(item.getFullActivityName()) );
    }

    public int compareTo(AppItem appItem) {
        if (appItem == null) {
            return 1;
        }

        if (this.getRank() > 0 || appItem.getRank() > 0) {
            if (this.getRank() > appItem.getRank()) {
                return 1;
            }
            if (this.getRank() < appItem.getRank()) {
                return -1;
            }
        }

        if (this.getName() == null && appItem.getName() == null) {
            return 0;
        }
        if (this.getName() == null && appItem.getName() != null) {
            return -1;
        }

        return ((this.getName() != null) ? this.getName() : "").toLowerCase().compareTo(
                (appItem.getName() != null ? appItem.getName() : "").toLowerCase());
    }
}
