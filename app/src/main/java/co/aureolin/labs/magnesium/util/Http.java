package co.aureolin.labs.magnesium.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.Proxy;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;

/**
 * Created by akinwale on 08/02/2016.
 */
public class Http {

    public static boolean USE_PROXY = false;

    public static boolean USE_PROXY_AUTH = false;

    public static String PROXY_HOST;

    public static String PROXY_TYPE;

    public static int PROXY_PORT;

    public static String PROXY_USERNAME;

    public static String PROXY_PASSWORD;

    private static String TAG = Http.class.getCanonicalName();

    private static boolean shouldUseProxy;

    private static boolean shouldUseProxyAuth;

    private static Proxy proxy;

    public static String get(String requestUrl) {
        boolean isSecure = isSecureUrl(requestUrl);

        String response = "";
        HttpURLConnection conn = null;
        BufferedReader reader = null;

        try {
            URL url = new URL(requestUrl);
            conn = (HttpURLConnection) ((shouldUseProxy && proxy != null) ?
                    url.openConnection(proxy) : url.openConnection());
            conn.setConnectTimeout(1000);
            //conn.setRequestProperty("User-Agent", Helper.getUserAgent(JumpgateActivity.VERSION_NAME));
            response = readInputStream(conn.getInputStream(), reader);

            /*Map<String, List<String>> headers = conn.getHeaderFields();
            for (Map.Entry<String, List<String>> header : headers.entrySet()) {
                Log.d(TAG, "HeaderName=" + header.getKey() + "; Values=" + Helper.getCharacterSeparatedValues(header.getValue()));
            }*/

            Log.d(TAG, String.format("Response from URL: %s\n%s", requestUrl, response));
        } catch (MalformedURLException e) {
            Log.e(TAG, String.format("Invalid URL: %s", requestUrl), e);
        } catch (IOException e) {
            Log.e(TAG, "Request failed", e);
        } finally {
            Helper.closeCloseable(reader);
            disconnect(conn);
        }

        return response;
    }

    public static String get(String requestUrl, Map<String, String> headers) {
        boolean isSecure = isSecureUrl(requestUrl);

        String response = "";
        HttpURLConnection conn = null;
        BufferedReader reader = null;

        try {
            URL url = new URL(requestUrl);
            conn = (HttpURLConnection) ((shouldUseProxy && proxy != null) ?
                    url.openConnection(proxy) : url.openConnection());
            conn.setConnectTimeout(1000);
            //conn.setRequestProperty("User-Agent", Helper.getUserAgent(JumpgateActivity.VERSION_NAME));
            if (headers != null) {
                for (Map.Entry<String, String> entry : headers.entrySet()) {
                    conn.setRequestProperty(entry.getKey(), entry.getValue());
                }
            }
            response = readInputStream(conn.getInputStream(), reader);

            //Log.d(TAG, String.format("Response from URL: %s\n%s", requestUrl, response));
        } catch (MalformedURLException e) {
            Log.e(TAG, String.format("Invalid URL: %s", requestUrl), e);
        } catch (IOException e) {
            Log.e(TAG, "Request failed", e);
        } finally {
            Helper.closeCloseable(reader);
            disconnect(conn);
        }

        return response;
    }

    public static String getWithBasicAuth(String requestUrl, String username, String password) {
        boolean isSecure = isSecureUrl(requestUrl);

        String response = "";
        HttpURLConnection conn = null;
        BufferedReader reader = null;

        try {
            URL url = new URL(requestUrl);
            conn = (HttpURLConnection) ((shouldUseProxy && proxy != null) ?
                    url.openConnection(proxy) : url.openConnection());
            conn.setConnectTimeout(1000);
            String encoded = Base64.encodeToString(String.format("%s:%s", username, password).getBytes("UTF-8"), Base64.NO_WRAP);
            conn.setRequestProperty("Authorization", String.format("Basic %s", encoded));
            //conn.setRequestProperty("User-Agent", Helper.getUserAgent(JumpgateActivity.VERSION_NAME));
            response = readInputStream(conn.getInputStream(), reader);

            //Log.d(TAG, String.format("Response from URL: %s\n%s", requestUrl, response));
        } catch (MalformedURLException e) {
            Log.e(TAG, String.format("Invalid URL: %s", requestUrl), e);
        } catch (IOException e) {
            Log.e(TAG, "Request failed", e);
        } finally {
            Helper.closeCloseable(reader);
            disconnect(conn);
        }

        return response;
    }

    public static String getWithConsumer(String requestUrl, OAuthConsumer consumer) {
        boolean isSecure = isSecureUrl(requestUrl);

        String response = "";
        HttpURLConnection conn = null;
        BufferedReader reader = null;

        try {
            URL url = new URL(requestUrl);
            conn = (HttpURLConnection) ((shouldUseProxy && proxy != null) ?
                    url.openConnection(proxy) : url.openConnection());
            conn.setConnectTimeout(1000);
            consumer.sign(conn);
            conn.connect();

            //conn.setRequestProperty("User-Agent", Helper.getUserAgent(JumpgateActivity.VERSION_NAME));
            response = readInputStream(conn.getInputStream(), reader);
            //Log.d(TAG, String.format("Response from URL: %s\n%s", requestUrl, response));
        } catch (MalformedURLException e) {
            Log.e(TAG, String.format("Invalid URL: %s", requestUrl), e);
        } catch (OAuthMessageSignerException ex) {
            Log.e(TAG, "Message signer exception occurred", ex);
        } catch (OAuthExpectationFailedException ex) {
            Log.e(TAG, "Authentication failed exception occurred", ex);
        } catch (OAuthCommunicationException ex) {
            Log.e(TAG, "Communication exception occurred", ex);
        } catch (IOException e) {
            Log.e(TAG, "Request failed", e);
        } finally {
            Helper.closeCloseable(reader);
            disconnect(conn);
        }

        return response;
    }

    public static String post(String requestUrl, String postData) {
        return Http.post(requestUrl, postData, null, null);
    }

    public static String post(String requestUrl, String postData, String cookiesString, Map<String, String> additionalHeaders) {
        String response = "";
        HttpURLConnection conn = null;
        BufferedReader reader = null;
        DataOutputStream wr = null;

        try {
            byte[] postDataBytes = postData.getBytes("UTF-8");

            URL url = new URL(requestUrl);
            conn = (HttpURLConnection) ((shouldUseProxy && proxy != null) ?
                    url.openConnection(proxy) : url.openConnection());
            conn.setConnectTimeout(10000);
            conn.setReadTimeout(10000);
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("charset", "utf-8");
            conn.setRequestProperty("Content-Length", Integer.toString(postDataBytes.length));
            if (!Helper.isEmpty(cookiesString)) {
                conn.setRequestProperty("Cookie", cookiesString);
            }
            if (additionalHeaders != null) {
                for (Map.Entry<String, String> entry : additionalHeaders.entrySet()) {
                    conn.setRequestProperty(entry.getKey(), entry.getValue());
                }
            }

            try {
                wr = new DataOutputStream(conn.getOutputStream());
                wr.write(postDataBytes);
            } finally {
                Helper.closeCloseable(wr);
            }
            //conn.setRequestProperty("User-Agent", Helper.getUserAgent(JumpgateActivity.VERSION_NAME));

            response = readInputStream(conn.getInputStream(), reader);
            //Log.d(TAG, String.format("Response from URL: %s\n%s", requestUrl, response));
        } catch (MalformedURLException e) {
            Log.e(TAG, String.format("Invalid URL: %s", requestUrl), e);
        } catch (IOException e) {
            Log.e(TAG, "Request failed", e);
        } finally {
            Helper.closeCloseable(wr);
            Helper.closeCloseable(reader);
            disconnect(conn);
        }

        return response;
    }

    private static void disableSSLCertificateChecking() {
        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            @Override
            public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
                // Not implemented
            }

            @Override
            public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
                // Not implemented
            }
        } };

        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public static String postWithConsumer(String requestUrl, String postData, String contentType, OAuthConsumer consumer) {
        String response = "";
        HttpURLConnection conn = null;
        BufferedReader reader = null;
        DataOutputStream wr = null;
        try {
            byte[] postDataBytes = null;

            URL url = new URL(requestUrl);
            conn = (HttpURLConnection) ((shouldUseProxy && proxy != null) ?
                    url.openConnection(proxy) : url.openConnection());
            conn.setConnectTimeout(10000);
            conn.setReadTimeout(10000);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("charset", "utf-8");
            if (!Helper.isEmpty(postData)) {
                postDataBytes = postData.getBytes("UTF-8");

                conn.setDoOutput(true);
                conn.setRequestProperty("Content-Length", Integer.toString(postDataBytes.length));
            }

            consumer.sign(conn);

            conn.setRequestProperty("Content-Type", contentType);
            if (postDataBytes != null) {
                wr = new DataOutputStream(conn.getOutputStream());
                wr.write(postDataBytes);
                wr.flush();
                wr.close();
            }

            conn.connect();
            response = readInputStream(conn.getInputStream(), reader);

            //Log.d(TAG, String.format("Response from URL: %s\n%s", requestUrl, response));
        } catch (MalformedURLException e) {
            Log.e(TAG, String.format("Invalid URL: %s", requestUrl), e);
        } catch (OAuthMessageSignerException ex) {
            Log.e(TAG, "Message signer exception occurred", ex);
        } catch (OAuthExpectationFailedException ex) {
            Log.e(TAG, "Authentication failed exception occurred", ex);
        } catch (OAuthCommunicationException ex) {
            Log.e(TAG, "Communication exception occurred", ex);
        } catch (IOException e) {
            Log.e(TAG, "Request failed", e);
        } finally {
            Helper.closeCloseable(wr);
            Helper.closeCloseable(reader);
            disconnect(conn);
        }

        return response;
    }

    public static void init(boolean useProxy,
                            String proxyType,
                            String proxyHostname,
                            int proxyPort,
                            boolean useProxyAuth,
                            String proxyUsername,
                            String proxyPassword) {
        USE_PROXY = useProxy;
        PROXY_TYPE = proxyType;
        PROXY_HOST = proxyHostname;
        PROXY_PORT = proxyPort;
        USE_PROXY_AUTH = useProxyAuth;
        PROXY_USERNAME = proxyUsername;
        PROXY_PASSWORD = proxyPassword;

        shouldUseProxy = (USE_PROXY && !Helper.isEmpty(PROXY_HOST) && PROXY_PORT > 0);
        shouldUseProxyAuth = (USE_PROXY_AUTH && !Helper.isEmpty(PROXY_USERNAME));
        if (shouldUseProxy) {
            proxy = new Proxy(
                    ("SOCKS".equals(PROXY_TYPE)) ? Proxy.Type.SOCKS : Proxy.Type.HTTP,
                    new InetSocketAddress(PROXY_HOST, PROXY_PORT));
        }
        if (shouldUseProxyAuth) {
            Authenticator authenticator = new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return (new PasswordAuthentication(
                            PROXY_USERNAME, PROXY_PASSWORD.toCharArray()
                    ));
                }
            };
            Authenticator.setDefault(authenticator);
        }
    }

    public static Bitmap getImage(String requestUrl, Context context) {
        if (context == null) {
            return null;
        }

        HttpURLConnection conn = null;
        Bitmap bitmap = null;
        InputStream in = null;
        OutputStream os = null;
        byte[] buffer = new byte[1024];
        try {
            URL url = new URL(requestUrl);
            conn = (HttpURLConnection) url.openConnection();

            in = conn.getInputStream();
            Map<String, List<String>> headers = conn.getHeaderFields();
            for (Map.Entry<String, List<String>> header : headers.entrySet()) {
                String key = header.getKey();
                List<String> values = header.getValue();
                if ("location".equalsIgnoreCase(key) && values.size() > 0) {
                    // Handle redirects
                    return getImage(header.getValue().get(0), context);
                }
            }

            File cacheDir = BitmapDiskCache.getDiskCacheDir(context, "imagedl");
            if (!cacheDir.isDirectory()) {
                cacheDir.mkdirs();
            }
            File outputFile = File.createTempFile("tmp_",
                String.format("_%s", requestUrl.substring(requestUrl.lastIndexOf('/') + 1)),
                cacheDir);
            os = new FileOutputStream(outputFile);
            int len;
            while ((len = in.read(buffer)) > 0) {
                os.write(buffer, 0, len);
            }
            os.flush();
            Helper.closeCloseable(os);

            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            options.inSampleSize = Helper.calculateInSampleSize(options, 192, 144);
            bitmap = BitmapFactory.decodeFile(outputFile.getAbsolutePath(), options);

            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            bitmap = BitmapFactory.decodeFile(outputFile.getAbsolutePath(), options);

            //bitmap = BitmapFactory.decodeStream(in);
            outputFile.delete();
        } catch (MalformedURLException e) {
            Log.e(TAG, String.format("Invalid URL: %s", requestUrl), e);
        } catch (IOException e) {
            Log.e(TAG, "Request failed", e);
        } finally {
            Helper.closeCloseable(in);
            Helper.closeCloseable(os);
            disconnect(conn);
        }

        return bitmap;
    }

    private static String readInputStream(InputStream in, BufferedReader reader)
        throws IOException {
        if (reader == null) {
            reader = new BufferedReader(new InputStreamReader(in, "utf-8"));
        }

        StringBuilder sb = new StringBuilder();
        String input;
        while ((input = reader.readLine()) != null) {
            sb.append(input);
        }

        return sb.toString();
    }

    private static boolean isSecureUrl(String url) {
        return (url != null && url.toLowerCase().startsWith("https"));
    }

    private static void disconnect(HttpURLConnection connection) {
        if (connection != null) {
            connection.disconnect();
        }
    }

    private static void disconnect(HttpsURLConnection connection) {
        if (connection != null) {
            connection.disconnect();
        }
    }
}