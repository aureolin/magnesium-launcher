package co.aureolin.labs.magnesium.util.handler;

import co.aureolin.labs.magnesium.model.EmailAccount;

/**
 * Created by akinwale on 14/02/2016.
 */
public interface EmailAccountSavedHandler {
    public void onEmailAccountSaved(EmailAccount account);
}
