package co.aureolin.labs.magnesium.service;

import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Environment;
import android.os.IBinder;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.provider.Telephony;
import android.util.Log;
import android.webkit.MimeTypeMap;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import co.aureolin.labs.magnesium.model.AppItem;
import co.aureolin.labs.magnesium.model.BaseItem;
import co.aureolin.labs.magnesium.model.FeedItem;
import co.aureolin.labs.magnesium.model.IndexedItem;
import co.aureolin.labs.magnesium.util.Helper;
import co.aureolin.labs.magnesium.util.db.MagnesiumDbContext;

/**
 * Steps:
 * 1. Index/Re-index contacts
 * 2. Index/Re-index SMS messages
 * 3. Index/Re-index e-mail messages
 */
public class IndexingService extends Service {
    public static final String ACTION_CALL_LOG_INDEXED = "co.aureolin.labs.magnesium.intent.action.CALL_LOG_INDEXED";

    public static final String ACTION_SMS_INDEXED = "co.aureolin.labs.magnesium.intent.action.SMS_INDEXED";

    private static final Object lock = new Object();

    private static final int NINETY_SECONDS = 90000;

    private static Map<String, String> localExtMimeTypeMap = new HashMap<String, String>();
    static {
        localExtMimeTypeMap.put("pdf", "application/pdf");
        localExtMimeTypeMap.put("txt", "text/plain");
    }

    private static final String TAG = IndexingService.class.getCanonicalName();

    // Run index every 6 hours?
    private static final int SERVICE_TIMER_INTERVAL = 3600000 * 6; // 1 hour * 6

    private static final int SMS_INDEX_TIMER_INTERVAL = 3600000 * 2; // 1 hour * 2

    private static final int CALL_LOG_INDEX_TIMER_INTERVAL = 60000 * 15; // 1 minute * 15

    private static final int FILE_SIZE_CONTENT_LIMIT = 128 * 1024; // 128KB plain text limit

    private static final int FILE_INDEX_TIMER_INTERVAL = 3600000 * 12; // 1 hour * 12

    private Timer serviceTimer;

    private TimerTask serviceTimerTask;

    private Timer fileIndexTimer;

    private TimerTask fileIndexTask;

    private Timer smsIndexTimer;

    private TimerTask smsIndexTimerTask;

    private Timer callLogIndexTimer;

    private TimerTask callLogIndexTask;

    private MagnesiumDbContext dbContext;

    private Context context;

    public IndexingService() {

    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        context = getApplicationContext();
        serviceTimerTask = new TimerTask() {
            @Override
            public void run() {
            indexApps();
            Helper.sleep(1000);

            indexContacts();
            Helper.sleep(1000);
            }
        };
        serviceTimer = new Timer();
        serviceTimer.scheduleAtFixedRate(serviceTimerTask, NINETY_SECONDS, SERVICE_TIMER_INTERVAL);

        fileIndexTask = new TimerTask() {
            @Override
            public void run() {
                indexFiles();
            }
        };
        fileIndexTimer = new Timer();
        fileIndexTimer.scheduleAtFixedRate(fileIndexTask, NINETY_SECONDS, FILE_INDEX_TIMER_INTERVAL);

        smsIndexTimerTask = new TimerTask() {
            @Override
            public void run() {
                indexSmsMessages();
            }
        };
        smsIndexTimer = new Timer();
        smsIndexTimer.scheduleAtFixedRate(smsIndexTimerTask, NINETY_SECONDS, SMS_INDEX_TIMER_INTERVAL);

        callLogIndexTask = new TimerTask() {
            @Override
            public void run() {
                indexCallLog();
            }
        };
        callLogIndexTimer = new Timer();
        callLogIndexTimer.scheduleAtFixedRate(callLogIndexTask, NINETY_SECONDS, CALL_LOG_INDEX_TIMER_INTERVAL);

        return START_STICKY;
    }

    private void indexApps() {
        SQLiteDatabase db = null;
        try {
            dbContext = new MagnesiumDbContext(context);
            db = dbContext.getWritableDatabase();

            PackageManager manager = context.getPackageManager();
            Intent i = new Intent(Intent.ACTION_MAIN, null);
            i.addCategory(Intent.CATEGORY_LAUNCHER);

            List<ResolveInfo> availableActivities = manager.queryIntentActivities(i, 0);
            List<AppItem> allApps = new ArrayList<AppItem>();
            for (ResolveInfo ri : availableActivities) {
                String packageName = ri.activityInfo.packageName;

                IndexedItem indexedItem = new IndexedItem();
                indexedItem.setType(BaseItem.ITEM_TYPE_APP);
                indexedItem.setRefId(packageName);
                indexedItem.setTitle(ri.loadLabel(manager).toString());
                indexedItem.setMetaFieldName(ri.activityInfo.name);

                synchronized (lock) {
                    if (!MagnesiumDbContext.indexedItemUniqueRefIdExists(indexedItem, db)) {
                        MagnesiumDbContext.insert(indexedItem, db);
                    } else {
                        MagnesiumDbContext.update(indexedItem, db);
                    }
                }
            }
        } catch (Exception ex) {
            Log.d(TAG, "Unable to index apps.");
        } finally {
            Helper.closeDb(db);
        }
    }

    private void indexContacts() {
        SQLiteDatabase db = null;
        Cursor cursor = null;
        Cursor phonesCursor = null;
        Cursor emailCursor = null;
        try {
            dbContext = new MagnesiumDbContext(context);
            db = dbContext.getWritableDatabase();

            ContentResolver cr = getContentResolver();
            cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
            if (cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    List<IndexedItem> indexedItems = new ArrayList<IndexedItem>();

                    // Create corresponding IndexedItems
                    String contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                    String displayName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                    IndexedItem contactItem = new IndexedItem();
                    contactItem.setType(FeedItem.ITEM_TYPE_CONTACT);
                    contactItem.setRefId(contactId);
                    contactItem.setUrl(Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, String.valueOf(contactId)).toString());
                    contactItem.setMetaFieldName(ContactsContract.Contacts.DISPLAY_NAME);
                    contactItem.setMetaContent(displayName);
                    indexedItems.add(contactItem);

                    // Get phone numbers
                    phonesCursor = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId,
                        null, null);
                    while (phonesCursor.moveToNext()) {
                        String number = phonesCursor.getString(phonesCursor.getColumnIndex(
                                ContactsContract.CommonDataKinds.Phone.NUMBER));
                        int type = phonesCursor.getInt(phonesCursor.getColumnIndex(
                                ContactsContract.CommonDataKinds.Phone.TYPE));

                        IndexedItem phoneItem = new IndexedItem();
                        phoneItem.setType(FeedItem.ITEM_TYPE_CONTACT_NUMBER);
                        phoneItem.setTitle(displayName);
                        phoneItem.setRefId(contactId);
                        phoneItem.setUrl(String.format("tel:%s", number));
                        phoneItem.setMetaContent(number);

                        switch (type) {
                            case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                                phoneItem.setMetaFieldName("home");
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
                                phoneItem.setMetaFieldName("work");
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                                phoneItem.setMetaFieldName("mobile");
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_MAIN:
                                phoneItem.setMetaFieldName("main");
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_WORK_MOBILE:
                                phoneItem.setMetaFieldName("work_mobile");
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_PAGER:
                                phoneItem.setMetaFieldName("pager");
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_WORK_PAGER:
                                phoneItem.setMetaFieldName("work_pager");
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_OTHER:
                            default:
                                phoneItem.setMetaFieldName("other");
                                break;
                        }

                        indexedItems.add(phoneItem);
                    }

                    emailCursor = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = " + contactId,
                            null, null);
                    while (emailCursor.moveToNext()) {
                        String email = emailCursor.getString(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                        int type = emailCursor.getInt(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE));

                        IndexedItem emailItem = new IndexedItem();
                        emailItem.setType(FeedItem.ITEM_TYPE_CONTACT_EMAIL);
                        emailItem.setTitle(displayName);
                        emailItem.setRefId(contactId);
                        emailItem.setUrl(String.format("mailto:%s", email));
                        emailItem.setMetaContent(email);

                        switch (type) {
                            case ContactsContract.CommonDataKinds.Email.TYPE_HOME:
                                emailItem.setMetaFieldName("home");
                                break;
                            case ContactsContract.CommonDataKinds.Email.TYPE_WORK:
                                emailItem.setMetaFieldName("work");
                                break;
                            case ContactsContract.CommonDataKinds.Email.TYPE_MOBILE:
                                emailItem.setMetaFieldName("mobile");
                                break;
                            case ContactsContract.CommonDataKinds.Email.TYPE_OTHER:
                            default:
                                emailItem.setMetaFieldName("other");
                                break;
                        }
                    }

                    Helper.closeCloseable(emailCursor);
                    Helper.closeCloseable(phonesCursor);

                    // Persist the list of indexed items
                    for (int i = 0; i < indexedItems.size(); i++) {
                        IndexedItem item = indexedItems.get(i);

                        synchronized (lock) {
                            if (!MagnesiumDbContext.indexedItemRefIdExists(item, db)) {
                                MagnesiumDbContext.insert(item, db);
                            }
                        }
                    }
                }
            }

        } catch (Exception ex) {
            Log.e(TAG, "Could not index contacts.", ex);
        } finally {
            Helper.closeCloseable(phonesCursor);
            Helper.closeCloseable(emailCursor);
            Helper.closeCloseable(cursor);
            Helper.closeDb(db);
        }
    }

    private void indexFiles() {
        SQLiteDatabase db = null;
        try {
            dbContext = new MagnesiumDbContext(context);
            db = dbContext.getWritableDatabase();
            File root = new File(Environment.getExternalStorageDirectory().getAbsolutePath());
            if (root.exists() && root.isDirectory()) {
                indexDirectory(root, db);
            }
        } catch (Exception ex) {
            Log.e(TAG, "Could not index files.", ex);
        } finally {
            Helper.closeDb(db);
        }
    }

    private void indexCallLog() {
        SQLiteDatabase db = null;
        Cursor cursor = null;
        try {
            dbContext = new MagnesiumDbContext(context);
            db = dbContext.getWritableDatabase();


            String selection = null;
            String[] selectionArgs = null;
            long latestTimestamp = MagnesiumDbContext.getLatestFeedItemTimestamp(BaseItem.ITEM_TYPE_CALL_LOG, db);
            if (latestTimestamp > 0) {
                selection = String.format("%s > ?", CallLog.Calls.DATE);
                selectionArgs = new String[] { String.valueOf(latestTimestamp) };
            }

            ContentResolver cr = getContentResolver();
            cursor = cr.query(CallLog.Calls.CONTENT_URI, null, selection, selectionArgs, null);
            while (cursor.moveToNext()) {
                String id = cursor.getString(cursor.getColumnIndex(CallLog.Calls._ID));
                int type = cursor.getInt(cursor.getColumnIndex(CallLog.Calls.TYPE));
                String number = cursor.getString(cursor.getColumnIndex(CallLog.Calls.NUMBER));
                long duration = cursor.getLong(cursor.getColumnIndex(CallLog.Calls.DURATION));
                long date = cursor.getLong(cursor.getColumnIndex(CallLog.Calls.DATE));

                FeedItem feedItem = new FeedItem();
                feedItem.setType(BaseItem.ITEM_TYPE_CALL_LOG);
                feedItem.setExternalRefId(id);
                feedItem.setContent(number);
                if (type == CallLog.Calls.INCOMING_TYPE) {
                    feedItem.setDirection(BaseItem.DIRECTION_INCOMING);
                }
                if (type == CallLog.Calls.OUTGOING_TYPE) {
                    feedItem.setDirection(BaseItem.DIRECTION_OUTGOING);
                }
                if (type == CallLog.Calls.MISSED_TYPE) {
                    feedItem.setDirection(BaseItem.DIRECTION_INCOMING);
                    feedItem.setMissed(true);
                }
                if (type == CallLog.Calls.VOICEMAIL_TYPE) {
                    feedItem.setDirection(BaseItem.DIRECTION_INCOMING);
                    feedItem.setVoicemail(true);
                }
                feedItem.setDuration(duration);
                feedItem.setEntryTime(new Date(date));
                feedItem.setUrl(String.format("tel:%s", number));

                synchronized (lock) {
                    if (!MagnesiumDbContext.externalRefIdExists(feedItem, db)) {
                        MagnesiumDbContext.insert(feedItem, db);
                    }
                }
            }
            Helper.sendBroadcast(ACTION_CALL_LOG_INDEXED, this);
        } catch (Exception ex) {
            Log.e(TAG, "Could not index call log.", ex);
        } finally {
            Helper.closeCloseable(cursor);
            Helper.closeDb(db);
        }
    }

    private void indexSmsMessages() {
        SQLiteDatabase db = null;
        Cursor messageCursor = null;
        try {
            dbContext = new MagnesiumDbContext(context);
            db = dbContext.getWritableDatabase();

            String selection = null;
            String[] selectionArgs = null;
            long latestTimestamp = MagnesiumDbContext.getLatestFeedItemTimestamp(BaseItem.ITEM_TYPE_SMS_MESSAGE, db);
            if (latestTimestamp > 0) {
                selection = String.format("%s > ?", Telephony.Sms.DATE);
                selectionArgs = new String[] { String.valueOf(latestTimestamp) };
            }

            ContentResolver cr = getContentResolver();
            Uri smsUri = Uri.parse("content://sms");
            messageCursor = cr.query(smsUri, null, selection, selectionArgs, null);
            int loopCount = 0;
            while (messageCursor.moveToNext()) {
                loopCount++;

                String id = messageCursor.getString(messageCursor.getColumnIndex(Telephony.Sms._ID));
                int type = messageCursor.getInt(messageCursor.getColumnIndex(Telephony.Sms.TYPE)); // 2 = sent, etc.
                String phone = messageCursor.getString(messageCursor.getColumnIndex(Telephony.Sms.ADDRESS));
                long dateSent = messageCursor.getLong(messageCursor.getColumnIndex(Telephony.Sms.DATE_SENT));
                long date = messageCursor.getLong(messageCursor.getColumnIndex(Telephony.Sms.DATE));
                int read = messageCursor.getInt(messageCursor.getColumnIndex(Telephony.Sms.READ));
                String body = messageCursor.getString(messageCursor.getColumnIndex(Telephony.Sms.BODY));
                String threadId = messageCursor.getString(messageCursor.getColumnIndex(Telephony.Sms.THREAD_ID));

                if ((type != 1 && type != 2)) {
                    continue;
                }

                FeedItem feedItem = new FeedItem();
                feedItem.setType(BaseItem.ITEM_TYPE_SMS_MESSAGE);
                feedItem.setExternalRefId(id);
                feedItem.setPosterId(phone);
                feedItem.setDirection(type == 1 ? BaseItem.DIRECTION_INCOMING : BaseItem.DIRECTION_OUTGOING);
                feedItem.setThreadId(threadId);

                feedItem.setEntryTime((type == 2) ? new Date(dateSent) : new Date(date));
                if (feedItem.getEntryTime() == null) {
                    continue;
                }

                Calendar cal = Calendar.getInstance();
                cal.setTime(feedItem.getEntryTime());
                if (cal.get(Calendar.YEAR) < 1980) {
                    continue;
                }

                feedItem.setRead(read == 1);
                feedItem.setContent(body);

                synchronized (lock) {
                    if (!MagnesiumDbContext.externalRefIdExists(feedItem, db)) {
                        MagnesiumDbContext.insert(feedItem, db);
                    }
                }

                if (loopCount % 200 == 0) {
                    Helper.sleep(1000);
                }
            }

            Helper.sendBroadcast(ACTION_SMS_INDEXED, this);
        } catch (Exception ex) {
            Log.e(TAG, "Could not index SMS messages.", ex);
        } finally {
            Helper.closeCloseable(messageCursor);
            Helper.closeDb(db);
        }
    }

    private void indexDirectory(File directory, SQLiteDatabase db) {
        if (directory.getName().startsWith(".")) {
            // Do not index . folders/directories
            return;
        }

        File[] filesInDir = directory.listFiles();
        for (int i = 0; i < filesInDir.length; i++) {
            File file = filesInDir[i];
            if (file.getName().startsWith(".")) {
                // Do not index . files
                continue;
            }
            if (file.isDirectory()) {
                indexDirectory(file, db);
            } else {
                String url = Uri.fromFile(file).toString();
                String mimeType = getMimeTypeForFile(file, context);

                Date lastModified = new Date(file.lastModified());
                IndexedItem item = new IndexedItem();
                item.setRefId(url); // Use the absolute path as the ref ID
                item.setType(BaseItem.ITEM_TYPE_FILE);
                item.setTitle(file.getName());
                item.setUrl(url);
                item.setMimeType(mimeType);
                item.setCreatedOn(lastModified);
                item.setModifiedOn(lastModified);

                // Save content for text/* mime types less than 128KB in size
                // TODO: Text content for PDF, .docx, etc
                if (mimeType != null && file.length() < FILE_SIZE_CONTENT_LIMIT) {
                    String fileContent = null;

                    if (mimeType.startsWith("text")) {
                        fileContent = readFileContents(file);
                    } else if (mimeType.equalsIgnoreCase(Helper.DOCX_MIME_TYPE)) {
                        fileContent = readDocxContents(file);
                    } /*else if (mimeType.equalsIgnoreCase(Helper.PDF_MIME_TYPE)) {
                        fileContent = readPdfContents(file);
                    }*/

                    if (!Helper.isEmpty(fileContent)) {
                        item.setMetaFieldName("file_content");
                        item.setMetaContent(fileContent);
                    }
                }

                if (i % 200 == 0) {
                    Helper.sleep(1000);
                }

                synchronized (lock) {
                    if (!MagnesiumDbContext.indexedItemUniqueRefIdExists(item, db)) {
                        MagnesiumDbContext.insert(item, db);
                    } else {
                        // Update lastModified, and content (if available)
                        // MagnesiumDbContext.update(item, db);
                    }
                }
            }
        }
    }

    private static String readFileContents(File file) {
        StringBuilder sb = new StringBuilder();
        FileInputStream fis = null;
        BufferedReader reader = null;

        try {
            String line = null;
            fis = new FileInputStream(file);
            reader = new BufferedReader(new InputStreamReader(fis));
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }
        } catch (IOException ex) {
            Log.d(TAG, "Could not read contents of file: " + file.getAbsolutePath());
        } finally {
            Helper.closeCloseable(fis);
            Helper.closeCloseable(reader);
        }

        return sb.toString();
    }

    private static String readDocxContents(File file) {
        StringBuilder sb = new StringBuilder();

        InputStream in = null;
        ByteArrayOutputStream fos = null;
        BufferedReader reader = null;
        byte[] buffer = new byte[1024];
        try {
            in = new FileInputStream(file);
            ZipInputStream zis = new ZipInputStream(in);
            ZipEntry ze = null;
            while ((ze = zis.getNextEntry()) != null) {
                if("document.xml".equals(new File(ze.getName()).getName())) {
                    break;
                }
            }

            if (ze != null) {
                fos = new ByteArrayOutputStream();
                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }
                fos.flush();

                String docxContent = fos.toString("utf-8").trim();
                sb.append(docxContent.replace("</w:p>", "\n").replaceAll("<[^>]+>", ""));
            }
        } catch (IOException e) {
            Log.e(TAG, "Could not get word XML from archive.", e);
        } finally {
            Helper.closeCloseable(in);
            Helper.closeCloseable(fos);
        }

        return sb.toString();
    }

    /*private static String readPdfContents(File file) {
        StringBuilder sb = new StringBuilder();
        PDDocument document = null;
        try {
            document = PDDocument.load(file);
            PDFTextStripper stripper = new PDFTextStripper();
            sb.append(stripper.getText(document));

        } catch (IOException ex) {
            Log.e(TAG, "Could not get text from PDF document.", ex);
        } finally {
            Helper.closeCloseable(document);
        }

        return sb.toString();
    }*/

    private static String getMimeTypeForFile(File file, Context context) {
        String mimeType = null;
        String absolutePath = file.getAbsolutePath();
        Uri fileUri = Uri.fromFile(file);
        String filename = file.getName();
        String ext = MimeTypeMap.getFileExtensionFromUrl(fileUri.toString().toLowerCase());
        if (Helper.isEmpty(ext) && filename.contains(".")) {
            ext = filename.substring(filename.lastIndexOf(".") + 1);
        }
        if (mimeType == null) {
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(ext);
        }
        if (Helper.isEmpty(mimeType) && localExtMimeTypeMap.containsKey(ext)) {
            mimeType = localExtMimeTypeMap.get(ext);
        }

        return mimeType;
    }
}
