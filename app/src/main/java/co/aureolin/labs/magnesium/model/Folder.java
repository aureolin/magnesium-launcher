package co.aureolin.labs.magnesium.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by akinwale on 13/09/2016.
 */
public class Folder {
    private String uniqueId;

    private String name;

    private List<String> activities;

    public Folder() {
        activities = new ArrayList<String>();
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getActivities() {
        return activities;
    }

    public void setActivities(List<String> activities) {
        this.activities = activities;
    }

    public static Folder fromAppItem(AppItem appItem) {
        Folder folder = new Folder();
        folder.setUniqueId(appItem.getUniqueId());
        folder.setName(appItem.getName());

        List<String> activities = new ArrayList<String>();
        List<AppItem> folderItems = appItem.getFolderItems();
        for (int i = 0; i < folderItems.size(); i++) {
            AppItem folderItem = folderItems.get(i);
            if (!activities.contains(folderItem.getFullActivityName())) {
                activities.add(folderItem.getFullActivityName());
            }
        }
        folder.setActivities(activities);

        return folder;
    }
}
