package co.aureolin.labs.magnesium.model;

import java.util.Date;
import java.util.List;

import co.aureolin.labs.magnesium.util.Helper;

public class FeedItem extends BaseItem {

    private String externalRefId;

    private String threadId;

    private long ownerId;

    private String content;

    private String inResponseTo;

    private String embeddedContent;

    private String imageUrl;

    private Date entryTime; // Feed item date

    private boolean read;

    private boolean missed;

    private boolean voicemail;

    private long duration;

    private String source;

    // Social media
    private boolean retweet;

    private String retweetedBy;

    private String retweetedById;

    private String mediaType;

    private String mediaUrl;

    private String posterId;

    private String posterImageUrl;

    private int commentCount;

    private int likeCount;

    private int shareCount; // Can also be used for retweet count (Twitter)

    private List<Attachment> attachments;

    public FeedItem() {

    }

    public String getExternalRefId() {
        return externalRefId;
    }

    public void setExternalRefId(String externalRefId) {
        this.externalRefId = externalRefId;
    }

    public long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }

    public String getThreadId() {
        return threadId;
    }

    public void setThreadId(String threadId) {
        this.threadId = threadId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getEntryTime() {
        return entryTime;
    }

    public void setEntryTime(Date entryTime) {
        this.entryTime = entryTime;
    }

    public String getPosterId() {
        return posterId;
    }

    public void setPosterId(String posterId) {
        this.posterId = posterId;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getMediaUrl() {
        return mediaUrl;
    }

    public void setMediaUrl(String mediaUrl) {
        this.mediaUrl = mediaUrl;
    }

    public String getPosterImageUrl() {
        return posterImageUrl;
    }

    public void setPosterImageUrl(String posterImageUrl) {
        this.posterImageUrl = posterImageUrl;
    }

    public String getSource() {
        return source;
    }

    public String getDisplaySource() {
        if (getType() == BaseItem.ITEM_TYPE_TWITTER_POST && !Helper.isEmpty(source)) {
            return String.format("via %s", source);
        }
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

    public int getShareCount() {
        return shareCount;
    }

    public void setShareCount(int shareCount) {
        this.shareCount = shareCount;
    }

    public String getInResponseTo() {
        return inResponseTo;
    }

    public void setInResponseTo(String inResponseTo) {
        this.inResponseTo = inResponseTo;
    }

    public String getEmbeddedContent() {
        return embeddedContent;
    }

    public void setEmbeddedContent(String embeddedContent) {
        this.embeddedContent = embeddedContent;
    }

    public boolean isRetweet() {
        return retweet;
    }

    public void setRetweet(boolean retweet) {
        this.retweet = retweet;
    }

    public String getRetweetedById() {
        return retweetedById;
    }

    public void setRetweetedById(String retweetedById) {
        this.retweetedById = retweetedById;
    }

    public String getRetweetedBy() {
        return retweetedBy;
    }

    public void setRetweetedBy(String retweetedBy) {
        this.retweetedBy = retweetedBy;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public boolean isMissed() {
        return missed;
    }

    public void setMissed(boolean missed) {
        this.missed = missed;
    }

    public boolean isVoicemail() {
        return voicemail;
    }

    public void setVoicemail(boolean voicemail) {
        this.voicemail = voicemail;
    }

    public List<Attachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<Attachment> attachments) {
        this.attachments = attachments;
    }

    @Override
    public String getReferenceId() { return externalRefId; }

    @Override
    public Date getTimestamp() {
        return (entryTime != null) ? entryTime : getCreatedOn();
    }

    @Override
    public String getValue() {
        return content;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof BaseItem)) {
            return false;
        }

        if (o instanceof IndexedItem) {
            IndexedItem indexedItem = (IndexedItem) o;
            if (indexedItem.getFeedItem() != null
                && (indexedItem.getFeedItem().getId() == getId() || indexedItem.getFeedItem().getExternalRefId() == getExternalRefId())) {
                return true;
            }
        }

        if (!(o instanceof  FeedItem)) {
            return false;
        }

        FeedItem item = (FeedItem) o;
        if (this.getExternalRefId() != null && item.getExternalRefId() != null) {
            return this.getExternalRefId().equals(item.getExternalRefId());
        }

        return this.getId() == item.getId();
    }

    @Override
    public int hashCode() {
        if (this.getExternalRefId() != null) {
            return this.getExternalRefId().hashCode();
        }

        return String.valueOf(this.getId()).hashCode();
    }
}
