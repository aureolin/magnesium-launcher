package co.aureolin.labs.magnesium.util.handler;

import co.aureolin.labs.magnesium.model.SocialMediaAccount;

/**
 * Created by akinwale on 11/02/2016.
 */
public interface SocialMediaAccountSavedHandler {
    public void onSocialMediaAccountSaved(SocialMediaAccount account);
}
