package co.aureolin.labs.magnesium.model;

import java.util.Date;

public class IndexedItem extends BaseItem {
    private String refId;

    private long feedItemId;

    private String mimeType;

    private String metaFieldName;

    private String metaContent;

    private FeedItem feedItem;

    private Date indexedOn;

    public IndexedItem() {

    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public long getFeedItemId() {
        return feedItemId;
    }

    public void setFeedItemId(long feedItemId) {
        this.feedItemId = feedItemId;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getMetaFieldName() {
        return metaFieldName;
    }

    public void setMetaFieldName(String metaFieldName) {
        this.metaFieldName = metaFieldName;
    }

    public String getMetaContent() {
        return metaContent;
    }

    public void setMetaContent(String metaContent) {
        this.metaContent = metaContent;
    }

    public FeedItem getFeedItem() {
        return feedItem;
    }

    public void setFeedItem(FeedItem feedItem) {
        this.feedItem = feedItem;
    }

    public Date getIndexedOn() {
        return indexedOn;
    }

    @Override
    public String getReferenceId() {
        return refId;
    }

    public void setIndexedOn(Date indexedOn) {
        this.indexedOn = indexedOn;
    }

    @Override
    public Date getTimestamp() {
        Date createdOn = getCreatedOn();
        return (createdOn != null) ? createdOn : indexedOn;
    }

    @Override
    public String getValue() {
        return metaContent;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof BaseItem)) {
            return false;
        }

        if (o instanceof FeedItem) {
            FeedItem thisFeedItem = (FeedItem) o;
            return thisFeedItem.equals(feedItem);
        }

        IndexedItem item = (IndexedItem) o;
        if (this.getRefId() != null && item.getRefId() != null) {
            return this.getRefId().equals(item.getRefId());
        }
        if (this.getFeedItem() != null && item.getFeedItem() != null) {
            return this.getFeedItem().equals(item.getFeedItem());
        }

        return this.getId() == item.getId();
    }

    @Override
    public int hashCode() {
        if (this.getRefId() != null) {
            return this.getRefId().hashCode();
        }

        return String.valueOf(this.getId()).hashCode();
    }
}
