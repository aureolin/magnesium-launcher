package co.aureolin.labs.magnesium.fragment.settings;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.view.ActionMode;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.aureolin.labs.magnesium.R;
import co.aureolin.labs.magnesium.SettingsActivity;
import co.aureolin.labs.magnesium.adapter.SocialMediaAccountAdapter;
import co.aureolin.labs.magnesium.dialog.SocialMediaOauthDialog;
import co.aureolin.labs.magnesium.model.SocialMediaAccount;
import co.aureolin.labs.magnesium.util.Helper;
import co.aureolin.labs.magnesium.util.handler.SocialMediaAccountSavedHandler;
import co.aureolin.labs.magnesium.util.db.MagnesiumDbContext;

/**
 * Created by akinwale on 11/02/2016.
 */
public class SocialMediaAccountsFragment extends Fragment implements ActionMode.Callback {

    private boolean deleteInProgress;

    private ActionMode actionMode;

    private Map<String, Integer> numSocialAccounts;

    // Load existing social media accounts
    private ListView socialMediaAccountsList;

    private SocialMediaAccountAdapter adapter;

    private FloatingActionButton addButton;

    private View noSocialMediaAccounts;

    public static SocialMediaAccountsFragment newInstance() {
        return new SocialMediaAccountsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_socialmedia_accounts, container, false);
        socialMediaAccountsList = (ListView) rootView.findViewById(R.id.socialmedia_accounts_list);
        noSocialMediaAccounts = rootView.findViewById(R.id.no_socialmedia_accounts_view);

        socialMediaAccountsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                if (actionMode != null) {
                    if (socialMediaAccountsList.getCheckedItemCount() == 0) {
                        actionMode.finish();
                    } else {
                        actionMode.setTitle(String.valueOf(socialMediaAccountsList.getCheckedItemCount()));
                    }
                } else {
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            socialMediaAccountsList.setItemChecked(position, false);
                        }
                    });
                }
            }
        });
        socialMediaAccountsList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, final View view, final int position, long id) {
                SettingsActivity activity = (SettingsActivity) getContext();
                if (activity != null) {
                    if (actionMode != null) {
                        new Handler().post(new Runnable() {
                            @Override
                            public void run() {
                            socialMediaAccountsList.setItemChecked(position, !socialMediaAccountsList.isItemChecked(position));
                            if (socialMediaAccountsList.getCheckedItemCount() == 0) {
                                actionMode.finish();
                            } else {
                                actionMode.setTitle(String.valueOf(socialMediaAccountsList.getCheckedItemCount()));
                            }
                            }
                        });
                    } else {
                        //firstSelectedItemPosition = position;
                        actionMode = activity.startSupportActionMode(SocialMediaAccountsFragment.this);
                        socialMediaAccountsList.setItemChecked(position, true);
                        actionMode.setTitle(String.valueOf(socialMediaAccountsList.getCheckedItemCount()));
                    }
                }

                return true;
            }
        });

        addButton = (FloatingActionButton) rootView.findViewById(R.id.socialmedia_add_account_button);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SocialMediaOauthDialog dialog = new SocialMediaOauthDialog();
                dialog.setNumSocialAccountsMap(numSocialAccounts);
                dialog.setSavedHandler(new SocialMediaAccountSavedHandler() {
                    @Override
                    public void onSocialMediaAccountSaved(SocialMediaAccount account) {
                        loadSocialMediaAccounts();
                    }
                });
                dialog.show(getFragmentManager(), "SocialMediaOauthDialog");
            }
        });

        return rootView;
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        MenuInflater inflater = mode.getMenuInflater();
        inflater.inflate(R.menu.menu_generic_list_select_mode, menu);

        MenuItem editMenuItem = menu.findItem(R.id.action_item_edit);
        editMenuItem.setVisible(false);

        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        if (socialMediaAccountsList != null) {
            for (int i = 0; i < adapter.getCount(); i++) {
                socialMediaAccountsList.setItemChecked(i, false);
            }
        }
        actionMode = null;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_item_delete:
                if (!deleteInProgress) {
                    final List<SocialMediaAccount> accountsToDelete = new ArrayList<SocialMediaAccount>();
                    if (adapter != null && socialMediaAccountsList != null) {
                        List<Integer> positions = new ArrayList<Integer>();
                        SparseBooleanArray checkedPositions = socialMediaAccountsList.getCheckedItemPositions();
                        for (int i = 0; i < adapter.getCount(); i++) {
                            SocialMediaAccount account = adapter.getItem(i);
                            if (checkedPositions.get(i)) {
                                accountsToDelete.add(account);
                            }
                        }
                    }

                    (new AsyncTask<Void, Void, Boolean>() {
                        protected void onPreExecute() {
                            deleteInProgress = true;
                        }
                        protected Boolean doInBackground(Void... params) {
                            SettingsActivity activity = (SettingsActivity) getContext();
                            if (activity != null) {
                                SQLiteDatabase db = activity.getDbContext().getWritableDatabase();
                                try {
                                    for (int i = 0; i < accountsToDelete.size(); i++) {
                                        MagnesiumDbContext.delete(accountsToDelete.get(i), db);
                                    }
                                    return true;
                                } catch (Exception ex) {
                                    return false;
                                }
                            }

                            return false;
                        }
                        protected void onPostExecute(Boolean result) {
                            deleteInProgress = false;
                            Context context = getContext();
                            if (!result) {
                                Helper.showToast("The selection could not be deleted.", context);
                                return;
                            }

                            loadSocialMediaAccounts();
                            Helper.showToast("The selection was successfully deleted.", context);
                            if (actionMode != null) {
                                actionMode.finish();
                            }
                        }
                    }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
                return true;
        }
        return false;
    };

    @Override
    public void onStart() {
        super.onStart();
        setupAdapter();
        loadSocialMediaAccounts();
    }

    private void initSocialAccountsMap() {
        if (numSocialAccounts == null) {
            numSocialAccounts = new HashMap<String, Integer>();
        }
        for (int i = 0; i < Helper.VALID_ACCOUNT_TYPES.size(); i++) {
            numSocialAccounts.put(Helper.VALID_ACCOUNT_TYPES.get(i), 0);
        }
    }

    private void loadSocialMediaAccounts() {
        (new AsyncTask<Void, Void, List<SocialMediaAccount>>() {
            protected List<SocialMediaAccount> doInBackground(Void... params) {
                SettingsActivity activity = (SettingsActivity) getContext();
                if (activity != null) {
                    SQLiteDatabase db = activity.getDbContext().getReadableDatabase();
                    return MagnesiumDbContext.getSocialMediaAccounts(db);
                }
                return new ArrayList<SocialMediaAccount>();
            }

            @Override
            protected void onPostExecute(List<SocialMediaAccount> accounts) {
                setupAdapter(accounts, false);
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void setupAdapter() {
        setupAdapter(null, true);
    }

    private void setupAdapter(List<SocialMediaAccount> accounts, boolean initialSetup) {
        boolean isNewAdapter = false;
        if (adapter == null) {
            adapter = new SocialMediaAccountAdapter(getContext());
            socialMediaAccountsList.setAdapter(adapter);
            isNewAdapter = true;
        }
        if (!isNewAdapter) {
            adapter.clear();
        }

        if (accounts != null) {
            initSocialAccountsMap();
            for (int i = 0; i < accounts.size(); i++) {
                String accountType = accounts.get(i).getSocialAccountType();
                numSocialAccounts.put(accountType, numSocialAccounts.get(accountType) + 1);
            }
            adapter.addAll(accounts);
        }

        if (!initialSetup) {
            boolean hasAccounts = (adapter != null && adapter.getCount() > 0);
            noSocialMediaAccounts.setVisibility(hasAccounts ? View.INVISIBLE : View.VISIBLE);
        }

        if (!isNewAdapter) {
            adapter.notifyDataSetChanged();
        }
    }
}
