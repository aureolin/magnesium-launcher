package co.aureolin.labs.magnesium.model;

import java.util.Date;

/**
 * Created by akinwale on 16/02/2016.
 */
public class RssFeed {
    private long id;

    private String title;

    private String url;

    private Date lastSyncedOn;

    public RssFeed() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Date getLastSyncedOn() {
        return lastSyncedOn;
    }

    public void setLastSyncedOn(Date lastSyncedOn) {
        this.lastSyncedOn = lastSyncedOn;
    }
}
