package co.aureolin.labs.magnesium.adapter;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import co.aureolin.labs.magnesium.R;
import co.aureolin.labs.magnesium.fragment.AppListFragment;
import co.aureolin.labs.magnesium.model.AppItem;
import co.aureolin.labs.magnesium.util.Helper;
import co.aureolin.labs.magnesium.util.listener.GenericItemClickListener;

/**
 * Created by akinwale on 12/09/2016.
 */
public class AppItemListAdapter extends RecyclerView.Adapter<AppItemListAdapter.ViewHolder> {

    private Context context;

    private Object lock;

    private String currentConstraint;

    private View noResultsContainer;

    private TextView noResultsView;

    private static final int VIEW_TYPE_FOLDER = -1000;

    private int textColor;

    private GenericItemClickListener itemClickListener;

    private List<AppItem> allAppsList;

    private List<AppItem> originalList;

    private List<AppItem> currentList;

    private View.OnDragListener itemDragListener;

    private boolean filterInProgress;

    public AppItemListAdapter(Context context) {
        this.context = context;
        lock = new Object();
        originalList = new ArrayList<AppItem>();
        currentList = new ArrayList<AppItem>();
    }

    public List<AppItem> getItems() {
        return new ArrayList<AppItem>(currentList);
    }

    public void setItems(List<AppItem> items) {
        this.originalList = items;
        synchronized (lock) {
            this.currentList = new ArrayList<AppItem>(originalList);
        }
    }

    public void setNoResultsContainer(View noResultsContainer) {
        this.noResultsContainer = noResultsContainer;
    }

    public void setNoResultsView(TextView noResultsView) {
        this.noResultsView = noResultsView;
    }

    public void setOriginalList(List<AppItem> appItemList) {
        originalList = new ArrayList<AppItem>(appItemList);
        if (Helper.isEmpty(currentConstraint)) {
            synchronized (lock) {
                currentList = new ArrayList<AppItem>(originalList);
            }
        }
    }

    public List<AppItem> getOriginalList() {
        return originalList;
    }

    public List<AppItem> getAllAppsList() {
        return allAppsList;
    }

    public void setAllAppsList(List<AppItem> allAppsList) {
        this.allAppsList = allAppsList;
    }

    public AppItem getItem(int position) {
        synchronized (lock) {
            return (currentList.size() > position) ? currentList.get(position) : null;
        }
    }

    public int indexOf(AppItem appItem) {
        synchronized (lock) {
            return currentList.indexOf(appItem);
        }
    }

    public int originalIndexOf(AppItem appItem) {
        synchronized (lock) {
            return originalList.indexOf(appItem);
        }
    }

    public void sortedAddItem(AppItem appItem, boolean notify) {
        synchronized (lock) {
            List<AppItem> copy = new ArrayList<AppItem>(currentList);
            copy.add(appItem);
            Collections.sort(copy);

            int targetIndex = copy.indexOf(appItem);
            if (currentList.size() > targetIndex) {
                currentList.add(targetIndex, appItem);
            } else {
                currentList.add(appItem);
            }


            if (notify) {
                targetIndex = currentList.indexOf(appItem);
                if (targetIndex > -1) {
                    notifyItemInserted(targetIndex);
                }
            }
        }

        originalList.add(appItem);
    }

    public void addItem(AppItem appItem, boolean notify) {
        synchronized (lock) {
            currentList.add(appItem);
            if (notify) {
                int index = currentList.indexOf(appItem);
                if (index > -1) {
                    notifyItemInserted(index);
                }
            }
        }

        originalList.add(appItem);
    }

    public AppItem removeItem(AppItem appItem, boolean notify) {
        AppItem removedItem = null;

        int index = indexOf(appItem);
        if (index > -1) {
            synchronized (lock) {
                removedItem = currentList.remove(index);
            }
            if (notify) {
                notifyItemRemoved(index);
            }
        }

        index = originalIndexOf(appItem);
        if (index > -1) {
            originalList.remove(index);
        }

        return removedItem;
    }

    public int getCurrentIndexForFolder(String uniqueId) {
        synchronized (lock) {
            for (int i = 0; i < currentList.size(); i++) {
                AppItem item = currentList.get(i);
                if (!item.isFolder()) {
                    continue;
                }
                if (uniqueId != null && uniqueId.equals(item.getUniqueId())) {
                    return i;
                }
            }
        }
        return -1;
    }

    public int getOriginalIndexForFolder(String uniqueId) {
        synchronized (lock) {
            for (int i = 0; i < originalList.size(); i++) {
                AppItem item = originalList.get(i);
                if (!item.isFolder()) {
                    continue;
                }
                if (uniqueId != null && uniqueId.equals(item.getUniqueId())) {
                    return i;
                }
            }
        }
        return -1;
    }

    public void addNewFolder(AppItem folder) {
        AppItem firstFolderItem = folder.getFolderItems().get(0);
        AppItem secondFolderItem = folder.getFolderItems().get(1);
        AppItem removedItem = null;

        int index = -1;
        synchronized (lock) {
            index = currentList.indexOf(firstFolderItem);
        }

        if (index > -1) {
            synchronized (lock) {
                currentList.add(index, folder);
            }
            notifyItemInserted(index);

            int nextIndex = index + 1;
            synchronized (lock) {
                removedItem = currentList.remove(nextIndex);
            }
            notifyItemRemoved(nextIndex);

            synchronized (lock) {
                index = currentList.indexOf(secondFolderItem);
            }

            if (index > -1) {
                synchronized (lock) {
                    removedItem = currentList.remove(index);
                }
                notifyItemRemoved(index);

                originalList.remove(removedItem);
            }

            originalList.remove(removedItem);
        } else {
            // If item not found in currentList, it's a folder-to-app drawer move
            sortedAddItem(folder, true);

            synchronized (lock) {
                index = currentList.indexOf(secondFolderItem);
            }

            if (index > -1) {
                synchronized (lock) {
                    removedItem = currentList.remove(index);
                }
                notifyItemRemoved(index);

                originalList.remove(removedItem);
            }
        }

        int folderIndex = -1;
        synchronized (lock) {
            folderIndex = currentList.indexOf(folder);
            if (folderIndex > -1) {
                removedItem = currentList.remove(folderIndex);
                currentList.add(0, removedItem);
                notifyItemMoved(folderIndex, 0);
            }
        }
    }

    public int getIndexForPackageName(String packageName) {
        synchronized (lock) {
            for (int i = 0; i < currentList.size(); i++) {
                if (packageName.equals(currentList.get(i).getPackageName())) {
                    return i;
                }
            }
        }

        return -1;
    }

    public int getTextColor() {
        return textColor;
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
    }

    public AppItem removeItem(int index) {
        synchronized (lock) {
            return currentList.remove(index);
        }
    }

    public void removeAppItemFromOriginalList(AppItem appItem) {
        synchronized (lock) {
            originalList.remove(appItem);
        }
    }

    public void filter(final String constraint) {
        if (filterInProgress) {
            return;
        }

        (new AsyncTask<Void, Void, List<AppItem>>() {
            @Override
            protected void onPreExecute() {
                filterInProgress = true;
                currentConstraint = constraint;
            }

            protected List<AppItem> doInBackground(Void... params) {
                List<AppItem> appItems = new ArrayList<AppItem>();

                if (constraint == null || constraint.length() == 0) {
                    int idxToRemove = -1;
                    for (int i = 0; i < originalList.size(); i++) {
                        if (originalList.get(i).isPlaceholder()) {
                            idxToRemove = i;
                            break;
                        }
                    }
                    if (idxToRemove > -1) {
                        originalList.remove(idxToRemove);
                    }


                    appItems = new ArrayList<AppItem>(originalList);
                } else {
                    String filterStr = constraint.trim();
                    boolean exactMatchFound = false;
                    for (int i = 0; i < originalList.size(); i++) {
                        AppItem thisItem = originalList.get(i);
                        String itemName = thisItem.getName();
                        if (itemName != null && itemName.toLowerCase().contains(filterStr.toLowerCase())) {
                            appItems.add(thisItem);
                        }
                        if (itemName != null && itemName.toLowerCase().equals(filterStr.toLowerCase())) {
                            exactMatchFound = true;
                        }
                    }

                    if (!exactMatchFound) {
                        /*AddressAlias placeholderAlias = new AddressAlias();
                        placeholderAlias.setPlaceholder(true);
                        placeholderAlias.setAlias(filterStr);
                        placeholderAlias.setAddress(filterStr);
                        filteredList.add(placeholderAlias);*/
                    }
                }

                return appItems;
            }

            protected void onPostExecute(List<AppItem> results) {
                synchronized (lock) {
                    currentList = results;
                }

                if (noResultsContainer != null && noResultsView != null) {
                    boolean hasResults = (currentList.size() > 0);
                    if (constraint != null) {
                        noResultsView.setText(String.format("No app matching \"%s\" was found.", constraint.toString()));
                    }
                    noResultsContainer.setVisibility(hasResults ? View.INVISIBLE : View.VISIBLE);
                }

                filterInProgress = false;
                notifyDataSetChanged();
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public int getItemCount() {
        synchronized (lock) {
            return (currentList != null ? currentList.size() : 0);
        }
    }

    public String getCurrentConstraint() {
        return currentConstraint;
    }

    public void setCurrentConstraint(String currentConstraint) {
        this.currentConstraint = currentConstraint;
    }

    public GenericItemClickListener getItemClickListener() {
        return itemClickListener;
    }

    public void setItemClickListener(GenericItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public void setItemDragListener(View.OnDragListener itemDragListener) {
        this.itemDragListener = itemDragListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_app_list_big, viewGroup, false);

        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.appTitle.setTypeface(Helper.getTypeface("normal", context));
        viewHolder.appTitle.setTextColor(textColor != 0 ? textColor : viewHolder.appTitle.getTextColors().getDefaultColor());
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        AppItem item = currentList.get(position);
        viewHolder.containerView.setTag(position);

        viewHolder.appIcon.setImageBitmap(null);
        viewHolder.folderCircle.setVisibility(item.isFolder() ? View.VISIBLE : View.INVISIBLE);

        // Load the App Icon
        if (item.isFolder()) {
            if (AppListFragment.FolderIconCache.containsKey(viewHolder.folderIcon)) {
                viewHolder.folderIcon.setImageBitmap(AppListFragment.FolderIconCache.get(item.getUniqueId()));
            } else {
                new AppListFragment.LoadFolderIconTask(item, viewHolder.folderIcon, context).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        } else {
            if (itemDragListener != null && !viewHolder.isDragListenerSet()) {
                viewHolder.containerView.setOnDragListener(itemDragListener);
                viewHolder.setDragListenerSet(true);
            }

            String fullActivityName = item.getFullActivityName();
            if (AppListFragment.AppIconCache.containsKey(fullActivityName)) {
                viewHolder.appIcon.setImageDrawable(AppListFragment.AppIconCache.get(fullActivityName));
            } else {
                new AppListFragment.LoadAppIconTask(item, viewHolder.appIcon, context).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        }

        viewHolder.appTitle.setText(item.getName());
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        protected View containerView;
        protected ImageView appIcon;
        protected ImageView folderIcon;
        protected TextView appTitle;
        protected View folderCircle;

        private boolean dragListenerSet;

        public boolean isDragListenerSet() {
            return dragListenerSet;
        }

        public void setDragListenerSet(boolean dragListenerSet) {
            this.dragListenerSet = dragListenerSet;
        }

        public View getContainerView() {
            return containerView;
        }

        public ViewHolder(final View containerView) {
            super(containerView);
            this.containerView = containerView;

            if (itemDragListener != null) {
                this.containerView.setOnDragListener(itemDragListener);
                dragListenerSet = true;
            }

            containerView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (itemClickListener != null) {
                        itemClickListener.onItemClick(getAdapterPosition(), containerView);
                    }
                }
            });
            containerView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    if (itemClickListener != null) {
                        itemClickListener.onItemLongClick(getAdapterPosition(), containerView);
                        return true;
                    }

                    return false;
                }
            });

            appIcon = (ImageView) containerView.findViewById(R.id.item_app_list_image);
            appTitle = (TextView) containerView.findViewById(R.id.item_app_list_title);
            folderIcon = (ImageView) containerView.findViewById(R.id.item_app_folder_icon);
            folderCircle = containerView.findViewById(R.id.item_app_folder_image);
        }
    }

    @Override
    public int getItemViewType(int position) {
        boolean isFolder;
        synchronized (lock) {
            isFolder = (currentList.size() > position) ? currentList.get(position).isFolder() : false;
        }

        return isFolder ? VIEW_TYPE_FOLDER : position;
    }
}
