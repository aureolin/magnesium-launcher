package co.aureolin.labs.magnesium.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.SeekBar;
import android.widget.TextView;

import co.aureolin.labs.magnesium.R;
import co.aureolin.labs.magnesium.util.ValueListener;

/**
 * Created by akinwale on 16/09/2016.
 */
public class NumberPickerDialog extends DialogFragment {

    private NumberPicker numberPicker;

    private TextView unitText;

    private int currentValue;

    private int minValue;

    private int maxValue;

    private String title;

    private String unit;

    private String pluralUnit;

    private ValueListener valueListener;

    private boolean dialogDisplayed;

    public static NumberPickerDialog newInstance(String title, int minValue, int maxValue, int currentValue, String unit, String pluralUnit, ValueListener valueListener) {
        NumberPickerDialog dialog = new NumberPickerDialog();
        dialog.setTitle(title);
        dialog.setMinValue(minValue);
        dialog.setMaxValue(maxValue);
        dialog.setCurrentValue(currentValue);
        dialog.setUnit(unit);
        dialog.setPluralUnit(pluralUnit);
        dialog.setValueListener(valueListener);

        return dialog;
    }

    public NumberPickerDialog() {

    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Context context = getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View dialogContent = inflater.inflate(R.layout.dialog_number_picker, null);

        numberPicker = (NumberPicker) dialogContent.findViewById(R.id.dialog_number_picker_npicker);
        unitText = (TextView) dialogContent.findViewById(R.id.dialog_number_picker_unit);

        numberPicker.setMinValue(minValue);
        numberPicker.setMaxValue(maxValue);
        numberPicker.setValue(currentValue);
        updateNumberPickerValue(false);

        numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                if (dialogDisplayed) {
                    updateNumberPickerValue(true);
                }
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        AlertDialog dialog = builder
                .setTitle(title)
                .setView(dialogContent)
                .create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                dialogDisplayed = true;
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if (valueListener != null) {
                    valueListener.onCompleted();
                }
            }
        });

        return dialog;
    }

    private void updateNumberPickerValue(boolean notifyValueListener) {
        currentValue = numberPicker.getValue();
        unitText.setText(currentValue == 1 ? unit : pluralUnit);
        if (notifyValueListener && valueListener != null) {
            valueListener.onValueChanged(currentValue);
        }
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getMinValue() {
        return minValue;
    }

    public void setMinValue(int minValue) {
        this.minValue = minValue;
    }

    public String getPluralUnit() {
        return pluralUnit;
    }

    public void setPluralUnit(String pluralUnit) {
        this.pluralUnit = pluralUnit;
    }

    public int getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(int currentValue) {
        this.currentValue = currentValue;
    }

    public int getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public ValueListener getValueListener() {
        return valueListener;
    }

    public void setValueListener(ValueListener valueListener) {
        this.valueListener = valueListener;
    }
}
