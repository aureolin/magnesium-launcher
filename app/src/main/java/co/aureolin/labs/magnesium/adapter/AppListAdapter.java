package co.aureolin.labs.magnesium.adapter;

import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import co.aureolin.labs.magnesium.HomeActivity;
import co.aureolin.labs.magnesium.R;
import co.aureolin.labs.magnesium.fragment.AppListFragment;
import co.aureolin.labs.magnesium.model.AppItem;
import co.aureolin.labs.magnesium.util.handler.AppListUninstallClickHandler;

/**
 * Created by akinwale on 07/02/2016.
 */
public class AppListAdapter extends ArrayAdapter<AppItem> {
    private final Object lock;

    private boolean appDockAdapter;

    private TextView noResultsView;

    private List<AppItem> originalList;

    private List<AppItem> currentList;

    public AppListAdapter(Context context) {
        super(context, 0);
        lock = new Object();
        originalList = new ArrayList<AppItem>();
        currentList = new ArrayList<AppItem>();
    }

    public void setNoResultsView(TextView noResultsView) {
        this.noResultsView = noResultsView;
    }

    public void setOriginalList(List<AppItem> appItemList) {
        originalList = new ArrayList<AppItem>(appItemList);
        currentList = new ArrayList<AppItem>(originalList);
    }

    public List<AppItem> getItems() {
        return currentList;
    }

    public boolean isAppDockAdapter() {
        return appDockAdapter;
    }

    public void setAppDockAdapter(boolean appDockAdapter) {
        this.appDockAdapter = appDockAdapter;
    }

    @Override
    public AppItem getItem(int position) {
        synchronized (lock) {
            return currentList.get(position);
        }
    }

    @Override
    public int getCount() {
        return currentList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public List<AppItem> getOriginalList() {
        return originalList;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                currentList = (ArrayList<AppItem>) results.values;

                if (noResultsView != null) {
                    boolean hasResults = (currentList.size() > 0);
                    if (constraint != null) {
                        noResultsView.setText(String.format("No app matching \"%s\" was found.", constraint.toString()));
                    }
                    noResultsView.setVisibility(hasResults ? View.INVISIBLE : View.VISIBLE);
                }

                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                List<AppItem> filteredList = new ArrayList<AppItem>();
                if (constraint == null || constraint.length() == 0) {
                    int idxToRemove = -1;
                    for (int i = 0; i < originalList.size(); i++) {
                        if (originalList.get(i).isPlaceholder()) {
                            idxToRemove = i;
                            break;
                        }
                    }
                    if (idxToRemove > -1) {
                        originalList.remove(idxToRemove);
                    }

                    results.count = originalList.size();
                    results.values = new ArrayList<AppItem>(originalList);
                } else {
                    String filterStr = constraint.toString().trim();
                    boolean exactMatchFound = false;
                    for (int i = 0; i < originalList.size(); i++) {
                        AppItem thisItem = originalList.get(i);
                        if (thisItem.getName().toLowerCase().contains(filterStr.toLowerCase())) {
                            filteredList.add(thisItem);
                        }
                        if (thisItem.getName().toLowerCase().equals(filterStr.toLowerCase())) {
                            exactMatchFound = true;
                        }
                    }

                    if (!exactMatchFound) {
                        /*AddressAlias placeholderAlias = new AddressAlias();
                        placeholderAlias.setPlaceholder(true);
                        placeholderAlias.setAlias(filterStr);
                        placeholderAlias.setAddress(filterStr);
                        filteredList.add(placeholderAlias);*/
                    }

                    results.count = filteredList.size();
                    results.values = filteredList;
                }

                return results;
            }
        };
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_app_list, null);
        }

        final AppItem appItem = getItem(position);
        TextView appNameView = (TextView) convertView.findViewById(R.id.item_app_list_title);
        appNameView.setTypeface(HomeActivity.getNormalTypeface(getContext()));
        appNameView.setVisibility(appDockAdapter ? View.GONE : View.VISIBLE);
        ImageView iconView = (ImageView) convertView.findViewById(R.id.item_app_list_image);

        if (appItem != null) {
            appNameView.setText(appItem.getName());

            String fullActivityName = appItem.getFullActivityName();
            iconView.setImageBitmap(null);
            if (AppListFragment.AppIconCache.containsKey(fullActivityName)) {
                iconView.setImageDrawable(AppListFragment.AppIconCache.get(fullActivityName));
            } else {
                new AppListFragment.LoadAppIconTask(appItem, iconView, getContext()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        } else {
            appNameView.setText(null);
            iconView.setImageBitmap(null);
        }

        return convertView;
    }

    public int getCurrentSelectedIndex() {
        for (int i = 0; i < getCount(); i++) {
            if (getItem(i).isSelected()) {
                return i;
            }
        }
        return -1;
    }

    /*private void removePlaceholderFromList(List<AppItem> appItemList) {
        int idxToRemove = -1;
        for (int i = 0; i < appItemList.size(); i++) {
            if (aliasList.get(i).isPlaceholder()) {
                idxToRemove = i;
            }
        }
        if (idxToRemove > -1) {
            appItemList.remove(idxToRemove);
        }
    }*/
}