package co.aureolin.labs.magnesium.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.CookieManager;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

import co.aureolin.labs.magnesium.HomeActivity;
import co.aureolin.labs.magnesium.R;
import co.aureolin.labs.magnesium.SettingsActivity;
import co.aureolin.labs.magnesium.fragment.settings.GeneralSettingsFragment;
import co.aureolin.labs.magnesium.model.SocialMediaAccount;
import co.aureolin.labs.magnesium.util.Helper;
import co.aureolin.labs.magnesium.util.Http;
import co.aureolin.labs.magnesium.util.handler.SocialMediaAccountSavedHandler;
import co.aureolin.labs.magnesium.util.db.MagnesiumDbContext;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.basic.DefaultOAuthConsumer;

/**
 * Created by akinwale on 11/02/2016.
 */
public class SocialMediaOauthDialog extends DialogFragment {

    public static final int DEFAULT_SOCIAL_ACCOUNT_LIMIT = 1;

    public static final int MAX_SOCIAL_ACCOUNT_LIMIT = 3;

    public static final String INSTAGRAM_CLASSIC_LOGIN = "https://www.instagram.com/accounts/login/?force_classic_login";

    public static final String INSTAGRAM_HOME = "https://www.instagram.com/";

    public static final String INSTAGRAM_LOGIN_SCRIPT_FORMAT =
            "javascript:(function() { " +
            "    document.forms[0].username.value = '%s';" +
            "    document.forms[0].password.value = '%s';" +
            "    var loginBtn = null;" +
            "    var btnElems = document.getElementsByTagName('input');" +
            "    for (var i = 0; i < btnElems.length; i++) {" +
            "        if ('log in' == btnElems[i].value.toLowerCase()) {" +
            "           loginBtn = btnElems[i];" +
            "           break;" +
            "        }" +
            "    }" +
            "    if (loginBtn) {" +
            "        loginBtn.click();" +
            "    }" +
            "}());";

    private Map<String, Integer> numSocialAccountsMap;

    private View socialFieldsContainer;

    private ObjectMapper objectMapper;

    private View spinnerContainer;

    private Spinner accountTypeSpinner;

    private WebView webView;

    private ProgressBar loadProgressBar;

    private EditText socialUsernameView;

    private EditText socialPasswordView;

    private SocialMediaAccountSavedHandler savedHandler;

    private String currentAccountType;

    private String lastLoadedUrl;

    private boolean instagramLoginRequestSent = false;

    private boolean loginInitialised = false;

    private int loginScriptExecCount;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Context context = getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View dialogContent = inflater.inflate(R.layout.dialog_socialmedia_oauth, null);
        spinnerContainer = dialogContent.findViewById(R.id.socialmedia_selector_container);
        loadProgressBar = (ProgressBar) dialogContent.findViewById(R.id.socialmedia_dialog_progress);
        socialFieldsContainer = dialogContent.findViewById(R.id.dialog_social_fields_container);
        socialUsernameView = (EditText) dialogContent.findViewById(R.id.dialog_social_username);
        socialPasswordView = (EditText) dialogContent.findViewById(R.id.dialog_social_password);

        accountTypeSpinner = (Spinner) dialogContent.findViewById(R.id.socialmedia_account_type_selector);
        ArrayAdapter<CharSequence> choicesAdapter = ArrayAdapter.createFromResource(context,
                R.array.socialmedia_types, android.R.layout.simple_spinner_item);
        choicesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        accountTypeSpinner.setAdapter(choicesAdapter);
        accountTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = (String) parent.getAdapter().getItem(position);
                String lcItem = item.toLowerCase();
                if (Helper.VALID_ACCOUNT_TYPES.contains(lcItem)) {
                    int maxLimit = DEFAULT_SOCIAL_ACCOUNT_LIMIT;
                    Context context = getContext();
                    if (context != null) {
                        int settingIndex = GeneralSettingsFragment.SETTING_INDEX_SOCIAL_MEDIA_ACCOUNT;
                        SharedPreferences preferences = context.getSharedPreferences(HomeActivity.COMMON_SHARED_PREF_NAME, Context.MODE_PRIVATE);
                        int purchased = preferences.getInt(GeneralSettingsFragment.SETTINGS_OWNED_KEYS[settingIndex], 0);
                        if (purchased == 1) {
                            maxLimit = MAX_SOCIAL_ACCOUNT_LIMIT;
                        }
                    }

                    int count = getSocialAccountCount(lcItem);
                    if (count >= maxLimit) {
                        handleSocialAccountCountError(item);
                        accountTypeSpinner.setSelection(0);
                        return;
                    }

                    spinnerContainer.setVisibility(View.GONE);
                    loadProgressBar.setVisibility(SocialMediaAccount.TYPE_INSTAGRAM.equals(lcItem) ? View.INVISIBLE : View.VISIBLE);

                    Dialog dialog = getDialog();
                    if (dialog != null) {
                        ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE).setVisibility(
                                SocialMediaAccount.TYPE_INSTAGRAM.equals(lcItem) ? View.VISIBLE : View.GONE);
                    }
                    socialFieldsContainer.setVisibility(SocialMediaAccount.TYPE_INSTAGRAM.equals(lcItem) ? View.VISIBLE : View.GONE);

                    if (SocialMediaAccount.TYPE_TWITTER.equals(lcItem)) {
                        startTwitterOauthFlow();
                    } else if (SocialMediaAccount.TYPE_FACEBOOK.equals(lcItem)) {
                        startFacebookOauthFlow();
                    } else if (SocialMediaAccount.TYPE_INSTAGRAM.equals(lcItem)) {
                        // Alternative Instagram stuff
                        loginScriptExecCount = 0;
                        instagramLoginRequestSent = false;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        webView = (WebView) dialogContent.findViewById(R.id.socialmedia_oauth_webview);
        webView.setFocusable(true);
        webView.setFocusableInTouchMode(true);
        webView.requestFocus(View.FOCUS_DOWN);
        webView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                    case MotionEvent.ACTION_UP:
                        if (!v.hasFocus()) {
                            v.requestFocus();
                        }
                        break;
                }
                return false;
            }
        });

        webView.setWebChromeClient(new WebChromeClient() {
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                if (consoleMessage.message().startsWith(">>MG:")) {
                    String message = consoleMessage.message().substring(5);
                    String username = socialUsernameView.getText().toString();
                    String password = socialPasswordView.getText().toString();

                    if (INSTAGRAM_CLASSIC_LOGIN.equals(lastLoadedUrl) && message.contains("js not-logged-in")) {
                        if (!loginInitialised) {
                            // Do login stuff
                            loginScriptExecCount++;
                            if (loginScriptExecCount >= 1) {
                                loginInitialised = true;
                            }

                            webView.loadUrl(String.format(INSTAGRAM_LOGIN_SCRIPT_FORMAT, username, password));
                        } else {
                            // Login failed, notify the user username and password credentials incorrect
                            handleInstagramAuthError();
                        }
                    } else if (INSTAGRAM_HOME.equals(lastLoadedUrl) && message.contains("js logged-in")) {
                        // Log in successful, save the instagram username / password
                        Helper.showToast("Magnesium Launcher successfully authenticated to Instagram.", context);

                        // Save the account and dismiss the dialog
                        final SocialMediaAccount account = new SocialMediaAccount();
                        account.setSocialAccountType(SocialMediaAccount.TYPE_INSTAGRAM);
                        account.setAccountId(username);
                        account.setDisplayName(username);
                        account.setOauthData(password);

                        (new AsyncTask<Void, Void, SocialMediaAccount>() {
                            protected SocialMediaAccount doInBackground(Void... params) {
                                SettingsActivity activity = (SettingsActivity) getContext();
                                if (activity != null) {
                                    SQLiteDatabase db = activity.getDbContext().getWritableDatabase();
                                    MagnesiumDbContext.insert(account, activity.getEncryptionKey(), db);

                                    return MagnesiumDbContext.getSocialMediaAccount(account.getAccountId(), account.getSocialAccountType(), db);
                                }

                                return null;
                            }

                            protected void onPostExecute(SocialMediaAccount account) {
                                if (account == null) {
                                    handleInstagramAccountSaveError();
                                    return;
                                }

                                if (savedHandler != null) {
                                    savedHandler.onSocialMediaAccountSaved(account);
                                }
                                currentAccountType = null;
                                dismiss();
                            }
                        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                }

                return false;
            }
        });
        webView.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                if (!url.startsWith("javascript:")) {
                    lastLoadedUrl = url;
                }

                boolean isInstagram = (INSTAGRAM_CLASSIC_LOGIN.equals(lastLoadedUrl) || INSTAGRAM_HOME.equals(lastLoadedUrl));
                if (loadProgressBar != null && !isInstagram) {
                    loadProgressBar.setVisibility(View.INVISIBLE);
                }
                if (url.startsWith(Helper.TWITTER_AUTHORIZE_URL)) {
                    toggleDialogCancelable(true);
                }

                if (webView != null) {
                    if (isInstagram) {
                        webView.loadUrl("javascript:console.log('>>MG:' + document.getElementsByTagName('html')[0].outerHTML);");
                    }
                    if (webView.getVisibility() != View.VISIBLE && !isInstagram) {
                        webView.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            @SuppressWarnings("deprecation")
            public boolean shouldOverrideUrlLoading(WebView view, final String url) {
                if (url != null) {
                    checkObjectMapper();

                    if (url.startsWith(Helper.MAGNESIUM_OAUTH_URL_PREFIX)) {
                        final String vars = url.substring(Helper.MAGNESIUM_OAUTH_URL_PREFIX.length() + 1);
                        final Map<String, String> queryParams = Helper.splitQuery(vars);

                        webView.setVisibility(View.GONE);
                        loadProgressBar.setVisibility(View.VISIBLE);
                        (new AsyncTask<Void, Void, SocialMediaAccount>() {
                            protected SocialMediaAccount doInBackground(Void... params) {
                                SocialMediaAccount account = new SocialMediaAccount();
                                account.setSocialAccountType(currentAccountType);

                                // POST for request to access token
                                if (SocialMediaAccount.TYPE_TWITTER.equals(currentAccountType)) {
                                    OAuthConsumer consumer = new DefaultOAuthConsumer(Helper.TWITTER_API_KEY, Helper.TWITTER_API_CONSUMER_SECRET);
                                    String authTokenResponse = Http.postWithConsumer(Helper.TWITTER_OAUTH_TOKEN_URL + "?" + vars, null, null, consumer);

                                    Map<String, String> authTokenParams = Helper.splitQuery(authTokenResponse);
                                    String oauthToken = authTokenParams.get("oauth_token");
                                    String oauthTokenSecret = authTokenParams.get("oauth_token_secret");
                                    if (Helper.isEmpty(oauthToken) || Helper.isEmpty(oauthTokenSecret)) {
                                        return null;
                                    }

                                    account.setAccountId(authTokenParams.get("screen_name"));
                                    account.setDisplayName(authTokenParams.get("screen_name"));
                                    account.setOauthData(String.format("oauth_token=%s&oauth_token_secret=%s", oauthToken, oauthTokenSecret));
                                } else if (SocialMediaAccount.TYPE_INSTAGRAM.equals(currentAccountType)) {
                                    if (!queryParams.containsKey("access_token")) {
                                        return null;
                                    }

                                    String oauthToken = queryParams.get("access_token");
                                    if (Helper.isEmpty(oauthToken)) {
                                        return null;
                                    }

                                    // Get name and display name for token
                                    account.setOauthData(String.format("%s=%s", Helper.OAUTH_TOKEN_KEY, oauthToken));
                                    try {
                                        String url = String.format(Helper.INSTAGRAM_USER_SELF_URL_FORMAT, Helper.getUrlEncodedString(oauthToken));
                                        String json = Http.get(url);
                                        JsonNode rootNode = objectMapper.readTree(json);
                                        if (rootNode.has("data")) {
                                            JsonNode dataNode = rootNode.get("data");
                                            account.setAccountId(Helper.getNodeValue("username", dataNode));
                                            account.setDisplayName(Helper.getNodeValue("full_name", dataNode));
                                            if (Helper.isEmpty(account.getDisplayName())) {
                                                account.setDisplayName(account.getAccountId());
                                            }
                                        }
                                    } catch (IOException ex) {
                                        return null;
                                    }
                                }

                                if (Helper.isEmpty(account.getAccountId()) || Helper.isEmpty(account.getDisplayName())) {
                                    return null;
                                }

                                SettingsActivity activity = (SettingsActivity) getContext();
                                if (activity != null) {
                                    SQLiteDatabase db = activity.getDbContext().getWritableDatabase();
                                    MagnesiumDbContext.insert(account, null, db);

                                    return MagnesiumDbContext.getSocialMediaAccount(account.getAccountId(), account.getSocialAccountType(), db);
                                }

                                return null;
                            }

                            protected void onPostExecute(SocialMediaAccount account) {
                                if (account == null) {
                                    handleAuthTokenVerificationError();
                                    return;
                                }

                                if (savedHandler != null) {
                                    savedHandler.onSocialMediaAccountSaved(account);
                                }
                                currentAccountType = null;
                                dismiss();
                            }
                        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        return true;
                    } else if (url.startsWith(Helper.FACEBOOK_OAUTH_REDIRECT_URL) && currentAccountType.equals(SocialMediaAccount.TYPE_FACEBOOK)) {
                        final String vars = url.substring(Helper.FACEBOOK_OAUTH_REDIRECT_URL.length() + 1);
                        final Map<String, String> queryParams = Helper.splitQuery(vars);

                        webView.setVisibility(View.GONE);
                        loadProgressBar.setVisibility(View.VISIBLE);
                        (new AsyncTask<Void, Void, SocialMediaAccount>() {
                            protected SocialMediaAccount doInBackground(Void... params) {
                                if (!queryParams.containsKey("access_token")) {
                                    return null;
                                }

                                String oauthToken = queryParams.get("access_token");
                                if (Helper.isEmpty(oauthToken)) {
                                    return null;
                                }

                                // Get the user info
                                SocialMediaAccount account = new SocialMediaAccount();
                                account.setSocialAccountType(currentAccountType);
                                account.setOauthData(String.format("%s=%s", Helper.OAUTH_TOKEN_KEY, oauthToken));

                                try {
                                    String url = Helper.getFacebookUrlWithAccessToken(Helper.FACEBOOK_GRAPH_ME_URL, oauthToken);
                                    String json = Http.get(url);
                                    JsonNode rootNode = objectMapper.readTree(json);
                                    account.setAccountId(Helper.getNodeValue("id", rootNode));
                                    account.setDisplayName(Helper.getNodeValue("name", rootNode));
                                } catch (IOException ex) {
                                    return null;
                                }

                                if (Helper.isEmpty(account.getAccountId()) || Helper.isEmpty(account.getDisplayName())) {
                                    return null;
                                }

                                // Save the social media account
                                SettingsActivity activity = (SettingsActivity) getContext();
                                if (activity != null) {
                                    SQLiteDatabase db = activity.getDbContext().getWritableDatabase();
                                    MagnesiumDbContext.insert(account, null, db);

                                    return MagnesiumDbContext.getSocialMediaAccount(account.getAccountId(), account.getSocialAccountType(), db);
                                }

                                return null;
                            }

                            protected void onPostExecute(SocialMediaAccount account) {
                                if (account == null) {
                                    handleAuthTokenVerificationError();
                                    return;
                                }

                                if (savedHandler != null) {
                                    savedHandler.onSocialMediaAccountSaved(account);
                                }
                                currentAccountType = null;
                                dismiss();
                            }
                        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                        return true;
                    }
                }
                return false;
            }
        });

        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final AlertDialog dialog = builder
                .setTitle(R.string.add_socialmedia_account)
                .setView(dialogContent)
                .setPositiveButton(R.string.add_account, null)
                .create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                final AlertDialog alertDialog = (AlertDialog) dialog;
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setVisibility(View.GONE);
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Helper.isEmpty(socialUsernameView.getText().toString())) {
                            Helper.showToast("Please specify your Instagram username to proceed.", context);
                            return;
                        }

                        if (Helper.isEmpty(socialPasswordView.getText().toString())) {
                            Helper.showToast("Please specify your Instagram password to proceed.", context);
                            return;
                        }

                        toggleDialogCancelable(false);
                        if (loadProgressBar != null) {
                            loadProgressBar.setVisibility(View.VISIBLE);
                        }

                        socialFieldsContainer.setVisibility(View.GONE);
                        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setVisibility(View.GONE);

                        //if (Build.VERSION.SDK_INT >= )
                        CookieManager.getInstance().removeAllCookie();
                        webView.loadUrl(INSTAGRAM_CLASSIC_LOGIN);
                    }
                });
            }
        });

        return dialog;
    }

    private void handleInstagramAuthError() {
        instagramLoginRequestSent = false;
        loginInitialised = false;
        loginScriptExecCount = 0;

        toggleDialogCancelable(true);

        if (loadProgressBar != null) {
            loadProgressBar.setVisibility(View.INVISIBLE);
        }
        if (webView != null) {
            webView.setVisibility(View.GONE);
        }
        if (socialFieldsContainer != null) {
            socialFieldsContainer.setVisibility(View.VISIBLE);
        }

        Dialog dialog = getDialog();
        if (dialog != null) {
            ((AlertDialog) dialog).getButton(DialogInterface.BUTTON_POSITIVE).setVisibility(View.VISIBLE);
        }

        Context context = getContext();
        if (context != null) {
            Helper.showToast("Magnesium Launcher could not authenticate to Instagram with the username / password specified. Please try again.", context);
        }
    }

    private void handleInstagramAccountSaveError() {
        if (loadProgressBar != null) {
            loadProgressBar.setVisibility(View.INVISIBLE);
        }
        if (webView != null) {
            webView.setVisibility(View.GONE);
        }
        if (socialFieldsContainer != null) {
            socialFieldsContainer.setVisibility(View.VISIBLE);
        }

        Dialog dialog = getDialog();
        if (dialog != null) {
            ((AlertDialog) dialog).getButton(DialogInterface.BUTTON_POSITIVE).setVisibility(View.VISIBLE);
        }

        Context context = getContext();
        if (context != null) {
            Helper.showToast("The Instagram account could not be saved. Please try again.", context);
        }
    }

    private void handleAuthTokenVerificationError() {
        if (loadProgressBar != null) {
            loadProgressBar.setVisibility(View.INVISIBLE);
        }
        if (webView != null) {
            webView.setVisibility(View.GONE);
        }
        if (accountTypeSpinner != null) {
            accountTypeSpinner.setSelection(0);
        }
        if (spinnerContainer != null) {
            spinnerContainer.setVisibility(View.VISIBLE);
        }
        Context context = getContext();
        if (context != null) {
            Toast.makeText(context, "The account authorisation process failed. Please try again.", Toast.LENGTH_SHORT).show();
        }
    }

    public void setSavedHandler(SocialMediaAccountSavedHandler savedHandler) {
        this.savedHandler = savedHandler;
    }

    private void toggleDialogCancelable(boolean cancelable) {
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.setCancelable(cancelable);
            dialog.setCanceledOnTouchOutside(cancelable);
        }
    }

    private void startTwitterOauthFlow() {
        currentAccountType = SocialMediaAccount.TYPE_TWITTER;
        (new AsyncTask<Void, Void, String>() {
            protected void onPreExecute() {
                toggleDialogCancelable(false);
            }
            protected String doInBackground(Void... params) {
                OAuthConsumer consumer = new DefaultOAuthConsumer(Helper.TWITTER_API_KEY, Helper.TWITTER_API_CONSUMER_SECRET);
                return Http.postWithConsumer(Helper.TWITTER_REQUEST_TOKEN_URL, null, null, consumer);
            }
            protected void onPostExecute(String response) {
                Context context = getContext();
                if (Helper.isEmpty(response)) {
                    handleResponseError(R.string.add_socialmedia_account_failed, context);
                    return;
                }

                Map<String, String> queryResponse = Helper.splitQuery(response);
                if (!queryResponse.containsKey("oauth_token") || Helper.isEmpty(queryResponse.get("oauth_token"))) {
                    handleResponseError(R.string.add_socialmedia_invalid_response, context);
                    return;
                }

                String url = String.format("%s?oauth_token=%s", Helper.TWITTER_AUTHORIZE_URL, queryResponse.get("oauth_token"));
                webView.loadUrl(url);
            }

            private void handleResponseError(int stringId, Context context) {
                currentAccountType = null;
                loadProgressBar.setVisibility(View.INVISIBLE);
                accountTypeSpinner.setSelection(0);
                spinnerContainer.setVisibility(View.VISIBLE);
                if (context != null) {
                    Toast.makeText(context, stringId, Toast.LENGTH_SHORT).show();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    private void startFacebookOauthFlow() {
        currentAccountType = SocialMediaAccount.TYPE_FACEBOOK;
        String url = String.format(Helper.FACEBOOK_OAUTH_DIALOG_URL_FORMAT,
            Helper.FACEBOOK_APP_ID,
            Helper.getUrlEncodedString(Helper.FACEBOOK_OAUTH_REDIRECT_URL),
            Helper.getCharacterSeparatedValues(",", Arrays.asList(Helper.FACEBOOK_SCOPES)));
        webView.loadUrl(url);
    }

    private void startInstagramOauthFlow() {
        currentAccountType = SocialMediaAccount.TYPE_INSTAGRAM;
        String url = String.format(Helper.INSTAGRAM_OAUTH_AUTHORIZE_URL_FORMAT,
            Helper.INSTAGRAM_CLIENT_ID,
            Helper.getUrlEncodedString(Helper.MAGNESIUM_OAUTH_URL_PREFIX),
            Helper.getCharacterSeparatedValues("+", Arrays.asList(Helper.INSTAGRAM_SCOPES)));
        webView.loadUrl(url);
    }

    private void checkObjectMapper() {
        if (objectMapper == null) {
            objectMapper = new ObjectMapper();
        }
    }

    public void setNumSocialAccountsMap(Map<String, Integer> numSocialAccountsMap) {
        this.numSocialAccountsMap = numSocialAccountsMap;
    }

    public int getSocialAccountCount(String accountType) {
        if (numSocialAccountsMap == null || !numSocialAccountsMap.containsKey(accountType)) {
            return 0;
        }
        return numSocialAccountsMap.get(accountType);
    }

    public void handleSocialAccountCountError(String accountType) {
        Context context = getContext();
        if (context != null) {
            Toast.makeText(context,
                    String.format(getString(R.string.socialmedia_account_count_error), accountType),
                    Toast.LENGTH_SHORT).show();
        }
    }
}
