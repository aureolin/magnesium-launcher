package co.aureolin.labs.magnesium.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import co.aureolin.labs.magnesium.R;
import co.aureolin.labs.magnesium.util.ValueListener;

/**
 * Created by akinwale on 15/09/2016.
 */
public class SliderValueSelectorDialog extends DialogFragment {

    private SeekBar seekBar;

    private TextView displayValueText;

    private int currentValue;

    private int maxValue;

    private String title;

    private String unit;

    private ValueListener valueListener;

    private boolean dialogDisplayed;

    public static SliderValueSelectorDialog newInstance(String title, int maxValue, int currentValue, String unit, ValueListener valueListener) {
        SliderValueSelectorDialog dialog = new SliderValueSelectorDialog();
        dialog.setTitle(title);
        dialog.setMaxValue(maxValue);
        dialog.setCurrentValue(currentValue);
        dialog.setUnit(unit);
        dialog.setValueListener(valueListener);

        return dialog;
    }

    public SliderValueSelectorDialog() {

    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Context context = getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View dialogContent = inflater.inflate(R.layout.dialog_slider_selector, null);

        seekBar = (SeekBar) dialogContent.findViewById(R.id.dialog_slider_seekbar);
        displayValueText = (TextView) dialogContent.findViewById(R.id.dialog_slider_display_value);

        seekBar.setProgress(currentValue);
        seekBar.setMax(maxValue);
        updateSeekbarValue(false);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (dialogDisplayed) {
                    updateSeekbarValue(true);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        AlertDialog dialog = builder
                .setTitle(title)
                .setView(dialogContent)
                .create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                dialogDisplayed = true;
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if (valueListener != null) {
                    valueListener.onCompleted();
                }
            }
        });

        return dialog;
    }

    private void updateSeekbarValue(boolean notifyValueListener) {
        int progress = seekBar.getProgress();
        currentValue = progress;

        String progressText = String.format("%d%s", progress, unit != null ? unit.trim() : "");
        displayValueText.setText(progressText);

        if (notifyValueListener && valueListener != null) {
            valueListener.onValueChanged(progress);
        }
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(int currentValue) {
        this.currentValue = currentValue;
    }

    public int getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public ValueListener getValueListener() {
        return valueListener;
    }

    public void setValueListener(ValueListener valueListener) {
        this.valueListener = valueListener;
    }
}
