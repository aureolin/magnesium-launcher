package co.aureolin.labs.magnesium.util;

import co.aureolin.labs.magnesium.adapter.FeedItemListAdapter;

/**
 * Created by akinwale on 18/09/2016.
 */
public interface SmsMessageSender {
    void sendMessage(String recipient, String message, FeedItemListAdapter.SmsMessageSenderCallback callback);
}
