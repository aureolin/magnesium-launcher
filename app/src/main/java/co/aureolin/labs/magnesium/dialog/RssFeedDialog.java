package co.aureolin.labs.magnesium.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import co.aureolin.labs.magnesium.HomeActivity;
import co.aureolin.labs.magnesium.R;
import co.aureolin.labs.magnesium.SettingsActivity;
import co.aureolin.labs.magnesium.fragment.settings.GeneralSettingsFragment;
import co.aureolin.labs.magnesium.model.RssFeed;
import co.aureolin.labs.magnesium.util.Helper;
import co.aureolin.labs.magnesium.util.RssFeedSavedHandler;
import co.aureolin.labs.magnesium.util.db.MagnesiumDbContext;

/**
 * Created by akinwale on 16/02/2016.
 */
public class RssFeedDialog extends DialogFragment {

    public static final int DEFAULT_RSS_FEED_LIMIT = 3;

    public static final int MAX_RSS_FEED_LIMIT = 10;

    private List<String> existingRssFeeds;

    private ValidateRssFeedTask validateTask;

    private EditText feedUrlInput;

    private int numRssFeeds;

    private RssFeedSavedHandler rssFeedSavedHandler;

    private View fieldsContainer;

    private ProgressBar loadProgressBar;

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Context context = getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View dialogContent = inflater.inflate(R.layout.dialog_rss_feed, null);
        fieldsContainer = dialogContent.findViewById(R.id.dialog_rss_feed_fields_container);
        loadProgressBar = (ProgressBar) dialogContent.findViewById(R.id.dialog_rss_feed_progress);
        feedUrlInput = (EditText) dialogContent.findViewById(R.id.dialog_rss_feed_url);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        AlertDialog dialog = builder
            .setTitle(R.string.add_feed)
            .setView(dialogContent)
            .setPositiveButton(R.string.add_feed, null)
            .setNegativeButton(R.string.cancel_button_text, null)
            .create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
            ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                int maxLimit = DEFAULT_RSS_FEED_LIMIT;
                Context context = getContext();
                if (context != null) {
                    int settingIndex = GeneralSettingsFragment.SETTING_INDEX_RSS_FEED_LIMIT;
                    SharedPreferences preferences = context.getSharedPreferences(HomeActivity.COMMON_SHARED_PREF_NAME, Context.MODE_PRIVATE);
                    int purchased = preferences.getInt(GeneralSettingsFragment.SETTINGS_OWNED_KEYS[settingIndex], 0);
                    if (purchased == 1) {
                        maxLimit = MAX_RSS_FEED_LIMIT;
                    }
                }

                if (numRssFeeds > maxLimit) {
                    Helper.showToast(R.string.add_rss_feed_limit_error, context);
                    return;
                }

                String feedUrl = getInputString(feedUrlInput);
                if (!URLUtil.isValidUrl(feedUrl)) {
                    Helper.showToast("Please enter a valid feed URL.", context);
                    return;
                } else if (existingRssFeeds != null && existingRssFeeds.contains(feedUrl.toLowerCase())) {
                    Helper.showToast("The feed URL you entered already exists.", context);
                    return;
                }

                RssFeed feed = new RssFeed();
                feed.setUrl(feedUrl);
                new ValidateRssFeedTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, feed);
                }
            });
            }
        });

        return dialog;
    }

    public void setExistingRssFeeds(List<String> existingRssFeeds) {
        this.existingRssFeeds = existingRssFeeds;
    }

    public void setNumRssFeeds(int numRssFeeds) {
        this.numRssFeeds = numRssFeeds;
    }

    public void setRssFeedSavedHandler(RssFeedSavedHandler rssFeedSavedHandler) {
        this.rssFeedSavedHandler = rssFeedSavedHandler;
    }

    private class ValidateRssFeedTask extends AsyncTask<RssFeed, Void, Boolean> {
        private RssFeed rssFeed;

        @Override
        protected void onPreExecute() {
            toggleDialogCancelable(false);
            toggleButtonsVisible(false);
            fieldsContainer.setVisibility(View.GONE);
            loadProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Boolean doInBackground(RssFeed... params) {
            if (params == null || params.length == 0) {
                return false;
            }

            rssFeed = params[0];

            try {
                URL url = new URL(rssFeed.getUrl());
                SyndFeedInput input = new SyndFeedInput();
                SyndFeed feed = input.build(new XmlReader(url));
                rssFeed.setTitle(feed.getTitle());

                SettingsActivity activity = (SettingsActivity) getContext();
                if (activity != null) {
                    SQLiteDatabase db = activity.getDbContext().getWritableDatabase();
                    return (MagnesiumDbContext.insert(rssFeed, db) > 0);
                }
            } catch (Exception ex) {
                return false;
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            toggleDialogCancelable(true);
            Context context = getContext();

            if (!result) {
                Helper.showToast("The feed information could not be retrieved. Please verify that the URL is correct and try again.", context);
                if (loadProgressBar != null) {
                    loadProgressBar.setVisibility(View.INVISIBLE);
                }
                if (fieldsContainer != null) {
                    fieldsContainer.setVisibility(View.VISIBLE);
                }
                toggleButtonsVisible(true);

                validateTask = null;
                return;
            }

            if (rssFeedSavedHandler != null) {
                rssFeedSavedHandler.onRssFeedSaved(rssFeed);
            }
            validateTask = null;
            dismiss();
            //showToast("The RSS feed was successfully validated.");
        }
    }

    private String getInputString(EditText editText) {
        return editText.getText().toString();
    }

    private void toggleButtonsVisible(boolean visible) {
        Dialog dialog = getDialog();
        if (dialog != null) {
            ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE).setVisibility(visible ? View.VISIBLE : View.GONE);
            ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_NEGATIVE).setVisibility(visible ? View.VISIBLE : View.GONE);
        }
    }

    private void toggleDialogCancelable(boolean cancelable) {
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.setCancelable(cancelable);
            dialog.setCanceledOnTouchOutside(cancelable);
        }
    }
}
