package co.aureolin.labs.magnesium.model;

/**
 * Created by akinwale on 09/09/2016.
 */
public class IconPack implements Comparable<IconPack> {
    private boolean none;

    private String name;

    private String packageName;

    public IconPack() {

    }

    public boolean isNone() {
        return none;
    }

    public void setNone(boolean none) {
        this.none = none;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getSettingValue() {
        return String.format("%s=%s", name, packageName);
    }

    public static IconPack fromSettingValue(String value) {
        IconPack iconPack = new IconPack();

        String[] parts = value.split("=");
        if (parts.length > 0) {
            iconPack.setName(parts[0]);
        }
        if (parts.length > 1) {
            iconPack.setPackageName(parts[1]);
        }

        if ("None".equals(iconPack.getName())
                && (iconPack.getPackageName() == null || iconPack.getPackageName().trim().length() == 0)) {
            iconPack.setNone(true);
        }

        return iconPack;
    }

    public boolean equals(Object o) {
        if (o == null || !(o instanceof IconPack)) {
            return false;
        }

        return (packageName != null && packageName.equals(((IconPack) o).getPackageName()));
    }

    public int compareTo(IconPack iconPack) {
        return name.compareTo(iconPack.getName());
    }
}
