package co.aureolin.labs.magnesium.model;

import co.aureolin.labs.magnesium.util.Helper;

/**
 * Created by akinwale on 11/02/2016.
 */
public class OauthData {
    private long localAccountId;

    private String socialAccountId;

    private String token;

    private String tokenSecret;

    public OauthData() {

    }

    public OauthData(long localAccountId, String socialAccountId, String token, String tokenSecret) {
        this.localAccountId = localAccountId;
        this.socialAccountId = socialAccountId;
        this.token = token;
        this.tokenSecret = tokenSecret;
    }

    public long getLocalAccountId() {
        return localAccountId;
    }

    public void setLocalAccountId(long localAccountId) {
        this.localAccountId = localAccountId;
    }

    public String getSocialAccountId() {
        return socialAccountId;
    }

    public void setSocialAccountId(String socialAccountId) {
        this.socialAccountId = socialAccountId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTokenSecret() {
        return tokenSecret;
    }

    public void setTokenSecret(String tokenSecret) {
        this.tokenSecret = tokenSecret;
    }

    public boolean isValid() {
        return (!Helper.isEmpty(token));
    }
}
