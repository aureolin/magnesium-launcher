package co.aureolin.labs.magnesium.model;

/**
 * Created by akinwale on 09/09/2016.
 */
public class NavigationDrawerItem {
    private long filterId;

    private boolean all;

    private int feedItemType;

    private int numUnseenItems;

    private String name;

    private int drawableId;

    public NavigationDrawerItem() {

    }

    public NavigationDrawerItem(String name, boolean all, int drawableId) {
        this.name = name;
        this.all = all;
        this.drawableId = drawableId;
    }

    public NavigationDrawerItem(String name, int feedItemType, int drawableId) {
        this.name = name;
        this.feedItemType = feedItemType;
        this.drawableId = drawableId;
    }

    public int getNumUnseenItems() {
        return numUnseenItems;
    }

    public void setNumUnseenItems(int numUnseenItems) {
        this.numUnseenItems = numUnseenItems;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDrawableId() {
        return drawableId;
    }

    public void setDrawableId(int drawableId) {
        this.drawableId = drawableId;
    }

    public long getFilterId() {
        return filterId;
    }

    public void setFilterId(long filterId) {
        this.filterId = filterId;
    }

    public boolean isAll() {
        return all;
    }

    public void setAll(boolean all) {
        this.all = all;
    }

    public int getFeedItemType() {
        return feedItemType;
    }

    public void setFeedItemType(int feedItemType) {
        this.feedItemType = feedItemType;
    }
}
