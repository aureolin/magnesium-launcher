package co.aureolin.labs.magnesium.util.handler;

import co.aureolin.labs.magnesium.model.IconPack;

/**
 * Created by akinwale on 09/09/2016.
 */
public interface IconPackSelectedHandler {
    public void onIconPackSelected(IconPack iconPack);
}
