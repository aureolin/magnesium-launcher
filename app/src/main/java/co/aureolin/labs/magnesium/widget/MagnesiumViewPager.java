package co.aureolin.labs.magnesium.widget;

import android.app.WallpaperManager;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Point;
import android.os.IBinder;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

/**
 * Created by akinwale on 10/02/2016.
 */
public class MagnesiumViewPager extends ViewPager {
    private int displayWidth;

    private int displayHeight;

    private int initialItem;

    private float totalWidth;

    private IBinder windowToken;

    private WallpaperManager wallpaperManager;

    public MagnesiumViewPager(Context context) {
        super(context);
        init();
    }

    public MagnesiumViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        wallpaperManager = WallpaperManager.getInstance(getContext());
        WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point displaySize = new Point();

        display.getSize(displaySize);
        displayWidth = displaySize.x;
        displayHeight = displaySize.y;

        totalWidth = displayWidth * 2;
    }

    public WallpaperManager getWallpaperManager() {
        return wallpaperManager;
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (windowToken == null) {
            windowToken = getWindowToken();
        }
        if (wallpaperManager != null && windowToken != null) {
            int x = getScrollX();
            if (initialItem == 1) {
                x += displayWidth;
            }
            wallpaperManager.setWallpaperOffsets(windowToken, x / totalWidth, 0);
        }
    }

    public void setInitialItem(int initialItem) {
        this.initialItem = initialItem;
    }

    public int getInitialItem() {
        return initialItem;
    }
}
