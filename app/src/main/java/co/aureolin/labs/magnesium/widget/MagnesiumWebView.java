package co.aureolin.labs.magnesium.widget;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.webkit.WebView;

/**
 * Created by akinwale on 13/02/2016.
 */
public class MagnesiumWebView extends WebView {
    boolean layoutChangedOnce = false;

    public MagnesiumWebView(Context context) {
        super(context);
    }

    public MagnesiumWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b)
    {
        if (!layoutChangedOnce)
        {
            super.onLayout(changed, l, t, r, b);
            layoutChangedOnce = true;
        }
    }

    @Override
    protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect)
    {
        super.onFocusChanged(true, direction, previouslyFocusedRect);
    }

    @Override
    public boolean onCheckIsTextEditor()
    {
        return true;
    }
}
