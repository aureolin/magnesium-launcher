package co.aureolin.labs.magnesium.util;

/**
 * Created by akinwale on 16/09/2016.
 */
public interface ValueListener {
    void onValueChanged(int value);

    void onCompleted();
}
