package co.aureolin.labs.magnesium.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import co.aureolin.labs.magnesium.R;
import co.aureolin.labs.magnesium.adapter.IconPackAdapter;
import co.aureolin.labs.magnesium.model.IconPack;
import co.aureolin.labs.magnesium.util.Helper;
import co.aureolin.labs.magnesium.util.handler.IconPackSelectedHandler;

/**
 * Created by akinwale on 09/09/2016.
 */
public class IconPackSelectionDialog extends DialogFragment {

    private ProgressBar loadProgress;

    private ListView iconPackList;

    private View downloadIconPacksView;

    private IconPackAdapter iconPackAdapter;

    private IconPackSelectedHandler iconPackSelectedHandler;

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Context context = getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View dialogContent = inflater.inflate(R.layout.dialog_icon_pack, null);
        loadProgress = (ProgressBar) dialogContent.findViewById(R.id.dialog_icon_pack_progress);
        iconPackList = (ListView) dialogContent.findViewById(R.id.dialog_icon_pack_list);
        downloadIconPacksView = dialogContent.findViewById(R.id.dialog_icon_pack_download_text);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final AlertDialog dialog = builder
                .setTitle(R.string.select_icon_pack)
                .setView(dialogContent)
                .create();

        //((TextView) dialog.findViewById(R.id.alertTitle)).setTypeface(Helper.getTypeface("normal", context));

        downloadIconPacksView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://search?c=apps&q=icon+pack")));
                } catch (android.content.ActivityNotFoundException ex) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/search?c=apps&q=icon+pack")));
                }
            }
        });

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                (new AsyncTask<Void, Void, List<IconPack>>() {
                    @Override
                    protected void onPreExecute() {
                        if (loadProgress != null) {
                            loadProgress.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    protected List<IconPack> doInBackground(Void... params) {
                        try {
                            return loadAvailableIconPacks();
                        } catch (Exception ex) {
                            return null;
                        }
                    }

                    @Override
                    protected void onPostExecute(List<IconPack> iconPacks) {
                        if (loadProgress != null) {
                            loadProgress.setVisibility(View.INVISIBLE);
                        }


                        if (iconPacks == null) {
                            Toast.makeText(context, "The icon packs could not be loaded.", Toast.LENGTH_SHORT).show();
                            dismiss();
                            return;
                        }

                        iconPackAdapter = new IconPackAdapter(context);
                        iconPackAdapter.addAll(iconPacks);
                        if (iconPackList != null) {
                            iconPackList.setAdapter(iconPackAdapter);
                            iconPackAdapter.notifyDataSetChanged();
                        }
                    }
                }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);;
            }
        });

        iconPackList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                IconPack iconPack = (IconPack) parent.getItemAtPosition(position);
                if (iconPack != null && iconPackSelectedHandler != null) {
                    iconPackSelectedHandler.onIconPackSelected(iconPack);
                }
                dismiss();
            }
        });

        return dialog;
    }

    private List<IconPack> loadAvailableIconPacks() {
        Context context = getContext();
        List<IconPack> iconPacks = new ArrayList<IconPack>();

        PackageManager pm = getContext().getPackageManager();
        List<ResolveInfo> adwlauncherthemes = pm.queryIntentActivities(new Intent("org.adw.launcher.THEMES"), PackageManager.GET_META_DATA);
        List<ResolveInfo> golauncherthemes = pm.queryIntentActivities(new Intent("com.gau.go.launcherex.theme"), PackageManager.GET_META_DATA);

        List<ResolveInfo> rinfo = new ArrayList<ResolveInfo>(adwlauncherthemes);
        rinfo.addAll(golauncherthemes);

        for(ResolveInfo ri  : rinfo)
        {
            IconPack iconPack = new IconPack();
            iconPack.setPackageName(ri.activityInfo.packageName);

            if (!iconPacks.contains(iconPack)) {
                ApplicationInfo ai = null;
                try {
                    ai = pm.getApplicationInfo(iconPack.getPackageName(), PackageManager.GET_META_DATA);
                    iconPack.setName(context.getPackageManager().getApplicationLabel(ai).toString());

                    iconPacks.add(iconPack);
                } catch (PackageManager.NameNotFoundException e) {

                }
            }
        }

        Collections.sort(iconPacks);

        // Add the none icon pack
        IconPack nonePack = new IconPack();
        nonePack.setName("None");
        nonePack.setNone(true);
        iconPacks.add(0, nonePack);

        return iconPacks;
    }

    public IconPackSelectedHandler getIconPackSelectedHandler() {
        return iconPackSelectedHandler;
    }

    public void setIconPackSelectedHandler(IconPackSelectedHandler iconPackSelectedHandler) {
        this.iconPackSelectedHandler = iconPackSelectedHandler;
    }
}
