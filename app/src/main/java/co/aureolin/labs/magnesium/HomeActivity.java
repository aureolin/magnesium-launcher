package co.aureolin.labs.magnesium;

import android.Manifest;
import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Process;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.MobileAds;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.aureolin.labs.magnesium.adapter.AppItemListAdapter;
import co.aureolin.labs.magnesium.adapter.AppListAdapter;
import co.aureolin.labs.magnesium.adapter.NavigationDrawerItemAdapter;
import co.aureolin.labs.magnesium.fragment.AppListFragment;
import co.aureolin.labs.magnesium.fragment.HomeFeedFragment;
import co.aureolin.labs.magnesium.fragment.settings.GeneralSettingsFragment;
import co.aureolin.labs.magnesium.model.AppItem;
import co.aureolin.labs.magnesium.model.BaseItem;
import co.aureolin.labs.magnesium.model.IconPack;
import co.aureolin.labs.magnesium.model.NavigationDrawerItem;
import co.aureolin.labs.magnesium.service.CoreService;
import co.aureolin.labs.magnesium.service.IndexingService;
import co.aureolin.labs.magnesium.service.SyncService;
import co.aureolin.labs.magnesium.util.listener.AppListListener;
import co.aureolin.labs.magnesium.util.BitmapDiskCache;
import co.aureolin.labs.magnesium.util.ContactsCache;
import co.aureolin.labs.magnesium.util.listener.GenericItemClickListener;
import co.aureolin.labs.magnesium.util.Helper;
import co.aureolin.labs.magnesium.util.Http;
import co.aureolin.labs.magnesium.widget.MagnesiumViewPager;

public class HomeActivity extends AppCompatActivity {

    public static HomeActivity thisActivity;

    private static final int APP_DOCK_ICON_WIDTH_DP = 64;

    public static final int APP_GRID_COLUMN_COUNT = 4;

    public static int MAX_DOCK_ITEMS = 5;

    public static final List<String> IMAGE_LOADING_TASKS = new ArrayList<String>();

    public static Map<String, List<ImageView>> PENDING_IMAGE_VIEWS = new HashMap<String, List<ImageView>>();

    public static final String COMMON_SHARED_PREF_NAME = "co.aureolin.labs.magnesium.Common";

    public static final String SP_DEFAULT_KEY = "co.aureolin.labs.magnesium.DefaultKey";

    public static final String SP_ENC_KEY = "Z";

    public static String COUNTRY_ISO_CODE = "NG"; // Default country code

    public static final String APP_DOCK_POSITION_NONE = "none";

    public static final String APP_DOCK_POSITION_APP_LIST = "applist";

    public static final String APP_DOCK_POSITION_GLOBAL = "global";

    public static final String APP_DOCK_FAVOURITE_KEY_PREFIX = "AppDock.";

    public static final String VALUE_APP_DRAWER = "appdrawer";

    public static final String VALUE_FEED = "feed";

    public static final String DEFAULT_VALUE_ICON_PACK_NONE = "None=";

    public static final String SETTING_KEY_APP_DOCK_POSITION = "AppDockPosition";

    public static final String SETTING_KEY_PREFERRED_HOME_SCREEN = "PreferredHomeScreen";

    public static final String SETTING_KEY_ICON_PACK = "IconPack";

    public static final String SETTING_KEY_FOLDERS = "Folders";

    public static final String SETTING_KEY_BACKGROUND_TRANSPARENCY = "BackgroundTransparency";

    public static final String SETTING_KEY_APP_DOCK_TRANSPARENCY = "DockBackgroundTransparency";

    public static final float DEFAULT_BACKGROUND_TRANSPARENCY = 0.4f;

    public static final float DEFAULT_DOCK_TRANSPARENCY = 0.66f;

    private MagnesiumViewPager homeViewPager;

    private boolean refreshAppList;

    private int currentAppDockPosition;

    private boolean appListPopulated;

    private AppListFragment appListFragment;

    private HomeFeedFragment homeFeedFragment;

    private HomePagerAdapter homePagerAdapter;

    private RecyclerView navDrawerItems;

    private DrawerLayout navDrawerLayout;

    private NavigationDrawerItemAdapter navDrawerAdapter;

    private NavigationDrawerItem currentNavigationDrawerItem;

    private Intent coreServiceIntent;

    private Intent indexingServiceIntent;

    private Intent syncServiceIntent;

    private AppListAdapter appDockAdapter;

    private AppItemListAdapter appItemListAdapter;

    private String appDockPosition;

    private View appDockContainer;

    private GridView appDock;

    private GridView appListAppDock;

    private BroadcastReceiver syncUpdatesReceiver;

    private BroadcastReceiver timeChangeReceiver;

    private BroadcastReceiver packageManagementReceiver;

    private SharedPreferences preferences;

    private String encryptionKey;

    private View appSelector;

    private TextView appSelectorTitle;

    private ProgressBar appSelectorProgress;

    private RecyclerView appSelectorGrid;

    private List<AppItem> dockedAppItems;

    private boolean appDockLoaded;

    private List<Fragment> fragments;

    public static HomeActivity getHomeActivity() {
        return thisActivity;
    }

    private View mainView;

    private IconPack currentIconPack;

    private int appSelectorHeight;

    private boolean weatherFrequencyChanged;

    private boolean backgroundTransparencyChanged;

    private boolean dockBackgroundTransparencyChanged;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        thisActivity = this;
        setContentView(R.layout.activity_home);
        calculateMaxDockItems();

        preferences = getSharedPreferences(COMMON_SHARED_PREF_NAME, Context.MODE_PRIVATE);

        mainView = findViewById(R.id.main_view);
        updateMainViewTransparency();

        MobileAds.initialize(getApplicationContext(), "ca-app-pub-7531727856808736~5375055206");
        loadIconPack();

        initialiseLauncher();
        initialiseEncryptionKey();
        setupNavigationDrawer();

        if (currentIconPack != null && !currentIconPack.isNone()) {
            AppListFragment.InitIconPack(currentIconPack);
        }

        appSelector = findViewById(R.id.app_dock_app_selector_container);
        appSelectorHeight = -1;
        appSelector.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                //mainViewHeight = findViewById(R.id.main_view).getMeasuredHeight();
                if (appSelectorHeight == -1) {
                    appSelectorHeight = appSelector.getMeasuredHeight();
                }
            }
        });

        appSelectorTitle = (TextView) findViewById(R.id.app_selector_title);
        appSelectorTitle.setTypeface(Helper.getTypeface("normal", this));

        appSelectorGrid = (RecyclerView) appSelector.findViewById(R.id.app_selector_grid);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, HomeActivity.APP_GRID_COLUMN_COUNT);
        appSelectorGrid.setLayoutManager(layoutManager);
        //appSelectorGrid.addItemDecoration(new GridSpaceItemDecoration(12, 6, HomeActivity.APP_GRID_COLUMN_COUNT));

        appSelectorProgress = (ProgressBar) appSelector.findViewById(R.id.app_selector_progress);
        appSelector.findViewById(R.id.app_selector_close_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            closeAppSelector();
            }
        });

        appDockContainer = findViewById(R.id.app_dock_container);
        appDock = (GridView) findViewById(R.id.app_dock_grid);
        appDock.scrollTo(0, 8);
        appDock.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (isAppSelectorOpen()) {
                    closeAppSelector();
                }

                AppItem item = appDockAdapter.getItem(position);
                if (item != null) {
                    launchApp(item.getPackageName(), item.getActivityName());
                }
            }
        });
        appDock.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_MOVE) {
                    return true;
                }
                return false;
            }
        });
        appDock.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                showAppSelector(position);
                return true;
            }
        });

        /*appSelectorGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object adapterItem = parent.getAdapter().getItem(position);
                if (adapterItem != null && adapterItem instanceof AppItem) {
                    AppItem appItem = (AppItem) adapterItem;
                    setFavouriteApp(currentAppDockPosition, appItem);
                }
            }
        });*/

        if (coreServiceIntent == null) {
            coreServiceIntent = new Intent(this, CoreService.class);
        }
        if (indexingServiceIntent == null) {
            indexingServiceIntent = new Intent(this, IndexingService.class);
        }
        if (syncServiceIntent == null) {
            syncServiceIntent = new Intent(this, SyncService.class);
        }

        checkAppDock();
    }

    public void updateMainViewTransparency() {
        float alpha = preferences.getFloat(SETTING_KEY_BACKGROUND_TRANSPARENCY, DEFAULT_BACKGROUND_TRANSPARENCY);
        int argbAlpha = Float.valueOf((1.0f - alpha) * 255).intValue();
        if (mainView != null) {
            mainView.setBackgroundColor(Color.argb(argbAlpha, 0, 0, 0));
        }
    }

    public void updateAppDockTransparency() {
        float alpha = preferences.getFloat(SETTING_KEY_APP_DOCK_TRANSPARENCY, DEFAULT_DOCK_TRANSPARENCY);
        int argbAlpha = Float.valueOf((1.0f - alpha) * 255).intValue();
        int backgroundColour = Color.argb(argbAlpha, 0, 0, 0);

        View appListAppDockContainer = null;
        if (appListFragment != null) {
            View view = appListFragment.getView();
            if (view != null) {
                appListAppDockContainer = view.findViewById(R.id.app_list_app_dock_container);
                appListAppDockContainer.setBackgroundColor(backgroundColour);
            }
        }
        if (appDockContainer != null) {
            appDockContainer.setBackgroundColor(backgroundColour);
        }
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (Intent.ACTION_MAIN.equals(intent.getAction())) {
            onHomePressed();
        }

        setIntent(intent);
    }

    private void onHomePressed() {
        // Set current item to app view
        String preferredHomeScreen = preferences.getString(SETTING_KEY_PREFERRED_HOME_SCREEN, VALUE_APP_DRAWER);
        homeViewPager.setCurrentItem(VALUE_FEED.equals(preferredHomeScreen) ? 0 : 1);
    }

    private void initialiseLauncher() {
        if (homeFeedFragment == null) {
            homeFeedFragment = HomeFeedFragment.newInstance();
        }
        if (appListFragment == null) {
            appListFragment = AppListFragment.newInstance();
        }

        if (fragments == null) {
            fragments = new ArrayList<Fragment>();
            fragments.add(homeFeedFragment);
            fragments.add(appListFragment);
        }

        if (homePagerAdapter == null) {
            homePagerAdapter = new HomePagerAdapter(getSupportFragmentManager(), this, fragments);
        }

        if (homeViewPager == null) {
            homeViewPager = (MagnesiumViewPager) findViewById(R.id.home_view_pager);
        }
        homeViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                //homeViewPager.getWallpaperManager().setWallpaperOffsets(homeViewPager.getWindowToken(), (position + positionOffset), 0);
            }

            @Override
            public void onPageSelected(int position) {
                //handleViewPagerFocus(position);
                //if (position == 1) {
                //    if (!appDockLoaded) {
                //        initialLoadAppList();
                //        appDockLoaded = true;
                //    }
                //}
                setDrawerLocked(position == 1);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        homeViewPager.setAdapter(homePagerAdapter);

        String preferredHomeScreen = preferences.getString(SETTING_KEY_PREFERRED_HOME_SCREEN, VALUE_APP_DRAWER);
        int pagerItem = VALUE_FEED.equals(preferredHomeScreen) ? 0 : 1;
        homeViewPager.setInitialItem(pagerItem);
        homeViewPager.setCurrentItem(pagerItem);
        setDrawerLocked(true);
    }

    public void initialiseAppDock(List<AppItem> dockedApps) {
        checkAppDock();

        if (appDockAdapter == null) {
            appDockAdapter = new AppListAdapter(this);
            appDockAdapter.setAppDockAdapter(true);
        }
        appDock.setAdapter(appDockAdapter);
        if (appListAppDock != null) {
            appListAppDock.setAdapter(appDockAdapter);
        }
        appDockAdapter.clear();
        appDockAdapter.setOriginalList(dockedApps);
        appDockAdapter.notifyDataSetChanged();

        updateAppDockTransparency();
    }

    private static final int PERMISSION_REQUEST_ALL = 200;

    private void requestServicePermissions() {
        String[] servicePermissions = {
            Manifest.permission.READ_CALL_LOG,
            Manifest.permission.READ_CONTACTS,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.READ_SMS,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        };
        boolean hasAllPermissions = true;
        for (int i = 0; i < servicePermissions.length; i++) {
            hasAllPermissions = hasAllPermissions && Helper.hasPermission(servicePermissions[i], this);
        }

        if (!hasAllPermissions) {
            ActivityCompat.requestPermissions(this, servicePermissions, PERMISSION_REQUEST_ALL);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_ALL:
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        Helper.showToast(R.string.denied_all_permission, this, Toast.LENGTH_LONG);
                        break;
                    }
                }
                break;
        }
    }

    private void initialiseEncryptionKey() {
        encryptionKey = preferences.getString(SP_ENC_KEY, null);
        if (encryptionKey == null) {
            // Get a default encryption key
            TelephonyManager manager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            if (manager != null) {
                try {
                    encryptionKey = manager.getSubscriberId();
                    if (Helper.isEmpty(encryptionKey)) {
                        encryptionKey = manager.getLine1Number();
                    }
                    if (Helper.isEmpty(encryptionKey)) {
                        encryptionKey = manager.getDeviceId();
                    }
                } catch (SecurityException ex) {
                    // Ignore and try the rest of the options
                }
            }
            if (Helper.isEmpty(encryptionKey)) {
                encryptionKey = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
            }

            // Final fallback
            if (Helper.isEmpty(encryptionKey)) {
                encryptionKey = SP_DEFAULT_KEY;
            }
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(SP_ENC_KEY, encryptionKey);
            editor.commit();
        }
    }

    private void handleViewPagerFocus(int position) {
        Fragment fragment = homePagerAdapter.getItem(position);
        if (fragment != null && fragment.getView() != null) {
            View fragmentView = fragment.getView();
            if (position == 0) {
                fragmentView.findViewById(R.id.home_feed_date_time_container).requestFocus();
            } else if (position == 1) {
                fragmentView.findViewById(R.id.app_list_grid_view).requestFocus();
            }
        }
    }

    public String getEncrytionKey() {
        return encryptionKey;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (hasBackgroundTransparencyChanged()) {
            updateMainViewTransparency();
            backgroundTransparencyChanged = false;
        }

        if (hasDockBackgroundTransparencyChanged()) {
            updateAppDockTransparency();
            dockBackgroundTransparencyChanged = false;
        }

        if (hasWeatherFrequencyChanged()) {
            // Restart the core service
            if (coreServiceIntent == null) {
                coreServiceIntent = new Intent(this, CoreService.class);
            }

            if (isServiceRunning(CoreService.class)) {
                stopService(coreServiceIntent);
            }
            startService(coreServiceIntent);
            weatherFrequencyChanged = false;
        }

        // Receivers
        if (timeChangeReceiver == null) {
            timeChangeReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                final String action = intent.getAction();
                if ((action.equals(Intent.ACTION_TIME_TICK)
                        || action.equals(Intent.ACTION_TIME_CHANGED)
                        || action.equals(Intent.ACTION_TIMEZONE_CHANGED))
                        && homeFeedFragment != null /** check current **/) {
                    homeFeedFragment.updateDateAndTime();
                }
                }
            };
        }

        if (syncUpdatesReceiver == null) {
            syncUpdatesReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    if (homeFeedFragment != null) {
                        if (CoreService.ACTION_REFRESH_WEATHER.equals(intent.getAction())) {
                            homeFeedFragment.refreshWeather();
                        } else {
                            homeFeedFragment.loadHomeFeedItems(false, false);
                        }
                    }
                }
            };
        }

        /*if (packageManagementReceiver == null) {
            packageManagementReceiver = new PackageManagementBroadcastReceiver();
        }

        IntentFilter packageManagementFilter = new IntentFilter();
        packageManagementFilter.addAction(Intent.ACTION_PACKAGE_ADDED);
        packageManagementFilter.addAction(Intent.ACTION_PACKAGE_CHANGED);
        packageManagementFilter.addAction(Intent.ACTION_PACKAGE_REMOVED);
        packageManagementFilter.addAction(Intent.ACTION_PACKAGE_FULLY_REMOVED);
        registerReceiver(packageManagementReceiver, packageManagementFilter);*/

        IntentFilter timeChangeIntentFilter = new IntentFilter();
        timeChangeIntentFilter.addAction(Intent.ACTION_TIME_TICK);
        timeChangeIntentFilter.addAction(Intent.ACTION_TIME_CHANGED);
        timeChangeIntentFilter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
        registerReceiver(timeChangeReceiver, timeChangeIntentFilter);

        IntentFilter syncUpdatesFilter = new IntentFilter();
        syncUpdatesFilter.addAction(IndexingService.ACTION_CALL_LOG_INDEXED);
        syncUpdatesFilter.addAction(IndexingService.ACTION_SMS_INDEXED);
        syncUpdatesFilter.addAction(SyncService.ACTION_FEED_SYNCHED);
        syncUpdatesFilter.addAction(SyncService.ACTION_EMAIL_ACCOUNT_SYNCHED);
        syncUpdatesFilter.addAction(CoreService.ACTION_REFRESH_WEATHER);
        registerReceiver(syncUpdatesReceiver, syncUpdatesFilter);

        // Check app dock
        checkAppDock();
    }

    public void checkAppDock() {
        View appListAppDockContainer = null;
        if (appListFragment != null) {
            View view = appListFragment.getView();
            if (view != null) {
                appListAppDockContainer = view.findViewById(R.id.app_list_app_dock_container);
                appListAppDock = (GridView) view.findViewById(R.id.app_list_app_dock_grid);
            }
        }

        // Check the setting
        String configuredAppDockPosition = preferences.getString(SETTING_KEY_APP_DOCK_POSITION, APP_DOCK_POSITION_APP_LIST);
        if (APP_DOCK_POSITION_GLOBAL.equals(configuredAppDockPosition)) {
            setViewVisibility(appDockContainer, View.VISIBLE);
            setViewVisibility(appListAppDockContainer, View.GONE);
        } else if (APP_DOCK_POSITION_APP_LIST.equals(configuredAppDockPosition)) {
            setViewVisibility(appDockContainer, View.GONE);
            setViewVisibility(appListAppDockContainer, View.VISIBLE);
        } else {
            setViewVisibility(appDockContainer, View.GONE);
            setViewVisibility(appListAppDockContainer, View.GONE);
        }
    }

    public static void setViewVisibility(View view, int visibility) {
        if (view != null) {
            view.setVisibility(visibility);
        }
    }

    @Override
    public void onBackPressed() {
        homeViewPager.setCurrentItem(1); // Set current item as app list
    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Network[] networks = cm.getAllNetworks();
            NetworkInfo networkInfo;
            for (Network network : networks) {
                networkInfo = cm.getNetworkInfo(network);
                NetworkInfo.State networkState = networkInfo.getState();
                if (NetworkInfo.State.CONNECTED.equals(networkState)/* || NetworkInfo.State.CONNECTING.equals(networkState)*/) {
                    return true;
                }
            }
        } else {
            NetworkInfo wifiNetworkInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (wifiNetworkInfo != null && wifiNetworkInfo.isConnected()) {
                return true;
            }

            NetworkInfo mobileNetworkInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if (mobileNetworkInfo != null && mobileNetworkInfo.isConnected()) {
                return true;
            }
        }

        return false;
    }

    private void calculateMaxDockItems() {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        MAX_DOCK_ITEMS = Double.valueOf(Math.floor(dpWidth / APP_DOCK_ICON_WIDTH_DP)).intValue();
    }

    @Override
    protected void onStart() {
        super.onStart();
        requestServicePermissions();

        if (indexingServiceIntent != null && !isServiceRunning(IndexingService.class)) {
            startService(indexingServiceIntent);
        }
        if (syncServiceIntent != null && !isServiceRunning(SyncService.class)) {
            startService(syncServiceIntent);
        }
        if (coreServiceIntent != null && !isServiceRunning(CoreService.class)) {
            startService(coreServiceIntent);
        }

        TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (manager != null) {
            COUNTRY_ISO_CODE = manager.getSimCountryIso();
        }
        if (!Helper.isEmpty(HomeActivity.COUNTRY_ISO_CODE)) {
            COUNTRY_ISO_CODE = HomeActivity.COUNTRY_ISO_CODE.toUpperCase();
        } else {
            COUNTRY_ISO_CODE = getResources().getConfiguration().locale.getCountry();
        }
        ContactsCache.preInitCacheMap(COUNTRY_ISO_CODE, this);

        // Check app dock
        checkAppDock();
    }

    private boolean isServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public String getAppDockPosition() {
        return appDockPosition;
    }

    @Override
    public void onPause() {
        if (timeChangeReceiver != null) {
            try {
                unregisterReceiver(timeChangeReceiver);
            } catch (IllegalArgumentException ex) {
                // continue
            }
        }

        if (syncUpdatesReceiver != null) {
            try {
                unregisterReceiver(syncUpdatesReceiver);
            } catch (IllegalArgumentException ex) {
                // continue
            }
        }

        if (packageManagementReceiver != null) {
            try {
                unregisterReceiver(packageManagementReceiver);
            } catch (IllegalArgumentException ex) {
                // continue
            }
        }
        super.onPause();
    }

    public static class LoadImageAvatarTask extends AsyncTask<String, Void, Drawable> {
        private Context context;

        private ImageView destImageView;

        private String cacheKey;

        private Resources resources;

        public LoadImageAvatarTask(Context context, ImageView destImageView, String cacheKey, Resources resources) {
            this.context = context;
            this.destImageView = destImageView;
            this.cacheKey = cacheKey;
            this.resources = resources;
        }

        @Override
        protected void onPreExecute() {
            if (IMAGE_LOADING_TASKS.contains(cacheKey)) {
                cancel(true);
            }
            IMAGE_LOADING_TASKS.add(cacheKey);
        }

        @Override
        protected Drawable doInBackground(String... params) {
            // Check for a cached version
            if (params.length < 3 || params[0] == null) {
                return null;
            }

            String src = params[0];
            String id = params[1];
            String url = params[2];

            //String cacheKey = String.format("%s_%s", src, id);
            Bitmap bitmap = BitmapDiskCache.getBitmap(cacheKey);
            if (bitmap == null
                    && (context instanceof HomeActivity)
                    && ((HomeActivity) context).isOnline()) {
                // Retrieve the portrait from the web
                bitmap = Http.getImage(url, context);
            }

            if (bitmap != null) {
                // Convert the bitmap to circle portrait
                BitmapDiskCache.putBitmap(cacheKey, bitmap);
                return getRoundedDrawableForBitmap(bitmap, resources);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Drawable result) {
            IMAGE_LOADING_TASKS.remove(cacheKey);

            if (result == null) {
                // TODO: use a default drawable as the character portrait
                return;
            }

            if (destImageView != null) {
                destImageView.setImageDrawable(result);
                destImageView.setVisibility(View.VISIBLE);
            }
            if (PENDING_IMAGE_VIEWS.containsKey(cacheKey)) {
                List<ImageView> pendingViews = PENDING_IMAGE_VIEWS.get(cacheKey);
                for (int i = 0; i < pendingViews.size(); i++) {
                    ImageView imageView = pendingViews.get(i);
                    if (imageView != null) {
                        imageView.setImageDrawable(result);
                        imageView.setVisibility(View.VISIBLE);
                    }
                }
                PENDING_IMAGE_VIEWS.remove(cacheKey);
            }
        }
    }

    public static class LoadImageUrlTask extends AsyncTask<String, Void, Bitmap> {
        private Context context;

        private ImageView destImageView;

        private Resources resources;

        private String cacheKey;

        public LoadImageUrlTask(Context context, ImageView destImageView, String cacheKey) {
            this.context = context;
            this.destImageView = destImageView;
            this.cacheKey = cacheKey;
        }

        protected void onPreExecute() {
            if (IMAGE_LOADING_TASKS.contains(cacheKey)) {
                cancel(true);
            }
            IMAGE_LOADING_TASKS.add(cacheKey);
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            // Check for a cached version
            if (params.length < 3 || params[0] == null) {
                return null;
            }

            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND + Process.THREAD_PRIORITY_LESS_FAVORABLE);

            String src = params[0];
            String id = params[1];
            String url = params[2];

            Bitmap bitmap = BitmapDiskCache.getBitmap(cacheKey);
            if (bitmap == null
                    && (context instanceof HomeActivity)
                    && ((HomeActivity) context).isOnline()) {
                bitmap = Http.getImage(url, context);
            }

            if (bitmap != null) {
                // Convert the bitmap to circle portrait
                BitmapDiskCache.putBitmap(cacheKey, bitmap);
                return bitmap;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            IMAGE_LOADING_TASKS.remove(cacheKey);

            if (result == null) {
                return;
            }

            if (destImageView != null) {
                destImageView.setVisibility(View.VISIBLE);
                destImageView.setImageBitmap(result);
            }
        }
    }

    public static Drawable getRoundedDrawableForBitmap(Bitmap bitmap, Resources resources) {
        RoundedBitmapDrawable drawable = RoundedBitmapDrawableFactory.create(resources, bitmap);
        drawable.setCircular(true);
        drawable.setAntiAlias(true);
        return drawable;
    }

    public static Typeface getNormalTypeface(Context context) {
        return Helper.getTypeface("normal", context);
    }
    public static Typeface getItalicTypeface(Context context) {
        return Helper.getTypeface("italic", context);
    }
    public static Typeface getBoldTypeface(Context context) {
        return Helper.getTypeface("bold", context);
    }

    public HomeFeedFragment getHomeFeedFragment() {
        return homeFeedFragment;
    }

    public AppListFragment getAppListFragment() {
        return appListFragment;
    }

    public void launchApp(String packageName, String activityName) {
        try {
            ComponentName componentName = new ComponentName(packageName, activityName);
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_LAUNCHER);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setComponent(componentName);
            startActivity(intent);
        } catch (Exception ex) {
            Helper.showToast("The app could not be started.", this);
        }
    }

    public static class PackageManagementBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (thisActivity != null) {
                AppListFragment fragment = thisActivity.getAppListFragment();

                if (fragment != null) {
                    String action = intent.getAction();
                    if (Intent.ACTION_PACKAGE_REMOVED.equals(action) || Intent.ACTION_PACKAGE_FULLY_REMOVED.equals(action)) {
                        String packageNameWithPrefix = intent.getData().toString();
                        fragment.onPackageUninstalled(packageNameWithPrefix.substring("package:".length()));
                    } else {
                        fragment.refreshAppList();
                    }
                }
            }
        }
    }

    public void showAppSelector(int position) {
        if (isAppSelectorOpen()) {
            return;
        }

        if (appSelector != null) {
            currentAppDockPosition = position;
            if (appSelectorGrid != null) {
                appItemListAdapter = new AppItemListAdapter(this);
                appSelectorGrid.setAdapter(appItemListAdapter);

                appItemListAdapter.setItemClickListener(new GenericItemClickListener() {
                    @Override
                    public void onItemClick(int position, View v) {
                        AppItem appItem = appItemListAdapter.getItem(position);
                        if (appItem != null) {
                            setFavouriteApp(currentAppDockPosition, appItem);
                        }
                    }

                    @Override
                    public void onItemLongClick(int position, View v) {

                    }
                });

                List<AppItem> currentAppList = appListFragment.getCurrentAppList();
                if (currentAppList.size() == 0) {
                    if (appSelectorProgress != null) {
                        appSelectorProgress.setVisibility(View.VISIBLE);
                    }

                    if (appListFragment.getAppListListener() == null) {
                        appListFragment.setAppListListener(new AppListListener() {
                            @Override
                            public void onAppListInitialised() {
                                List<AppItem> appList = appListFragment.getCurrentAppList();
                                appItemListAdapter.setOriginalList(appList);
                                appItemListAdapter.notifyDataSetChanged();
                                if (appSelectorProgress != null) {
                                    appSelectorProgress.setVisibility(View.INVISIBLE);
                                }
                            }
                        });
                    }

                    appListFragment.refreshAppList();
                } else {
                    appItemListAdapter.setOriginalList(currentAppList);
                    appItemListAdapter.notifyDataSetChanged();
                }
            }

            openAppSelector();
        }
    }

    public void openAppSelector() {
        appSelector.getLayoutParams().height = 0;
        appSelector.setVisibility(View.VISIBLE);

        ValueAnimator valueAnimator = ValueAnimator.ofInt(0, appSelectorHeight);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                appSelector.getLayoutParams().height = (int) valueAnimator.getAnimatedValue();
                appSelector.requestLayout();
            }
        });
        valueAnimator.setInterpolator(new AccelerateInterpolator());
        valueAnimator.setDuration(100);
        valueAnimator.start();
    }

    public void closeAppSelector() {
        ValueAnimator valueAnimator = ValueAnimator.ofInt(appSelectorHeight, 0);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                appSelector.getLayoutParams().height = (int) valueAnimator.getAnimatedValue();
                appSelector.requestLayout();
            }
        });
        valueAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                if (appSelector != null) {
                    appSelector.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        valueAnimator.setInterpolator(new AccelerateInterpolator());
        valueAnimator.setDuration(100);
        valueAnimator.start();

        currentAppDockPosition = -1;
    }

    public boolean isAppSelectorOpen() {
        return (appSelector != null && appSelector.getLayoutParams().height > 0 && appSelector.getVisibility() == View.VISIBLE);
    }

    public void setFavouriteApp(int position, AppItem appItem) {
        final String configKey = String.format("%s%d", APP_DOCK_FAVOURITE_KEY_PREFIX, position);
        final String value = String.format("%s|%s", appItem.getPackageName(), appItem.getActivityName());
        (new AsyncTask<Void, Void, Void>() {
            protected Void doInBackground(Void... params) {
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString(configKey, value);
                editor.commit();
                return null;
            }
            protected void onPostExecute(Void result) {
                reloadAppDock();
                closeAppSelector();
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private List<AppItem> currentAppItems;

    public void initialLoadAppList() {
        (new AsyncTask<Void, Void, List<AppItem>>() {
            protected void onPreExecute() {
                currentAppItems = new ArrayList<AppItem>();
            }

            protected List<AppItem> doInBackground(Void... params) {
                PackageManager manager = getPackageManager();
                Intent i = new Intent(Intent.ACTION_MAIN, null);
                i.addCategory(Intent.CATEGORY_LAUNCHER);

                List<ApplicationInfo> installedApps = manager.getInstalledApplications(PackageManager.GET_META_DATA);
                List<ResolveInfo> availableActivities = manager.queryIntentActivities(i, 0);
                for (ResolveInfo ri : availableActivities) {
                    String packageName = ri.activityInfo.packageName;

                    AppItem app = new AppItem();
                    app.setName(ri.loadLabel(manager).toString());
                    app.setPackageName(packageName);
                    app.setActivityInfo(ri.activityInfo);
                    app.setActivityName(ri.activityInfo.name);
                    app.setRank(AppListFragment.DefaultAppsRank.containsKey(packageName) ? AppListFragment.DefaultAppsRank.get(packageName) : 99);
                    app.setSystemApp((getFlagsForPackageName(packageName, installedApps) & (ApplicationInfo.FLAG_SYSTEM | ApplicationInfo.FLAG_UPDATED_SYSTEM_APP)) != 0);
                    currentAppItems.add(app);
                }
                Collections.sort(currentAppItems);
                return currentAppItems;
            }

            protected void onPostExecute(List<AppItem> results) {
                reloadAppDock();
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private int getFlagsForPackageName(String packageName, List<ApplicationInfo> applicationInfo) {
        for (int i = 0; i < applicationInfo.size(); i++) {
            ApplicationInfo appInfo = applicationInfo.get(i);
            if (appInfo.packageName.equals(packageName)) {
                return appInfo.flags;
            }
        }
        return 0;
    }

    public void reloadAppDock() {
        if (currentAppItems == null || currentAppItems.size() == 0) {
            currentAppItems = appListFragment.getCurrentAppList();
        }
        (new AsyncTask<Void, Void, List<AppItem>>() {
            protected List<AppItem> doInBackground(Void... params) {
                List<AppItem> dockedItems = new ArrayList<AppItem>();

                for (int i = 0; i < MAX_DOCK_ITEMS; i++) {
                    boolean dockedAppFound = false;
                    String favouriteKey = String.format("%s%d", APP_DOCK_FAVOURITE_KEY_PREFIX, i);
                    String favouriteValue = preferences.getString(favouriteKey, null);
                    String packageName = null;
                    String activityName = null;
                    if (!Helper.isEmpty(favouriteValue)) {
                        String[] parts = favouriteValue.split("\\|");
                        if (parts.length > 1) {
                            packageName = parts[0];
                            activityName = parts[1];
                        }

                        if (!Helper.isEmpty(packageName)
                                && !Helper.isEmpty(activityName)) {
                            for (int j = 0; j < currentAppItems.size(); j++) {
                                AppItem currentAppItem = currentAppItems.get(j);
                                if (packageName.equals(currentAppItem.getPackageName())
                                        && activityName.equals(currentAppItem.getActivityName())) {
                                    dockedItems.add(currentAppItem);
                                    dockedAppFound = true;
                                    break;
                                }
                            }
                        }
                    }

                    if (!dockedAppFound && currentAppItems.size() > i) {
                        dockedItems.add(currentAppItems.get(i));
                    }
                }

                return dockedItems;
            }
            protected void onPostExecute(List<AppItem> dockedItems) {
                dockedAppItems = dockedItems;
                initialiseAppDock(dockedItems);
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public boolean isAppDockLoaded() {
        return appDockLoaded;
    }

    public void setAppDockLoaded(boolean appDockLoaded) {
        this.appDockLoaded = appDockLoaded;
    }

    public void setDockedAppItems(List<AppItem> dockedAppItems) {
        this.dockedAppItems = dockedAppItems;
    }

    public GridView getAppListAppDock() {
        return appListAppDock;
    }

    public void setAppListAppDock(GridView appListAppDock) {
        this.appListAppDock = appListAppDock;
    }

    public void loadIconPack() {
        currentIconPack = IconPack.fromSettingValue(preferences.getString(SETTING_KEY_ICON_PACK, DEFAULT_VALUE_ICON_PACK_NONE));
    }

    public boolean hasBackgroundTransparencyChanged() {
        return backgroundTransparencyChanged;
    }

    public void setBackgroundTransparencyChanged(boolean backgroundTransparencyChanged) {
        this.backgroundTransparencyChanged = backgroundTransparencyChanged;
    }

    public boolean hasDockBackgroundTransparencyChanged() {
        return dockBackgroundTransparencyChanged;
    }

    public void setDockBackgroundTransparencyChanged(boolean dockBackgroundTransparencyChanged) {
        this.dockBackgroundTransparencyChanged = dockBackgroundTransparencyChanged;
    }

    public boolean hasWeatherFrequencyChanged() {
        return weatherFrequencyChanged;
    }

    public void setWeatherFrequencyChanged(boolean weatherFrequencyChanged) {
        this.weatherFrequencyChanged = weatherFrequencyChanged;
    }

    public IconPack getCurrentIconPack() {
        if (currentIconPack == null) {
            currentIconPack = new IconPack();
            currentIconPack.setName("None");
            currentIconPack.setNone(true);
        }

        return currentIconPack;
    }

    public NavigationDrawerItem getCurrentNavigationDrawerItem() {
        return currentNavigationDrawerItem;
    }

    public void setDrawerLocked(boolean locked) {
        if (navDrawerLayout != null) {
            navDrawerLayout.setDrawerLockMode(locked ? DrawerLayout.LOCK_MODE_LOCKED_CLOSED : DrawerLayout.LOCK_MODE_UNLOCKED);
        }
    }

    public void closeDrawer() {
        navDrawerLayout.closeDrawer(Gravity.LEFT);
    }

    public boolean shouldRefreshAppList() {
        return refreshAppList;
    }

    public boolean canRemoveAds() {
        boolean removeAds = false;
        if (preferences != null) {
            int settingIndex = GeneralSettingsFragment.SETTING_INDEX_REMOVE_ADS;
            removeAds = (preferences.getInt(GeneralSettingsFragment.PRO_OWNED_KEY, 0) == 1)
                    || (preferences.getInt(GeneralSettingsFragment.SETTINGS_OWNED_KEYS[settingIndex], 0) == 1);
        }

        return removeAds;
    }

    public void setRefreshAppList(boolean refreshAppList) {
        this.refreshAppList = refreshAppList;
    }

    private void setupNavigationDrawer() {
        TextView navDrawerTitle = (TextView) findViewById(R.id.nav_drawer_universal_feed_title);
        Typeface normal = Helper.getTypeface("normal", this);
        navDrawerTitle.setTypeface(normal);
        ((TextView) findViewById(R.id.nav_drawer_settings_text)).setTypeface(normal);
        ((TextView) findViewById(R.id.nav_drawer_rate_app_text)).setTypeface(normal);

        navDrawerLayout = (DrawerLayout) findViewById(R.id.nav_drawer_layout);
        setDrawerLocked(true);

        navDrawerItems = (RecyclerView) findViewById(R.id.nav_drawer_items);
        if (navDrawerAdapter == null) {
            navDrawerAdapter = new NavigationDrawerItemAdapter(this, null);
        }

        navDrawerAdapter.setItemClickListener(new GenericItemClickListener() {
            @Override
            public void onItemLongClick(int position, View v) {

            }

            @Override
            public void onItemClick(int position, View v) {
                currentNavigationDrawerItem = navDrawerAdapter.getItem(position);
                closeDrawer();

                if (homeFeedFragment != null) {
                    homeFeedFragment.loadHomeFeedItems(false, true);
                }
            }
        });

        findViewById(R.id.nav_drawer_settings_item).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeDrawer();

                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setClassName("co.aureolin.labs.magnesium", SettingsActivity.class.getName());
                startActivity(intent);
            }
        });

        findViewById(R.id.nav_drawer_rate_app_item).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeDrawer();
                final String packageName = getPackageName();
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)));
                } catch (android.content.ActivityNotFoundException ex) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + packageName)));
                }
            }
        });

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        navDrawerItems.setLayoutManager(layoutManager);
        navDrawerItems.setItemAnimator(new DefaultItemAnimator());
        navDrawerItems.setAdapter(navDrawerAdapter);

        (new AsyncTask<Void, Void, List<NavigationDrawerItem>>() {
            @Override
            protected List<NavigationDrawerItem> doInBackground(Void... params) {
                List<NavigationDrawerItem> navigationDrawerItems = new ArrayList<NavigationDrawerItem>();

                // Add all items, call logs, text messages
                navigationDrawerItems.add(new NavigationDrawerItem("All Items", true, R.drawable.ic_forum_white_24dp));
                navigationDrawerItems.add(new NavigationDrawerItem("Call Logs", BaseItem.ITEM_TYPE_CALL_LOG, R.drawable.ic_phone_white_24dp));
                navigationDrawerItems.add(new NavigationDrawerItem("Email Messages", BaseItem.ITEM_TYPE_EMAIL_MESSAGE, R.drawable.ic_email_white_24dp));
                navigationDrawerItems.add(new NavigationDrawerItem("Facebook", BaseItem.ITEM_TYPE_FACEBOOK_POST, R.drawable.ic_facebook));
                navigationDrawerItems.add(new NavigationDrawerItem("Instagram", BaseItem.ITEM_TYPE_INSTAGRAM_POST, R.drawable.ic_instagram));
                navigationDrawerItems.add(new NavigationDrawerItem("RSS Feeds", BaseItem.ITEM_TYPE_RSS_FEED_ITEM, R.drawable.ic_rss_feed_white_24dp));
                navigationDrawerItems.add(new NavigationDrawerItem("Text Messages", BaseItem.ITEM_TYPE_SMS_MESSAGE, R.drawable.ic_sms_white_24dp));
                navigationDrawerItems.add(new NavigationDrawerItem("Twitter", BaseItem.ITEM_TYPE_TWITTER_POST, R.drawable.ic_twitter));

                // TODO: Individual email and social accounts
                return navigationDrawerItems;
            }

            @Override
            protected void onPostExecute(List<NavigationDrawerItem> navigationDrawerItems) {
                navDrawerAdapter.setItems(navigationDrawerItems);
                navDrawerAdapter.notifyDataSetChanged();
                //navDrawerItems.requestLayout();
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
}
