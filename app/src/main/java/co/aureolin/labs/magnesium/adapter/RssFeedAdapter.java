package co.aureolin.labs.magnesium.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import co.aureolin.labs.magnesium.R;
import co.aureolin.labs.magnesium.model.RssFeed;

public class RssFeedAdapter extends ArrayAdapter<RssFeed> {
    public RssFeedAdapter(Context context) {
        super(context, 0);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_string, null);
        }

        RssFeed item = getItem(position);

        TextView itemTextView = (TextView) convertView.findViewById(R.id.item_text);
        itemTextView.setText(item.getTitle());

        return convertView;
    }
}
