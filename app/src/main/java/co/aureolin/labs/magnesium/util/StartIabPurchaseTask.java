package co.aureolin.labs.magnesium.util;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.RemoteException;

import com.android.vending.billing.IInAppBillingService;

import co.aureolin.labs.magnesium.SettingsActivity;

/**
 * Created by akinwale on 18/02/2016.
 */
public class StartIabPurchaseTask extends AsyncTask<Void, Void, Bundle> {

    private IabPurchaseStartedCallback callback;

    private Context context;

    private String sku;

    private IInAppBillingService billingService;

    public StartIabPurchaseTask(String sku, IInAppBillingService billingService, Context context, IabPurchaseStartedCallback callback) {
        this.sku = sku;
        this.billingService = billingService;
        this.context = context;
        this.callback = callback;
    }

    protected Bundle doInBackground(Void... params) {
        try {
            if (context != null) {
                Bundle bundle = billingService.getBuyIntent(3, context.getPackageName(), sku, "inapp", null);
                return bundle;
            }
        } catch (RemoteException ex) {

        }
        return null;
    }

    protected void onPostExecute(Bundle result) {
        if (callback != null) {
            callback.onIabPurchaseStarted(result);
        }

        if (result == null) {
            Helper.showToast("The in-app purchase process could not be started at this time. Please try again later.", context);
            return;
        }

        int response = result.getInt("RESPONSE_CODE");
        if (response != 0) {
            Helper.showToast("The request to start the in-app purchase process failed. Please try again later.", context);
            return;
        }

        PendingIntent pendingIntent = result.getParcelable("BUY_INTENT");
        SettingsActivity activity = (SettingsActivity) context;
        try {
            activity.startIntentSenderForResult(
                    pendingIntent.getIntentSender(),
                    SettingsActivity.IAB_PURCHASE_REQUEST_CODE,
                    new Intent(), Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0));
        } catch (IntentSender.SendIntentException ex) {
            Helper.showToast("An error occurred while trying to start the in-app purchase process. Please try again later.", context);
        }
    }
}