package co.aureolin.labs.magnesium.fragment.settings;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import co.aureolin.labs.magnesium.HomeActivity;
import co.aureolin.labs.magnesium.R;
import co.aureolin.labs.magnesium.SettingsActivity;
import co.aureolin.labs.magnesium.adapter.HiddenAppListAdapter;
import co.aureolin.labs.magnesium.fragment.AppListFragment;
import co.aureolin.labs.magnesium.model.AppItem;
import co.aureolin.labs.magnesium.util.listener.GenericItemClickListener;
import co.aureolin.labs.magnesium.widget.GridSpaceItemDecoration;

/**
 * Created by akinwale on 11/09/2016.
 */
public class HiddenAppsFragment extends Fragment {
    private HiddenAppListAdapter adapter;

    private RecyclerView hiddenAppsGrid;

    private ProgressBar hiddenAppsProgress;

    private View noHiddenApps;

    public static HiddenAppsFragment newInstance() {
        return new HiddenAppsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_hidden_apps, container, false);

        int columnCount = 3;
        hiddenAppsGrid = (RecyclerView) rootView.findViewById(R.id.hidden_apps_grid);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(), columnCount);
        hiddenAppsGrid.setLayoutManager(layoutManager);
        hiddenAppsGrid.addItemDecoration(new GridSpaceItemDecoration(24, 64, columnCount));

        hiddenAppsProgress = (ProgressBar) rootView.findViewById(R.id.hidden_apps_progress);
        noHiddenApps = rootView.findViewById(R.id.hidden_apps_empty);

        return rootView;
    }

    public void onStart() {
        super.onStart();
        initHiddenApps();
    }

    public void onPause() {
        saveHiddenApps();
        super.onPause();
    }

    private void initHiddenApps() {
        final Context context = getContext();
        (new AsyncTask<Void, Void, List<AppItem>>() {
            protected void onPreExecute() {
                if (hiddenAppsProgress != null) {
                    hiddenAppsProgress.setVisibility(View.VISIBLE);
                }
            }

            protected List<AppItem> doInBackground(Void... params) {
                List<AppItem> items = new ArrayList<AppItem>();

                // Load the applications
                SettingsActivity activity = SettingsActivity.CurrentActivity;
                if (activity != null) {
                    SharedPreferences preferences = activity.getSharedPreferences(HomeActivity.COMMON_SHARED_PREF_NAME, Context.MODE_PRIVATE);
                    Set<String> hiddenPackageNames = preferences.getStringSet(PersonalisationFragment.HIDDEN_APPS_KEY, new HashSet<String>());

                    PackageManager manager = activity.getPackageManager();
                    Intent i = new Intent(Intent.ACTION_MAIN, null);
                    i.addCategory(Intent.CATEGORY_LAUNCHER);

                    List<ResolveInfo> availableActivities = manager.queryIntentActivities(i, 0);
                    for (ResolveInfo ri : availableActivities) {
                        String packageName = ri.activityInfo.packageName;

                        AppItem app = new AppItem();
                        app.setName(ri.loadLabel(manager).toString());
                        app.setPackageName(packageName);
                        app.setActivityInfo(ri.activityInfo);
                        app.setActivityName(ri.activityInfo.name);
                        app.setRank(AppListFragment.DefaultAppsRank.containsKey(packageName) ? AppListFragment.DefaultAppsRank.get(packageName) : 99);
                        app.setHidden(hiddenPackageNames.contains(app.getFullActivityName()));
                        items.add(app);
                    }
                    Collections.sort(items);
                }

                return items;
            }

            protected void onPostExecute(List<AppItem> results) {
                if (hiddenAppsProgress != null) {
                    hiddenAppsProgress.setVisibility(View.INVISIBLE);
                }
                if (adapter == null) {
                    adapter = new HiddenAppListAdapter(context, results);
                    adapter.setItemClickListener(new GenericItemClickListener() {
                        @Override
                        public void onItemClick(int position, View v) {
                            CheckBox checkBox = (CheckBox) ((v instanceof RelativeLayout) ?
                                v.findViewById(R.id.item_hidden_app_list_checkbox) : v);
                            if (checkBox != null) {
                                boolean hidden = (v instanceof RelativeLayout) ? !checkBox.isChecked() : checkBox.isChecked();
                                AppItem appItem = adapter.getItem(position);
                                appItem.setHidden(hidden);

                                adapter.notifyItemChanged(position);
                                saveHiddenApps();
                            }
                        }

                        @Override
                        public void onItemLongClick(int position, View v) {

                        }
                    });
                }
                hiddenAppsGrid.setAdapter(adapter);
                noHiddenApps.setVisibility(adapter.getItemCount() == 0 ? View.VISIBLE : View.INVISIBLE);
                adapter.notifyDataSetChanged();
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void saveHiddenApps() {
        final Set<String> hiddenPackageNames = new HashSet<String>();
        if (adapter != null) {
            for (int i = 0; i < adapter.getItemCount(); i++) {
                AppItem item = adapter.getItem(i);
                if (item.isHidden() && !hiddenPackageNames.contains(item.getFullActivityName())) {
                    hiddenPackageNames.add(item.getFullActivityName());
                }
            }

            (new AsyncTask<Void, Void, Void>() {
                protected Void doInBackground(Void... params) {
                    Context context = getContext();
                    if (context != null) {
                        SharedPreferences preferences = context.getSharedPreferences(HomeActivity.COMMON_SHARED_PREF_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putStringSet(PersonalisationFragment.HIDDEN_APPS_KEY, hiddenPackageNames);
                        editor.commit();
                    }
                    return null;
                }

                protected void onPostExecute(Void result) {
                    if (HomeActivity.thisActivity != null) {
                        HomeActivity.thisActivity.setRefreshAppList(true);
                        AppListFragment appListFragment = HomeActivity.thisActivity.getAppListFragment();
                        if (appListFragment != null) {
                            appListFragment.initialiseAppList(true);
                        }
                    }
                }
            }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }
}
