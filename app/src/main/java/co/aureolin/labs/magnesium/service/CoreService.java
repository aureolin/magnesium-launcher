package co.aureolin.labs.magnesium.service;

import android.Manifest;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.provider.CallLog;
import android.provider.Telephony;
import android.util.Log;

import java.security.Security;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import co.aureolin.labs.magnesium.HomeActivity;
import co.aureolin.labs.magnesium.fragment.settings.GeneralSettingsFragment;
import co.aureolin.labs.magnesium.fragment.settings.IndexingAndSearchFragment;
import co.aureolin.labs.magnesium.model.BaseItem;
import co.aureolin.labs.magnesium.model.FeedItem;
import co.aureolin.labs.magnesium.util.Helper;
import co.aureolin.labs.magnesium.util.db.MagnesiumDbContext;

/**
 * Created by akinwale on 21/02/2016.
 */
public class CoreService extends Service {

    private static final String TAG = CoreService.class.getCanonicalName();

    private static final Object lock = new Object();

    public static final String ACTION_REFRESH_WEATHER = "co.aureolin.labs.magnesium.intent.action.REFRESH_WEATHER";

    private static final int ONE_HOUR_MILLIS = 3600000;

    private Timer purgeTimer;

    private TimerTask purgeTimerTask;

    private Timer refreshWeatherTimer;

    private TimerTask refreshWeatherTimerTask;

    private static final int PURGE_INTERVAL = 60000 * 60 * 24; // Every 24 hours = 1 minute * 60 * 24

    private Context context;

    private MagnesiumDbContext dbContext;

    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        context = getApplicationContext();

        SharedPreferences preferences = getSharedPreferences(HomeActivity.COMMON_SHARED_PREF_NAME, Context.MODE_PRIVATE);
        int weatherRefreshHours = preferences.getInt(GeneralSettingsFragment.SETTING_KEY_WRI, 4);
        if (weatherRefreshHours > 0) {
            refreshWeatherTimerTask = new TimerTask() {
                @Override
                public void run() {
                    Helper.sendBroadcast(CoreService.ACTION_REFRESH_WEATHER, CoreService.this);
                }
            };
            refreshWeatherTimer = new Timer();
            refreshWeatherTimer.scheduleAtFixedRate(refreshWeatherTimerTask, 0, weatherRefreshHours * ONE_HOUR_MILLIS);
        }

        purgeTimerTask = new TimerTask() {
            @Override
            public void run() {
                purgeOldItems();
            }
        };
        purgeTimer = new Timer();
        purgeTimer.scheduleAtFixedRate(purgeTimerTask, 0, PURGE_INTERVAL);

        ContentObserver callLogObserver = new ContentObserver(new Handler()) {
            @Override
            public void onChange(boolean selfChange) {
                super.onChange(selfChange);
                if (!Helper.hasPermission(Manifest.permission.READ_CALL_LOG, context)) {
                    return;
                }

                SQLiteDatabase db = null;
                Cursor cursor = null;
                try {
                    dbContext = new MagnesiumDbContext(context);
                    db = dbContext.getWritableDatabase();


                    String selection = null;
                    String[] selectionArgs = null;
                    long latestTimestamp = MagnesiumDbContext.getLatestFeedItemTimestamp(BaseItem.ITEM_TYPE_CALL_LOG, db);
                    if (latestTimestamp > 0) {
                        selection = String.format("%s > ?", CallLog.Calls.DATE);
                        selectionArgs = new String[] { String.valueOf(latestTimestamp) };
                    }

                    ContentResolver cr = getContentResolver();
                    cursor = cr.query(CallLog.Calls.CONTENT_URI, null, selection, selectionArgs, null);
                    while (cursor.moveToNext()) {
                        String id = cursor.getString(cursor.getColumnIndex(CallLog.Calls._ID));
                        int type = cursor.getInt(cursor.getColumnIndex(CallLog.Calls.TYPE));
                        String number = cursor.getString(cursor.getColumnIndex(CallLog.Calls.NUMBER));
                        long duration = cursor.getLong(cursor.getColumnIndex(CallLog.Calls.DURATION));
                        long date = cursor.getLong(cursor.getColumnIndex(CallLog.Calls.DATE));

                        FeedItem feedItem = new FeedItem();
                        feedItem.setType(BaseItem.ITEM_TYPE_CALL_LOG);
                        feedItem.setExternalRefId(id);
                        feedItem.setContent(number);
                        if (type == CallLog.Calls.INCOMING_TYPE) {
                            feedItem.setDirection(BaseItem.DIRECTION_INCOMING);
                        }
                        if (type == CallLog.Calls.OUTGOING_TYPE) {
                            feedItem.setDirection(BaseItem.DIRECTION_OUTGOING);
                        }
                        if (type == CallLog.Calls.MISSED_TYPE) {
                            feedItem.setDirection(BaseItem.DIRECTION_INCOMING);
                            feedItem.setMissed(true);
                        }
                        if (type == CallLog.Calls.VOICEMAIL_TYPE) {
                            feedItem.setDirection(BaseItem.DIRECTION_INCOMING);
                            feedItem.setVoicemail(true);
                        }
                        feedItem.setDuration(duration);
                        feedItem.setEntryTime(new Date(date));
                        feedItem.setUrl(String.format("tel:%s", number));

                        synchronized (lock) {
                            if (!MagnesiumDbContext.externalRefIdExists(feedItem, db)) {
                                MagnesiumDbContext.insert(feedItem, db);
                            }
                        }
                    }
                    Helper.sendBroadcast(IndexingService.ACTION_CALL_LOG_INDEXED, CoreService.this);
                } catch (SecurityException ex) {
                    Log.e(TAG, "Read call log permission not granted.", ex);
                } catch (Exception ex) {
                    Log.e(TAG, "Could not load the latest calls in the call log.", ex);
                } finally {
                    Helper.closeCloseable(cursor);
                    Helper.closeDb(db);
                }
            }
        };

        ContentObserver smsObserver = new ContentObserver(new Handler()) {
            @Override
            public void onChange(boolean selfChange) {
                super.onChange(selfChange);
                if (!Helper.hasPermission(Manifest.permission.READ_SMS, context)) {
                    return;
                }

                SQLiteDatabase db = null;
                Cursor messageCursor = null;
                try {
                    dbContext = new MagnesiumDbContext(context);
                    db = dbContext.getWritableDatabase();

                    String selection = null;
                    String[] selectionArgs = null;
                    long latestTimestamp = MagnesiumDbContext.getLatestFeedItemTimestamp(BaseItem.ITEM_TYPE_SMS_MESSAGE, db);
                    if (latestTimestamp > 0) {
                        selection = String.format("%s > ?", Telephony.Sms.DATE);
                        selectionArgs = new String[] { String.valueOf(latestTimestamp) };
                    }

                    ContentResolver cr = getContentResolver();
                    Uri smsUri = Uri.parse("content://sms");
                    messageCursor = cr.query(smsUri, null, selection, selectionArgs, null);
                    int loopCount = 0;
                    while (messageCursor.moveToNext()) {
                        loopCount++;

                        String id = messageCursor.getString(messageCursor.getColumnIndex(Telephony.Sms._ID));
                        int type = messageCursor.getInt(messageCursor.getColumnIndex(Telephony.Sms.TYPE)); // 2 = sent, etc.
                        String phone = messageCursor.getString(messageCursor.getColumnIndex(Telephony.Sms.ADDRESS));
                        long dateSent = messageCursor.getLong(messageCursor.getColumnIndex(Telephony.Sms.DATE_SENT));
                        long date = messageCursor.getLong(messageCursor.getColumnIndex(Telephony.Sms.DATE));
                        int read = messageCursor.getInt(messageCursor.getColumnIndex(Telephony.Sms.READ));
                        String body = messageCursor.getString(messageCursor.getColumnIndex(Telephony.Sms.BODY));
                        String threadId = messageCursor.getString(messageCursor.getColumnIndex(Telephony.Sms.THREAD_ID));

                        if (type != 1 && type != 2) {
                            continue;
                        }

                        FeedItem feedItem = new FeedItem();
                        feedItem.setType(BaseItem.ITEM_TYPE_SMS_MESSAGE);
                        feedItem.setExternalRefId(id);
                        feedItem.setPosterId(phone);
                        feedItem.setDirection(type == 1 ? BaseItem.DIRECTION_INCOMING : BaseItem.DIRECTION_OUTGOING);
                        feedItem.setThreadId(threadId);
                        feedItem.setEntryTime((type == 2) ? new Date(dateSent) : new Date(date));
                        if (feedItem.getEntryTime() == null) {
                            continue;
                        }

                        Calendar cal = Calendar.getInstance();
                        cal.setTime(feedItem.getEntryTime());
                        if (cal.get(Calendar.YEAR) < 1980) {
                            continue;
                        }

                        feedItem.setRead(read == 1);
                        feedItem.setContent(body);

                        synchronized (lock) {
                            if (!MagnesiumDbContext.externalRefIdExists(feedItem, db)) {
                                MagnesiumDbContext.insert(feedItem, db);
                            }
                        }

                        if (loopCount % 200 == 0) {
                            Helper.sleep(1000);
                        }
                    }

                    Helper.sendBroadcast(IndexingService.ACTION_SMS_INDEXED, CoreService.this);
                } catch (SecurityException ex) {
                    Log.e(TAG, "Read SMS permission not granted.", ex);
                } catch (Exception ex) {
                    Log.e(TAG, "Could not load the latest SMS messages.", ex);
                } finally {
                    Helper.closeCloseable(messageCursor);
                    Helper.closeDb(db);
                }
            }
        };

        ContentResolver contentResolver = getContentResolver();
        if (Helper.hasPermission(Manifest.permission.READ_CALL_LOG, context)) {
            contentResolver.registerContentObserver(CallLog.Calls.CONTENT_URI, true, callLogObserver);
        }

        if (Helper.hasPermission(Manifest.permission.READ_SMS, context)) {
            contentResolver.registerContentObserver(Uri.parse("content://sms"), true, smsObserver);
        }

        return START_STICKY;
    }

    private void purgeOldItems() {
        SharedPreferences preferences = context.getSharedPreferences(HomeActivity.COMMON_SHARED_PREF_NAME, Context.MODE_PRIVATE);
        int numDeleteMonths = preferences.getInt(IndexingAndSearchFragment.DELETE_KEY, IndexingAndSearchFragment.DEFAULT_DELETE_MONTHS);
        int numSecondsForMonths = 86400 * 30 * numDeleteMonths;
        SQLiteDatabase db = null;
        try {
            dbContext = new MagnesiumDbContext(context);
            db = dbContext.getWritableDatabase();
            db.beginTransactionNonExclusive();

            db.execSQL("DELETE FROM IndexedItems WHERE (strftime('%s', 'now') - strftime('%s', CreatedOn)) > (CAST (? AS INTEGER))",
                new String[] { String.valueOf(numSecondsForMonths) });
            db.execSQL("DELETE FROM FeedItems WHERE (strftime('%s', 'now') - strftime('%s', EntryTime)) > (CAST (? AS INTEGER))"
                     + " OR (strftime('%s', 'now') - strftime('%s', CreatedOn)) > (CAST (? AS INTEGER))",
                new String[] { String.valueOf(numSecondsForMonths), String.valueOf(numSecondsForMonths) });

            db.setTransactionSuccessful();
        } catch (Exception ex) {
            Log.e(TAG, "Could not purge old items.", ex);
        } finally {
            try {
                if (db != null) {
                    db.endTransaction();
                }
            } catch (Exception ex) {
                // Ignore and continue
            }
            Helper.closeDb(db);
        }
    }
}
