package co.aureolin.labs.magnesium.adapter;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.smaato.soma.nativead.NativeAd;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.aureolin.labs.magnesium.HomeActivity;
import co.aureolin.labs.magnesium.R;
import co.aureolin.labs.magnesium.fragment.settings.IndexingAndSearchFragment;
import co.aureolin.labs.magnesium.model.BaseItem;
import co.aureolin.labs.magnesium.model.FeedItem;
import co.aureolin.labs.magnesium.model.IndexedItem;
import co.aureolin.labs.magnesium.util.BitmapMemoryCache;
import co.aureolin.labs.magnesium.util.ContactsCache;
import co.aureolin.labs.magnesium.util.Helper;
import co.aureolin.labs.magnesium.util.IndexedItemSearchAdapterLoader;
import co.aureolin.labs.magnesium.util.NativeAdControlInterface;
import co.aureolin.labs.magnesium.util.WebSearchAdapterLoader;

public class FeedListAdapter extends ArrayAdapter<BaseItem> {
    private static final int MAX_ITEMS = 500;

    private boolean adsEnabled;

    private ListView feedListView;

    private NativeAd nativeAd;

    private final Object lock;

    private TextView feedEmptyView;

    private TextView noResultsView;

    private List<BaseItem> originalList;

    private List<BaseItem> currentList;

    private List<BaseItem> searchResultsList;

    private IndexedItemSearchAdapterLoader searchAdapterLoader;

    private WebSearchAdapterLoader webSearchAdapterLoader;

    private NativeAdControlInterface nativeAdControlInterface;

    private boolean is24HourFormat;

    private boolean nativeAdConfigured;

    private static final int DEFAULT_FILE_EXT_DRAWABLE = R.drawable.document325;
    private static Map<String, Integer> FileExtensionDrawableMap = new HashMap<String, Integer>();
    static {
        FileExtensionDrawableMap.put("docx", R.drawable.docx);
        FileExtensionDrawableMap.put("txt", R.drawable.txt2);
        FileExtensionDrawableMap.put("pdf", R.drawable.pdf19);
        FileExtensionDrawableMap.put("jpg", R.drawable.mountain35);
        FileExtensionDrawableMap.put("png", R.drawable.mountain35);
        FileExtensionDrawableMap.put("gif", R.drawable.mountain35);
        FileExtensionDrawableMap.put("jpeg", R.drawable.mountain35);
        FileExtensionDrawableMap.put("bmp", R.drawable.mountain35);
    }

    public FeedListAdapter(Context context) {
        super(context, 0);
        lock = new Object();
        originalList = new ArrayList<BaseItem>();
        currentList = new ArrayList<BaseItem>();
        searchResultsList = new ArrayList<BaseItem>();
    }

    public void clearOriginalList() {
        setOriginalList(new ArrayList<BaseItem>());
    }

    public void setOriginalList(List<BaseItem> appItemList) {
        originalList = new ArrayList<BaseItem>(appItemList);

        if (originalList.size() > 0) {
            placeAdInList(originalList);
        }

        resetScrollPosition();
        synchronized (lock) {
            currentList = new ArrayList<BaseItem>(originalList);
        }
    }

    public void setSearchAdapterLoader(IndexedItemSearchAdapterLoader searchAdapterLoader) {
        this.searchAdapterLoader = searchAdapterLoader;
    }

    public WebSearchAdapterLoader getWebSearchAdapterLoader() {
        return webSearchAdapterLoader;
    }

    public void setWebSearchAdapterLoader(WebSearchAdapterLoader webSearchAdapterLoader) {
        this.webSearchAdapterLoader = webSearchAdapterLoader;
    }

    public void setFeedEmptyView(TextView feedEmptyView) {
        this.feedEmptyView = feedEmptyView;
    }

    public void setAdsEnabled(boolean adsEnabled) {
        this.adsEnabled = adsEnabled;
        this.nativeAdConfigured = false;
    }

    public void setNoResultsView(TextView noResultsView) {
        this.noResultsView = noResultsView;
    }

    public void setNativeAd(NativeAd nativeAd) {
        this.nativeAd = nativeAd;
    }

    public void setFeedListView(ListView feedListView) {
        this.feedListView = feedListView;
    }

    public void setNativeAdControlInterface(NativeAdControlInterface nativeAdControlInterface) {
        this.nativeAdControlInterface = nativeAdControlInterface;
    }

    public void addItemsToOriginalList(List<BaseItem> appItemList) {
        removeAdFromList(originalList);

        for (int i = 0; i < appItemList.size(); i++) {
            BaseItem item = appItemList.get(i);
            if (!originalList.contains(item)) {
                originalList.add(item);
            }
        }
        Collections.sort(originalList, Collections.reverseOrder());
        placeAdInList(originalList);

        // Trim originalList to 500 items
        if (originalList.size() > MAX_ITEMS) {
            synchronized (lock) {
                for (int i = MAX_ITEMS; i < originalList.size(); i++) {
                    originalList.remove(i);
                }
            }
        }

        if (currentConstraint == null) {
            synchronized (lock) {
                currentList = new ArrayList<BaseItem>(originalList);
            }
        }
    }

    public List<BaseItem> getItems() {
        return currentList;
    }

    @Override
    public BaseItem getItem(int position) {
        synchronized (lock) {
            return currentList.get(position);
        }
    }

    @Override
    public int getCount() {
        synchronized (lock) {
            return currentList.size();
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private String currentConstraint;

    private void resetScrollPosition() {
        if (feedListView != null) {
            feedListView.setSelection(0);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                currentConstraint = getConstraintFromSequence(constraint);
                synchronized (lock) {
                    currentList = (ArrayList<BaseItem>) results.values;
                }
                resetScrollPosition();

                if (searchAdapterLoader != null && constraint != null && !Helper.isEmpty(constraint.toString())) {
                    //searchAdapterLoader.searchForIndexedItems(currentConstraint, FeedListAdapter.this);
                } else {
                    updateFeedEmptyView(currentConstraint);
                    updateNoResultsView(currentConstraint);
                    notifyDataSetChanged();
                }
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                List<BaseItem> filteredList = new ArrayList<BaseItem>();
                if (constraint == null || constraint.length() == 0) {
                    int idxToRemove = -1;
                    for (int i = 0; i < originalList.size(); i++) {
                        if (originalList.get(i).isPlaceholder()) {
                            idxToRemove = i;
                            break;
                        }
                    }
                    if (idxToRemove > -1) {
                        originalList.remove(idxToRemove);
                    }

                    results.count = originalList.size();
                    results.values = new ArrayList<BaseItem>(originalList);
                } else {
                    Context context = getContext();
                    SharedPreferences preferences = context.getSharedPreferences(HomeActivity.COMMON_SHARED_PREF_NAME, Context.MODE_PRIVATE);
                    String includedResultsString = preferences.getString(IndexingAndSearchFragment.ISR_KEY, IndexingAndSearchFragment.DEFAULT_INCLUDE_SEARCH_RESULTS);
                    List<String> includedResults = Arrays.asList(includedResultsString.split(","));

                    String filterStr = constraint.toString().trim();
                    boolean exactMatchFound = false;
                    for (int i = 0; i < originalList.size(); i++) {
                        BaseItem thisItem = originalList.get(i);
                        if (thisItem.isAd()) {
                            filteredList.add(thisItem);
                        } else if (itemMatchesFilterString(thisItem, filterStr) && Helper.isInIncludedResults(thisItem, includedResults)) {
                            filteredList.add(thisItem);
                        }
                        if (itemExactMatchesFilterString(thisItem, filterStr) && Helper.isInIncludedResults(thisItem, includedResults)) {
                            exactMatchFound = true;
                        }
                    }

                    if (!exactMatchFound) {
                        // Determine the type of input
                        IndexedItem placeholderItem = new IndexedItem();
                        placeholderItem.setMetaContent(filterStr);
                        placeholderItem.setPlaceholder(true);
                        if (Helper.isTelephoneNumber(filterStr) || Helper.isUssd(filterStr)) {
                            placeholderItem.setType(BaseItem.ITEM_TYPE_CONTACT_NUMBER);
                            placeholderItem.setMetaFieldName("number");
                            placeholderItem.setUrl(String.format("tel:%s", filterStr));
                            placeholderItem.setCreatedOn(new Date(0));
                            filteredList.add(placeholderItem);
                        }
                    }

                    results.count = filteredList.size();
                    results.values = filteredList;
                }

                return results;
            }
        };
    }

    private List<IndexedItem> currentExternalResults;

    public void addIndexedSearchResults(List<IndexedItem> results, boolean webSearchInProgress) {
        purgeExternalResultsFromCurrentList();
        currentExternalResults = new ArrayList<IndexedItem>(results);
        synchronized (lock) {
            removeAdFromList(currentList);
            for (int i = 0; i < currentExternalResults.size(); i++) {
                BaseItem result = currentExternalResults.get(i);
                if (!currentList.contains(result)) {
                    currentList.add(result); // TODO: only add all to current list if current filter is set
                }
            }

            Collections.sort(currentList, Collections.reverseOrder());
            placeAdInList(currentList);

            if (Helper.isTelephoneNumber(currentConstraint) || Helper.isUssd(currentConstraint)) {
                int placeholderIdx = getPlaceholderIndexFromList(currentList);
                if (placeholderIdx > -1) {
                    boolean exactMatchFound = false;
                    for (int i = 0; i < currentList.size(); i++) {
                        BaseItem item = currentList.get(i);
                        if (item.isPlaceholder()
                                || item.getType() != BaseItem.ITEM_TYPE_CONTACT_NUMBER) {
                            continue;
                        }
                        exactMatchFound = itemExactMatchesFilterString(item, currentConstraint);
                    }
                    if (exactMatchFound) {
                        currentList.remove(placeholderIdx);
                    }
                }
            }
        }

        resetScrollPosition();

        if ((currentConstraint != null && currentConstraint.trim().length() < 3)
                || webSearchAdapterLoader == null || Helper.isUssd(currentConstraint)) {
            updateNoResultsView(currentConstraint);
            updateFeedEmptyView(currentConstraint);
        }
    }

    private void removeAdFromList(List<BaseItem> list) {
        if (adsEnabled) {
            FeedItem adItem = new FeedItem();
            adItem.setExternalRefId("feed_ad");
            int index = list.indexOf(adItem);
            if (index > -1) {
                list.remove(adItem);
            }
        }
    }

    private void placeAdInList(List<BaseItem> list) {
        if (adsEnabled) {
            FeedItem adItem = new FeedItem();
            adItem.setExternalRefId("feed_ad");
            adItem.setType(BaseItem.ITEM_TYPE_AD);
            adItem.setAd(true);

            if (!list.contains(adItem)) {
                if (list.size() > 2) {
                    list.add(1, adItem);
                } else {
                    list.add(adItem);
                }
            }
        }
    }

    public void intersperseWebSearchResults(List<IndexedItem> results) {
        synchronized (lock) {
            for (int i = 0, j = 4; i < results.size(); i++, j += 5) {
                IndexedItem item = results.get(i);
                if (currentList != null) {
                    if (j < currentList.size()) {
                        currentList.add(j, item);
                    } else {
                        currentList.add(item);
                    }
                }
            }
        }

        resetScrollPosition();
        updateNoResultsView(currentConstraint);
        updateFeedEmptyView(currentConstraint);
    }

    private int getPlaceholderIndexFromList(List<BaseItem> items) {
        for (int i = 0; i < items.size(); i++) {
            BaseItem item = items.get(i);
            if (item.isPlaceholder()) {
                return i;
            }
        }
        return -1;
    }

    private void purgeExternalResultsFromCurrentList() {
        if (currentExternalResults != null && currentList != null) {
            List<Integer> idxsToRemove = new ArrayList<Integer>();
            synchronized (lock) {
                for (int i = 0; i < currentExternalResults.size(); i++) {
                    IndexedItem ext = currentExternalResults.get(i);
                    if (currentList.contains(ext)) {
                        idxsToRemove.add(currentList.indexOf(ext));
                    }
                }

                for (int i = 0; i < idxsToRemove.size(); i++) {
                    currentList.remove(idxsToRemove.get(i));
                }
            }
        }
    }

    private boolean itemExactMatchesFilterString(BaseItem item, String filterString) {
        String lcFilterString = filterString.toLowerCase();
        if (getLcString(item.getValue()).equals(lcFilterString)) {
            return true;
        }
        return getLcString(item.getTitle()).equals(filterString.toLowerCase());
    }

    private boolean itemMatchesFilterString(BaseItem item, String filterString) {
        String lcFilterString = filterString.toLowerCase();
        if (getLcString(item.getValue()).equals(lcFilterString)) {
            return true;
        }

        return (getLcString(item.getTitle()).contains(lcFilterString)
                || getLcString(item.getAuthor()).contains(lcFilterString)
                || getLcString(item.getRecipients()).contains(lcFilterString)
                || getLcString(item.getUrl()).contains(lcFilterString)
                || getLcString(item.getReplyTo()).contains(lcFilterString));
    }

    private void updateNoResultsView(String constraint) {
        int currentListSize = currentList.size();
        for (int i = 0; i < currentList.size(); i++) {
            if (currentList.get(i).isAd()) {
                currentListSize--;
            }
        }
        boolean hasResults = (!Helper.isEmpty(constraint) && currentListSize > 0) || constraint == null;
        if (noResultsView != null) {
            noResultsView.setText(String.format("No item matching \"%s\" was found.", constraint));
            noResultsView.setVisibility(hasResults ? View.INVISIBLE : View.VISIBLE);
        }
    }

    public boolean is24HourFormat() {
        return is24HourFormat;
    }

    public void setIs24HourFormat(boolean is24HourFormat) {
        this.is24HourFormat = is24HourFormat;
    }

    private void updateFeedEmptyView(String constraint) {
        boolean feedEmpty =
            feedEmptyView != null && constraint == null && originalList.size() == 0 && currentList.size() == 0;
        if (feedEmptyView != null) {
            feedEmptyView.setVisibility(feedEmpty ? View.VISIBLE : View.INVISIBLE);
        }
    }

    private String getConstraintFromSequence(CharSequence constraint) {
        return (constraint != null) ? constraint.toString() : null;
    }

    private static String getLcString(String value) {
        if (value != null) {
            return value.trim().toLowerCase();
        }
        return "";
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_home_feed, null);
        }

        Context context = getContext();

        // Get a reference all the views first
        View itemActionsContainer = convertView.findViewById(R.id.item_actions_container);

        ImageView largeIconView = (ImageView) convertView.findViewById(R.id.item_large_icon);
        ImageView mediumIconView = (ImageView) convertView.findViewById(R.id.item_medium_icon);
        ImageView actionIconView = (ImageView) convertView.findViewById(R.id.item_action_icon);
        ImageView largeImageView = (ImageView) convertView.findViewById(R.id.item_image_large);

        ImageView action1View = (ImageView) convertView.findViewById(R.id.item_action_1);
        ImageView action2View = (ImageView) convertView.findViewById(R.id.item_action_2);
        ImageView action3View = (ImageView) convertView.findViewById(R.id.item_action_3);
        ImageView action4View = (ImageView) convertView.findViewById(R.id.item_action_4);
        ImageView action5View = (ImageView) convertView.findViewById(R.id.item_action_5);
        //View dividerView = convertView.findViewById(R.id.item_divider_view);

        TextView annotationView = (TextView) convertView.findViewById(R.id.item_annotation);
        TextView subjectView = (TextView) convertView.findViewById(R.id.item_subject); // email
        TextView titleView = (TextView) convertView.findViewById(R.id.item_title);
        TextView subtitleView = (TextView) convertView.findViewById(R.id.item_subtitle);
        TextView summaryView = (TextView) convertView.findViewById(R.id.item_summary);
        TextView sourceView = (TextView) convertView.findViewById(R.id.item_source);
        TextView displayUrlView = (TextView) convertView.findViewById(R.id.item_display_url);

        TextView largeNameView = (TextView) convertView.findViewById(R.id.item_name_large);
        TextView itemDateTimeView = (TextView) convertView.findViewById(R.id.item_date_time);

        titleView.setSingleLine(true);
        titleView.setTypeface(HomeActivity.getBoldTypeface(context));
        TextView[] normalTextViews = {
            subjectView, annotationView, subtitleView, summaryView, sourceView,
            displayUrlView, largeNameView, itemDateTimeView
        };
        for (int i = 0; i < normalTextViews.length; i++) {
            normalTextViews[i].setTypeface(HomeActivity.getNormalTypeface(context));
        }

        itemActionsContainer.setVisibility(View.GONE);
        largeIconView.setVisibility(View.GONE);
        mediumIconView.setVisibility(View.INVISIBLE);
        actionIconView.setVisibility(View.GONE);
        largeImageView.setVisibility(View.GONE);

        action1View.setVisibility(View.GONE);
        action2View.setVisibility(View.GONE);
        action3View.setVisibility(View.GONE);
        action4View.setVisibility(View.GONE);
        action5View.setVisibility(View.GONE);

        annotationView.setVisibility(View.GONE);
        subjectView.setVisibility(View.GONE);
        titleView.setVisibility(View.GONE);
        subtitleView.setVisibility(View.GONE);
        summaryView.setVisibility(View.GONE);
        sourceView.setVisibility(View.GONE);
        displayUrlView.setVisibility(View.GONE);

        largeNameView.setVisibility(View.GONE);
        itemDateTimeView.setVisibility(View.GONE);

        BaseItem item = getItem(position);
        int itemType = item.getType();

        action1View.setOnClickListener(new ItemActionHandler(context, 1, item));
        action2View.setOnClickListener(new ItemActionHandler(context, 2, item));
        action3View.setOnClickListener(new ItemActionHandler(context, 3, item));
        /*action4View.setOnClickListener(new ItemActionHandler(getContext(), 4, item));
        action5View.setOnClickListener(new ItemActionHandler(getContext(), 3, item));*/

        RelativeLayout.LayoutParams miLayoutParams = (RelativeLayout.LayoutParams) mediumIconView.getLayoutParams();


        int side = Helper.getValueForDip(8, context);
        int top = Helper.getValueForDip(12, context);
        convertView.setPadding(side, top, side, top);
        mediumIconView.setImageBitmap(null);
        miLayoutParams.topMargin = 0;

        switch (itemType) {
            case BaseItem.ITEM_TYPE_AD:
                if (nativeAd != null && !nativeAdConfigured) {
                    nativeAd.setTitleView(titleView);
                    nativeAd.setTextView(summaryView);
                    nativeAd.setIconImageView(mediumIconView);
                    //nativeAd.setMainLayout((RelativeLayout) convertView);

                    nativeAdConfigured = true;
                }
                if (nativeAdControlInterface != null) {
                    boolean isAdLoaded = nativeAdControlInterface.isAdLoaded();
                    mediumIconView.setVisibility(isAdLoaded ? View.VISIBLE : View.GONE);
                    titleView.setVisibility(isAdLoaded ? View.VISIBLE : View.GONE);
                    summaryView.setVisibility(isAdLoaded ? View.VISIBLE : View.GONE);
                    if (!isAdLoaded) {
                        convertView.setPadding(0, 0, 0, 0);
                    }

                    nativeAdControlInterface.loadNativeAd(position, mediumIconView, titleView, summaryView, convertView);
                }
                break;

            case BaseItem.ITEM_TYPE_APP:
                String packageName = item.getReferenceId();
                titleView.setVisibility(View.VISIBLE);
                titleView.setText(item.getTitle());

                PackageManager pkgManager = context.getPackageManager();
                try {
                    Drawable d = pkgManager.getApplicationIcon(packageName);
                    mediumIconView.setImageDrawable(d);
                    mediumIconView.setVisibility(View.VISIBLE);
                } catch (PackageManager.NameNotFoundException ex) {
                    // continue
                }

                break;

            case BaseItem.ITEM_TYPE_CONTACT:
                if (item instanceof IndexedItem) {
                    IndexedItem indexedItem = (IndexedItem) item;
                    titleView.setVisibility(View.VISIBLE);
                    actionIconView.setVisibility(View.GONE);

                    mediumIconView.setImageResource(R.drawable.ic_person_outline_white_36dp);
                    mediumIconView.setVisibility(View.VISIBLE);

                    String contactId = indexedItem.getRefId();
                    titleView.setText(indexedItem.getMetaContent());

                    // TODO: Load contact avatar?
                }

                break;

            case BaseItem.ITEM_TYPE_CONTACT_NUMBER:
            case BaseItem.ITEM_TYPE_CONTACT_EMAIL:
                if (item instanceof IndexedItem) {
                    IndexedItem indexedItem = (IndexedItem) item;
                    String title = indexedItem.getTitle();

                    itemActionsContainer.setVisibility(View.VISIBLE);
                    titleView.setVisibility(Helper.isEmpty(title) ? View.GONE : View.VISIBLE);
                    largeNameView.setVisibility(View.VISIBLE);

                    boolean canBeAdded = Helper.isEmpty(item.getTitle());
                    action1View.setImageResource(itemType == BaseItem.ITEM_TYPE_CONTACT_NUMBER ? R.drawable.ic_phone_white_24dp : R.drawable.ic_call_made_white_48dp);
                    action1View.setVisibility(View.VISIBLE);

                    if (canBeAdded) {
                        if (itemType == BaseItem.ITEM_TYPE_CONTACT_EMAIL || Helper.isUssd(indexedItem.getValue())) {
                            action2View.setImageResource(R.drawable.ic_person_add_white_24dp);
                            action2View.setVisibility(View.VISIBLE);
                        } else {
                            action3View.setImageResource(R.drawable.ic_person_add_white_24dp);
                            action3View.setVisibility(View.VISIBLE);
                        }
                    }

                    if (itemType == BaseItem.ITEM_TYPE_CONTACT_NUMBER && !Helper.isUssd(indexedItem.getValue())) {
                        // Phone Number (Action 2 will be SMS)
                        action2View.setImageResource(R.drawable.ic_message_white_24dp);
                        action2View.setVisibility(View.VISIBLE);
                    }

                    String contactId = indexedItem.getRefId();
                    if (!Helper.isEmpty(title)) {
                        titleView.setText(title);
                    }
                    //indexedItem.getMetaFieldName()
                    largeNameView.setText(indexedItem.getMetaContent());
                    // TODO: Load contact avatar?
                }
                break;

            case BaseItem.ITEM_TYPE_FILE:
                largeImageView.setImageBitmap(null);

                String filename = item.getTitle();
                titleView.setVisibility(View.VISIBLE);
                titleView.setText(filename);

                (new AsyncTask<Object, Void, Integer>() {
                    private ImageView imageView;

                    private ImageView lgImageView;

                    private String itemFilename;

                    private String itemUrl;

                    private boolean isImage;

                    @Override
                    protected Integer doInBackground(Object... params) {
                        int drawable = DEFAULT_FILE_EXT_DRAWABLE;

                        itemFilename = (String) params[0];
                        itemUrl = (String) params[1];
                        imageView = (ImageView) params[2];
                        lgImageView = (ImageView) params[3];

                        if (itemFilename.endsWith(".pdf")) {
                            drawable = R.drawable.pdf19;
                        } else if (itemFilename.endsWith(".docx")) {
                            drawable = R.drawable.docx;
                        } else if (Helper.isImage(itemFilename)) {
                            drawable = R.drawable.mountain35;
                            isImage = true;
                        }

                        return drawable;
                    }

                    @Override
                    protected void onPostExecute(Integer result) {
                        if (result != null && imageView != null) {
                            imageView.setImageResource(result);
                            imageView.getDrawable().setColorFilter(new PorterDuffColorFilter(0xffdddddd, PorterDuff.Mode.SRC_ATOP));
                            imageView.setVisibility(View.VISIBLE);
                        }
                        if (isImage && lgImageView != null) {
                            new LoadImageFileTask(lgImageView).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, itemUrl);
                        }
                    }
                }).executeOnExecutor(AsyncTask.SERIAL_EXECUTOR, filename, item.getUrl(), mediumIconView, largeImageView);


                if (item instanceof IndexedItem) {
                    IndexedItem indexedItem = (IndexedItem) item;
                    String content = item.getValue();
                    String mimeType = indexedItem.getMimeType();

                    if (!Helper.isEmpty(content)) {
                        summaryView.setText(content);
                        summaryView.setVisibility(View.VISIBLE);
                    }
                }
                if (item.getTimestamp() != null) { // TODO: Change to ModifiedOn
                    itemDateTimeView.setText(Helper.getDateTimeString(item.getTimestamp(), is24HourFormat));
                    itemDateTimeView.setVisibility(View.VISIBLE);
                }

                break;

            case BaseItem.ITEM_TYPE_CALL_LOG:
                FeedItem callLogItem = null;
                if (item instanceof IndexedItem) {
                    callLogItem = ((IndexedItem) item).getFeedItem();
                } else if (item instanceof FeedItem) {
                    callLogItem = (FeedItem) item;
                }

                if (callLogItem != null) {
                    String number = callLogItem.getContent();
                    String plainNumber = number;
                    if (number != null && number.indexOf(' ') > -1) {
                        plainNumber = number.replaceAll(" ", "");
                    }
                    largeNameView.setText(number);
                    largeNameView.setVisibility(View.VISIBLE);
                    titleView.setText("");
                    ContactsCache.updateViewWithContactName(plainNumber, HomeActivity.COUNTRY_ISO_CODE, titleView, context, false);

                    itemActionsContainer.setVisibility(View.VISIBLE);

                    if (callLogItem.isVoicemail()) {
                        mediumIconView.setImageResource(R.drawable.ic_voicemail_white_24dp);
                    } else if (callLogItem.isMissed()) {
                        mediumIconView.setImageResource(R.drawable.ic_phone_missed_white_48dp);
                    } else {
                        mediumIconView.setImageResource(
                            (callLogItem.getDirection() == BaseItem.DIRECTION_INCOMING) ?
                            R.drawable.ic_call_received_white_48dp : R.drawable.ic_call_made_white_48dp);
                    }
                    mediumIconView.setVisibility(View.VISIBLE);

                    long duration = callLogItem.getDuration();
                    sourceView.setText(duration > 0 ? Helper.getReadableTime(callLogItem.getDuration(), true) : "");
                    sourceView.setVisibility(duration > 0 ? View.VISIBLE : View.GONE);

                    boolean canBeAdded = Helper.isEmpty(titleView.getText().toString());
                    action1View.setImageResource(R.drawable.ic_phone_white_24dp);
                    action1View.setVisibility(View.VISIBLE);

                    if (canBeAdded) {
                        if (Helper.isUssd(number)) {
                            action2View.setImageResource(R.drawable.ic_person_add_white_24dp);
                            action2View.setVisibility(View.VISIBLE);
                        } else {
                            action3View.setImageResource(R.drawable.ic_person_add_white_24dp);
                            action3View.setVisibility(View.VISIBLE);
                        }
                    }

                    if (!Helper.isUssd(number)) {
                        // Phone Number (Action 2 will be SMS)
                        action2View.setImageResource(R.drawable.ic_message_white_24dp);
                        action2View.setVisibility(View.VISIBLE);
                    }
                }

                if (item.getTimestamp() != null) {
                    itemDateTimeView.setText(Helper.getDateTimeString(item.getTimestamp(), is24HourFormat));
                    itemDateTimeView.setVisibility(View.VISIBLE);
                }

                break;


            case BaseItem.ITEM_TYPE_TWITTER_POST:
                titleView.setVisibility(View.VISIBLE);
                titleView.setText(item.getAuthor());

                FeedItem twitterItem = null;
                if (item instanceof IndexedItem) {
                    twitterItem = ((IndexedItem) item).getFeedItem();
                } else if (item instanceof FeedItem) {
                    twitterItem = (FeedItem) item;
                }

                if (twitterItem != null) {
                    annotationView.setText(twitterItem.isRetweet() ? String.format("%s retweeted", twitterItem.getRetweetedBy()) : null);
                    annotationView.setVisibility(twitterItem.isRetweet() ? View.VISIBLE : View.GONE);
                    miLayoutParams.topMargin = twitterItem.isRetweet() ? Helper.getValueForDip(16, context) : 0;

                    subtitleView.setText(String.format(twitterItem.getPosterId()));
                    subtitleView.setVisibility(View.VISIBLE);

                    summaryView.setText(Html.fromHtml(Helper.htmlise(twitterItem.getContent())));
                    summaryView.setVisibility(View.VISIBLE);

                    String portraitKey = String.format("twitter_%s", twitterItem.getPosterId());
                    Bitmap cachedBitmap = BitmapMemoryCache.getBitmap(portraitKey);
                    if (cachedBitmap != null) {
                        mediumIconView.setVisibility(View.VISIBLE);
                        mediumIconView.setImageDrawable(HomeActivity.getRoundedDrawableForBitmap(cachedBitmap, context.getResources()));
                    } else {
                        if (!Helper.isEmpty(twitterItem.getPosterImageUrl())) {
                            if (!HomeActivity.IMAGE_LOADING_TASKS.contains(portraitKey)) {
                                new HomeActivity.LoadImageAvatarTask(
                                        context, mediumIconView, portraitKey, context.getResources()).executeOnExecutor(
                                        AsyncTask.THREAD_POOL_EXECUTOR, "twitter", twitterItem.getPosterId(), twitterItem.getPosterImageUrl());
                            } else {
                                if (!HomeActivity.PENDING_IMAGE_VIEWS.containsKey(portraitKey)) {
                                    HomeActivity.PENDING_IMAGE_VIEWS.put(portraitKey, new ArrayList<ImageView>());
                                }
                                HomeActivity.PENDING_IMAGE_VIEWS.get(portraitKey).add(mediumIconView);
                            }
                        }
                    }

                    sourceView.setText(Html.fromHtml(twitterItem.getDisplaySource()));
                    sourceView.setVisibility(View.VISIBLE);
                }
                if (item.getTimestamp() != null) { // TODO: Change to ModifiedOn
                    itemDateTimeView.setText(Helper.getDateTimeString(item.getTimestamp(), is24HourFormat));
                    itemDateTimeView.setVisibility(View.VISIBLE);
                }
                // Actions : Reply / Retweet (Retweet count) / Like.Favorite (count) / Share

                break;

            case BaseItem.ITEM_TYPE_FACEBOOK_POST:
                titleView.setVisibility(View.VISIBLE);
                titleView.setText(item.getAuthor());

                FeedItem fbItem = null;
                if (item instanceof IndexedItem) {
                    fbItem = ((IndexedItem) item).getFeedItem();
                } else if (item instanceof FeedItem) {
                    fbItem = (FeedItem) item;
                }

                if (fbItem != null) {
                    summaryView.setText(fbItem.getContent());
                    summaryView.setVisibility(View.VISIBLE);

                    String portraitKey = String.format("facebook_%s", fbItem.getPosterId());
                    Bitmap cachedBitmap = BitmapMemoryCache.getBitmap(portraitKey);
                    if (cachedBitmap != null) {
                        mediumIconView.setVisibility(View.VISIBLE);
                        mediumIconView.setImageDrawable(HomeActivity.getRoundedDrawableForBitmap(cachedBitmap, context.getResources()));
                    } else {
                        if (!Helper.isEmpty(fbItem.getPosterImageUrl())) {
                            if (!HomeActivity.IMAGE_LOADING_TASKS.contains(portraitKey)) {
                                new HomeActivity.LoadImageAvatarTask(
                                        context, mediumIconView, portraitKey, context.getResources()).executeOnExecutor(
                                        AsyncTask.THREAD_POOL_EXECUTOR, "facebook", fbItem.getPosterId(), fbItem.getPosterImageUrl());
                            } else {
                                if (!HomeActivity.PENDING_IMAGE_VIEWS.containsKey(portraitKey)) {
                                    HomeActivity.PENDING_IMAGE_VIEWS.put(portraitKey, new ArrayList<ImageView>());
                                }
                                HomeActivity.PENDING_IMAGE_VIEWS.get(portraitKey).add(mediumIconView);
                            }
                        }
                    }
                }
                if (item.getTimestamp() != null) {
                    itemDateTimeView.setText(Helper.getDateTimeString(item.getTimestamp(), is24HourFormat));
                    itemDateTimeView.setVisibility(View.VISIBLE);
                }
                // Actions : Comment / Like.Favorite (count) / Share

                break;

            case BaseItem.ITEM_TYPE_INSTAGRAM_POST:
                titleView.setText(item.getAuthor());
                titleView.setVisibility(View.VISIBLE);

                FeedItem igItem = null;
                if (item instanceof IndexedItem) {
                    igItem = ((IndexedItem) item).getFeedItem();
                } else if (item instanceof FeedItem) {
                    igItem = (FeedItem) item;
                }

                if (igItem != null) {
                    summaryView.setText(igItem.getContent());
                    summaryView.setVisibility(View.VISIBLE);

                    String portraitKey = String.format("instagram_%s", igItem.getPosterId());
                    Bitmap cachedBitmap = BitmapMemoryCache.getBitmap(portraitKey);
                    if (cachedBitmap != null) {
                        mediumIconView.setVisibility(View.VISIBLE);
                        mediumIconView.setImageDrawable(HomeActivity.getRoundedDrawableForBitmap(cachedBitmap, context.getResources()));
                    } else if (!Helper.isEmpty(igItem.getPosterImageUrl())) {
                        if (!HomeActivity.IMAGE_LOADING_TASKS.contains(portraitKey)) {
                            new HomeActivity.LoadImageAvatarTask(
                                    context, mediumIconView, portraitKey, context.getResources()).executeOnExecutor(
                                    AsyncTask.THREAD_POOL_EXECUTOR, "instagram", igItem.getPosterId(), igItem.getPosterImageUrl());
                        } else {
                            if (!HomeActivity.PENDING_IMAGE_VIEWS.containsKey(portraitKey)) {
                                HomeActivity.PENDING_IMAGE_VIEWS.put(portraitKey, new ArrayList<ImageView>());
                            }
                            HomeActivity.PENDING_IMAGE_VIEWS.get(portraitKey).add(mediumIconView);
                        }
                    }

                    if (!Helper.isEmpty(igItem.getImageUrl())) {
                        String igImageKey = String.format("igmedia_%s", igItem.getExternalRefId());
                        Bitmap cachedMedia = BitmapMemoryCache.getBitmap(igImageKey);

                        largeImageView.setImageBitmap(null);
                        largeImageView.setVisibility(View.VISIBLE);

                        if (cachedMedia != null) {
                            largeImageView.setImageBitmap(cachedMedia);
                        } else {
                            if (!HomeActivity.IMAGE_LOADING_TASKS.contains(igImageKey)) {
                                new HomeActivity.LoadImageUrlTask(context, largeImageView, igImageKey).executeOnExecutor(
                                        AsyncTask.THREAD_POOL_EXECUTOR,
                                        "igmedia", igItem.getExternalRefId(), igItem.getImageUrl());
                            }
                        }
                    }
                }
                if (item.getTimestamp() != null) {
                    itemDateTimeView.setText(Helper.getDateTimeString(item.getTimestamp(), is24HourFormat));
                    itemDateTimeView.setVisibility(View.VISIBLE);
                }
                // Actions : Comment / Like.Favorite (count) / Share

                break;

            case BaseItem.ITEM_TYPE_EMAIL_MESSAGE:
                titleView.setVisibility(View.VISIBLE);
                titleView.setText(item.getAuthor());

                mediumIconView.setImageResource(R.drawable.ic_email_white_24dp);
                mediumIconView.setVisibility(View.VISIBLE);

                FeedItem msgItem = null;
                if (item instanceof IndexedItem) {
                    msgItem = ((IndexedItem) item).getFeedItem();
                } else if (item instanceof FeedItem) {
                    msgItem = (FeedItem) item;
                }

                if (msgItem != null) {
                    if (Helper.isEmpty(item.getAuthor())) {
                        titleView.setText(msgItem.getPosterId());
                    }

                    subjectView.setText(msgItem.getTitle());
                    subjectView.setVisibility(View.VISIBLE);

                    summaryView.setText(msgItem.getContent());
                    summaryView.setVisibility(View.VISIBLE);
                }

                if (item.getTimestamp() != null) {
                    itemDateTimeView.setText(Helper.getDateTimeString(item.getTimestamp(), is24HourFormat));
                    itemDateTimeView.setVisibility(View.VISIBLE);
                }

                break;

            case BaseItem.ITEM_TYPE_RSS_FEED_ITEM:
                titleView.setText(item.getTitle());
                titleView.setSingleLine(false);
                titleView.setVisibility(View.VISIBLE);

                mediumIconView.setImageResource(R.drawable.ic_rss_feed_white_24dp);
                mediumIconView.setVisibility(View.VISIBLE);

                FeedItem rssFeedItem = null;
                if (item instanceof IndexedItem) {
                    rssFeedItem = ((IndexedItem) item).getFeedItem();
                } else if (item instanceof FeedItem) {
                    rssFeedItem = (FeedItem) item;
                }

                if (rssFeedItem != null) {
                    subjectView.setText(rssFeedItem.getAuthor());
                    subjectView.setVisibility(View.VISIBLE);

                    summaryView.setText(rssFeedItem.getContent());
                    summaryView.setVisibility(View.VISIBLE);

                    sourceView.setText(rssFeedItem.getSource());
                    sourceView.setVisibility(View.VISIBLE);
                }

                if (item.getTimestamp() != null) {
                    itemDateTimeView.setText(Helper.getDateTimeString(item.getTimestamp(), is24HourFormat));
                    itemDateTimeView.setVisibility(View.VISIBLE);
                }

                break;

            case BaseItem.ITEM_TYPE_SMS_MESSAGE:
                mediumIconView.setImageResource(R.drawable.ic_sms_white_24dp);
                mediumIconView.setVisibility(View.VISIBLE);

                FeedItem smsItem = null;
                if (item instanceof IndexedItem) {
                    smsItem = ((IndexedItem) item).getFeedItem();
                } else if (item instanceof FeedItem) {
                    smsItem = (FeedItem) item;
                }

                if (smsItem != null) {
                    String from = smsItem.getPosterId();
                    titleView.setText(from);
                    titleView.setVisibility(View.VISIBLE);

                    if (Helper.isTelephoneNumber(from)) {
                        ContactsCache.updateViewWithContactName(from, HomeActivity.COUNTRY_ISO_CODE, titleView, context, true);
                    }

                    summaryView.setText(smsItem.getContent());
                    summaryView.setVisibility(View.VISIBLE);
                }

                if (item.getTimestamp() != null) {
                    itemDateTimeView.setText(Helper.getDateTimeString(item.getTimestamp(), is24HourFormat));
                    itemDateTimeView.setVisibility(View.VISIBLE);
                }

                break;

            case BaseItem.ITEM_TYPE_WEB_SEARCH_RESULT:
                mediumIconView.setVisibility(View.GONE);

                titleView.setText(item.getTitle());
                titleView.setSingleLine(false);
                titleView.setVisibility(View.VISIBLE);

                if (item instanceof IndexedItem) {
                    IndexedItem searchResult = (IndexedItem) item;
                    summaryView.setText(searchResult.getMetaContent());
                    summaryView.setVisibility(View.VISIBLE);

                    displayUrlView.setText(searchResult.getRefId());
                    displayUrlView.setVisibility(View.VISIBLE);
                }

                if (item.getTimestamp() != null) {
                    itemDateTimeView.setText(Helper.getDateTimeString(item.getTimestamp(), is24HourFormat));
                    itemDateTimeView.setVisibility(View.VISIBLE);
                }

                break;
        }

        return convertView;
    }

    private void removePlaceholderFromList(List<BaseItem> baseItemList) {
        int idxToRemove = -1;
        for (int i = 0; i < baseItemList.size(); i++) {
            if (baseItemList.get(i).isPlaceholder()) {
                idxToRemove = i;
            }
        }
        if (idxToRemove > -1) {
            baseItemList.remove(idxToRemove);
        }
    }

    private class ItemActionHandler implements View.OnClickListener {
        private Context context;
        private BaseItem item;
        private int actionIndex;
        public ItemActionHandler(Context context, int actionIndex, BaseItem item) {
            this.context = context;
            this.actionIndex = actionIndex;
            this.item = item;
        }
        @Override
        public void onClick(View v) {
            handleItemAction();
        }

        private void handleItemAction() {
            int itemType = item.getType();
            Intent intent = null;
            switch (actionIndex) {
                case 1:
                    // Primary action
                    switch (itemType) {
                        default:
                            break;
                        case BaseItem.ITEM_TYPE_CONTACT_NUMBER: // Phone Dial action
                        case BaseItem.ITEM_TYPE_CALL_LOG:
                            intent = new Intent(Intent.ACTION_CALL, Uri.parse(item.getUrl()));
                            break;
                        case BaseItem.ITEM_TYPE_CONTACT_EMAIL: // Send Email action
                            intent = new Intent(Intent.ACTION_SEND, Uri.parse(item.getUrl()));
                            intent.setType("message/rfc822");
                            break;
                    }
                    break;

                case 2:
                    // Secondary action
                    switch (itemType) {
                        default:
                            break;
                        case BaseItem.ITEM_TYPE_CONTACT_NUMBER:
                        case BaseItem.ITEM_TYPE_CONTACT_EMAIL: // Add Contact action
                        case BaseItem.ITEM_TYPE_CONTACT:
                        case BaseItem.ITEM_TYPE_CALL_LOG:
                            if (itemType == BaseItem.ITEM_TYPE_CONTACT_EMAIL || Helper.isUssd(item.getValue())) {
                                intent = new Intent(Intent.ACTION_INSERT);
                                intent.setType(ContactsContract.Contacts.CONTENT_TYPE);

                                intent.putExtra(itemType == BaseItem.ITEM_TYPE_CONTACT_NUMBER ?
                                    ContactsContract.Intents.Insert.PHONE :
                                    ContactsContract.Intents.Insert.EMAIL, item.getValue());
                            } else {
                                // Send SMS. TODO: Local/inline SMS handling
                                intent = new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", item.getValue(), null));
                            }
                            break;
                    }
                    break;

                case 3:
                    // Tertiary action
                    switch (itemType) {
                        default:
                            break;
                        case BaseItem.ITEM_TYPE_CONTACT_NUMBER:
                        case BaseItem.ITEM_TYPE_CALL_LOG:
                            intent = new Intent(Intent.ACTION_INSERT);
                            intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
                            intent.putExtra(ContactsContract.Intents.Insert.PHONE, item.getValue());
                            break;
                    }
                    break;

            }

            if (intent != null && context != null) {
                try {
                    context.startActivity(intent);
                } catch (ActivityNotFoundException ex) {
                    Toast.makeText(context, context.getString(R.string.cannot_handle_action), Toast.LENGTH_SHORT);
                }
            }
        }
    }

    private class LoadImageFileTask extends AsyncTask<String, Void, Bitmap> {
        private ImageView imageView;

        public LoadImageFileTask(ImageView imageView) {
            this.imageView = imageView;
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            if (params.length == 0 || Helper.isEmpty(params[0])) {
                return null;
            }
            File file = new File(Uri.parse(params[0]).getPath());
            if (file.exists()) {
                final BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(file.getAbsolutePath(), options);

                // Calculate inSampleSize
                options.inSampleSize = Helper.calculateInSampleSize(options, 256, 192);

                // Decode bitmap with inSampleSize set
                options.inJustDecodeBounds = false;
                Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), options);
                return bitmap;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if (result != null && imageView != null) {
                imageView.setImageBitmap(result);
            }
            imageView.setVisibility((result != null) ? View.VISIBLE : View.GONE);
        }
    }
}
