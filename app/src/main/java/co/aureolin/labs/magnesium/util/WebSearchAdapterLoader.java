package co.aureolin.labs.magnesium.util;

import co.aureolin.labs.magnesium.adapter.FeedItemListAdapter;
import co.aureolin.labs.magnesium.adapter.FeedListAdapter;

/**
 * Created by akinwale on 17/02/2016.
 */
public interface WebSearchAdapterLoader {
    public void searchWebItems(String filterString, FeedItemListAdapter adapter);
}
