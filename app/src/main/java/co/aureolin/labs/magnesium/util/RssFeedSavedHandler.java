package co.aureolin.labs.magnesium.util;

import co.aureolin.labs.magnesium.model.RssFeed;

/**
 * Created by akinwale on 16/02/2016.
 */
public interface RssFeedSavedHandler {
    public void onRssFeedSaved(RssFeed rssFeed);
}
