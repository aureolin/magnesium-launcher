package co.aureolin.labs.magnesium.model;

import co.aureolin.labs.magnesium.util.Helper;

public class SettingItem {

    private String name;

    private String value;

    private String requiredSku;

    private String annotation;

    private boolean owned;

    private boolean showAnnotation;

    public SettingItem() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getRequiredSku() {
        return requiredSku;
    }

    public void setRequiredSku(String requiredSku) {
        this.requiredSku = requiredSku;
    }

    public boolean isOwned() {
        return owned;
    }

    public void setOwned(boolean owned) {
        this.owned = owned;
    }

    public String getAnnotation() {
        return annotation;
    }

    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    public String getDisplayValue() {
        return Helper.getDisplayValueForSettingValue(value);
    }

    public boolean shouldShowAnnotation() {
        return showAnnotation;
    }

    public void setShowAnnotation(boolean showAnnotation) {
        this.showAnnotation = showAnnotation;
    }
}
