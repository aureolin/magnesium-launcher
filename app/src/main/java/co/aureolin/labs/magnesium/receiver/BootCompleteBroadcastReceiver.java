package co.aureolin.labs.magnesium.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import co.aureolin.labs.magnesium.service.CoreService;
import co.aureolin.labs.magnesium.service.IndexingService;
import co.aureolin.labs.magnesium.service.SyncService;

/**
 * Created by akinwale on 21/02/2016.
 */
public class BootCompleteBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent coreServiceIntent = new Intent(context, CoreService.class);
        context.startService(coreServiceIntent);

        Intent indexingServiceIntent = new Intent(context, IndexingService.class);
        context.startService(indexingServiceIntent);

        Intent syncServiceIntent = new Intent(context, SyncService.class);
        context.startService(syncServiceIntent);
    }
}