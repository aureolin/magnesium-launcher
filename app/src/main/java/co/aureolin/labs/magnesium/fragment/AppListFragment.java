package co.aureolin.labs.magnesium.fragment;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.ClipData;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Display;
import android.view.DragEvent;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.TypeFactory;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import co.aureolin.labs.magnesium.HomeActivity;
import co.aureolin.labs.magnesium.R;
import co.aureolin.labs.magnesium.SettingsActivity;
import co.aureolin.labs.magnesium.adapter.AppItemListAdapter;
import co.aureolin.labs.magnesium.fragment.settings.PersonalisationFragment;
import co.aureolin.labs.magnesium.model.AppItem;
import co.aureolin.labs.magnesium.model.Folder;
import co.aureolin.labs.magnesium.model.IconPack;
import co.aureolin.labs.magnesium.util.listener.AppListListener;
import co.aureolin.labs.magnesium.util.Helper;
import co.aureolin.labs.magnesium.util.listener.GenericItemClickListener;

/**
 * Created by akinwale on 07/02/2016.
 */
public class AppListFragment extends Fragment {
    //private static Map<String, AppItem> CachedAppList = new HashMap<String, AppItem>();
    private static SecureRandom random = new SecureRandom();

    private static final int APP_LIST_WIDTH = 88;

    private static final int APP_LIST_WIDTH_SQUARE = 96;

    private static final int APP_LIST_HEIGHT = 104;

    private static final int FOLDER_POPOUT_PADDING = 6;

    private static final int FOLDER_POPOUT_BOTTOM_HEIGHT = 44;

    private Context currentContext;

    private boolean popoutAlignedRight;

    private AppListListener appListListener;

    public static Map<String, Bitmap> FolderIconCache = new HashMap<String, Bitmap>();

    public static Map<String, Drawable> AppIconCache = new HashMap<String, Drawable>();

    public static Map<String, Integer> DefaultAppsRank = new HashMap<String, Integer>();
    static {
        DefaultAppsRank.put("com.android.dialer", 1);
        DefaultAppsRank.put("com.android.contacts", 2);
        DefaultAppsRank.put("com.android.mms", 3);
        DefaultAppsRank.put("com.android.settings", 4);
        DefaultAppsRank.put("com.android.calendar", 5);
        DefaultAppsRank.put("com.android.email", 6);
        DefaultAppsRank.put("com.android.deskclock", 7);
        DefaultAppsRank.put("com.android.camera2", 8);
    }

    private RecyclerView appsGrid;

    private float appsGridScrollTopLimit;

    private float appsGridScrollBottomLimit;

    private AppItemListAdapter appListAdapter;

    private EditText searchInput;

    private TextView noResultsView;

    private View noResultsContainer;

    private Button playStoreSearchButton;

    private View clearInputView;

    private ImageView sourceDragIcon;

    private boolean inSelectionMode;

    private boolean inDragDropMode;

    private ProgressBar appListProgress;

    private GridView localAppDock;

    private Handler handler;

    private View appListFindAppInputContainer;

    private View appListDragContextContainer;

    private View appListUninstallView;

    private View appListMoveToAppDrawerView;

    private boolean hasExited;

    private boolean hasEnteredFolderPopout;

    private boolean folderOverlayShown;

    private boolean folderBeingCreated;

    private View currentAppIconDragView;

    private PopupWindow folderPopup;

    private AppIconDragEventListener appIconDragListener;

    private String currentFolderUniqueId;

    private String dropTargetFolderUniqueId;

    private TextWatcher folderNameTextWatcher;

    private AppItemListAdapter folderListAdapter;

    private boolean commitFoldersInProgress;

    private RecyclerView folderRecyclerView;

    public static Resources IconPackResources;

    public static float IconPackFactor;

    private static final HashMap<String, Folder> folders = new HashMap<String, Folder>();

    private static final HashMap<String, String> packagesFoldersMap = new HashMap<String, String>();

    private static final ObjectMapper objectMapper = new ObjectMapper();

    private View.DragShadowBuilder appIconShadow;

    private String dragSourceFolderId;

    private View dragSourceFolderView;

    private boolean shouldCancelDragFromFolder;

    private View folderPopoutOverlay;

    private View folderPopoutPlaceholder;

    //private List<Bitmap> iconPackBackImages = new ArrayList<Bitmap>();

    public static Bitmap IconPackMaskImage;

    public static Bitmap IconPackFrontImage;

    public static Map<String, String> IconPackDrawables = new HashMap<String, String>();

    public static AppListFragment newInstance() {
        return new AppListFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_app_list, container, false);

        noResultsContainer = rootView.findViewById(R.id.app_list_no_match_container);
        noResultsView = (TextView) rootView.findViewById(R.id.app_list_no_match_found);

        appListFindAppInputContainer = rootView.findViewById(R.id.app_list_find_app_input_container);
        appListDragContextContainer = rootView.findViewById(R.id.app_list_drag_context_container);

        appListMoveToAppDrawerView = rootView.findViewById(R.id.app_list_move_to_app_drawer_container);
        appListMoveToAppDrawerView.setOnDragListener(new View.OnDragListener() {
            private ImageView iconView = (ImageView) appListMoveToAppDrawerView.findViewById(R.id.app_list_move_to_app_drawer_icon);

            private TextView textView = (TextView) appListMoveToAppDrawerView.findViewById(R.id.app_list_move_to_app_drawer_text);

            @Override
            public boolean onDrag(View view, DragEvent event) {
                final int action = event.getAction();
                Object source = event.getLocalState();
                if (!(source instanceof AppItem)) {
                    return false;
                }

                AppItem sourceItem = (AppItem) source;
                switch (action) {
                    case DragEvent.ACTION_DRAG_STARTED:
                        if (!sourceItem.isFolder()) {
                            return true;
                        }

                        return false;

                    case DragEvent.ACTION_DRAG_ENTERED:
                        if (textView != null && iconView != null) {
                            int colour = Color.parseColor("#1976D2");

                            textView.setTextColor(colour);
                            iconView.setColorFilter(new PorterDuffColorFilter(colour, PorterDuff.Mode.SRC_ATOP));

                            textView.invalidate();
                            iconView.invalidate();
                        }
                        return true;

                    case DragEvent.ACTION_DRAG_EXITED:
                        if (textView != null && iconView != null) {
                            textView.setTextColor(Color.WHITE);
                            iconView.setColorFilter(new PorterDuffColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP));
                            textView.invalidate();
                            iconView.invalidate();
                        }
                        return true;

                    case DragEvent.ACTION_DROP:
                        if (Helper.isEmpty(dragSourceFolderId)) {
                            return false;
                        }
                        moveItemFromFolderToAppDrawer(sourceItem, dragSourceFolderId);
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                updateAppListViewTags();
                            }
                        });
                        return true;

                    case DragEvent.ACTION_DRAG_ENDED:
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                appListDragContextContainer.setVisibility(View.GONE);
                                appListMoveToAppDrawerView.setVisibility(View.GONE);
                                appListFindAppInputContainer.setVisibility(View.VISIBLE);
                                if (textView != null && iconView != null) {
                                    textView.setTextColor(Color.WHITE);
                                    iconView.setColorFilter(new PorterDuffColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP));

                                    textView.invalidate();
                                    iconView.invalidate();
                                }
                            }
                        });

                        resetDragDropVariables();
                        updateAppListViews();
                        return true;

                    default:
                        break;
                }

                return false;
            }
        });

        appListUninstallView = rootView.findViewById(R.id.app_list_uninstall_container);
        appListUninstallView.setOnDragListener(new View.OnDragListener() {
            private ImageView iconView = (ImageView) appListUninstallView.findViewById(R.id.app_list_uninstall_icon);

            private TextView textView = (TextView) appListUninstallView.findViewById(R.id.app_list_uninstall_text);

            @Override
            public boolean onDrag(View view, DragEvent event) {
                final int action = event.getAction();
                Object source = event.getLocalState();
                if (!(source instanceof AppItem)) {
                    return false;
                }

                AppItem sourceItem = (AppItem) source;
                switch (action) {
                    case DragEvent.ACTION_DRAG_STARTED:
                        if (!sourceItem.isFolder() && !sourceItem.isSystemApp()) {
                            return true;
                        }

                        return false;

                    case DragEvent.ACTION_DRAG_ENTERED:
                          if (textView != null && iconView != null) {
                              textView.setTextColor(Color.RED);
                              iconView.setColorFilter(new PorterDuffColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP));
                              textView.invalidate();
                              iconView.invalidate();
                        }
                        return true;

                    case DragEvent.ACTION_DRAG_EXITED:
                        if (textView != null && iconView != null) {
                            textView.setTextColor(Color.WHITE);
                            iconView.setColorFilter(new PorterDuffColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP));
                            textView.invalidate();
                            iconView.invalidate();
                        }
                        return true;

                    case DragEvent.ACTION_DROP:
                        sourceItem = (AppItem) source;
                        performUninstall(sourceItem);
                        return true;

                    case DragEvent.ACTION_DRAG_ENDED:
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                appListDragContextContainer.setVisibility(View.GONE);
                                appListMoveToAppDrawerView.setVisibility(View.GONE);
                                appListFindAppInputContainer.setVisibility(View.VISIBLE);

                                if (textView != null && iconView != null) {
                                    textView.setTextColor(Color.WHITE);
                                    iconView.setColorFilter(new PorterDuffColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP));

                                    textView.invalidate();
                                    iconView.invalidate();
                                }
                            }
                        });

                        resetDragDropVariables();
                        updateAppListViews();
                        return true;

                    default:
                        break;
                }

                return false;
            }
        });

        Typeface normalTypeface = Helper.getTypeface("normal", getContext());
        ((TextView) appListMoveToAppDrawerView.findViewById(R.id.app_list_move_to_app_drawer_text)).setTypeface(normalTypeface);
        ((TextView) appListUninstallView.findViewById(R.id.app_list_uninstall_text)).setTypeface(normalTypeface);

        folderPopoutOverlay = rootView.findViewById(R.id.app_list_folder_popout_overlay);
        folderPopoutOverlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (folderPopoutPlaceholder != null) {
                    EditText folderNameInput = (EditText) folderPopoutPlaceholder.findViewById(R.id.folder_popout_name_input);
                    String folderName = folderNameInput.getText().toString();

                    Folder folder = folders.get(currentFolderUniqueId);
                    if (folder != null) {
                        String prevFolderName = folders.get(currentFolderUniqueId).getName();
                        if (prevFolderName != null && !prevFolderName.equals(folderName)) {
                            updatePopoutFolderName(folderName, folder.getUniqueId());
                        }
                    }

                    Helper.clearEditTextFocus(folderNameInput, getContext());
                }

                dismissFolderPopoutPlaceholder(popoutAlignedRight, true);
            }
        });

        folderPopoutPlaceholder = rootView.findViewById(R.id.app_list_folder_popout_container);
        folderPopoutPlaceholder.setOnDragListener(new FolderPopoutDragEventListener());

        clearInputView = rootView.findViewById(R.id.app_list_clear_find_app);
        appListProgress = (ProgressBar) rootView.findViewById(R.id.app_list_load_progress);
        localAppDock = (GridView) rootView.findViewById(R.id.app_list_app_dock_grid);
        localAppDock.scrollTo(0, 8);
        localAppDock.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object item = parent.getAdapter().getItem(position);
                HomeActivity activity = (HomeActivity) getContext();
                if (activity != null) {
                    if (activity.isAppSelectorOpen()) {
                        activity.closeAppSelector();
                    }

                    if (item instanceof AppItem) {
                        AppItem appItem = (AppItem) item;
                        activity.launchApp(appItem.getPackageName(), appItem.getActivityName());
                    }
                }
            }
        });
        localAppDock.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_MOVE) {
                    return true;
                }
                return false;
            }
        });
        localAppDock.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                HomeActivity activity = (HomeActivity) getContext();
                if (activity != null) {
                    activity.showAppSelector(position);
                }
                return true;
            }
        });

        searchInput = (EditText) rootView.findViewById(R.id.app_list_find_app_input);
        searchInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_DONE
                || actionId == EditorInfo.IME_ACTION_GO
                || actionId == EditorInfo.IME_ACTION_SEARCH) {
                Helper.clearEditTextFocus(searchInput, getContext());
                return true;
            }

            return false;
            }
        });

        searchInput.setTypeface(HomeActivity.getNormalTypeface(getContext()));
        searchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (appListAdapter != null) {
                    final String text = searchInput.getText().toString();
                    if (text.trim().length() > 0) {
                        appListAdapter.filter(text);
                    } else {
                        appListAdapter.filter(null);
                    }
                    clearInputView.setVisibility(text.trim().length() > 0 ? View.VISIBLE : View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        clearInputView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if (searchInput.getText().length() > 0) {
                searchInput.setText(null);
            }
            clearInputView.setVisibility(View.GONE);
            }
        });

        playStoreSearchButton = (Button) rootView.findViewById(R.id.app_list_search_play_store);
        playStoreSearchButton.setTypeface(Helper.getTypeface("normal", getContext()));
        playStoreSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String q = searchInput.getText().toString();
                if (!Helper.isEmpty(q)) {
                    q = Uri.encode(q);
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("market://search?c=apps&q=%s", q))));
                    } catch (android.content.ActivityNotFoundException ex) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("https://play.google.com/store/search?c=apps&q=%s", q))));
                    }
                }
            }
        });

        appsGrid = (RecyclerView) rootView.findViewById(R.id.app_list_grid_view);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(), HomeActivity.APP_GRID_COLUMN_COUNT);
        appsGrid.setLayoutManager(layoutManager);

        //appsGrid.addItemDecoration(new GridSpaceItemDecoration(0, 12, HomeActivity.APP_GRID_COLUMN_COUNT));
        appsGrid.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View view, DragEvent event) {
                final int action = event.getAction();
                Object source = event.getLocalState();
                if (!(source instanceof AppItem)) {
                    return false;
                }

                int position = -1;
                AppItem sourceItem = (AppItem) source;
                switch (action) {
                    case DragEvent.ACTION_DRAG_STARTED:
                        if (Helper.isEmpty(dragSourceFolderId)) {
                            return false;
                        }
                        return true;
                    case DragEvent.ACTION_DRAG_ENTERED:
                        return true;
                    case DragEvent.ACTION_DRAG_EXITED:
                        return true;
                    case DragEvent.ACTION_DRAG_LOCATION:
                        lastDragEventY = event.getY();
                        appsGridScrollBottomLimit = appsGrid.getMeasuredHeight() - Helper.getValueForDip(8, getContext());

                        View underView = appsGrid.findChildViewUnder(event.getX(), event.getY());
                        if (underView != null && dragSourceFolderView != null
                                && underView.getTag() != null
                                && underView.getTag().equals(dragSourceFolderView.getTag())) {
                            try {
                                position = Integer.parseInt(underView.getTag().toString());
                                AppItem targetItem = appListAdapter.getItem(position);
                                if (targetItem != null) {
                                    if (dragSourceFolderId.equals(targetItem.getUniqueId())) {
                                        displayFolderPopoutPlaceholder(popoutAlignedRight, false);
                                    } else {
                                        alternativeShowFolderPopout(targetItem, underView, true, false);
                                    }

                                    hasExited = true;
                                    hasEnteredFolderPopout = true;
                                }
                            } catch (NumberFormatException ex) {
                                // Skip
                            }
                        }

                        /*if (lastDragEventY >= appsGridScrollBottomLimit) {
                            if (!appsGridIsScrolling && appsGrid.canScrollVertically(1)) {
                                handler.removeCallbacksAndMessages(null);
                                if (scrollAppsGridDownTask == null) {
                                    scrollAppsGridDownTask = new TimerTask() {
                                        @Override
                                        public void run() {
                                            if (lastDragEventY < appsGridScrollBottomLimit || !appsGrid.canScrollVertically(1)) {
                                                appsGridIsScrolling = false;
                                                scrollAppsGridDownTimer.cancel();

                                                scrollAppsGridDownTimer = null;
                                                scrollAppsGridDownTask = null;

                                                HomeActivity.thisActivity.runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        appsGrid.requestLayout();
                                                    }
                                                });
                                                return;
                                            }

                                            appsGridIsScrolling = true;
                                            appsGrid.smoothScrollBy(0, Helper.getValueForDip(24, getContext()));

                                            HomeActivity.thisActivity.runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    appsGrid.requestLayout();
                                                }
                                            });
                                        }
                                    };
                                }
                                scrollAppsGridDownTimer = new Timer();
                                scrollAppsGridDownTimer.scheduleAtFixedRate(scrollAppsGridDownTask, 0, 100);
                            }
                        }*/

                        return true;
                    case DragEvent.ACTION_DROP:
                        if (Helper.isEmpty(dragSourceFolderId)) {
                            return false;
                        }

                        moveItemFromFolderToAppDrawer(sourceItem, dragSourceFolderId);
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                updateAppListViewTags();
                            }
                        });

                        return true;
                    case DragEvent.ACTION_DRAG_ENDED:
                        resetDragDropVariables();
                        updateAppListViews();
                        return true;
                    default:

                        break;
                }

                return false;
            }
        });

        folderNameTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String folderName = editable.toString().trim();
                if (currentFolderUniqueId != null && !Helper.isEmpty(folderName)
                        && folders.containsKey(currentFolderUniqueId)) {
                    folders.get(currentFolderUniqueId).setName(folderName);
                }
            }
        };

        return rootView;
    }

    private boolean appsGridIsScrolling;

    private float lastDragEventY;

    private Timer scrollAppsGridDownTimer;

    private TimerTask scrollAppsGridDownTask;

    private void moveItemFromFolderToAppDrawer(AppItem sourceItem, String sourceFolderId) {
        Folder sourceFolder = folders.get(sourceFolderId);
        if (sourceFolder != null) {
            int folderIndex = appListAdapter.getCurrentIndexForFolder(sourceFolder.getUniqueId());
            if (folderIndex > -1) {
                List<AppItem> folderItems = appListAdapter.getItem(folderIndex).getFolderItems();

                int index = folderItems.indexOf(sourceItem);
                if (index > -1) {
                    AppItem removedItem = folderItems.remove(index);

                    if (folderListAdapter != null) {
                        int adapterIndex = folderListAdapter.indexOf(removedItem);
                        if (adapterIndex > -1) {
                            folderListAdapter.removeItem(adapterIndex);
                            folderListAdapter.notifyItemRemoved(adapterIndex);
                        }
                    }

                    // Add the item to appListAdapter
                    appListAdapter.sortedAddItem(removedItem, true);

                    sourceFolder.getActivities().remove(removedItem.getFullActivityName());
                }

                if (folderItems.size() == 0) {
                    // Delete the folder since there are no items in it
                    folders.remove(sourceFolder.getUniqueId());
                    appListAdapter.removeItem(appListAdapter.getItem(folderIndex), true);
                } else {
                    if (folderItems.size() <= 4 && FolderIconCache.containsKey(sourceFolder.getUniqueId())) {
                        FolderIconCache.remove(sourceFolder.getUniqueId());
                    }

                    appListAdapter.notifyItemChanged(folderIndex);
                }

                // commit folders
                commitFolders();
            }
        }
    }

    private void clearSelectionMode() {
        if (appListAdapter != null) {
            for (int i = 0; i < appListAdapter.getItemCount(); i++) {
                /*if (appListAdapter.getItem(i).isSelected()) {
                    appListAdapter.getItem(i).setSelected(false);
                }*/
            }
            appListAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                initialiseAppList(HomeActivity.thisActivity.shouldRefreshAppList());
            }
        });
        inSelectionMode = false;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (appListDragContextContainer != null) {
            appListDragContextContainer.setVisibility(View.INVISIBLE);
        }
        if (appListFindAppInputContainer != null) {
            appListFindAppInputContainer.setVisibility(View.VISIBLE);
        }

        if (searchInput != null) {
            searchInput.clearFocus();
        }

        if (handler == null) {
            handler = new Handler();
        }

        if (appIconDragListener == null) {
            appIconDragListener = new AppIconDragEventListener();
        }

        HomeActivity activity = (HomeActivity) getActivity();
        if (activity != null) {
            // Check previous and current app icons
            SharedPreferences preferences = activity.getSharedPreferences(HomeActivity.COMMON_SHARED_PREF_NAME, Context.MODE_PRIVATE);
            IconPack prevIconPack = activity.getCurrentIconPack();
            IconPack currentIconPack = IconPack.fromSettingValue(
                preferences.getString(HomeActivity.SETTING_KEY_ICON_PACK, HomeActivity.DEFAULT_VALUE_ICON_PACK_NONE));
            if (!currentIconPack.equals(prevIconPack)) {
                activity.loadIconPack();
                reloadAppIcons();
            }


            if (!activity.isAppDockLoaded()) {
                activity.setAppListAppDock(localAppDock);
                activity.initialLoadAppList();
                activity.setAppDockLoaded(true);
            }
        }

        View view = getView();
        if (appsGrid == null && view != null) {
            appsGrid = (RecyclerView) view.findViewById(R.id.app_list_grid_view);
            if (appsGrid.getAdapter() == null) {
                appsGrid.setAdapter(appListAdapter);
            }
        }
    }

    public static void InitIconPack(IconPack iconPack) {
        AppIconCache.clear();
        IconPackFactor = 0;
        IconPackMaskImage = null;
        IconPackFrontImage = null;
        IconPackDrawables.clear();
        IconPackResources = null;

        HomeActivity activity = HomeActivity.thisActivity;
        PackageManager pm = activity.getPackageManager();
        try {
            XmlPullParser parser = null;

            IconPackResources = pm.getResourcesForApplication(iconPack.getPackageName());
            int appFilterId = IconPackResources.getIdentifier("appfilter", "xml", iconPack.getPackageName());
            if (appFilterId > 0) {
                parser = IconPackResources.getXml(appFilterId);
            } else {
                // Try loading from the assets folder
                try
                {
                    InputStream appfilterstream = IconPackResources.getAssets().open("appfilter.xml");

                    XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    parser = factory.newPullParser();
                    parser.setInput(appfilterstream, "utf-8");
                }
                catch (IOException ioe)
                {

                }
            }

            if (parser != null) {
                int eventType = parser.getEventType();
                while (eventType != XmlPullParser.END_DOCUMENT)
                {
                    if (eventType == XmlPullParser.START_TAG)
                    {
                            /*if (parser.getName().equals("iconback"))
                            {
                                for(int i = 0; i < parser.getAttributeCount(); i++)
                                {
                                    if (parser.getAttributeName(i).startsWith("img"))
                                    {
                                        String drawableName = parser.getAttributeValue(i);
                                        Bitmap iconback = loadIconPackBitmap(IconPackResources, drawableName, iconPack);
                                        if (iconback != null) {
                                            iconPackBackImages.add(iconback);
                                        }
                                    }
                                }
                            }
                            else */
                        if (parser.getName().equals("iconmask"))
                        {
                            if (parser.getAttributeCount() > 0 && parser.getAttributeName(0).equals("img1"))
                            {
                                String drawableName = parser.getAttributeValue(0);
                                IconPackMaskImage = loadIconPackBitmap(IconPackResources, drawableName, iconPack);
                            }
                        }
                        else if (parser.getName().equals("iconupon"))
                        {
                            if (parser.getAttributeCount() > 0 && parser.getAttributeName(0).equals("img1"))
                            {
                                String drawableName = parser.getAttributeValue(0);
                                IconPackFrontImage = loadIconPackBitmap(IconPackResources, drawableName, iconPack);
                            }
                        }
                        else if (parser.getName().equals("scale"))
                        {
                            // mFactor
                            if (parser.getAttributeCount() > 0 && parser.getAttributeName(0).equals("factor"))
                            {
                                IconPackFactor = Float.valueOf(parser.getAttributeValue(0));
                            }
                        }
                        else if (parser.getName().equals("item"))
                        {
                            String componentName = null;
                            String drawableName = null;

                            for(int i=0; i < parser.getAttributeCount(); i++)
                            {
                                if (parser.getAttributeName(i).equals("component"))
                                {
                                    componentName = parser.getAttributeValue(i);
                                }
                                else if (parser.getAttributeName(i).equals("drawable"))
                                {
                                    drawableName = parser.getAttributeValue(i);
                                }
                            }
                            if (!IconPackDrawables.containsKey(componentName)) {
                                IconPackDrawables.put(componentName, drawableName);
                            }
                        }
                    }
                    eventType = parser.next();
                }
            }
        } catch (PackageManager.NameNotFoundException e) {
            //Ln.d("Cannot load icon pack");
        } catch (XmlPullParserException e) {
            //Ln.d("Cannot parse icon pack appfilter.xml");
        } catch (IOException e) {

        }
    }

    public void reloadAppIcons() {
        HomeActivity activity = (HomeActivity) getActivity();
        if (activity != null) {
            activity.loadIconPack();
            IconPack iconPack = activity.getCurrentIconPack();

            AppIconCache.clear();
            IconPackFactor = 0;
            IconPackMaskImage = null;
            IconPackFrontImage = null;
            IconPackDrawables.clear();
            IconPackResources = null;

            if (iconPack.isNone()) {
                activity.reloadAppDock();
                return;
            }

            InitIconPack(iconPack);

            if (appListAdapter != null) {
                appListAdapter.notifyDataSetChanged();
            }

            activity.reloadAppDock();
        }
    }

    private static Bitmap loadIconPackBitmap(Resources iconPackResources, String drawableName, IconPack iconPack)
    {
        int id = iconPackResources.getIdentifier(drawableName, "drawable", iconPack.getPackageName());
        if (id > 0)
        {
            Drawable bitmap = ResourcesCompat.getDrawable(iconPackResources, id, null);
            if (bitmap instanceof BitmapDrawable) {
                return ((BitmapDrawable) bitmap).getBitmap();
            }
        }

        return null;
    }

    public static Bitmap createFolderIcon(AppItem folder, Context context) {
        int dimension = 32;
        List<AppItem> folderItems = folder.getFolderItems();
        int x = folderItems.size() == 1 ? 20 : 2;
        int y = folderItems.size() == 1 ? 20 : (folderItems.size() == 2 ? 22 : 2);

        Bitmap dst = Bitmap.createBitmap(72, 72, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(dst);

        for (int i = 0; i < folderItems.size() && i < 4; i++) {
            AppItem appItem = folderItems.get(i);
            Bitmap icon = drawableToBitmap(getIconForAppItem(appItem, context));
            Bitmap scaledIcon = resizeBitmap(icon, dimension, dimension);

            Rect srcRect = new Rect(0, 0, scaledIcon.getWidth(), scaledIcon.getHeight());
            Rect destRect = new Rect(x, y, x + dimension, y + dimension);
            canvas.drawBitmap(scaledIcon, srcRect, destRect, new Paint(Paint.FILTER_BITMAP_FLAG));

            if ((i + 1) % 2 == 0) {
                x = folderItems.size() == 3 ? 22 : 2;
                y += 36;
            } else {
                x += 36;
            }
        }


        Bitmap newDst = Bitmap.createBitmap(72, 72, Bitmap.Config.ARGB_8888);
        Canvas roundCanvas = new Canvas(newDst);
        Rect rect = new Rect(0, 0, newDst.getWidth(), newDst.getHeight());

        Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);
        roundCanvas.drawARGB(0, 0, 0, 0);

        paint.setColor(0xffff0000);
        roundCanvas.drawCircle(36, 36, 27, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        roundCanvas.drawBitmap(dst, rect, rect, paint);

        return newDst;
    }

    public static Bitmap resizeBitmap(Bitmap bitmap, int newWidth, int newHeight) {
        Bitmap scaledBitmap = Bitmap.createBitmap(newWidth, newHeight, Bitmap.Config.ARGB_8888);

        float ratioX = newWidth / (float) bitmap.getWidth();
        float ratioY = newHeight / (float) bitmap.getHeight();
        float middleX = newWidth / 2.0f;
        float middleY = newHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);
        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bitmap, middleX - bitmap.getWidth() / 2, middleY - bitmap.getHeight() / 2, paint);

        return scaledBitmap;
    }

    public static Bitmap drawableToBitmap (Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    private static Drawable loadIconPackDrawable(Resources iconPackResources, String drawableName, IconPack iconPack) {
        int id = iconPackResources.getIdentifier(drawableName, "drawable", iconPack.getPackageName());
        if (id > 0)
        {
            Drawable bitmap = ResourcesCompat.getDrawable(iconPackResources, id, null);
            return bitmap;
        }
        return null;
    }

    public void onUninstallLongClicked(AppItem appItem) {
        clearSelectionMode();
        inSelectionMode = false;
    }


    public List<AppItem> getCurrentAppList() {
        List<AppItem> appItems = new ArrayList<AppItem>();
        if (appListAdapter != null) {
            appItems.addAll(appListAdapter.getAllAppsList());
        }
        return appItems;
    }

    private void performUninstall(AppItem appItem) {
        Uri packageUri = Uri.parse(String.format("package:%s", appItem.getPackageName()));
        Intent uninstallIntent = new Intent(Intent.ACTION_UNINSTALL_PACKAGE, packageUri);
        startActivity(uninstallIntent);
    }

    private void initialiseAppList() {
        initialiseAppList(false);
    }

    public void refreshAppList() {
        initialiseAppList(true);
    }

    public void setAppListListener(AppListListener appListListener) {
        this.appListListener = appListListener;
    }

    public AppListListener getAppListListener() {
        return appListListener;
    }

    public void initialiseAppList(boolean force) {
        if (appListAdapter == null || appListAdapter.getItemCount() == 0 || force) {
            (new AsyncTask<Void, Void, List<AppItem>>() {
                private List<AppItem> allApps = new ArrayList<AppItem>();

                private List<AppItem> displayApps = new ArrayList<AppItem>();

                protected void onPreExecute() {
                    if (appListProgress != null && (appListAdapter == null || appListAdapter.getItemCount() == 0)) {
                        appListProgress.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                protected List<AppItem> doInBackground(Void... params) {
                    HomeActivity activity = HomeActivity.getHomeActivity();
                    if (activity != null) {
                        SharedPreferences preferences = activity.getSharedPreferences(HomeActivity.COMMON_SHARED_PREF_NAME, Context.MODE_PRIVATE);
                        Set<String> hiddenPackageNames = preferences.getStringSet(PersonalisationFragment.HIDDEN_APPS_KEY, new HashSet<String>());

                        // Load folders
                        folders.clear();
                        TypeFactory typeFactory = objectMapper.getTypeFactory();
                        MapType mapType = typeFactory.constructMapType(HashMap.class, String.class, Folder.class);
                        try {
                            folders.putAll((HashMap<String, Folder>) objectMapper.readValue(preferences.getString(HomeActivity.SETTING_KEY_FOLDERS, "{}"), mapType));
                        } catch (IOException ex) {
                            // Cannot read folders
                        }

                        Map<String, AppItem> foldersMap = new HashMap<String, AppItem>();
                        for (Map.Entry<String, Folder> entry : folders.entrySet()) {
                            Folder folder = entry.getValue();
                            List<String> activities = folder.getActivities();
                            for (int i = 0; i < activities.size(); i++) {
                                packagesFoldersMap.put(activities.get(i), folder.getUniqueId());
                            }

                            AppItem appFolder = new AppItem();
                            appFolder.setFolder(true);
                            appFolder.setName(folder.getName());
                            appFolder.setRank(0);
                            appFolder.setUniqueId(folder.getUniqueId());
                            appFolder.setFolderItems(new ArrayList<AppItem>());
                            foldersMap.put(entry.getKey(), appFolder);
                        }

                        PackageManager manager = activity.getPackageManager();
                        Intent i = new Intent(Intent.ACTION_MAIN, null);
                        i.addCategory(Intent.CATEGORY_LAUNCHER);

                        List<ApplicationInfo> installedApps = manager.getInstalledApplications(PackageManager.GET_META_DATA);
                        List<ResolveInfo> availableActivities = manager.queryIntentActivities(i, 0);
                        for (ResolveInfo ri : availableActivities) {
                            String packageName = ri.activityInfo.packageName;
                            String fullActivityName = String.format("%s.%s", packageName, ri.activityInfo.name);
                            if (hiddenPackageNames.contains(fullActivityName)) {
                                continue;
                            }

                            AppItem app = new AppItem();
                            app.setName(ri.loadLabel(manager).toString());
                            app.setPackageName(packageName);
                            app.setActivityInfo(ri.activityInfo);
                            app.setActivityName(ri.activityInfo.name);
                            app.setRank(DefaultAppsRank.containsKey(packageName) ? DefaultAppsRank.get(packageName) : 99);
                            app.setSystemApp((getFlagsForPackageName(packageName, installedApps) & (ApplicationInfo.FLAG_SYSTEM | ApplicationInfo.FLAG_UPDATED_SYSTEM_APP)) != 0);
                            allApps.add(app);

                            if (packagesFoldersMap.containsKey(ri.activityInfo.name)) {
                                String folderUniqueId = packagesFoldersMap.get(ri.activityInfo.name);
                                if (foldersMap.containsKey(folderUniqueId)) {
                                    foldersMap.get(folderUniqueId).getFolderItems().add(app);
                                }
                            } else {
                                displayApps.add(app);
                            }
                        }

                        for (AppItem folder : foldersMap.values()) {
                            if (folder.getFolderItems() != null && folder.getFolderItems().size() > 0) {
                                displayApps.add(folder);
                            }
                        }

                        Collections.sort(allApps);
                        Collections.sort(displayApps);
                    }

                    return displayApps;
                }

                private int getFlagsForPackageName(String packageName, List<ApplicationInfo> applicationInfo) {
                    for (int i = 0; i < applicationInfo.size(); i++) {
                        ApplicationInfo appInfo = applicationInfo.get(i);
                        if (appInfo.packageName.equals(packageName)) {
                            return appInfo.flags;
                        }
                    }
                    return 0;
                }

                @Override
                protected void onProgressUpdate(Void... progress) {
                    Context context = getContext();
                    /*if (context != null) {
                        if (appListAdapter == null) {
                            appListAdapter = new AppListAdapter(context);
                            appListAdapter.setNoResultsView(noResultsView);
                            appListAdapter.setUninstallClickHandler(AppListFragment.this);
                        }
                        List<AppItem> appItems = new ArrayList<AppItem>(displayApps);
                        Collections.sort(appItems);
                        appListAdapter.setOriginalList(appItems);
                        appListAdapter.notifyDataSetChanged();
                    }*/
                }

                @Override
                protected void onPostExecute(List<AppItem> displayApps) {
                    if (appListProgress != null) {
                        appListProgress.setVisibility(View.INVISIBLE);
                    }

                    new CacheAllAppIconsTask(allApps).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                    HomeActivity activity = HomeActivity.getHomeActivity();
                    if (activity != null) {
                        activity.setRefreshAppList(false);
                        SharedPreferences preferences = activity.getSharedPreferences(HomeActivity.COMMON_SHARED_PREF_NAME, Context.MODE_PRIVATE);
                        List<AppItem> dockedApps = new ArrayList<AppItem>();
                        for (int i = 0; i < HomeActivity.MAX_DOCK_ITEMS; i++) {
                            String configKey = String.format("%s%d", HomeActivity.APP_DOCK_FAVOURITE_KEY_PREFIX, i);
                            String configValue = preferences.getString(configKey, null);
                            boolean dockedAppFound = false;
                            if (configValue != null) {
                                String packageName = null;
                                String activityName = null;
                                if (!Helper.isEmpty(configValue)) {
                                    String[] parts = configValue.split("\\|");
                                    if (parts.length > 1) {
                                        packageName = parts[0];
                                        activityName = parts[1];
                                    }
                                }
                                if (!Helper.isEmpty(activityName)
                                        && !Helper.isEmpty(packageName)) {
                                    for (int j = 0; j < allApps.size(); j++) {
                                        AppItem appItem = allApps.get(j);
                                        if (packageName.equals(appItem.getPackageName())
                                                && activityName.equals(appItem.getActivityName())) {
                                            dockedApps.add(appItem);
                                            dockedAppFound = true;
                                            break;
                                        }
                                    }
                                }
                            }

                            if (!dockedAppFound && allApps.size() > i) {
                                dockedApps.add(allApps.get(i));
                            }
                        }

                        activity.initialiseAppDock(dockedApps);

                        if (appListAdapter == null) {
                            appListAdapter = new AppItemListAdapter(activity);
                            appListAdapter.setItemClickListener(new GenericItemClickListener() {
                                @Override
                                public void onItemClick(int position, View v) {
                                    if (position < 0) {
                                        return;
                                    }


                                    AppItem appItem = appListAdapter.getItem(position);
                                    if (appItem != null) {
                                        if (searchInput.getText().length() > 0) {
                                            searchInput.setText(null);
                                        }

                                        Context context = getContext();
                                        if (appItem.isFolder()) {
                                            dismissFolderPopoutPlaceholder(popoutAlignedRight, true);
                                            alternativeShowFolderPopout(appItem, v, false, false);
                                        } else {
                                            HomeActivity activity = (HomeActivity) context;
                                            if (activity != null) {
                                                activity.launchApp(appItem.getPackageName(), appItem.getActivityInfo().name);
                                            }
                                        }
                                    }
                                }

                                @Override
                                @SuppressWarnings("deprecation")
                                public void onItemLongClick(int position, View v) {
                                    AppItem appItem = appListAdapter.getItem(position);
                                    if (appItem != null && !appItem.isFolder()) {
                                        sourceDragIcon = (ImageView) v.findViewById(R.id.item_app_list_image);

                                        ClipData.Item item = new ClipData.Item("");
                                        ClipData clipData = new ClipData("", new String[] { "text/plain" } ,item);
                                        appIconShadow = new AppIconShadowBuilder(sourceDragIcon); //View.DragShadowBuilder(appIcon);
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                            sourceDragIcon.startDragAndDrop(clipData, appIconShadow, appItem, 0);
                                        } else {
                                            sourceDragIcon.startDrag(clipData, appIconShadow, appItem, 0);
                                        }

                                        if (!appItem.isFolder() && !appItem.isSystemApp()) {
                                            appListFindAppInputContainer.setVisibility(View.INVISIBLE);
                                            appListDragContextContainer.setVisibility(View.VISIBLE);
                                            appListUninstallView.setVisibility(View.VISIBLE);
                                        }
                                    }
                                }
                            });

                            appListAdapter.setNoResultsContainer(noResultsContainer);
                            appListAdapter.setNoResultsView(noResultsView);
                            appListAdapter.setItemDragListener(appIconDragListener);
                        }

                        //appListAdapter.setUninstallClickHandler(AppListFragment.this);
                        appListAdapter.setAllAppsList(allApps);
                        appListAdapter.setOriginalList(displayApps);

                        if (appsGrid != null && appsGrid.getAdapter() == null) {
                            appsGrid.setAdapter(appListAdapter);
                        }
                        appListAdapter.notifyDataSetChanged();

                        if (appListListener != null) {
                            appListListener.onAppListInitialised();
                        }
                    }
                }
            }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            if (HomeActivity.thisActivity != null) {
                HomeActivity.thisActivity.setRefreshAppList(false);
            }
        }
    }

    private void updatePopoutFolderName(String folderName, String folderUniqueId) {
        if (folderUniqueId != null && !Helper.isEmpty(folderName) && folders.containsKey(folderUniqueId)) {
            folders.get(folderUniqueId).setName(folderName);
        }
        commitFolders();

        // Update the adapter
        int index = appListAdapter.getCurrentIndexForFolder(folderUniqueId);
        if (index > -1) {
            appListAdapter.getItem(index).setName(folderName);
            appListAdapter.notifyItemChanged(index);
        }

        index = appListAdapter.getOriginalIndexForFolder(folderUniqueId);
        if (index > -1) {
            appListAdapter.getOriginalList().get(index).setName(folderName);
        }
    }

    public void alternativeShowFolderPopout(AppItem appItem, View anchor, boolean dragDropTarget, boolean expandForDrop) {
        final Context context = getContext();
        if (context != null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View popupView = layoutInflater.inflate(R.layout.content_folder_popout, null);

            final EditText folderNameInput = (EditText) popupView.findViewById(R.id.folder_popout_name_input);
            folderNameInput.setTypeface(Helper.getTypeface("bold", context));
            folderNameInput.setText(appItem.getName());
            folderNameInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int action, KeyEvent keyEvent) {
                    if (action == EditorInfo.IME_ACTION_DONE || action == EditorInfo.IME_ACTION_GO) {
                        Helper.clearEditTextFocus(folderNameInput, getContext());
                        String folderName = folderNameInput.getText().toString();
                        updatePopoutFolderName(folderName, currentFolderUniqueId);
                        return true;
                    }
                    return false;
                }
            });
            folderNameInput.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean hasFocus) {
                    if (!hasFocus) {
                        String folderName = folderNameInput.getText().toString();
                        updatePopoutFolderName(folderName, currentFolderUniqueId);
                    }
                }
            });

            if (dragDropTarget) {
                dropTargetFolderUniqueId = appItem.getUniqueId();
            } else {
                currentFolderUniqueId = appItem.getUniqueId();
            }

            if (folderNameTextWatcher != null) {
                folderNameInput.addTextChangedListener(folderNameTextWatcher);
            }

            int numItems = appItem.getNumFolderItems();
            if (dragDropTarget && expandForDrop && numItems <= 9) {
                numItems += 1;
            }

            int columns = 0;
            if (numItems <= 4) {
                columns = 2;
            } else if (numItems <= 9) {
                columns = 3;
            } else {
                columns = 4;
            }

            columns = Math.min(columns, HomeActivity.APP_GRID_COLUMN_COUNT);
            Point size = getPopupWidthAndHeightForNumItems(numItems);
            folderRecyclerView = (RecyclerView) popupView.findViewById(R.id.folder_popout_apps_grid);
            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(), columns);
            folderRecyclerView.setLayoutManager(layoutManager);
            if (dragDropTarget) {
                folderRecyclerView.setOnDragListener(new FolderPopoutDragEventListener());
            }

            folderListAdapter = new AppItemListAdapter(context);
            folderListAdapter.setTextColor(Color.parseColor("#7B7B7B"));
            folderListAdapter.setOriginalList(appItem.getFolderItems());
            folderRecyclerView.setAdapter(folderListAdapter);

            folderListAdapter.setItemClickListener(new GenericItemClickListener() {
                @Override
                public void onItemClick(int position, View v) {
                    AppItem itemInFolder = folderListAdapter.getItem(position);
                    if (itemInFolder != null) {
                        HomeActivity activity = (HomeActivity) getContext();
                        if (activity != null) {
                            activity.launchApp(itemInFolder.getPackageName(), itemInFolder.getActivityInfo().name);
                        }

                        Helper.clearEditTextFocus(folderNameInput, getContext());

                        dismissFolderPopoutPlaceholder(popoutAlignedRight, true);
                    }
                }

                @Override
                @SuppressWarnings("deprecation")
                public void onItemLongClick(int position, View v) {
                    AppItem appItem = folderListAdapter.getItem(position);
                    if (appItem != null && !appItem.isFolder()) {
                        sourceDragIcon = (ImageView) v.findViewById(R.id.item_app_list_image);
                        dragSourceFolderId = currentFolderUniqueId;
                        dragSourceFolderView = v;

                        ClipData.Item item = new ClipData.Item("");
                        ClipData clipData = new ClipData("", new String[] { "text/plain" } ,item);
                        appIconShadow = new AppIconShadowBuilder(sourceDragIcon); //View.DragShadowBuilder(appIcon);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            sourceDragIcon.startDragAndDrop(clipData, appIconShadow, appItem, 0);
                        } else {
                            sourceDragIcon.startDrag(clipData, appIconShadow, appItem, 0);
                        }

                        appListFindAppInputContainer.setVisibility(View.INVISIBLE);
                        appListDragContextContainer.setVisibility(View.VISIBLE);

                        boolean canUninstall = (!appItem.isFolder() && !appItem.isSystemApp());
                        int margin = canUninstall ? Helper.getValueForDip(60, context) : 0;

                        ((LinearLayout.LayoutParams) appListMoveToAppDrawerView.getLayoutParams()).setMargins(0, 0, margin, 0);
                        appListMoveToAppDrawerView.setVisibility(View.VISIBLE);
                        appListUninstallView.setVisibility(canUninstall ? View.VISIBLE : View.GONE);
                    }
                }
            });

            int dpWidth = Helper.getValueForDip(size.x, context);
            int dpHeight = Helper.getValueForDip(size.y, context);
            int popoutXOffset = 0;
            int popoutYOffset = Helper.getValueForDip(-90, context);

            float xOffset = appsGrid.getX();
            float yOffset = appsGrid.getY();

            xOffset += anchor.getX();
            yOffset += anchor.getY() + Helper.getValueForDip(6, context);

            WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point screenSize = new Point();
            display.getSize(screenSize);

            folderPopoutPlaceholder.getLayoutParams().width = dpWidth;
            folderPopoutPlaceholder.getLayoutParams().height = dpHeight;

            ((ViewGroup) folderPopoutPlaceholder).removeAllViews();
            ((ViewGroup) folderPopoutPlaceholder).addView(popupView);
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) folderPopoutPlaceholder.getLayoutParams();

            int leftMargin = 0;
            int rightMargin = 0;
            int topMargin = 0;
            int bottomMargin = 0;

            if ((xOffset + dpWidth) > screenSize.x) {
                // Check Align Right
                rightMargin = Helper.getValueForDip(12, context);
                popoutAlignedRight = true;

                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT, 0);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            } else {
                leftMargin = (int) xOffset;
                popoutAlignedRight = false;

                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 0);
            }

            if ((yOffset + dpHeight) > screenSize.y) {
                // Check Align Bottom
                bottomMargin = screenSize.y - Helper.getValueForDip(80, context);

                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            } else {
                topMargin = (int) yOffset;
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, 0);
            }

            layoutParams.setMargins(leftMargin, topMargin, rightMargin, bottomMargin);
            if (dragDropTarget) {
                // TODO: animate popout display
                dropTargetFolderUniqueId = appItem.getUniqueId();

                folderRecyclerView.setEnabled(false);
                folderNameInput.setEnabled(false);
            }

            currentFolderUniqueId = appItem.getUniqueId();
            folderPopoutPlaceholder.clearAnimation();
            displayFolderPopoutPlaceholder(popoutAlignedRight, !dragDropTarget);
        }
    }

    public void displayFolderPopoutPlaceholder(boolean popoutAlignedRight, boolean displayOverlay) {
        Animation animation = createScaleAnimationForPopout(0, 1, popoutAlignedRight);
        animation.setFillAfter(true);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                folderPopoutOverlay.bringToFront();
                folderPopoutPlaceholder.bringToFront();
                folderPopoutPlaceholder.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        folderPopoutPlaceholder.startAnimation(animation);
        if (folderPopoutOverlay != null) {
            folderPopoutOverlay.setVisibility(displayOverlay ? View.VISIBLE : View.GONE);
        }
    }

    public void dismissFolderPopoutPlaceholder(boolean popoutAlignedRight, boolean destroy) {
        Animation animation = createScaleAnimationForPopout(1, 0, popoutAlignedRight);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (folderPopoutPlaceholder != null) {
                    folderPopoutPlaceholder.setVisibility(View.GONE);
                    folderPopoutPlaceholder.invalidate();
                }
                if (folderPopoutOverlay != null) {
                    folderPopoutOverlay.setVisibility(View.GONE);
                }
                if (appsGrid != null) {
                    appsGrid.bringToFront();
                    appsGrid.invalidate();
                    appsGrid.requestLayout();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        if (folderPopoutPlaceholder.getVisibility() == View.VISIBLE) {
            folderPopoutPlaceholder.startAnimation(animation);
        }

        if (folderPopoutOverlay != null) {
            folderPopoutOverlay.setVisibility(View.GONE);
        }

        if (destroy) {
            popoutAlignedRight = false;
            currentFolderUniqueId = null;
            dropTargetFolderUniqueId = null;
        }
    }

    public Point getPopupWidthAndHeightForNumItems(int numItems) {
        int width = 0;
        int height = 0;

        if (numItems <= 4) {
            width = (APP_LIST_WIDTH + FOLDER_POPOUT_PADDING) * 2;
            height = (numItems == 2) ?
                    (APP_LIST_HEIGHT + FOLDER_POPOUT_BOTTOM_HEIGHT + FOLDER_POPOUT_PADDING) :
                    ((APP_LIST_HEIGHT * 2) + FOLDER_POPOUT_BOTTOM_HEIGHT + FOLDER_POPOUT_PADDING);
        } else if (numItems <= 9) {
            width = (APP_LIST_WIDTH + FOLDER_POPOUT_PADDING) * 3;
            height = (numItems <= 6) ?
                    ((APP_LIST_HEIGHT * 2) + FOLDER_POPOUT_BOTTOM_HEIGHT + FOLDER_POPOUT_PADDING) :
                    ((APP_LIST_HEIGHT * 3) + FOLDER_POPOUT_BOTTOM_HEIGHT + FOLDER_POPOUT_PADDING);
        } else {
            width = (APP_LIST_WIDTH + FOLDER_POPOUT_PADDING) * 4;
            int rows = (numItems <= 12) ? 3 : 4;
            height = ((APP_LIST_HEIGHT * rows) + FOLDER_POPOUT_BOTTOM_HEIGHT + FOLDER_POPOUT_PADDING);
        }

        Point point = new Point();
        point.set(width, height);

        return point;
    }

    public static int getIndexForActivityNameUsingPackageName(String packageName, List<String> activities) {
        for (int i = 0; i < activities.size(); i++) {
            if (activities.get(i).startsWith(packageName)) {
                return i;
            }
        }

        return -1;
    }

    public static int getIndexForPackageName(String packageName, List<AppItem> appItems) {
        for (int i = 0; i < appItems.size(); i++) {
            if (packageName.equals(appItems.get(i).getPackageName())) {
                return i;
            }
        }

        return -1;
    }

    public void onPackageUninstalled(String packageName) {
        if (appListAdapter != null) {
            List<String> matchingPackageKeys = new ArrayList<String>();
            for (String key : packagesFoldersMap.keySet()) {
                if (key.startsWith(packageName)) {
                    matchingPackageKeys.add(key);
                }
            }

            if (matchingPackageKeys.size() > 0) {
                List<String> processedUniqueIds = new ArrayList<String>();
                for (int i = 0; i < matchingPackageKeys.size(); i++) {
                    String uniqueId = packagesFoldersMap.get(matchingPackageKeys.get(i));
                    if (!processedUniqueIds.contains(uniqueId)) {
                        Folder folder = folders.get(uniqueId);
                        if (folder != null) {
                            List<String> activities = folder.getActivities();
                            int idxToRemove = getIndexForActivityNameUsingPackageName(packageName, activities);
                            while (idxToRemove > -1) {
                                activities.remove(idxToRemove);
                                idxToRemove = getIndexForActivityNameUsingPackageName(packageName, activities);
                            }

                            folders.put(uniqueId, folder);
                        }

                        int folderIndex = appListAdapter.getCurrentIndexForFolder(uniqueId);
                        if (folderIndex > -1) {
                            List<AppItem> folderItems = appListAdapter.getItem(folderIndex).getFolderItems();

                            int idxToRemove = getIndexForPackageName(packageName, folderItems);
                            while (idxToRemove > -1) {
                                AppItem removedItem = folderItems.remove(idxToRemove);
                                if (removedItem != null) {
                                    if (folderListAdapter != null && uniqueId.equals(currentFolderUniqueId)) {
                                        folderListAdapter.removeItem(removedItem, true);
                                    }
                                }

                                idxToRemove = getIndexForPackageName(packageName, folderItems);
                            }
                            appListAdapter.notifyItemChanged(folderIndex);
                        }

                        processedUniqueIds.add(uniqueId);
                    }
                }

                commitFolders();
            } else {
                int index = appListAdapter.getIndexForPackageName(packageName);
                while (index > -1) {
                    AppItem removedItem = appListAdapter.removeItem(index);
                    if (removedItem != null) {
                        appListAdapter.removeAppItemFromOriginalList(removedItem);
                    }
                    appListAdapter.notifyItemRemoved(index);
                    index = appListAdapter.getIndexForPackageName(packageName);
                }
            }

            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    updateAppListViewTags();
                }
            });
        }
    }

    public void commitFolders() {
        if (commitFoldersInProgress) {
            return;
        }

        (new AsyncTask<Void, Void, Void>() {
            @Override
            protected void onPreExecute() {
                commitFoldersInProgress = true;
            }

            @Override
            protected Void doInBackground(Void... params) {
                Context context = getContext();
                if (context != null) {
                    SharedPreferences preferences = context.getSharedPreferences(HomeActivity.COMMON_SHARED_PREF_NAME, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();

                    try {
                        String json = objectMapper.writeValueAsString(folders);
                        editor.putString(HomeActivity.SETTING_KEY_FOLDERS, json);
                    } catch (JsonProcessingException ex) {
                        // Do nothing
                    }

                    editor.commit();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                commitFoldersInProgress = false;
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void saveNewFolder(final AppItem appItem) {
        if (!appItem.isFolder()) {
            return;
        }

        Folder folder = Folder.fromAppItem(appItem);
        if (!folders.containsKey(folder.getUniqueId())) {
            folders.put(folder.getUniqueId(), folder);
        }

        commitFolders();
    }

    public void updateAppListViews() {
        if (appListAdapter != null) {
            for (int i = 0; i < appListAdapter.getItemCount(); i++) {
                //AppItem appItem = appListAdapter.getItem(i);
                if (appsGrid != null) {
                    AppItemListAdapter.ViewHolder viewHolder = (AppItemListAdapter.ViewHolder) appsGrid.findViewHolderForAdapterPosition(i);
                    if (viewHolder != null && viewHolder.getContainerView() != null) {
                        TextView title = (TextView) viewHolder.getContainerView().findViewById(R.id.item_app_list_title);
                        if (title.getVisibility() != View.VISIBLE) {
                            title.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }
        }
    }

    public void updateAppListViewTags() {
        if (appListAdapter != null) {
            for (int i = 0; i < appListAdapter.getItemCount(); i++) {
                //AppItem appItem = appListAdapter.getItem(i);
                if (appsGrid != null) {
                    AppItemListAdapter.ViewHolder viewHolder = (AppItemListAdapter.ViewHolder) appsGrid.findViewHolderForAdapterPosition(i);
                    if (viewHolder != null && viewHolder.getContainerView() != null) {
                        viewHolder.getContainerView().setTag(i);
                    }
                }
            }
        }
    }

    private class FolderPopoutDragEventListener implements View.OnDragListener {
        public boolean onDrag(final View v, DragEvent event) {
            final int action = event.getAction();
            Object source = event.getLocalState();
            if (!(source instanceof AppItem)) {
                return false;
            }

            int position = -1;
            AppItem sourceItem = (AppItem) source;
            switch (action) {
                case DragEvent.ACTION_DRAG_STARTED:
                    return true;
                case DragEvent.ACTION_DRAG_ENTERED:
                    if (v.getVisibility() != View.VISIBLE) {
                        return false;
                    }
                    hasEnteredFolderPopout = true;
                    /*if (currentFolderUniqueId != null && !currentFolderUniqueId.equals(dragSourceFolderId)) {
                        displayFolderPopoutPlaceholder();
                    }*/
                    return true;
                case DragEvent.ACTION_DRAG_EXITED:
                    dismissFolderPopoutPlaceholder(popoutAlignedRight, false);
                    if (v != null) {
                        v.invalidate();
                        v.requestLayout();
                    }

                    if (appsGrid != null) {
                        appsGrid.invalidate();
                        appsGrid.requestLayout();
                    }

                    hasEnteredFolderPopout = false;
                    return true;
                case DragEvent.ACTION_DROP:
                    // TODO: Move items and then close the folder popout
                    if (currentFolderUniqueId != null && currentFolderUniqueId.equals(dragSourceFolderId)) {
                        if (folderPopoutOverlay.getVisibility() != View.VISIBLE) {
                            folderPopoutOverlay.setVisibility(View.VISIBLE);
                        }
                        folderPopoutOverlay.bringToFront();
                        folderPopoutPlaceholder.bringToFront();
                        return false;
                    }

                    Folder sourceFolder = folders.get(dragSourceFolderId);
                    Folder targetFolder = folders.get(dropTargetFolderUniqueId);
                    if (targetFolder != null) {
                        int folderIndex = appListAdapter.getCurrentIndexForFolder(targetFolder.getUniqueId());
                        if (folderIndex > -1) {
                            AppItem item = null;
                            if (sourceFolder != null) {
                                item = removeItemFromSourceFolder(sourceItem, sourceFolder);
                            } else {
                                item = appListAdapter.removeItem(sourceItem, true);
                            }

                            if (item != null) {
                                List<AppItem> folderItems = appListAdapter.getItem(folderIndex).getFolderItems();
                                if (folderItems == null) {
                                    folderItems = new ArrayList<AppItem>();
                                    appListAdapter.getItem(folderIndex).setFolderItems(folderItems);
                                }
                                int prevItemCount = folderItems.size();

                                String fullActivityName = item.getFullActivityName();
                                folderItems.add(item);
                                if (!targetFolder.getActivities().contains(fullActivityName)) {
                                    targetFolder.getActivities().add(fullActivityName);
                                }

                                packagesFoldersMap.put(fullActivityName, targetFolder.getUniqueId());

                                folderListAdapter.sortedAddItem(item, true);
                                if (prevItemCount <= 3 && FolderIconCache.containsKey(targetFolder.getUniqueId())) {
                                    FolderIconCache.remove(targetFolder.getUniqueId());
                                }

                                appListAdapter.notifyItemChanged(folderIndex);
                                if (folderPopoutOverlay.getVisibility() != View.VISIBLE) {
                                    folderPopoutOverlay.setVisibility(View.VISIBLE);
                                }
                                folderPopoutOverlay.bringToFront();
                                folderPopoutPlaceholder.bringToFront();

                                /*handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        dismissFolderPopoutPlaceholder(popoutAlignedRight, true);
                                    }
                                }, 300);*/
                            }

                            folders.put(targetFolder.getUniqueId(), targetFolder);
                            commitFolders();
                        }

                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                updateAppListViewTags();
                            }
                        });
                    }

                    return true;
                case DragEvent.ACTION_DRAG_ENDED:
                    resetDragDropVariables();
                    updateAppListViewTags();
                    return true;
                default:

                    break;
            }

            return false;
        }
    }

    private AppItem removeItemFromSourceFolder(AppItem sourceItem, Folder sourceFolder) {
        AppItem item = null;
        int srcFolderIndex = appListAdapter.getCurrentIndexForFolder(sourceFolder.getUniqueId());
        if (srcFolderIndex > -1) {
            List<AppItem> folderItems = appListAdapter.getItem(srcFolderIndex).getFolderItems();
            int itemIndex = folderItems.indexOf(sourceItem);
            if (itemIndex > -1) {
                item = folderItems.remove(itemIndex);
            }

            if (folderItems.size() == 0) {
                // Delete the folder since there are no items in it
                folders.remove(sourceFolder.getUniqueId());
                appListAdapter.removeItem(appListAdapter.getItem(srcFolderIndex), true);
            } else {
                if (folderItems.size() <= 4 && FolderIconCache.containsKey(sourceFolder.getUniqueId())) {
                    FolderIconCache.remove(sourceFolder.getUniqueId());
                }
                appListAdapter.notifyItemChanged(srcFolderIndex);
            }

            if (item != null) {
                String fullActivityName = item.getFullActivityName();
                sourceFolder.getActivities().remove(fullActivityName);
                packagesFoldersMap.remove(fullActivityName);
            }

            folders.put(sourceFolder.getUniqueId(), sourceFolder);
        }

        return item;
    }

    public static Animation createScaleAnimationForPopout(final float startScale, final float endScale, boolean right) {
        Animation animation = new ScaleAnimation(startScale, endScale, startScale, endScale,
                Animation.RELATIVE_TO_SELF, right ? 0.75f : 0.25f,
                Animation.RELATIVE_TO_SELF, 0.15f);
        animation.setDuration(100);
        return animation;
    }

    private void resetDragDropVariables() {
        dragSourceFolderId = null;
        dragSourceFolderView = null;
        dropTargetFolderUniqueId = null;

        // Reset the visibility of all text visible views

    }

    private class AppIconDragEventListener implements View.OnDragListener {
        public boolean onDrag(final View v, DragEvent event) {
            final int action = event.getAction();
            Object source = event.getLocalState();
            if (!(source instanceof AppItem)) {
                return false;
            }

            int position = -1;
            final AppItem sourceItem = (AppItem) source;
            switch (action) {
                case DragEvent.ACTION_DRAG_STARTED:
                    if (sourceItem != null) {
                        // returns true to indicate that the View can accept the dragged data.
                        inDragDropMode = true;
                        folderBeingCreated = false;

                        return true;
                    }

                    return false;

                case DragEvent.ACTION_DRAG_ENTERED:
                    if (appsGridIsScrolling) {
                        return false;
                    }

                    try {
                        position = Integer.parseInt(v.getTag().toString());
                    } catch (Exception ex) {
                        return false;
                    }

                    if (position == -1) {
                        return false;
                    }

                    final AppItem targetItem = appListAdapter.getItem(position);
                    if (targetItem == null) {
                        return false;
                    }

                    String srcActivityName = sourceItem.getFullActivityName();
                    if (srcActivityName != null && srcActivityName.equals(targetItem.getFullActivityName())) {
                        return false;
                    }

                    hasExited = false;
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (!hasExited) {
                                if (targetItem.isFolder()) {
                                    if (dropTargetFolderUniqueId != null && dropTargetFolderUniqueId.equals(targetItem.getUniqueId())) {
                                        displayFolderPopoutPlaceholder(popoutAlignedRight, false);
                                    } else {
                                        alternativeShowFolderPopout(targetItem, v, true,
                                                !targetItem.getUniqueId().equals(dragSourceFolderId));
                                    }

                                    hasExited = true;
                                    hasEnteredFolderPopout = true;
                                } else {
                                    Context context = getContext();
                                    currentAppIconDragView = v;
                                    final View folderHover = v.findViewById(R.id.item_app_folder_hover_image);
                                    final View appLabel = v.findViewById(R.id.item_app_list_title);
                                    if (folderHover != null) {
                                        folderHover.clearAnimation();
                                    }

                                    if (folderHover != null && context != null) {
                                        v.findViewById(R.id.item_app_folder_hover_container).setVisibility(View.VISIBLE);
                                        ValueAnimator valueAnimator = ValueAnimator.ofInt(0, Helper.getValueForDip(72, context));
                                        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                                            @Override
                                            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                                                if (appLabel != null && appLabel.getVisibility() == View.VISIBLE) {
                                                    appLabel.setVisibility(View.INVISIBLE);
                                                }
                                                if (folderHover != null) {
                                                    folderHover.getLayoutParams().width = (int) valueAnimator.getAnimatedValue();
                                                    folderHover.getLayoutParams().height = (int) valueAnimator.getAnimatedValue();
                                                    folderHover.requestLayout();
                                                }
                                            }
                                        });
                                        valueAnimator.setInterpolator(new AccelerateInterpolator());
                                        valueAnimator.setDuration(300);
                                        valueAnimator.start();

                                        folderOverlayShown = true;
                                    }
                                }
                            }
                        }
                    }, 500);
                    return true;

                case DragEvent.ACTION_DRAG_EXITED:
                    handler.removeCallbacksAndMessages(null);
                    if (!hasEnteredFolderPopout) {
                        if (folderPopoutPlaceholder != null && folderPopoutPlaceholder.getVisibility() == View.VISIBLE) {

                            // TODO: Animate exit animation
                            folderPopoutPlaceholder.setVisibility(View.GONE);
                        }
                    }
                    if (folderOverlayShown) {
                        Context context = getContext();
                        final View folderHover = v.findViewById(R.id.item_app_folder_hover_image);
                        if (folderHover != null && context != null) {
                            ValueAnimator valueAnimator = ValueAnimator.ofInt(Helper.getValueForDip(72, context), 0);
                            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                                @Override
                                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                                    if (folderHover != null) {
                                        folderHover.getLayoutParams().width = (int) valueAnimator.getAnimatedValue();
                                        folderHover.getLayoutParams().height = (int) valueAnimator.getAnimatedValue();
                                        folderHover.requestLayout();
                                    }
                                }
                            });
                            valueAnimator.addListener(new Animator.AnimatorListener() {
                                @Override
                                public void onAnimationStart(Animator animator) {

                                }

                                @Override
                                public void onAnimationEnd(Animator animator) {
                                    v.findViewById(R.id.item_app_list_title).setVisibility(View.VISIBLE);
                                    v.findViewById(R.id.item_app_folder_hover_container).setVisibility(View.INVISIBLE);
                                    folderOverlayShown = false;
                                }

                                @Override
                                public void onAnimationCancel(Animator animator) {

                                }

                                @Override
                                public void onAnimationRepeat(Animator animator) {

                                }
                            });
                            valueAnimator.setInterpolator(new AccelerateInterpolator());
                            valueAnimator.setDuration(200);
                            valueAnimator.start();
                        }
                    }

                    if (!hasEnteredFolderPopout) {
                        hasExited = true;
                    }
                    return true;

                case DragEvent.ACTION_DRAG_LOCATION:
                    return false;

                case DragEvent.ACTION_DROP:
                    if (!hasExited) {
                        // Dropped here, create the necessary folder
                        try {
                            position = Integer.parseInt(v.getTag().toString());
                        } catch (Exception ex) {
                            return false;
                        }

                        if (position == -1) {
                            return false;
                        }

                        AppItem dropTarget = appListAdapter.getItem(position);
                        if (dropTarget == null || dropTarget.equals(sourceItem)) {
                            return false;
                        }

                        if (!dropTarget.isFolder()) {
                            folderBeingCreated = true;
                            v.findViewById(R.id.item_app_folder_hover_image).setVisibility(View.GONE);

                            // Create a new folder
                            final AppItem folder = new AppItem();
                            folder.setFolder(true);
                            folder.setUniqueId(new BigInteger(130, random).toString(32));

                            List<AppItem> folderItems = new ArrayList<AppItem>();
                            folderItems.add(dropTarget);
                            folderItems.add(sourceItem);
                            folder.setFolderItems(folderItems);

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    Folder sourceFolder = folders.get(dragSourceFolderId);
                                    if (sourceFolder != null) {
                                        removeItemFromSourceFolder(sourceItem, sourceFolder);
                                    }

                                    appListAdapter.addNewFolder(folder);
                                    updateAppListViewTags();

                                    if (appsGrid != null) {
                                        appsGrid.smoothScrollToPosition(0);
                                    }

                                    saveNewFolder(folder);

                                }
                            });
                        }
                    }

                    return true;

                case DragEvent.ACTION_DRAG_ENDED:
                    inDragDropMode = false;
                    if (folderBeingCreated) {
                        return true;
                    }

                    //inDragDropMode = false;
                    if (currentAppIconDragView != null && folderOverlayShown) {
                        currentAppIconDragView.findViewById(R.id.item_app_list_title).setVisibility(View.VISIBLE);

                        Context context = getContext();
                        final View folderHover = currentAppIconDragView.findViewById(R.id.item_app_folder_hover_image);
                        if (folderHover != null && context != null) {
                            ValueAnimator valueAnimator = ValueAnimator.ofInt(Helper.getValueForDip(72, context), 0);
                            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                                @Override
                                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                                    if (folderHover != null) {
                                        folderHover.getLayoutParams().width = (int) valueAnimator.getAnimatedValue();
                                        folderHover.getLayoutParams().height = (int) valueAnimator.getAnimatedValue();
                                        folderHover.requestLayout();
                                    }
                                }
                            });
                            valueAnimator.addListener(new Animator.AnimatorListener() {
                                @Override
                                public void onAnimationStart(Animator animator) {

                                }

                                @Override
                                public void onAnimationEnd(Animator animator) {
                                    if (currentAppIconDragView != null) {
                                        currentAppIconDragView.findViewById(R.id.item_app_folder_hover_container).setVisibility(View.INVISIBLE);
                                    }
                                    folderOverlayShown = false;
                                    currentAppIconDragView = null;
                                }

                                @Override
                                public void onAnimationCancel(Animator animator) {

                                }

                                @Override
                                public void onAnimationRepeat(Animator animator) {

                                }
                            });
                            valueAnimator.setInterpolator(new AccelerateInterpolator());
                            valueAnimator.setDuration(200);
                            valueAnimator.start();
                        }
                    }

                    resetDragDropVariables();
                    hasExited = true;
                    return true;

                default:
                    break;
            }

            return false;
        }
    }

    private static class AppIconShadowBuilder extends View.DragShadowBuilder {
        private static Drawable shadow;

        private int width, height;

        public AppIconShadowBuilder(ImageView v) {
            super(v);
            shadow = v.getDrawable().getConstantState().newDrawable();
            shadow.setAlpha(225);
        }

        // Defines a callback that sends the drag shadow dimensions and touch point back to the
        // system.
        @Override
        public void onProvideShadowMetrics(Point size, Point touch) {
            width = getView().getWidth();
            height = getView().getHeight();

            shadow.setBounds(0, 0, width, height);

            size.set(width, height);
            touch.set((int)(width / 1.25), (int) (height / 1.25));
        }

        @Override
        public void onDrawShadow(Canvas canvas) {
            shadow.draw(canvas);
        }
    }

    private class CacheAllAppIconsTask extends AsyncTask<Void, Void, Void> {
        private List<AppItem> appItems;

        public CacheAllAppIconsTask(List<AppItem> appItems) {
            this.appItems = appItems;
        }

        protected Void doInBackground(Void... params) {
            Context context = getContext();
            if (context != null) {
                PackageManager manager = context.getPackageManager();
                for (int i = 0; i < appItems.size(); i++) {
                    AppItem appItem = appItems.get(i);
                    if (appItem.isFolder()) {
                        continue;
                    }

                    String activityName = appItem.getFullActivityName();
                    if (AppIconCache.containsKey(activityName)) {
                        continue;
                    }

                    Drawable icon = null;
                    try {
                        ActivityInfo activityInfo = appItem.getActivityInfo();
                        IconPack iconPack = ((HomeActivity) context).getCurrentIconPack();

                        Intent launchIntent = manager.getLaunchIntentForPackage(activityInfo.packageName);
                        if (launchIntent != null) {
                            String componentName = new ComponentName(activityInfo.packageName, activityInfo.name).toString();
                            if (IconPackDrawables.containsKey(componentName)) {
                                String drawableName = IconPackDrawables.get(componentName);
                                if (drawableName != null) {
                                    icon = loadIconPackDrawable(IconPackResources, drawableName, iconPack);
                                }
                            }
                        }
                    } catch (Exception ex) {

                    }

                    if (icon == null) {
                        icon = appItem.getActivityInfo().loadIcon(manager); //(fullActivityName);
                    }

                    if (icon != null) {
                        AppIconCache.put(activityName, icon);
                    }
                }
            }
            return null;
        }

        protected void onPostExecute(Void result) {
            if (appListAdapter != null) {
                appListAdapter.notifyDataSetChanged();
            }
        }
    }

    public static Drawable getIconForAppItem(AppItem appItem, Context context) {
        Drawable icon = null;
        if (context != null) {
            PackageManager manager = context.getPackageManager();
            ActivityInfo activityInfo = appItem.getActivityInfo();
            IconPack iconPack = (context instanceof HomeActivity) ?
                    ((HomeActivity) context).getCurrentIconPack() : ((SettingsActivity) context).getCurrentIconPack();

            try {
                Intent launchIntent = manager.getLaunchIntentForPackage(activityInfo.packageName);
                if (launchIntent != null) {
                    String componentName = new ComponentName(activityInfo.packageName, activityInfo.name).toString();
                    if (IconPackDrawables.containsKey(componentName)) {
                        String drawableName = IconPackDrawables.get(componentName);
                        if (drawableName != null) {
                            icon = loadIconPackDrawable(IconPackResources, drawableName, iconPack);
                        }
                    }
                }
            } catch (Exception ex) {
                // continue
            }

            if (icon == null) {
                icon = appItem.getActivityInfo().loadIcon(manager); //(fullActivityName);
            }
        }

        return icon;
    }

    public static class LoadFolderIconTask extends AsyncTask<Void, Void, Bitmap> {

        private AppItem appItem;

        private ImageView iconView;

        private Context context;

        public LoadFolderIconTask(AppItem appItem, ImageView iconView, Context context) {
            this.appItem = appItem;
            this.iconView = iconView;
            this.context = context;
        }

        protected Bitmap doInBackground(Void... params) {
            Process.setThreadPriority(Process.THREAD_PRIORITY_FOREGROUND);
            Bitmap bitmap = null;
            if (context != null) {
                bitmap = createFolderIcon(appItem, context);
            }

            return bitmap;
        }

        protected void onPostExecute(Bitmap icon) {
            if (icon != null) {
                FolderIconCache.put(appItem.getUniqueId(), icon);
            }

            if (icon != null && iconView != null) {
                iconView.setImageBitmap(icon);
            }
        }
    }

    public static class LoadAppIconTask extends AsyncTask<Void, Void, Drawable> {

        private AppItem appItem;

        private ImageView iconView;

        private Context context;

        public LoadAppIconTask(AppItem appItem, ImageView iconView, Context context) {
            this.appItem = appItem;
            this.iconView = iconView;
            this.context = context;
        }

        protected Drawable doInBackground(Void... params) {
            Process.setThreadPriority(Process.THREAD_PRIORITY_FOREGROUND);
            Drawable icon = null;

            try {
                if (context != null) {
                    PackageManager manager = context.getPackageManager();
                    ActivityInfo activityInfo = appItem.getActivityInfo();
                    IconPack iconPack = (context instanceof HomeActivity) ?
                        ((HomeActivity) context).getCurrentIconPack() : ((SettingsActivity) context).getCurrentIconPack();

                    try {
                        Intent launchIntent = manager.getLaunchIntentForPackage(activityInfo.packageName);
                        if (launchIntent != null) {
                            String componentName = new ComponentName(activityInfo.packageName, activityInfo.name).toString();
                            if (IconPackDrawables.containsKey(componentName)) {
                                String drawableName = IconPackDrawables.get(componentName);
                                if (drawableName != null) {
                                    icon = loadIconPackDrawable(IconPackResources, drawableName, iconPack);
                                }
                            }
                        }
                    } catch (Exception ex) {
                        // continue
                    }

                    if (icon == null) {
                        icon = appItem.getActivityInfo().loadIcon(manager); //(fullActivityName);
                    }

                    return icon;
                }
            } catch (Exception ex) {

            }
            return null;
        }

        protected void onPostExecute(Drawable icon) {
            if (icon != null && !AppIconCache.containsKey(appItem.getFullActivityName())) {
                AppIconCache.put(appItem.getFullActivityName(), icon);
            }

            if (icon != null && iconView != null) {
                iconView.setImageDrawable(icon);
            }
        }
    }
}
