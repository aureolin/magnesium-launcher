package co.aureolin.labs.magnesium.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import co.aureolin.labs.magnesium.HomeActivity;

/**
 * Created by Akinwale on 21/09/2015.
 */
public class BitmapDiskCache {

    private static final String TAG = BitmapDiskCache.class.getCanonicalName();

    private static final Object cacheLock = new Object();

    private static File cacheDir;

    private static Context context;

    private static final String CACHE_PATH = "images";

    public static void init(Context context) {
        BitmapDiskCache.context = context;
        cacheDir = getDiskCacheDir(context, CACHE_PATH);
        if (!cacheDir.isDirectory()) {
            cacheDir.mkdirs();
        }
    }

    public static void putBitmap(String key, Bitmap bitmap) {
        // First store to the memory cache
        if (BitmapMemoryCache.getBitmap(key) == null) {
            BitmapMemoryCache.putBitmap(key, bitmap);
        }
        synchronized (cacheLock) {
            if (cacheDir == null && context != null) {
                cacheDir = getDiskCacheDir(context, CACHE_PATH);
                if (!cacheDir.isDirectory()) {
                    cacheDir.mkdirs();
                }
            }
            writeBitmapToFileCache(key, bitmap);
        }
    }

    public static Bitmap getBitmap(String key) {
        Bitmap bitmap = BitmapMemoryCache.getBitmap(key);
        if (bitmap != null) {
            return bitmap;
        }
        synchronized (cacheLock) {
            return readBitmapFromFileCache(key);
        }
    }

    private static void writeBitmapToFileCache(String key, Bitmap bitmap) {
        if (cacheDir == null) {
            return;
        }

        FileOutputStream out = null;
        try {
            String filename = String.format("%s%s%s", cacheDir.getAbsolutePath(), File.separator, key);
            out = new FileOutputStream(filename);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
        } catch (IOException e) {
            Log.e(TAG, String.format("Could not cache bitmap to file: %s", key), e);
        } finally {
            Helper.closeCloseable(out);
        }
    }

    private static Bitmap readBitmapFromFileCache(String key) {
        if (cacheDir == null) {
            return null;
        }

        String filename = String.format("%s%s%s",
                cacheDir.getAbsolutePath(), File.separator, key);
        Bitmap bitmap = BitmapFactory.decodeFile(filename);
        return bitmap;
    }

    public static File getDiskCacheDir(Context context, String uniqueName) {
        // Check if media is mounted or storage is built-in, if so, try and use external cache dir
        // otherwise use internal cache dir
        final String cachePath =
                ((Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()) ||
                 !Environment.isExternalStorageRemovable()) &&
                        (((HomeActivity) context).getExternalCacheDir() != null)) ?
                        ((HomeActivity) context).getExternalCacheDir().getPath()  :
                        context.getCacheDir().getPath();

        return new File(cachePath + File.separator + uniqueName);
    }
}
