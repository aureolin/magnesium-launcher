package co.aureolin.labs.magnesium.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.AuthenticationFailedException;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;

import co.aureolin.labs.magnesium.HomeActivity;
import co.aureolin.labs.magnesium.R;
import co.aureolin.labs.magnesium.SettingsActivity;
import co.aureolin.labs.magnesium.fragment.settings.GeneralSettingsFragment;
import co.aureolin.labs.magnesium.model.EmailAccount;
import co.aureolin.labs.magnesium.util.handler.EmailAccountSavedHandler;
import co.aureolin.labs.magnesium.util.Helper;
import co.aureolin.labs.magnesium.util.Http;
import co.aureolin.labs.magnesium.util.db.MagnesiumDbContext;

/**
 * Created by akinwale on 14/02/2016.
 */
public class EmailAccountDialog extends DialogFragment {

    public static final int DEFAULT_EMAIL_ACCOUNT_LIMIT = 3;

    public static final int MAX_EMAIL_ACCOUNT_LIMIT = 10;

    private ObjectMapper objectMapper;

    private List<String> existingEmailAddresses;

    private ValidateEmailAccountTask validateTask;

    private int numEmailAccounts;

    private EmailAccountSavedHandler emailAccountSavedHandler;

    private View fieldsContainer;

    private View checkboxContainer;

    private ProgressBar loadProgressBar;

    private TextInputLayout emailAddressLayout;

    private EditText emailAddressInput;

    private TextInputLayout usernameLayout;

    private EditText usernameInput;

    private TextInputLayout passwordLayout;

    private EditText passwordInput;

    private TextInputLayout hostnameLayout;

    private EditText hostnameInput;

    private TextInputLayout portLayout;

    private EditText portInput;

    private CheckBox manualConfiguration;

    private CheckBox oauthConfiguration;

    private View connectionTypeTitle;

    private RadioGroup radioGroup;

    private RadioButton clearRadioButton;

    private RadioButton startTlsRadioButton;

    private RadioButton tlsRadioButton;

    private WebView webView;

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Context context = getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View dialogContent = inflater.inflate(R.layout.dialog_email_account, null);
        checkboxContainer = dialogContent.findViewById(R.id.dialog_email_checkbox_container);
        fieldsContainer = dialogContent.findViewById(R.id.dialog_email_fields_container);
        radioGroup = (RadioGroup) dialogContent.findViewById(R.id.dialog_email_connection_type_group);
        connectionTypeTitle = dialogContent.findViewById(R.id.dialog_email_connection_type_title);

        webView = (WebView) dialogContent.findViewById(R.id.dialog_email_oauth_webview);
        webView.setFocusable(true);
        webView.setFocusableInTouchMode(true);
        webView.requestFocus(View.FOCUS_DOWN);
        webView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                    case MotionEvent.ACTION_UP:
                        if (!v.hasFocus()) {
                            v.requestFocus();
                        }
                        break;
                }
                return false;
            }
        });
        webView.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                if (loadProgressBar != null) {
                    loadProgressBar.setVisibility(View.INVISIBLE);
                }
                if (webView != null && webView.getVisibility() != View.VISIBLE) {
                    webView.setVisibility(View.VISIBLE);
                }
            }

            public boolean shouldOverrideUrlLoading(WebView view, final String url) {
                if (url != null) {
                    checkObjectMapper();

                    if (url.startsWith(Helper.MAGNESIUM_OAUTH_URL_PREFIX)) {
                        final String vars = url.substring(Helper.MAGNESIUM_OAUTH_URL_PREFIX.length() + 1);
                        final Map<String, String> queryParams = Helper.splitQuery(vars);
                        webView.setVisibility(View.GONE);
                        loadProgressBar.setVisibility(View.VISIBLE);


                        (new AsyncTask<Void, Void, EmailAccount>() {
                            protected EmailAccount doInBackground(Void... params) {
                                if (!queryParams.containsKey("code")) {
                                    return null;
                                }
                                String code = queryParams.get("code");
                                if (Helper.isEmpty(code)) {
                                    return null;
                                }

                                String postData = String.format("code=%s&client_id=%s&client_secret=%s&redirect_uri=%s&grant_type=authorization_code",
                                        Helper.getUrlEncodedString(code),
                                        Helper.getUrlEncodedString(Helper.GOOGLE_OAUTH_CLIENT_ID),
                                        Helper.getUrlEncodedString(Helper.GOOGLE_OAUTH_CLIENT_SECRET),
                                        Helper.getUrlEncodedString(Helper.MAGNESIUM_OAUTH_URL_PREFIX));
                                String requestUrl = Helper.GOOGLE_OAUTH_TOKEN_URL;
                                String json = Http.post(requestUrl, postData);
                                try {
                                    JsonNode rootNode = objectMapper.readTree(json);
                                    String accessToken = Helper.getNodeValue("access_token", rootNode);
                                    String refreshToken = Helper.getNodeValue("refresh_token", rootNode);
                                    if (!Helper.isEmpty(accessToken) && !Helper.isEmpty(refreshToken)) {
                                        EmailAccount emailAccount = new EmailAccount();

                                        // Get the user's email address
                                        String profileUrl = Helper.GMAIL_PROFILE_URL;

                                        Map<String, String> headers = new HashMap<String, String>();
                                        headers.put("Authorization", String.format("Bearer %s", accessToken));
                                        String profileJson = Http.get(profileUrl, headers);

                                        JsonNode profileNode = objectMapper.readTree(profileJson);
                                        if (profileNode != null) {
                                            emailAccount.setEmailAddress(Helper.getNodeValue("emailAddress", profileNode));
                                        }
                                        emailAccount.setConnectionType(EmailAccount.CONNECTION_TYPE_OAUTH);
                                        emailAccount.setHostName(accessToken); // Set the Access Token as the host name
                                        emailAccount.setPassword(accessToken);
                                        emailAccount.setUsername(refreshToken);

                                        SettingsActivity activity = (SettingsActivity) getContext();
                                        if (activity != null) {
                                            SQLiteDatabase db = activity.getDbContext().getWritableDatabase();
                                            if (MagnesiumDbContext.insert(emailAccount, activity.getEncryptionKey(), db) > 0) {
                                                return emailAccount;
                                            }
                                        }
                                    }
                                } catch (IOException ex) {
                                    // Request failed. Cannot create email account
                                }

                                return null;
                            }

                            protected void onPostExecute(EmailAccount result) {
                                if (result == null) {
                                    handleAuthTokenVerificationError();
                                    return;
                                }

                                if (emailAccountSavedHandler != null) {
                                    emailAccountSavedHandler.onEmailAccountSaved(result);
                                }
                                dismiss();
                            }
                        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                        return true;
                    }
                }
                return false;
            }
        });

        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);

        loadProgressBar = (ProgressBar) dialogContent.findViewById(R.id.dialog_email_progress);

        emailAddressLayout = (TextInputLayout) dialogContent.findViewById(R.id.dialog_email_email_address_layout);
        usernameLayout = (TextInputLayout) dialogContent.findViewById(R.id.dialog_email_username_layout);
        passwordLayout = (TextInputLayout) dialogContent.findViewById(R.id.dialog_email_password_layout);
        hostnameLayout = (TextInputLayout) dialogContent.findViewById(R.id.dialog_email_hostname_layout);
        portLayout = (TextInputLayout) dialogContent.findViewById(R.id.dialog_email_port_layout);

        emailAddressInput = (EditText) dialogContent.findViewById(R.id.dialog_email_email_address);
        usernameInput = (EditText) dialogContent.findViewById(R.id.dialog_email_username);
        passwordInput = (EditText) dialogContent.findViewById(R.id.dialog_email_password);
        hostnameInput = (EditText) dialogContent.findViewById(R.id.dialog_email_hostname);
        portInput = (EditText) dialogContent.findViewById(R.id.dialog_email_port);

        manualConfiguration = (CheckBox) dialogContent.findViewById(R.id.dialog_email_manual_configuration);
        oauthConfiguration = (CheckBox) dialogContent.findViewById(R.id.dialog_email_oauth_configuration);
        manualConfiguration.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            usernameLayout.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            hostnameLayout.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            portLayout.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            connectionTypeTitle.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            radioGroup.setVisibility(isChecked ? View.VISIBLE : View.GONE);

            String currentEmail = emailAddressInput.getText().toString();
            if (isChecked && !Helper.isEmpty(currentEmail)) {
                String emailHost = currentEmail.substring(currentEmail.indexOf("@") + 1);
                String imapHost = String.format("imap.%s", emailHost);
                usernameInput.setText(currentEmail);
                hostnameInput.setText(imapHost);
            }
            }
        });

        clearRadioButton = (RadioButton) dialogContent.findViewById(R.id.dialog_email_connection_type_clear);
        startTlsRadioButton = (RadioButton) dialogContent.findViewById(R.id.dialog_email_connection_type_starttls);
        tlsRadioButton = (RadioButton) dialogContent.findViewById(R.id.dialog_email_connection_type_tls);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        AlertDialog dialog = builder
            .setTitle(R.string.add_account)
            .setView(dialogContent)
            .setPositiveButton(R.string.add_account_button_text, null)
            .setNegativeButton(R.string.cancel_button_text, null)
            .create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
            ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int maxLimit = DEFAULT_EMAIL_ACCOUNT_LIMIT;
                    Context context = getContext();
                    if (context != null) {
                        int settingIndex = GeneralSettingsFragment.SETTING_INDEX_EMAIL_ACCOUNT;
                        SharedPreferences preferences = context.getSharedPreferences(HomeActivity.COMMON_SHARED_PREF_NAME, Context.MODE_PRIVATE);
                        int purchased = preferences.getInt(GeneralSettingsFragment.SETTINGS_OWNED_KEYS[settingIndex], 0);
                        if (purchased == 1) {
                            maxLimit = MAX_EMAIL_ACCOUNT_LIMIT;
                        }
                    }

                    if (numEmailAccounts > maxLimit) {
                        Helper.showToast(R.string.add_email_account_limit_error, getContext());
                        return;
                    }

                    boolean isGmailOauth = oauthConfiguration.isChecked();
                    if (isGmailOauth) {
                        emailAddressLayout.setVisibility(View.GONE);
                        passwordLayout.setVisibility(View.GONE);
                        checkboxContainer.setVisibility(View.GONE);
                        toggleButtonsVisible(false);

                        if (loadProgressBar != null) {
                            loadProgressBar.setVisibility(View.VISIBLE);
                        };

                        String gmailOauthUrl = String.format(Helper.GMAIL_OAUTH_CODE_URL_FORMAT, Helper.GOOGLE_OAUTH_CLIENT_ID);
                        webView.loadUrl(gmailOauthUrl);
                    } else {
                        // Validation
                        // Check valid email address
                        boolean isManualConfig = manualConfiguration.isChecked();
                        String emailAddress = getInputString(emailAddressInput);
                        String username = getInputString(usernameInput);
                        String password = getInputString(passwordInput);
                        String hostname = getInputString(hostnameInput);
                        String portStr = getInputString(portInput);
                        int connectionType = 0;
                        if (isManualConfig) {
                            if (clearRadioButton.isChecked()) {
                                connectionType = 1;
                            } else if (tlsRadioButton.isChecked()) {
                                connectionType = 2;
                            }
                        }

                        if (Helper.isEmpty(emailAddress)) { // TODO: Regex email validation
                            Helper.showToast("Please enter a valid email address.", context);
                            return;
                        } else if (existingEmailAddresses != null && existingEmailAddresses.contains(emailAddress.toLowerCase())) {
                            Helper.showToast("You have already added this email address.", context);
                            return;
                        }

                        if (Helper.isEmpty(password)) {
                            Helper.showToast("Please specify your password.", context);
                            return;
                        }
                        if (isManualConfig) {
                            if (Helper.isEmpty(username)) {
                                Helper.showToast("Please enter your username.", context);
                                return;
                            }
                            if (Helper.isEmpty(hostname)) {
                                Helper.showToast("Please enter a valid host name.", context);
                                return;
                            }
                            if (!Helper.isInteger(portStr)) {
                                Helper.showToast("Please specify a valid port number.", context);
                                return;
                            }
                            if (!clearRadioButton.isChecked()
                                    && !startTlsRadioButton.isChecked()
                                    && !tlsRadioButton.isChecked()) {
                                Helper.showToast("Please select a connection type.", context);
                                return;
                            }
                        }

                        EmailAccount account = new EmailAccount();
                        account.setEmailAddress(emailAddress.trim());
                        account.setUsername(username);
                        account.setPassword(password);
                        account.setHostName(hostname);
                        account.setPort(Helper.parseInt(portStr));
                        account.setConnectionType(connectionType);
                        if (validateTask == null) {
                            validateTask = new ValidateEmailAccountTask(isManualConfig);
                            validateTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, account);
                        }
                    }
                }
            });
            }
        });
        oauthConfiguration.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    manualConfiguration.setChecked(false);
                }
                manualConfiguration.setEnabled(!isChecked);
                emailAddressLayout.setVisibility(isChecked ? View.GONE : View.VISIBLE);
                passwordLayout.setVisibility(isChecked ? View.GONE : View.VISIBLE);
            }
        });

        return dialog;
    }

    private void handleAuthTokenVerificationError() {
        if (loadProgressBar != null) {
            loadProgressBar.setVisibility(View.INVISIBLE);
        }
        if (webView != null) {
            webView.setVisibility(View.GONE);
        }
        emailAddressLayout.setVisibility(oauthConfiguration.isChecked() ? View.GONE : View.VISIBLE);
        passwordLayout.setVisibility(oauthConfiguration.isChecked() ? View.GONE : View.VISIBLE);
        checkboxContainer.setVisibility(View.VISIBLE);

        toggleButtonsVisible(true);
        Context context = getContext();
        if (context != null) {
            Toast.makeText(context, "The account authorisation process failed. Please try again.", Toast.LENGTH_SHORT).show();
        }
    }

    private void checkObjectMapper() {
        if (objectMapper == null) {
            objectMapper = new ObjectMapper();
        }
    }

    private class ValidateEmailAccountTask extends AsyncTask<EmailAccount, Void, Boolean> {

        private boolean authFailedError;

        private EmailAccount currentEmailAccount;

        private List<EmailAccount> accountQueue;

        private boolean isManualConfig;

        public ValidateEmailAccountTask(boolean isManualConfig) {
            this.isManualConfig = isManualConfig;
            this.accountQueue = new ArrayList<EmailAccount>();
        }

        private boolean testEmailAccount(EmailAccount emailAccount) throws AuthenticationFailedException {
            boolean secure = (emailAccount.getConnectionType() == 2);
            String hostName = emailAccount.getHostName();
            String protocol = (secure) ? "imaps" : "imap";
            if (hostName.startsWith("pop")) {
                protocol = (secure) ? "pops" : "pop";
            }

            Properties properties = System.getProperties();
            properties.setProperty("mail.imap.connectiontimeout", "2000");
            properties.setProperty("mail.imap.timeout", "2000");
            Session mailSession = Session.getInstance(properties, null);
            try {
                Store store = mailSession.getStore(protocol);
                store.connect(hostName, emailAccount.getPort(), emailAccount.getUsername(), emailAccount.getPassword());
                return true;
            } catch (AuthenticationFailedException ex) {
                throw ex;
            } catch (MessagingException ex) {
                return false;
            }
        }

        @Override
        protected void onPreExecute() {
            toggleDialogCancelable(false);
            toggleButtonsVisible(false);
            fieldsContainer.setVisibility(View.GONE);
            loadProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Boolean doInBackground(EmailAccount... params) {
            if (params == null || params.length == 0) {
                return false;
            }

            EmailAccount account = params[0];
            if (isManualConfig) {
                // Test the email account directly
                accountQueue.add(account);
            } else {
                // Try automatic detection
                String emailAddress = account.getEmailAddress();
                String password = account.getPassword();
                String emailHost = emailAddress.substring(emailAddress.indexOf("@") + 1);
                String usernamePart = emailAddress.substring(0, emailAddress.indexOf("@"));
                String imapHost = String.format("imap.%s", emailHost);
                String popHost = String.format("pop.%s", emailHost);

                // First try IMAP TLS with email address as username
                accountQueue.add(accountFromFields(emailAddress, emailAddress, password, imapHost, 993, EmailAccount.CONNECTION_TYPE_SECURE_TLS));
                // Next IMAP TLS with username part as username
                accountQueue.add(accountFromFields(emailAddress, usernamePart, password, imapHost, 993, EmailAccount.CONNECTION_TYPE_SECURE_TLS));
                // Next IMAP plain/clear with email address as username
                accountQueue.add(accountFromFields(emailAddress, emailAddress, password, imapHost, 143, EmailAccount.CONNECTION_TYPE_CLEAR));
                // Next IMAP plain/clear with username part as username
                accountQueue.add(accountFromFields(emailAddress, usernamePart, password, imapHost, 143, EmailAccount.CONNECTION_TYPE_CLEAR));
                // First try IMAP TLS with email address as username, email host
                accountQueue.add(accountFromFields(emailAddress, emailAddress, password, emailHost, 993, EmailAccount.CONNECTION_TYPE_SECURE_TLS));
                // Next IMAP TLS with username part as username
                accountQueue.add(accountFromFields(emailAddress, usernamePart, password, emailHost, 993, EmailAccount.CONNECTION_TYPE_SECURE_TLS));
                // Next IMAP plain/clear with email address as username
                accountQueue.add(accountFromFields(emailAddress, emailAddress, password, emailHost, 143, EmailAccount.CONNECTION_TYPE_CLEAR));
                // Next IMAP plain/clear with username part as username
                accountQueue.add(accountFromFields(emailAddress, usernamePart, password, emailHost, 143, EmailAccount.CONNECTION_TYPE_CLEAR));

                // TODO: POP 3?
            }

            for (int i = 0; i < accountQueue.size(); i++) {
                currentEmailAccount = accountQueue.get(i);
                try {
                    if (testEmailAccount(currentEmailAccount)) {
                        // Save the account
                        SettingsActivity activity = (SettingsActivity) getContext();
                        if (activity != null) {
                            SQLiteDatabase db = activity.getDbContext().getWritableDatabase();
                            return (MagnesiumDbContext.insert(currentEmailAccount, activity.getEncryptionKey(), db) > 0);
                        }
                    }
                } catch (AuthenticationFailedException ex) {
                    authFailedError = true;
                    return false;
                }
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            toggleDialogCancelable(true);
            Context context = getContext();

            if (!result) {
                if (authFailedError) {
                    Helper.showToast("Authentication failed. Please check your credentials and try again.", context);
                } else {
                    if (!isManualConfig) {
                        Helper.showToast("The email account settings could not be automatically detected. Please try the manual configuration.", context);
                    } else {
                        Helper.showToast("The email account could not be validated. Please verify that your settings are correct.", context);
                    }
                }

                if (loadProgressBar != null) {
                    loadProgressBar.setVisibility(View.INVISIBLE);
                }
                if (fieldsContainer != null) {
                    fieldsContainer.setVisibility(View.VISIBLE);
                }
                toggleButtonsVisible(true);

                validateTask = null;
                return;
            }

            if (emailAccountSavedHandler != null) {
                emailAccountSavedHandler.onEmailAccountSaved(currentEmailAccount);
            }
            validateTask = null;
            dismiss();
            //showToast("The email account was successfully validated.");
        }
    }

    private static EmailAccount accountFromFields(String emailAddress, String username, String password,
                                                  String hostName, int port, int connectionType) {
        EmailAccount account = new EmailAccount();
        account.setEmailAddress(emailAddress);
        account.setUsername(username);
        account.setPassword(password);
        account.setHostName(hostName);
        account.setPort(port);
        account.setConnectionType(connectionType);

        return account;
    }

    private String getInputString(EditText editText) {
        return editText.getText().toString();
    }

    private void toggleButtonsVisible(boolean visible) {
        Dialog dialog = getDialog();
        if (dialog != null) {
            ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE).setVisibility(visible ? View.VISIBLE : View.GONE);
            ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_NEGATIVE).setVisibility(visible ? View.VISIBLE : View.GONE);
        }
    }

    private void toggleDialogCancelable(boolean cancelable) {
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.setCancelable(cancelable);
            dialog.setCanceledOnTouchOutside(cancelable);
        }
    }

    public void setExistingEmailAddresses(List<String> existingEmailAddresses) {
        this.existingEmailAddresses = existingEmailAddresses;
    }

    public void setEmailAccountSavedHandler(EmailAccountSavedHandler emailAccountSavedHandler) {
        this.emailAccountSavedHandler = emailAccountSavedHandler;
    }

    public void setNumEmailAccounts(int numEmailAccounts) {
        this.numEmailAccounts = numEmailAccounts;
    }
}
