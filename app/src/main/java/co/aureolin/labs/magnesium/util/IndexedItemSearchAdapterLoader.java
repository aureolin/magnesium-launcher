package co.aureolin.labs.magnesium.util;

import co.aureolin.labs.magnesium.adapter.FeedItemListAdapter;

/**
 * Created by akinwale on 09/02/2016.
 */
public interface IndexedItemSearchAdapterLoader {
    public void searchForIndexedItems(String filterString, FeedItemListAdapter adapter);
}
