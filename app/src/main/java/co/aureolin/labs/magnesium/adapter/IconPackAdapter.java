package co.aureolin.labs.magnesium.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import co.aureolin.labs.magnesium.R;
import co.aureolin.labs.magnesium.model.IconPack;
import co.aureolin.labs.magnesium.model.RssFeed;
import co.aureolin.labs.magnesium.util.Helper;

/**
 * Created by akinwale on 09/09/2016.
 */
public class IconPackAdapter extends ArrayAdapter<IconPack> {
    public IconPackAdapter(Context context) {
        super(context, 0);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_string, null);
        }

        IconPack item = getItem(position);

        TextView itemTextView = (TextView) convertView.findViewById(R.id.item_text);
        itemTextView.setText(item.getName());

        return convertView;
    }
}
