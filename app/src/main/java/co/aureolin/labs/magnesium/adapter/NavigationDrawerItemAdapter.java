package co.aureolin.labs.magnesium.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import co.aureolin.labs.magnesium.R;
import co.aureolin.labs.magnesium.model.NavigationDrawerItem;
import co.aureolin.labs.magnesium.util.listener.GenericItemClickListener;
import co.aureolin.labs.magnesium.util.Helper;

/**
 * Created by akinwale on 09/09/2016.
 */
public class NavigationDrawerItemAdapter extends RecyclerView.Adapter<NavigationDrawerItemAdapter.ViewHolder> {

    private Context context;

    private List<NavigationDrawerItem> items;

    private GenericItemClickListener itemClickListener;

    public void setItems(List<NavigationDrawerItem> items) {
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_nav_drawer, viewGroup, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        NavigationDrawerItem item = items.get(i);

        if (item.getDrawableId() != 0) {
            viewHolder.iconView.setImageResource(item.getDrawableId());
        }
        viewHolder.textView.setTypeface(Helper.getTypeface("normal", context));
        viewHolder.textView.setText(item.getName());
    }

    @Override
    public int getItemCount() {
        return (items != null ? items.size() : 0);
    }

    public NavigationDrawerItemAdapter(Context context, List<NavigationDrawerItem> items) {
        this.items = items;
        this.context = context;
    }

    public GenericItemClickListener getItemClickListener() {
        return itemClickListener;
    }

    public void setItemClickListener(GenericItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public NavigationDrawerItem getItem(int position) {
        return items.get(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        protected ImageView iconView;
        protected TextView textView;

        public ViewHolder(View view) {
            super(view);
            if (itemClickListener != null) {
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (itemClickListener != null) {
                            itemClickListener.onItemClick(getAdapterPosition(), view);
                        }
                    }
                });
            }

            this.iconView = (ImageView) view.findViewById(R.id.item_icon);
            this.textView = (TextView) view.findViewById(R.id.item_text);
        }
    }

}
