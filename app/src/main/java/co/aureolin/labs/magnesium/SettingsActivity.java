package co.aureolin.labs.magnesium;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.android.vending.billing.IInAppBillingService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

import co.aureolin.labs.magnesium.fragment.settings.AboutFragment;
import co.aureolin.labs.magnesium.fragment.settings.EmailAccountsFragment;
import co.aureolin.labs.magnesium.fragment.settings.GeneralSettingsFragment;
import co.aureolin.labs.magnesium.fragment.settings.HiddenAppsFragment;
import co.aureolin.labs.magnesium.fragment.settings.IndexingAndSearchFragment;
import co.aureolin.labs.magnesium.fragment.settings.PersonalisationFragment;
import co.aureolin.labs.magnesium.fragment.settings.RssFeedsFragment;
import co.aureolin.labs.magnesium.fragment.settings.SocialMediaAccountsFragment;
import co.aureolin.labs.magnesium.model.IconPack;
import co.aureolin.labs.magnesium.util.Helper;
import co.aureolin.labs.magnesium.util.db.MagnesiumDbContext;

/**
 * Created by akinwale on 11/02/2016.
 */
public class SettingsActivity extends AppCompatActivity {

    public static final int IAB_PURCHASE_REQUEST_CODE = 8001;

    private static final String TAG = SettingsActivity.class.getCanonicalName();

    public static SettingsActivity CurrentActivity;

    private Fragment prevFragment;

    private Fragment currentFragment;

    private ListView settingsOptionsList;

    private StringListAdapter settingsOptionsAdapter;

    private MagnesiumDbContext dbContext;

    private SharedPreferences preferences;

    private String encryptionKey;

    private IInAppBillingService iabService;

    private ServiceConnection iabServiceConnection;

    private static final int[] SETTINGS_LIST_ITEMS = {
        R.string.general,
        R.string.personalisation,
        R.string.email_accounts,
        R.string.indexing_and_search,
        R.string.rss_feeds,
        R.string.socialmedia_accounts,
        R.string.about
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CurrentActivity = this;
        setContentView(R.layout.activity_settings);

        preferences = getSharedPreferences(HomeActivity.COMMON_SHARED_PREF_NAME, Context.MODE_PRIVATE);
        initialiseEncryptionKey();

        dbContext = new MagnesiumDbContext(this);
        if (getSupportActionBar() == null) {
            Toolbar toolbar = (Toolbar) findViewById(R.id.activity_toolbar);
            setSupportActionBar(toolbar);
        }

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(R.string.settings);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        settingsOptionsList = (ListView) findViewById(R.id.settings_options_list);
        settingsOptionsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String title = (String) parent.getAdapter().getItem(position);
                switch (position) {
                    case 0: // General
                        setCurrentFragment(title, GeneralSettingsFragment.newInstance());
                        break;

                    case 1: // Personalisation
                        setCurrentFragment(title, PersonalisationFragment.newInstance());
                        break;

                    case 2: // Email accounts
                        setCurrentFragment(title, EmailAccountsFragment.newInstance());
                        break;

                    case 3: // Indexing and search
                        setCurrentFragment(title, IndexingAndSearchFragment.newInstance());
                        break;

                    case 4: // RSS feeds
                        setCurrentFragment(title, RssFeedsFragment.newInstance());
                        break;

                    case 5: // Social media account
                        setCurrentFragment(title, SocialMediaAccountsFragment.newInstance());
                        break;

                    case 6: // About
                        setCurrentFragment(title, AboutFragment.newInstance());
                        break;
                }
            }
        });

        settingsOptionsAdapter = new StringListAdapter(this);
        for (int i = 0; i < SETTINGS_LIST_ITEMS.length; i++) {
            settingsOptionsAdapter.add(getString(SETTINGS_LIST_ITEMS[i]));
        }
        settingsOptionsList.setAdapter(settingsOptionsAdapter);

        iabServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                iabService = IInAppBillingService.Stub.asInterface(service);
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                iabService = null;
            }
        };
        Intent serviceIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        serviceIntent.setPackage("com.android.vending");
        bindService(serviceIntent, iabServiceConnection, Context.BIND_AUTO_CREATE);
    }

    public void onDestroy() {
        if (dbContext != null) {
            dbContext.close();
        }
        if (iabService != null || iabServiceConnection != null) {
            unbindService(iabServiceConnection);
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        handleBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                handleBackPressed();
                return true;
        }
        return false;
    }

    public void onResume() {
        super.onResume();
        if (currentFragment != null) {
            settingsOptionsList.setVisibility(View.INVISIBLE);
            try {
                getSupportFragmentManager().beginTransaction().remove(currentFragment).commit();
            } catch (IllegalStateException ex) {
                // Could not perform an action
            }
            currentFragment = null;
        }

        getSupportActionBar().setTitle(R.string.settings);
        settingsOptionsList.setVisibility(View.VISIBLE);
    }

    private void handleBackPressed() {
        if (currentFragment != null) {

            if (currentFragment instanceof HiddenAppsFragment) {
                setCurrentFragment(getString(R.string.personalisation), prevFragment);
            } else {
                getSupportActionBar().setTitle(R.string.settings);
                settingsOptionsList.setVisibility(View.VISIBLE);

                getSupportFragmentManager().beginTransaction().remove(currentFragment).commit();
                currentFragment = null;
            }
        } else {
            finish();
        }
    }

    public MagnesiumDbContext getDbContext() {
        return dbContext;
    }

    public void setCurrentFragment(String title, Fragment fragment) {
        if (currentFragment != null && currentFragment.equals(fragment)) {
            return;
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                settingsOptionsList.setVisibility(View.GONE);
            }
        });
        fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
        if (fragment instanceof HiddenAppsFragment) {
            prevFragment = currentFragment;
        }
        currentFragment = fragment;

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(title);
        }
    }

    public IconPack getCurrentIconPack() {
        IconPack iconPack = IconPack.fromSettingValue(preferences.getString(HomeActivity.SETTING_KEY_ICON_PACK, HomeActivity.DEFAULT_VALUE_ICON_PACK_NONE));
        return iconPack;
    }

    private class StringListAdapter extends ArrayAdapter<String> {
        public StringListAdapter(Context context) {
            super(context, 0);
        }
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_string, null);
            }

            String item = getItem(position);
            TextView itemTextView = (TextView) convertView.findViewById(R.id.item_text);
            itemTextView.setText(item);

            return convertView;
        }
    }

    public String getEncryptionKey() {
        return this.encryptionKey;
    }

    private void initialiseEncryptionKey() {
        encryptionKey = preferences.getString(HomeActivity.SP_ENC_KEY, null);
        if (encryptionKey == null) {
            // Get a default encryption key
            TelephonyManager manager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            try {
                if (manager != null) {
                    encryptionKey = manager.getSubscriberId();
                    if (Helper.isEmpty(encryptionKey)) {
                        encryptionKey = manager.getLine1Number();
                    }
                    if (Helper.isEmpty(encryptionKey)) {
                        encryptionKey = manager.getDeviceId();
                    }
                }
            } catch (SecurityException ex) {

            }
            if (Helper.isEmpty(encryptionKey)) {
                encryptionKey = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
            }

            // Final fallback
            if (Helper.isEmpty(encryptionKey)) {
                encryptionKey = HomeActivity.SP_DEFAULT_KEY;
            }
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(HomeActivity.SP_ENC_KEY, encryptionKey);
            editor.commit();
        }
    }

    public IInAppBillingService getIabService() {
        return iabService;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == IAB_PURCHASE_REQUEST_CODE) {
            int responseCode = data.getIntExtra("RESPONSE_CODE", 0);
            String jsonPurchaseData = data.getStringExtra("INAPP_PURCHASE_DATA");
            if (responseCode != 0) {
                Helper.showToast("The in-app purchase process could not be completed at this time. Please try again later.", this);
                return;
            }

            try {
                ObjectMapper mapper = new ObjectMapper();
                JsonNode jsonNode = mapper.readTree(jsonPurchaseData);
                final String sku = Helper.getNodeValue("productId", jsonNode);
                final String purchaseToken = Helper.getNodeValue("purchaseToken", jsonNode);
                if (currentFragment instanceof IndexingAndSearchFragment
                        && IndexingAndSearchFragment.SKU_WEB_SEARCH_RESULTS.equals(sku)) {
                    IndexingAndSearchFragment isFragment = (IndexingAndSearchFragment) currentFragment;
                    isFragment.onWsrPurchased();
                } else if (currentFragment instanceof GeneralSettingsFragment) {
                    GeneralSettingsFragment gsFragment = (GeneralSettingsFragment) currentFragment;
                    gsFragment.onPurchased(sku);
                }

                // NOTE: No need to consume single-purchase products
                // Consume the product
                /*(new AsyncTask<Void, Void, Integer>() {
                    protected Integer doInBackground(Void... params) {
                        try {
                            return iabService.consumePurchase(3, getPackageName(), purchaseToken);
                        } catch (RemoteException ex) {
                            Log.e(TAG, "Could not consume the SKU purchase: " + sku, ex);
                        }
                        return -1;
                    }
                }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);*/
            } catch (IOException ex) {
                Helper.showToast("The server returned an invalid response for the in-app purchase. Please try again later.", this);
            }
        }
    }
}
