package co.aureolin.labs.magnesium.util.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.jsoup.Jsoup;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import co.aureolin.labs.magnesium.fragment.settings.IndexingAndSearchFragment;
import co.aureolin.labs.magnesium.model.Attachment;
import co.aureolin.labs.magnesium.model.BaseItem;
import co.aureolin.labs.magnesium.model.EmailAccount;
import co.aureolin.labs.magnesium.model.FeedItem;
import co.aureolin.labs.magnesium.model.IndexedItem;
import co.aureolin.labs.magnesium.model.OauthData;
import co.aureolin.labs.magnesium.model.RssFeed;
import co.aureolin.labs.magnesium.model.SocialMediaAccount;
import co.aureolin.labs.magnesium.util.Helper;

/**
 * Created by akinwale on 08/02/2016.
 */
public class MagnesiumDbContext extends SQLiteOpenHelper {
    public static final String TAG = MagnesiumDbContext.class.getCanonicalName();

    private static final String databaseName = "magnesium.launcher";

    private static final int databaseVersion = 4;

    private static final String SQL_CREATE_FEED_ITEMS_TABLE =
        "CREATE TABLE FeedItems" +
        "(" +
        "    Id INTEGER PRIMARY KEY AUTOINCREMENT" +
        "  , OwnerId INTEGER " +
        "  , Type INTEGER NOT NULL" +
        "  , ExternalRefId TEXT " +
        "  , ThreadId TEXT " +
        "  , Title TEXT" +
        "  , Content TEXT" +
        "  , ContentIndex TEXT" +
        "  , ContentLength INTEGER" +
        "  , Url TEXT" +
        "  , ImageUrl TEXT" +
        "  , MediaUrl TEXT" +
        "  , MediaType TEXT" +
        "  , EmbeddedContent TEXT " +
        "  , InResponseTo TEXT " +
        "  , Direction INTEGER" +
        "  , Recipients TEXT" +
        "  , CcRecipients TEXT " +
        "  , BccRecipients TEXT " +
        "  , ReplyTo TEXT " +
        "  , Author TEXT " +
        "  , CommentCount INTEGER " +
        "  , LikeCount INTEGER " +
        "  , ShareCount INTEGER " +
        "  , IsRetweet INTEGER " +
        "  , PosterId TEXT" +
        "  , PosterImageUrl TEXT" +
        "  , RetweetedById TEXT " +
        "  , RetweetedBy TEXT " +
        "  , Source TEXT " +
        "  , Duration INTEGER " +
        "  , IsRead INTEGER" +
        "  , IsArchived INTEGER" +
        "  , IsMissed INTEGER " +
        "  , IsVoicemail INTEGER " +
        "  , EntryTime TEXT" +
        "  , CreatedOn TEXT NOT NULL" +
        "  , ModifiedOn TEXT NOT NULL" +
        "  , LastSyncedOn TEXT " +
        ")";

    private static final String SQL_CREATE_ATTACHMENTS_TABLE =
        "CREATE TABLE Attachments" +
        "(" +
        "    Id INTEGER PRIMARY KEY AUTOINCREMENT " +
        "  , FeedItemId INTEGER " +
        "  , ParentId TEXT " +
        "  , ExternalId TEXT " +
        "  , ContentType TEXT " +
        "  , FileName TEXT " +
        "  , ContentLength INTEGER " +
        ")";

    private static final String SQL_CREATE_INDEXED_ITEMS_TABLE =
        "CREATE TABLE IndexedItems" +
        "(" +
        "    Id INTEGER PRIMARY KEY AUTOINCREMENT" +
        "  , RefId TEXT " +
        "  , FeedItemId INTEGER" +
        "  , MimeType TEXT " +
        "  , Title TEXT " +
        "  , MetaFieldName TEXT" +
        "  , MetaContent TEXT" +
        "  , MetaContentIndex TEXT" +
        "  , ContentLength INTEGER" +
        "  , Type INTEGER NOT NULL" +
        "  , Direction INTEGER " +
        "  , ReplyTo TEXT " +
        "  , Author TEXT " +
        "  , Recipients TEXT " +
        "  , Url TEXT" +
        "  , IsArchived INTEGER" +
        "  , IndexedOn TEXT NOT NULL " +
        "  , CreatedOn TEXT" +
        "  , ModifiedOn TEXT" +
        "  , LastSyncedOn TEXT" +
        ")";

    private static final String SQL_CREATE_SOCIAL_MEDIA_ACCOUNTS_TABLE =
        "CREATE TABLE SocialMediaAccounts " +
        "(" +
        "    Id INTEGER PRIMARY KEY AUTOINCREMENT " +
        "  , AccountId TEXT NOT NULL " +
        "  , SocialAccountType TEXT NOT NULL " +
        "  , DisplayName TEXT " +
        "  , OauthData TEXT " +
        "  , LastSyncedOn TEXT " +
        "  , UNIQUE (AccountId, SocialAccountType) " +
        ")";

    private static final String SQL_CREATE_EMAIL_ACCOUNTS_TABLE =
        "CREATE TABLE EmailAccounts " +
        "(" +
        "    Id INTEGER PRIMARY KEY AUTOINCREMENT " +
        "  , EmailAddress TEXT NOT NULL " +
        "  , Username TEXT NOT NULL " +
        "  , Password TEXT NOT NULL " +
        "  , HostName TEXT NOT NULL " +
        "  , Port INTEGER NOT NULL " +
        "  , ConnectionType INTEGER NOT NULL " +
        "  , LastSyncedOn TEXT " +
        "  , UNIQUE (EmailAddress) " +
        ")";

    private static final String SQL_CREATE_RSS_FEEDS_TABLE =
        "CREATE TABLE RssFeeds " +
        "(" +
        "    Id INTEGER PRIMARY KEY AUTOINCREMENT " +
        "  , Title TEXT NOT NULL " +
        "  , Url TEXT NOT NULL " +
        "  , LastSyncedOn TEXT " +
        "  , UNIQUE (Url) " +
        ")";

    private static final String SQL_CREATE_ITEM_SEARCH_INDEX_TABLE =
        "CREATE VIRTUAL TABLE ItemSearchIndex USING fts4" +
        "(" +
        "    Id INTEGER" +
        "  , Author TEXT" +
        "  , Title TEXT " +
        "  , MetaContent TEXT" +
        "  , Recipients TEXT " +
        ")";

    private static final String SQL_CREATE_INDEXES_V1 =
        "CREATE INDEX Idx_FeedItemType ON FeedItems (Type);" +
        "CREATE INDEX Idx_FeedItemExternalRef ON FeedItems (ExternalRefId);" +
        "CREATE INDEX Idx_FeedItemAuthor ON FeedItems (Author);" +
        "CREATE INDEX Idx_FeedItemContent ON FeedItems (ContentIndex);" +
        "CREATE INDEX Idx_FeedItemPoster ON FeedItems (PosterId);" +
        "CREATE INDEX Idx_FeedItemEntryDate ON FeedItems (EntryDate);" +
        "CREATE INDEX Idx_FeedItemCreated ON FeedItems (CreatedOn);" +
        "CREATE INDEX Idx_IndexedFeedItem ON IndexedItems (FeedItemId);" +
        "CREATE INDEX Idx_IndexedItemMetaField ON IndexedItems (MetaFieldName);" +
        "CREATE INDEX Idx_IndexedItemAuthor ON IndexedItems (Author);" +
        "CREATE INDEX Idx_IndexedItemMetaContent ON IndexedItems (MetaContentIndex);" +
        "CREATE INDEX Idx_IndexedItemType ON IndexedItems (Type);" +
        "CREATE INDEX Idx_IndexedItemIndexedOn ON IndexedItems (IndexedOn);" +
        "CREATE INDEX Idx_IndexedItemCreatedOn ON IndexedItems (CreatedOn);" +
        "CREATE INDEX Idx_IndexedItemCreatedOn ON IndexedItems (CreatedOn);" +
        "CREATE INDEX Idx_EmailAccountAddress ON EmailAccounts (EmailAddress);" +
        "CREATE INDEX Idx_SocialMediaAccountId ON SocialMediaAccounts (AccountId);" +
        "CREATE INDEX Idx_AttachmentParentId ON Attachments (ParentId);";

    private static final String SQL_CREATE_FEED_ITEM_INSERTED_TRIGGER_1 =
        "CREATE TRIGGER Trg_OnFeedItemInserted AFTER INSERT ON FeedItems " +
        "FOR EACH ROW " +
        "BEGIN " +
        "    INSERT INTO IndexedItems (FeedItemId, Type, Author, Title, MetaContent, Url, Recipients, CreatedOn, ModifiedOn, IndexedOn) " +
        "        VALUES (NEW.Id, NEW.Type, NEW.Author, NEW.Title, NEW.Content, NEW.Url, NEW.Recipients, NEW.EntryTime, NEW.ModifiedOn, CURRENT_TIMESTAMP); " +
        "END";

    private static final String SQL_CREATE_FEED_ITEM_INSERTED_TRIGGER_1_V3 =
        "CREATE TRIGGER Trg_OnFeedItemInserted AFTER INSERT ON FeedItems " +
        "FOR EACH ROW " +
        "BEGIN " +
        "    INSERT INTO IndexedItems (FeedItemId, Type, Author, Title, MetaContent, ContentLength, Url, Recipients, CreatedOn, ModifiedOn, IndexedOn) " +
        "        VALUES (NEW.Id, NEW.Type, NEW.Author, NEW.Title, NEW.Content, NEW.ContentLength, NEW.Url, NEW.Recipients, NEW.EntryTime, NEW.ModifiedOn, CURRENT_TIMESTAMP); " +
        "END";

    private static final String SQL_CREATE_FEED_ITEM_INSERTED_TRIGGER_2 =
        "CREATE TRIGGER Trg_UpdateContentIndexOnFeedItemInserted AFTER INSERT ON FeedItems " +
        "FOR EACH ROW " +
        "BEGIN " +
        "    UPDATE FeedItems SET ContentIndex = " +
        "        (CASE WHEN (New.Content IS NOT NULL AND New.ContentLength > 160) THEN SUBSTR(NEW.Content, 1, 160) ELSE NEW.Content END)" +
        "    WHERE Id = NEW.Id;" +
        "END";

    private static final String SQL_CREATE_INDEXED_ITEM_INSERTED_TRIGGER_1 =
        "CREATE TRIGGER Trg_OnIndexedItemInserted AFTER INSERT ON IndexedItems " +
        "FOR EACH ROW " +
        "BEGIN " +
        "    INSERT INTO ItemSearchIndex (Id, Author, MetaContent, Recipients) " +
        "        VALUES (NEW.Id, NEW.Author, NEW.MetaContent, NEW.Recipients); " +
        "END ";

    private static final String SQL_CREATE_INDEXED_ITEM_INSERTED_TRIGGER_2_V1 =
        "CREATE TRIGGER Trg_UpdateMetaContentIndexOnIndexedItemInserted AFTER INSERT ON IndexedItems " +
        "FOR EACH ROW " +
        "BEGIN " +
        "    UPDATE IndexedItems SET MetaContentIndex = " +
        "        (CASE WHEN (New.MetaContent IS NOT NULL AND LENGTH(New.MetaContent) > 160) THEN SUBSTR(NEW.MetaContent, 1, 160) ELSE NEW.MetaContent END) " +
        "    WHERE Id = NEW.Id;" +
        "END";

    private static final String SQL_CREATE_INDEXED_ITEM_INSERTED_TRIGGER_2_V3 =
        "CREATE TRIGGER Trg_UpdateMetaContentIndexOnIndexedItemInserted AFTER INSERT ON IndexedItems " +
        "FOR EACH ROW " +
        "BEGIN " +
        "    UPDATE IndexedItems SET MetaContentIndex = " +
        "        (CASE WHEN (New.MetaContent IS NOT NULL AND LENGTH(New.MetaContent) > 160) THEN SUBSTR(NEW.MetaContent, 1, 160) ELSE NEW.MetaContent END), " +
        "    ContentLength = (CASE WHEN (New.ContentLength IS NULL) THEN LENGTH(New.MetaContent) ELSE New.ContentLength END)" +
        "    WHERE Id = NEW.Id;" +
        "END ";

    private static final String SQL_V1_V2_ATTACHMENTS_ADD_FEED_ITEM_ID_COLUMN = "ALTER TABLE Attachments ADD COLUMN FeedItemId INTEGER";
    private static final String SQL_V1_V2_ATTACHMENTS_ADD_EXTERNAL_ID_COLUMN = "ALTER TABLE Attachments ADD COLUMN ExternalId TEXT";

    private static final String SQL_V2_V3_FEED_ITEMS_ADD_CONTENT_LENGTH_COLUMN = "ALTER TABLE FeedItems ADD COLUMN ContentLength INTEGER";
    private static final String SQL_V2_V3_INDEXED_ITEMS_ADD_CONTENT_LENGTH_COLUMN = "ALTER TABLE IndexedItems ADD COLUMN ContentLength INTEGER";
    private static final String SQL_V2_V3_UPDATE_FEED_ITEMS_CONTENT_LENGTH = "UPDATE FeedItems SET ContentLength = LENGTH(Content) WHERE ContentLength IS NULL";
    private static final String SQL_V2_V3_UPDATE_INDEXED_ITEMS_CONTENT_LENGTH = "UPDATE IndexedItems SET ContentLength = LENGTH(MetaContent) WHERE ContentLength IS NULL";
    private static final String SQL_V2_V3_DROP_CREATE_FEED_ITEM_INSERTED_TRIGGER_1 = "DROP TRIGGER Trg_OnFeedItemInserted";

    private static final String SQL_V3_V4_INDEXED_ITEMS_ADD_IS_ARCHIVED_COLUMN = "ALTER TABLE IndexedItems ADD COLUMN IsArchived INTEGER";

    private static final String[] MAIN_SETUP_SQL = new String[] {
        SQL_CREATE_FEED_ITEMS_TABLE,
        SQL_CREATE_INDEXED_ITEMS_TABLE,
        SQL_CREATE_ITEM_SEARCH_INDEX_TABLE,
        SQL_CREATE_EMAIL_ACCOUNTS_TABLE,
        SQL_CREATE_RSS_FEEDS_TABLE,
        SQL_CREATE_ATTACHMENTS_TABLE,
        SQL_CREATE_SOCIAL_MEDIA_ACCOUNTS_TABLE,
        SQL_CREATE_INDEXES_V1,
        SQL_CREATE_FEED_ITEM_INSERTED_TRIGGER_1_V3,
        SQL_CREATE_FEED_ITEM_INSERTED_TRIGGER_2,
        SQL_CREATE_INDEXED_ITEM_INSERTED_TRIGGER_1,
        SQL_CREATE_INDEXED_ITEM_INSERTED_TRIGGER_2_V3
    };

    private static final String[] UPGRADE_V1_V2_SETUP_SQL = new String[] {
        SQL_V1_V2_ATTACHMENTS_ADD_FEED_ITEM_ID_COLUMN,
        SQL_V1_V2_ATTACHMENTS_ADD_EXTERNAL_ID_COLUMN
    };

    private static final String[] UPGRADE_V2_V3_SETUP_SQL = new String[] {
        SQL_V2_V3_FEED_ITEMS_ADD_CONTENT_LENGTH_COLUMN,
        SQL_V2_V3_INDEXED_ITEMS_ADD_CONTENT_LENGTH_COLUMN,
        SQL_V2_V3_UPDATE_FEED_ITEMS_CONTENT_LENGTH,
        SQL_V2_V3_UPDATE_INDEXED_ITEMS_CONTENT_LENGTH,
        SQL_V2_V3_DROP_CREATE_FEED_ITEM_INSERTED_TRIGGER_1,
        SQL_CREATE_FEED_ITEM_INSERTED_TRIGGER_1_V3
    };

    private static final String[] UPGRADE_V3_V4_SETUP_SQL = new String[] {
        SQL_V3_V4_INDEXED_ITEMS_ADD_IS_ARCHIVED_COLUMN
    };

    private static final String SQL_INDEXED_ITEM_REF_ID_EXISTS = "SELECT Id FROM IndexedItems WHERE RefId = ?";

    private static final String SQL_INDEXED_ITEM_WITH_REF_ID_EXISTS =
        "SELECT Id FROM IndexedItems WHERE RefId = ? AND Type = ? AND MetaFieldName = ? AND MetaContent = ?";

    private static final String SQL_FEED_ITEM_EXT_REF_ID_EXISTS =
        "SELECT Id FROM FeedItems WHERE ExternalRefId = ? AND Type = ?";

    private static final String SQL_FEED_ITEM_EXT_REF_ID_FOR_OWNER_EXISTS =
        "SELECT Id FROM FeedItems WHERE OwnerId = ? AND ExternalRefId = ? AND Type = ?";

    private static final String SQL_GET_EMAIL_ACCOUNTS =
        "SELECT Id, EmailAddress, Username, Password, HostName, Port, ConnectionType, LastSyncedOn FROM EmailAccounts ORDER BY LOWER(EmailAddress) ASC";

    private static final String SQL_GET_EMAIL_ACCOUNT_BY_ID =
        "SELECT Id, EmailAddress, Username, Password, HostName, Port, ConnectionType, LastSyncedOn FROM EmailAccounts WHERE Id = ?";

    private static final String SQL_GET_EMAIL_ACCOUNT_BY_EMAIL_ADDRESS =
        "SELECT Id, EmailAddress, Username, Password, HostName, Port, ConnectionType, LastSyncedOn FROM EmailAccounts WHERE EmailAddress = ?";

    private static final String SQL_GET_RSS_FEEDS =
        "SELECT Id, Title, Url, LastSyncedOn FROM RssFeeds";

    private static final String SQL_GET_RSS_FEED_BY_ID =
        "SELECT Id, Title, Url, LastSyncedOn FROM RssFeeds WHERE Id = ?";

    private static final String SQL_GET_RSS_FEED_BY_URL =
        "SELECT Id, Title, Url, LastSyncedOn FROM RssFeeds WHERE Url = ?";

    private static final String SQL_GET_SOCIAL_MEDIA_ACCOUNTS =
        "SELECT Id, AccountId, SocialAccountType, DisplayName, OauthData, LastSyncedOn FROM SocialMediaAccounts ORDER BY LOWER(DisplayName) ASC";

    private static final String SQL_GET_INSTAGRAM_ACCOUNTS =
            "SELECT Id, AccountId, SocialAccountType, DisplayName, OauthData, LastSyncedOn FROM SocialMediaAccounts WHERE SocialAccountType = 'instagram' ORDER BY LOWER(DisplayName) ASC";

    private static final String SQL_GET_SOCIAL_MEDIA_ACCOUNT =
        "SELECT Id, AccountId, SocialAccountType, DisplayName, OauthData, LastSyncedOn FROM SocialMediaAccounts WHERE AccountId = ? AND SocialAccountType = ? ";

    private static final String SQL_GET_OAUTH_DATA_LIST_FOR_TYPE =
        "SELECT Id, AccountId, OauthData FROM SocialMediaAccounts WHERE SocialAccountType = ?";

    private static final String SQL_GET_EMAIL_FEED_ITEM =
        "SELECT Id, OwnerId, ExternalRefId, Type, PosterId, Author, ReplyTo, Recipients, CcRecipients, BccRecipients, Title, Content, EntryTime FROM FeedItems WHERE Id = ?";

    private static final String SQL_GET_ATTACHMENTS_FOR_FEED_ITEM =
        "SELECT Id, ExternalId, FeedItemId, ParentId, FileName, ContentType, ContentLength FROM Attachments WHERE FeedItemId = ?";

    private static final String SQL_GET_FEED_ITEMS =
            "SELECT FI.Id" +
            "   , FI.OwnerId " +
            "   , FI.ExternalRefId" +
            "   , FI.Title" +
            "   , (CASE WHEN (FI.Type = 6 OR FI.Type = 8 OR FI.Type = 9) THEN FI.Content ELSE FI.ContentIndex END) AS Content" +
            "   , FI.ContentLength" +
            "   , FI.Type" +
            "   , FI.Direction " +
            "   , FI.ReplyTo " +
            "   , FI.Author" +
            "   , FI.Recipients" +
            "   , FI.Url" +
            "   , FI.ImageUrl" +
            "   , FI.MediaUrl" +
            "   , FI.PosterId" +
            "   , FI.PosterImageUrl" +
            "   , FI.CommentCount" +
            "   , FI.LikeCount " +
            "   , FI.ShareCount " +
            "   , FI.ThreadId " +
            "   , FI.Source" +
            "   , FI.IsRetweet " +
            "   , FI.RetweetedById " +
            "   , FI.RetweetedBy " +
            "   , FI.Duration " +
            "   , FI.IsMissed " +
            "   , FI.IsVoicemail " +
            "   , FI.EntryTime" +
            "   , FI.CreatedOn " +
            "   , FI.ModifiedOn " +
            "FROM FeedItems FI " +
            "WHERE 1 = 1 AND FI.IsArchived <> 1 %s " +
            "ORDER BY IFNULL(FI.EntryTime, FI.CreatedOn) DESC LIMIT %d";

    private static final String SQL_FIND_INDEXED_ITEMS =
            "SELECT   II.Id" +
            "       , II.RefId" +
            "       , II.FeedItemId" +
            "       , II.MimeType" +
            "       , IFNULL(II.Title, FI.Title)" +
            "       , II.MetaFieldName" +
            "       , (CASE WHEN (IFNULL(II.Type, FI.Type) = 6 OR IFNULL(II.Type, FI.Type) = 8 OR IFNULL(II.Type, FI.Type) = 9) THEN IFNULL(II.MetaContent, FI.Content) ELSE IFNULL(II.MetaContentIndex, FI.ContentIndex) END) AS Content " +
            "       , IFNULL(II.Type, FI.Type) " +
            "       , IFNULL(II.Direction, FI.Direction) " +
            "       , IFNULL(II.ReplyTo, FI.ReplyTo) " +
            "       , IFNULL(II.Author, FI.Author) " +
            "       , IFNULL(II.Recipients, FI.Recipients) " +
            "       , IFNULL(II.Url, FI.Url) " +
            "       , II.IndexedOn " +
            "       , IFNULL(IFNULL(II.CreatedOn, FI.CreatedOn), II.IndexedOn) " +
            "       , IFNULL(II.ModifiedOn, FI.ModifiedOn) " +
            "       , IFNULL(II.ContentLength, FI.ContentLength) " +
            "       , FI.Id " +
            "       , FI.OwnerId " +
            "       , FI.ExternalRefId " +
            "       , FI.PosterId " +
            "       , FI.PosterImageUrl " +
            "       , FI.IsRead " +
            "       , FI.IsArchived " +
            "       , FI.ImageUrl " +
            "       , FI.MediaUrl " +
            "       , FI.CommentCount" +
            "       , FI.LikeCount " +
            "       , FI.ShareCount " +
            "       , FI.ThreadId " +
            "       , FI.Source " +
            "       , FI.IsRetweet " +
            "       , FI.RetweetedById " +
            "       , FI.RetweetedBy " +
            "       , FI.Duration " +
            "       , FI.IsMissed " +
            "       , FI.IsVoicemail " +
            "       , IFNULL(FI.EntryTime, FI.CreatedOn) " +
            "FROM IndexedItems II " +
            "LEFT JOIN FeedItems FI ON FI.Id = II.FeedItemId " +
            "WHERE IFNULL(II.IsArchived, FI.IsArchived) <> 1 AND IFNULL(II.Type, FI.Type) IN (%s) AND (II.Id IN (" +
            "          SELECT Id FROM ItemSearchIndex ISI WHERE ISI.Author MATCH ? " +
            "    UNION SELECT Id FROM ItemSearchIndex ISI WHERE ISI.Title MATCH ? " +
            "    UNION SELECT Id FROM ItemSearchIndex ISI WHERE ISI.MetaContent MATCH ? " +
            "    UNION SELECT Id FROM ItemSearchIndex ISI WHERE ISI.Recipients MATCH ? " +
            ") OR II.Id IN (" +
            "          SELECT III.Id FROM IndexedItems III WHERE III.Author LIKE ?" +
            "       OR III.Title LIKE ? OR III.MetaContentIndex LIKE ? OR III.Recipients LIKE ? OR III.Url LIKE ?" +
            ")) LIMIT %d";

    private static final String ATTACHMENTS_TABLE = "Attachments";

    private static final String FEED_ITEMS_TABLE = "FeedItems";

    private static final String INDEXED_ITEMS_TABLE = "IndexedItems";

    private static final String SOCIAL_MEDIA_ACCOUNTS_TABLE = "SocialMediaAccounts";

    private static final String EMAIL_ACCOUNTS_TABLE = "EmailAccounts";

    private static final String RSS_FEEDS_TABLE = "RssFeeds";

    private static final String REF_ID_COLUMN = "RefId";

    private static final String THREAD_ID_COLUMN = "ThreadId";

    private static final String EXTERNAL_REF_ID_COLUMN = "ExternalRefId";

    private static final String AUTHOR_COLUMN = "Author";

    private static final String RECIPIENTS_COLUMN = "Recipients";

    private static final String CC_RECIPIENTS_COLUMN = "CcRecipients";

    private static final String BCC_RECIPIENTS_COLUMN = "BccRecipients";

    private static final String TYPE_COLUMN = "Type";

    private static final String MIME_TYPE_COLUMN = "MimeType";

    private static final String TITLE_COLUMN = "Title";

    private static final String CONTENT_COLUMN = "Content";

    private static final String URL_COLUMN = "Url";

    private static final String IMAGE_URL_COLUMN = "ImageUrl";

    private static final String MEDIA_TYPE_COLUMN = "MediaType";

    private static final String MEDIA_URL_COLUMN = "MediaUrl";

    private static final String DIRECTION_COLUMN = "Direction";

    private static final String IS_READ_COLUMN = "IsRead";

    private static final String IS_ARCHIVED_COLUMN = "IsArchived";

    private static final String IS_MISSED_COLUMN = "IsMissed";

    private static final String IS_VOICEMAIL_COLUMN = "IsVoicemail";

    private static final String DURATION_COLUMN = "Duration";

    private static final String POSTER_ID_COLUMN = "PosterId";

    private static final String POSTER_IMAGE_URL_COLUMN = "PosterImageUrl";

    private static final String ENTRY_TIME_COLUMN = "EntryTime";

    private static final String CREATED_ON_COLUMN = "CreatedOn";

    private static final String MODIFIED_ON_COLUMN = "ModifiedOn";

    private static final String FEED_ITEM_ID_COLUMN = "FeedItemId";

    private static final String PARENT_ID_COLUMN = "ParentId";

    private static final String EXTERNAL_ID_COLUMN = "ExternalId";

    private static final String CONTENT_TYPE_COLUMN = "ContentType";

    private static final String FILE_NAME_COLUMN = "FileName";

    private static final String CONTENT_LENGTH_COLUMN = "ContentLength";

    private static final String META_FIELD_NAME_COLUMN = "MetaFieldName";

    private static final String META_CONTENT_COLUMN = "MetaContent";

    private static final String INDEXED_ON_COLUMN = "IndexedOn";

    private static final String ACCOUNT_ID_COLUMN = "AccountId";

    private static final String SOCIAL_ACCOUNT_TYPE_COLUMN = "SocialAccountType";

    private static final String DISPLAY_NAME_COLUMN = "DisplayName";

    private static final String OAUTH_DATA_COLUMN = "OauthData";

    private static final String OWNER_ID_COLUMN = "OwnerId";

    private static final String EMBEDDED_CONTENT_COLUMN = "EmbeddedContent";

    private static final String IN_RESPONSE_TO_COLUMN = "InResponseTo";

    private static final String COMMENT_COUNT_COLUMN = "CommentCount";

    private static final String LIKE_COUNT_COLUMN = "LikeCount";

    private static final String SHARE_COUNT_COLUMN = "ShareCount";

    private static final String SOURCE_COLUMN = "Source";

    private static final String LAST_SYNCED_ON_COLUMN = "LastSyncedOn";

    private static final String IS_RETWEET_COLUMN = "IsRetweet";

    private static final String RETWEETED_BY_COLUMN = "RetweetedBy";

    private static final String RETWEETED_BY_ID_COLUMN = "RetweetedById";

    private static final String EMAIL_ADDRESS_COLUMN = "EmailAddress";

    private static final String USERNAME_COLUMN = "Username";

    private static final String PASSWORD_COLUMN = "Password";

    private static final String HOSTNAME_COLUMN = "HostName";

    private static final String PORT_COLUMN = "Port";

    private static final String CONNECTION_TYPE_COLUMN = "ConnectionType";

    public MagnesiumDbContext(Context context) {
        super(context, databaseName, null, databaseVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        for (int i = 0; i < MAIN_SETUP_SQL.length; i++) {
            database.execSQL(MAIN_SETUP_SQL[i]);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        if (oldVersion < 2) {
            for (int i = 0; i < UPGRADE_V1_V2_SETUP_SQL.length; i++) {
                database.execSQL(UPGRADE_V1_V2_SETUP_SQL[i]);
            }
        }
        if (oldVersion < 3) {
            for (int i = 0; i < UPGRADE_V2_V3_SETUP_SQL.length; i++) {
                database.execSQL(UPGRADE_V2_V3_SETUP_SQL[i]);
            }
        }
        if (oldVersion < 4) {
            for (int i = 0; i < UPGRADE_V3_V4_SETUP_SQL.length; i++) {
                database.execSQL(UPGRADE_V3_V4_SETUP_SQL[i]);
            }
        }
    }

    private static FeedItem feedItemFromCursor(Cursor cursor) {
        FeedItem feedItem = new FeedItem();
        feedItem.setId(cursor.getLong(0));
        feedItem.setOwnerId(cursor.getLong(1));
        feedItem.setExternalRefId(cursor.getString(2));
        feedItem.setTitle(cursor.getString(3));
        feedItem.setContentLength(cursor.getInt(5));
        feedItem.setType(cursor.getInt(6));
        feedItem.setDirection(cursor.getInt(7));
        feedItem.setReplyTo(cursor.getString(8));
        feedItem.setAuthor(cursor.getString(9));
        feedItem.setRecipients(cursor.getString(10));
        feedItem.setUrl(cursor.getString(11));
        feedItem.setImageUrl(cursor.getString(12));
        feedItem.setMediaUrl(cursor.getString(13));
        feedItem.setPosterId(cursor.getString(14));
        feedItem.setPosterImageUrl(cursor.getString(15));
        feedItem.setCommentCount(cursor.getInt(16));
        feedItem.setLikeCount(cursor.getInt(17));
        feedItem.setShareCount(cursor.getInt(18));
        feedItem.setThreadId(cursor.getString(19));
        feedItem.setSource(cursor.getString(20));
        feedItem.setRetweet(cursor.getInt(21) == 1);
        feedItem.setRetweetedById(cursor.getString(22));
        feedItem.setRetweetedBy(cursor.getString(23));
        feedItem.setDuration(cursor.getLong(24));
        feedItem.setMissed(cursor.getInt(25) == 1);
        feedItem.setVoicemail(cursor.getInt(26) == 1);
        feedItem.setEntryTime(getDateFromString(cursor.getString(27)));
        feedItem.setCreatedOn(getDateFromString(cursor.getString(28)));
        feedItem.setModifiedOn(getDateFromString(cursor.getString(29)));

        String content = cursor.getString(4);
        if (!Helper.isEmpty(content)
                && (feedItem.getType() == BaseItem.ITEM_TYPE_EMAIL_MESSAGE || feedItem.getType() == BaseItem.ITEM_TYPE_RSS_FEED_ITEM)) {
            content = Jsoup.parse(content).text();
            if (content.length() > 160) {
                content = content.substring(0, 160).trim();
            }
        }
        if (content != null) {
            feedItem.setContent((feedItem.getContentLength() > 160  && feedItem.getType() != BaseItem.ITEM_TYPE_SMS_MESSAGE) ? String.format("%s...", content) : content);
        }

        return feedItem;
    }

    /*public static List<BaseItem> getFeedItems(SQLiteDatabase db, int limit) {
        return getFeedItems(db, limit, 0);
    }*/

    public static List<BaseItem> getFeedItems(SQLiteDatabase db, int limit, int itemType) {
        List<BaseItem> results = new ArrayList<BaseItem>();

        String addWhere = (itemType > 0) ? String.format(" AND FI.Type = %d", itemType) : "";
        String sql = String.format(SQL_GET_FEED_ITEMS, addWhere, limit);
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(sql, null);
            while (cursor.moveToNext()) {
                BaseItem item = feedItemFromCursor(cursor);
                results.add(item);
            }
        } catch (Exception ex) {
            Log.e(TAG, "Could not retrieve feed items", ex);
        } finally {
            Helper.closeCloseable(cursor);
        }
        return results;
    }

    public static FeedItem getEmailWithAttachments(long feedItemId, SQLiteDatabase db) {
        FeedItem feedItem = null;

        String sql = SQL_GET_EMAIL_FEED_ITEM;
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(sql, new String[] { String.valueOf(feedItemId) });
            if (cursor.moveToNext()) {
                feedItem = emailFeedItemFromCursor(cursor);
            }
        } catch (Exception ex) {
            Log.e(TAG, "Could not retrieve email feed item", ex);
        } finally {
            Helper.closeCloseable(cursor);
        }

        if (feedItem != null) {
            List<Attachment> attachments = new ArrayList<Attachment>();
            sql = SQL_GET_ATTACHMENTS_FOR_FEED_ITEM;
            try {
                cursor = db.rawQuery(sql, new String[] { String.valueOf(feedItemId) });
                while (cursor.moveToNext()) {
                    Attachment attachment = attachmentFromCursor(cursor);
                    attachments.add(attachment);
                }
            } catch (Exception ex) {
                Log.e(TAG, "Could not retrieve feed item attachments", ex);
            } finally {
                Helper.closeCloseable(cursor);
            }
            feedItem.setAttachments(attachments);
        }

        return feedItem;
    }

    private static FeedItem emailFeedItemFromCursor(Cursor cursor) {
        FeedItem feedItem = new FeedItem();
        feedItem.setId(cursor.getLong(0));
        feedItem.setOwnerId(cursor.getLong(1));
        feedItem.setExternalRefId(cursor.getString(2));
        feedItem.setType(cursor.getInt(3));
        feedItem.setPosterId(cursor.getString(4));
        feedItem.setAuthor(cursor.getString(5));
        feedItem.setReplyTo(cursor.getString(6));
        feedItem.setRecipients(cursor.getString(7));
        feedItem.setCcRecipients(cursor.getString(8));
        feedItem.setBccRecipients(cursor.getString(9));
        feedItem.setTitle(cursor.getString(10));
        feedItem.setContent(cursor.getString(11));
        feedItem.setEntryTime(getDateFromString(cursor.getString(12)));

        return feedItem;
    }

    private static Attachment attachmentFromCursor(Cursor cursor) {
        Attachment attachment = new Attachment();
        attachment.setId(cursor.getLong(0));
        attachment.setExternalId(cursor.getString(1));
        attachment.setFeedItemId(cursor.getLong(2));
        attachment.setParentId(cursor.getString(3));
        attachment.setFilename(cursor.getString(4));
        attachment.setContentType(cursor.getString(5));
        attachment.setContentLength(cursor.getLong(6));

        return attachment;
    }

    public static List<IndexedItem> findIndexedItems(String query, SQLiteDatabase db, int limit, String includeResults) {
        String wildcardQuery = "%" + query + "%";
        String ftsQuery = String.format("%s*", query);

        List<IndexedItem> results = new ArrayList<IndexedItem>();
        if (Helper.isEmpty(includeResults)) {
            includeResults = IndexingAndSearchFragment.DEFAULT_INCLUDE_SEARCH_RESULTS;
        }
        String sql = String.format(SQL_FIND_INDEXED_ITEMS, includeResults, limit);
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(sql, new String[] { ftsQuery, ftsQuery, ftsQuery, ftsQuery,
                wildcardQuery, wildcardQuery, wildcardQuery, wildcardQuery, wildcardQuery });
            while (cursor.moveToNext()) {
                IndexedItem item = indexedItemFromCursor(cursor);
                results.add(item);
            }
        } finally {
            Helper.closeCloseable(cursor);
        }
        return results;
    }

        /*  "SELECT   II.Id" +
            "       , II.RefId" +
            "       , II.FeedItemId" +
            "       , II.MimeType" +
            "       , IFNULL(II.Title, FI.Title)" +
            "       , II.MetaFieldName" +
            "       , IFNULL(II.MetaContentIndex, FI.ContentIndex) " +
            "       , IFNULL(II.Type, FI.Type) " +
            "       , IFNULL(II.Direction, FI.Direction) " +
            "       , IFNULL(II.ReplyTo, FI.ReplyTo) " +
            "       , IFNULL(II.Author, FI.Author) " +
            "       , IFNULL(II.Recipients, FI.Recipients) " +
            "       , IFNULL(II.Url, FI.Url) " +
            "       , II.IndexedOn " +
            "       , IFNULL(IFNULL(II.CreatedOn, FI.CreatedOn), II.IndexedOn) " +
            "       , LENGTH(IFNULL(II.MetaContent, FI.Content)) " +
            "       , FI.Id " +
            "       , FI.OwnerId " +
            "       , FI.ExternalRefId " +
            "       , FI.PosterId " +
            "       , FI.PosterImageUrl " +
            "       , FI.IsRead " +
            "       , FI.IsArchived " +
            "       , FI.ImageUrl " +
            "       , FI.MediaUrl " +
            "       , FI.CommentCount" +
            "       , FI.LikeCount " +
            "       , FI.ShareCount " +
            "       , FI.ThreadId " +
            "       , FI.Source" +
            "       , IFNULL(FI.EntryTime, FI.CreatedOn) "  */
    private static IndexedItem indexedItemFromCursor(Cursor cursor) {
        String content = cursor.getString(6);
        if (content != null) {
            content = content.trim();
        }

        IndexedItem indexedItem = new IndexedItem();
        indexedItem.setId(cursor.getLong(0));
        indexedItem.setRefId(cursor.getString(1));
        indexedItem.setFeedItemId(cursor.getLong(2));
        indexedItem.setMimeType(cursor.getString(3));
        indexedItem.setTitle(cursor.getString(4));
        indexedItem.setMetaFieldName(cursor.getString(5));
        indexedItem.setType(cursor.getInt(7));
        indexedItem.setDirection(cursor.getInt(8));
        indexedItem.setReplyTo(cursor.getString(9));
        indexedItem.setAuthor(cursor.getString(10));
        indexedItem.setRecipients(cursor.getString(11));
        indexedItem.setUrl(cursor.getString(12));
        indexedItem.setIndexedOn(getDateFromString(cursor.getString(13)));
        indexedItem.setCreatedOn(getDateFromString(cursor.getString(14)));
        indexedItem.setModifiedOn(getDateFromString(cursor.getString(15)));

        if (content != null
                && content.length() > 0
                && (indexedItem.getType() == BaseItem.ITEM_TYPE_EMAIL_MESSAGE || indexedItem.getType() == BaseItem.ITEM_TYPE_RSS_FEED_ITEM)) {
            content = Jsoup.parse(content).text();
            if (content.length() > 160) {
                content = content.substring(0, 160).trim();
            }
        }

        indexedItem.setContentLength(cursor.getInt(16)); // Get the content length
        if (content != null) {
            indexedItem.setMetaContent(
                (indexedItem.getContentLength() > 160 && indexedItem.getType() != BaseItem.ITEM_TYPE_SMS_MESSAGE) ? String.format("%s...", content) : content);
        }

          /*"       , FI.Id " + (17)
            "       , FI.OwnerId " + (18)
            "       , FI.ExternalRefId " + (19)
            "       , FI.PosterId " + (20)
            "       , FI.PosterImageUrl " + (21)
            "       , FI.IsRead " + (22)
            "       , FI.IsArchived " + (23)
            "       , FI.ImageUrl " + (24)
            "       , FI.MediaUrl " + (25)
            "       , FI.CommentCount" + (26)
            "       , FI.LikeCount " + (27)
            "       , FI.ShareCount " + (28)
            "       , FI.Source" + (29)
            "       , IFNULL(FI.EntryTime, FI.CreatedOn) " (30)
            */
        long feedItemId = cursor.getLong(17);
        if (feedItemId > 0) {
            // IndexedItem has a valid feed item, create the feed item corresponding to the indexed item
            FeedItem feedItem = new FeedItem();
            feedItem.setId(feedItemId);
            feedItem.setOwnerId(cursor.getLong(18));
            feedItem.setExternalRefId(cursor.getString(19));
            feedItem.setPosterId(cursor.getString(20));
            feedItem.setPosterImageUrl(cursor.getString(21));
            feedItem.setRead(cursor.getInt(22) > 0);
            feedItem.setArchived(cursor.getInt(23) > 0);
            feedItem.setImageUrl(cursor.getString(24));
            feedItem.setMediaUrl(cursor.getString(25));
            feedItem.setCommentCount(cursor.getInt(26));
            feedItem.setLikeCount(cursor.getInt(27));
            feedItem.setShareCount(cursor.getInt(28));
            feedItem.setThreadId(cursor.getString(29));
            feedItem.setSource(cursor.getString(30));
            feedItem.setRetweet(cursor.getInt(31) == 1);
            feedItem.setRetweetedById(cursor.getString(32));
            feedItem.setRetweetedBy(cursor.getString(33));
            feedItem.setDuration(cursor.getLong(34));
            feedItem.setMissed(cursor.getInt(35) == 1);
            feedItem.setVoicemail(cursor.getInt(36) == 1);
            feedItem.setEntryTime(getDateFromString(cursor.getString(37)));

            // Set other values
            feedItem.setTitle(indexedItem.getTitle());
            feedItem.setContent(indexedItem.getMetaContent());
            feedItem.setType(indexedItem.getType());
            feedItem.setDirection(indexedItem.getDirection());
            feedItem.setReplyTo(indexedItem.getReplyTo());
            feedItem.setAuthor(indexedItem.getAuthor());
            feedItem.setRecipients(indexedItem.getRecipients());
            feedItem.setUrl(indexedItem.getUrl());
            feedItem.setCreatedOn(indexedItem.getCreatedOn());
            feedItem.setModifiedOn(indexedItem.getModifiedOn());
            feedItem.setContentLength(indexedItem.getContentLength());
            indexedItem.setFeedItem(feedItem);
        }

        return indexedItem;
    }

    public static boolean indexedItemUniqueRefIdExists(IndexedItem indexedItem, SQLiteDatabase db) {
        boolean exists = false;
        String sql = SQL_INDEXED_ITEM_REF_ID_EXISTS;
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(sql, new String[] { indexedItem.getRefId() });
            if (cursor.moveToNext()) {
                exists = cursor.getLong(0) > 0;
            }
        } finally {
            Helper.closeCloseable(cursor);
        }
        return exists;
    }

    public static boolean externalRefIdExists(FeedItem feedItem, SQLiteDatabase db) {
        boolean exists = false;
        String sql = SQL_FEED_ITEM_EXT_REF_ID_EXISTS;
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(sql, new String[] { feedItem.getExternalRefId(), String.valueOf(feedItem.getType()) });
            if (cursor.moveToNext()) {
                exists = cursor.getLong(0) > 0;
            }
        } finally {
            Helper.closeCloseable(cursor);
        }
        return exists;
    }

    public static boolean externalRefIdForOwnerExists(FeedItem feedItem, SQLiteDatabase db) {
        boolean exists = false;
        String sql = SQL_FEED_ITEM_EXT_REF_ID_FOR_OWNER_EXISTS;
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(sql, new String[] {
                String.valueOf(feedItem.getOwnerId()), feedItem.getExternalRefId(), String.valueOf(feedItem.getType()) });
            if (cursor.moveToNext()) {
                exists = cursor.getLong(0) > 0;
            }
        } finally {
            Helper.closeCloseable(cursor);
        }
        return exists;
    }


    public static boolean indexedItemRefIdExists(IndexedItem indexedItem, SQLiteDatabase db) {
        boolean exists = false;
        String sql = SQL_INDEXED_ITEM_WITH_REF_ID_EXISTS;
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(sql, new String[] { indexedItem.getRefId(),
                String.valueOf(indexedItem.getType()), indexedItem.getMetaFieldName(),
                Helper.isEmpty(indexedItem.getMetaContent()) ? "" : indexedItem.getMetaContent() });
            if (cursor.moveToNext()) {
                exists = cursor.getLong(0) > 0;
            }
        } finally {
            Helper.closeCloseable(cursor);
        }
        return exists;
    }

    public static void update(FeedItem feedItem, SQLiteDatabase db) {
        ContentValues values = new ContentValues();
        putValue(IS_ARCHIVED_COLUMN, feedItem.isArchived() ? 1 : 0, values);

        db.update(FEED_ITEMS_TABLE, values, "Type = ? AND Id = ?",
                new String[] { String.valueOf(feedItem.getType()), String.valueOf(feedItem.getId()) });
    }

    public static void update(IndexedItem indexedItem, SQLiteDatabase db) {
        ContentValues values = new ContentValues();
        putValue(TITLE_COLUMN, indexedItem.getTitle(), values);
        putValue(IS_ARCHIVED_COLUMN, indexedItem.isArchived() ? 1 : 0, values);
        putValue(META_FIELD_NAME_COLUMN, indexedItem.getMetaFieldName(), values);
        putValue(META_CONTENT_COLUMN, indexedItem.getMetaContent(), values);

        db.update(INDEXED_ITEMS_TABLE, values, "Type = ? AND RefId = ?",
                new String[] { String.valueOf(indexedItem.getType()), indexedItem.getRefId() });
    }

    public static long insert(IndexedItem indexedItem, SQLiteDatabase db) {
        ContentValues values = new ContentValues();

        putValue(AUTHOR_COLUMN, indexedItem.getAuthor(), values);
        putValue(FEED_ITEM_ID_COLUMN, indexedItem.getFeedItemId(), values);
        putValue(RECIPIENTS_COLUMN, indexedItem.getRecipients(), values);
        putValue(MIME_TYPE_COLUMN, indexedItem.getMimeType(), values);
        putValue(REF_ID_COLUMN, indexedItem.getRefId(), values);
        putValue(TITLE_COLUMN, indexedItem.getTitle(), values);
        putValue(URL_COLUMN, indexedItem.getUrl(), values);
        putValue(TYPE_COLUMN, indexedItem.getType(), values);
        putValue(META_FIELD_NAME_COLUMN, indexedItem.getMetaFieldName(), values);
        putValue(META_CONTENT_COLUMN, indexedItem.getMetaContent(), values);
        putValue(CONTENT_LENGTH_COLUMN, Helper.getStringLength(indexedItem.getMetaContent()), values);
        putValue(DIRECTION_COLUMN, indexedItem.getDirection(), values);
        putValue(IS_ARCHIVED_COLUMN, indexedItem.isArchived() ? 1 : 0, values);
        putDateValue(CREATED_ON_COLUMN, indexedItem.getCreatedOn(), values);
        putDateValue(MODIFIED_ON_COLUMN, indexedItem.getModifiedOn(), values);
        putDateValue(INDEXED_ON_COLUMN, new Date(), values);
        putDateValue(LAST_SYNCED_ON_COLUMN, indexedItem.getLastSyncedOn(), values);

        return db.insert(INDEXED_ITEMS_TABLE, null, values);
    }

    public static long insert(FeedItem feedItem, SQLiteDatabase db) {
        ContentValues values = new ContentValues();

        Date dt = new Date();
        putValue(OWNER_ID_COLUMN, feedItem.getOwnerId(), values);
        putValue(TYPE_COLUMN, feedItem.getType(), values);
        putValue(AUTHOR_COLUMN, feedItem.getAuthor(), values);
        putValue(EXTERNAL_REF_ID_COLUMN, feedItem.getExternalRefId(), values);
        putValue(RECIPIENTS_COLUMN, feedItem.getRecipients(), values);
        putValue(CC_RECIPIENTS_COLUMN, feedItem.getCcRecipients(), values);
        putValue(BCC_RECIPIENTS_COLUMN, feedItem.getBccRecipients(), values);
        putValue(TITLE_COLUMN, feedItem.getTitle(), values);
        putValue(URL_COLUMN, feedItem.getUrl(), values);
        putValue(POSTER_ID_COLUMN, feedItem.getPosterId(), values);
        putValue(POSTER_IMAGE_URL_COLUMN, feedItem.getPosterImageUrl(), values);
        putValue(CONTENT_COLUMN, feedItem.getContent(), values);
        putValue(CONTENT_LENGTH_COLUMN, Helper.getStringLength(feedItem.getContent()), values);
        putValue(MEDIA_TYPE_COLUMN, feedItem.getMediaType(), values);
        putValue(IMAGE_URL_COLUMN, feedItem.getImageUrl(), values);
        putValue(MEDIA_URL_COLUMN, feedItem.getMediaUrl(), values);
        putValue(IN_RESPONSE_TO_COLUMN, feedItem.getInResponseTo(), values);
        putValue(EMBEDDED_CONTENT_COLUMN, feedItem.getEmbeddedContent(), values);
        putValue(THREAD_ID_COLUMN, feedItem.getThreadId(), values);
        putValue(SOURCE_COLUMN, feedItem.getSource(), values);
        putValue(IS_RETWEET_COLUMN, feedItem.isRetweet() ? 1 : 0, values);
        putValue(RETWEETED_BY_ID_COLUMN, feedItem.getRetweetedById(), values);
        putValue(RETWEETED_BY_COLUMN, feedItem.getRetweetedBy(), values);
        putValue(COMMENT_COUNT_COLUMN, feedItem.getCommentCount(), values);
        putValue(LIKE_COUNT_COLUMN, feedItem.getLikeCount(), values);
        putValue(SHARE_COUNT_COLUMN, feedItem.getShareCount(), values);
        putValue(DURATION_COLUMN, feedItem.getDuration(), values);
        putValue(IS_MISSED_COLUMN, feedItem.isMissed() ? 1 : 0, values);
        putValue(IS_VOICEMAIL_COLUMN, feedItem.isVoicemail() ? 1 : 0, values);
        putValue(IS_READ_COLUMN, feedItem.isRead() ? 1 : 0, values);
        putValue(IS_ARCHIVED_COLUMN, feedItem.isArchived() ? 1 : 0, values);
        putValue(DIRECTION_COLUMN, feedItem.getDirection(), values);
        putDateValue(CREATED_ON_COLUMN, dt, values);
        putDateValue(MODIFIED_ON_COLUMN, dt, values);
        putDateValue(LAST_SYNCED_ON_COLUMN, feedItem.getLastSyncedOn(), values);
        putDateValue(ENTRY_TIME_COLUMN, feedItem.getEntryTime(), values);

        try {
            return db.insert(FEED_ITEMS_TABLE, null, values);
        } catch (Exception ex) {
            return -1;
        }
    }

    public static boolean insertFeedItemWithAttachments(FeedItem feedItem, SQLiteDatabase db) {
        long feedItemId = insert(feedItem, db);
        if (feedItemId < 0) {
            return false;
        }

        if (feedItem.getAttachments() != null) {
            List<Attachment> attachments = feedItem.getAttachments();
            db.beginTransaction();
            try {
                for (int i = 0; i < attachments.size(); i++) {
                    Attachment attachment = attachments.get(i);
                    attachment.setFeedItemId(feedItemId);
                    attachment.setParentId(feedItem.getExternalRefId());
                    insert(attachment, db);
                }
                db.setTransactionSuccessful();
            } catch (Exception ex) {
                return false;
            } finally {
                db.endTransaction();
            }
        }

        return true;
    }

    public static long insert(Attachment attachment, SQLiteDatabase db) {
        ContentValues values = new ContentValues();

        putValue(FEED_ITEM_ID_COLUMN, attachment.getFeedItemId(), values);
        putValue(EXTERNAL_ID_COLUMN, attachment.getExternalId(), values);
        putValue(PARENT_ID_COLUMN, attachment.getParentId(), values);
        putValue(FILE_NAME_COLUMN, attachment.getFilename(), values);
        putValue(CONTENT_TYPE_COLUMN, attachment.getContentType(), values);
        putValue(CONTENT_LENGTH_COLUMN, attachment.getContentLength(), values);

        try {
            return db.insert(ATTACHMENTS_TABLE, null, values);
        } catch (Exception ex) {
            return -1;
        }
    }

    private static void putValue(String columnName, String value, ContentValues values) {
        if (!Helper.isEmpty(value)) {
            values.put(columnName, value);
        }
    }

    private static void putValue(String columnName, int value, ContentValues values) {
        values.put(columnName, value);
    }

    private static void putValue(String columnName, long value, ContentValues values) {
        values.put(columnName, value);
    }

    private static void putDateValue(String columnName, Date value, ContentValues values) {
        if (value != null) {
            putValue(columnName, getDateString(value), values);
        }
    }

    public static void insert(SocialMediaAccount socialMediaAccount, String encryptionKey, SQLiteDatabase db) {
        ContentValues values = new ContentValues();
        putValue(ACCOUNT_ID_COLUMN, socialMediaAccount.getAccountId(), values);
        putValue(SOCIAL_ACCOUNT_TYPE_COLUMN, socialMediaAccount.getSocialAccountType(), values);
        putValue(DISPLAY_NAME_COLUMN, socialMediaAccount.getDisplayName(), values);
        putValue(OAUTH_DATA_COLUMN,
            !Helper.isEmpty(encryptionKey) ? socialMediaAccount.getEncryptedOauthData(encryptionKey) : socialMediaAccount.getOauthData(), values);

        db.insert(SOCIAL_MEDIA_ACCOUNTS_TABLE, null, values);
    }

    public static long insert(EmailAccount emailAccount, String encryptionKey, SQLiteDatabase db) {
        ContentValues values = new ContentValues();
        putValue(EMAIL_ADDRESS_COLUMN, emailAccount.getEmailAddress(), values);
        putValue(USERNAME_COLUMN, emailAccount.getUsername(), values);
        putValue(PASSWORD_COLUMN, emailAccount.getEncryptedPassword(encryptionKey), values);
        putValue(HOSTNAME_COLUMN, emailAccount.getHostName(), values);
        putValue(PORT_COLUMN, emailAccount.getPort(), values);
        putValue(CONNECTION_TYPE_COLUMN, emailAccount.getConnectionType(), values);

        return db.insert(EMAIL_ACCOUNTS_TABLE, null, values);
    }

    private static final String SQL_GET_MAX_TWITTER_SINCE_ID =
        "SELECT ExternalRefId FROM FeedItems WHERE Type = 23 ORDER BY IFNULL(EntryTime, CreatedOn) DESC LIMIT 1";

    private static final String SQL_GET_LATEST_FEED_ITEM_TIMESTAMP =
        "SELECT IFNULL(EntryTime, CreatedOn) FROM FeedItems WHERE Type = ? ORDER BY IFNULL(EntryTime, CreatedOn) DESC LIMIT 1";

    private static final String SQL_GET_LATEST_FEED_ITEM_TIMESTAMP_FOR_OWNER =
            "SELECT IFNULL(EntryTime, CreatedOn) FROM FeedItems WHERE Type = ? AND OwnerId = ? ORDER BY IFNULL(EntryTime, CreatedOn) DESC LIMIT 1";

    public static String getMaxTwitterSinceId(SQLiteDatabase db) {
        String sinceId = "";
        String sql = SQL_GET_MAX_TWITTER_SINCE_ID;
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(sql, null);
            if (cursor.moveToNext()) {
                sinceId = cursor.getString(0);
            }
        } finally {
            Helper.closeCloseable(cursor);
        }
        return sinceId;
    }

    public static long getLatestFeedItemTimestampForOwner(int type, long accountId, SQLiteDatabase db) {
        long timestamp = 0;
        String sql = SQL_GET_LATEST_FEED_ITEM_TIMESTAMP_FOR_OWNER;
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(sql, new String[] { String.valueOf(type), String.valueOf(accountId) });
            if (cursor.moveToNext()) {
                Date dt = getDateFromString(cursor.getString(0));
                if (dt != null) {
                    timestamp = dt.getTime();
                }
            }
        } finally {
            Helper.closeCloseable(cursor);
        }

        return timestamp;
    }

    public static long getLatestFeedItemTimestamp(int type, SQLiteDatabase db) {
        long timestamp = 0;
        String sql = SQL_GET_LATEST_FEED_ITEM_TIMESTAMP;
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(sql, new String[] { String.valueOf(type) });
            if (cursor.moveToNext()) {
                Date dt = getDateFromString(cursor.getString(0));
                if (dt != null) {
                    timestamp = dt.getTime();
                }
            }
        } finally {
            Helper.closeCloseable(cursor);
        }

        return timestamp;
    }

    public static long insert(RssFeed rssFeed, SQLiteDatabase db) {
        ContentValues values = new ContentValues();
        putValue(TITLE_COLUMN, rssFeed.getTitle(), values);
        putValue(URL_COLUMN, rssFeed.getUrl(), values);

        return db.insert(RSS_FEEDS_TABLE, null, values);
    }

    public static List<EmailAccount> getEmailAccounts(String encryptionKey, SQLiteDatabase db) {
        List<EmailAccount> accounts = new ArrayList<EmailAccount>();
        String sql = SQL_GET_EMAIL_ACCOUNTS;
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(sql, null);
            while (cursor.moveToNext()) {
                accounts.add(emailAccountFromCursor(encryptionKey, cursor));
            }
        } finally {
            Helper.closeCloseable(cursor);
        }

        return accounts;
    }

    public static List<RssFeed> getRssFeeds(SQLiteDatabase db) {
        List<RssFeed> rssFeeds = new ArrayList<RssFeed>();
        String sql = SQL_GET_RSS_FEEDS;
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(sql, null);
            while (cursor.moveToNext()) {
                rssFeeds.add(rssFeedFromCursor(cursor));
            }
        } finally {
            Helper.closeCloseable(cursor);
        }

        return rssFeeds;
    }

    public static List<SocialMediaAccount> getSocialMediaAccounts(SQLiteDatabase db) {
        List<SocialMediaAccount> accounts = new ArrayList<SocialMediaAccount>();
        String sql = SQL_GET_SOCIAL_MEDIA_ACCOUNTS;
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(sql, null);
            while (cursor.moveToNext()) {
                accounts.add(socialMediaAccountFromCursor(null, cursor));
            }
        } finally {
            Helper.closeCloseable(cursor);
        }

        return accounts;
    }

    public static List<SocialMediaAccount> getInstagramAccounts(String encryptionKey, SQLiteDatabase db) {
        List<SocialMediaAccount> accounts = new ArrayList<SocialMediaAccount>();
        String sql = SQL_GET_INSTAGRAM_ACCOUNTS;
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(sql, null);
            while (cursor.moveToNext()) {
                accounts.add(socialMediaAccountFromCursor(encryptionKey, cursor));
            }
        } finally {
            Helper.closeCloseable(cursor);
        }

        return accounts;
    }

    public static SocialMediaAccount getSocialMediaAccount(String accountId, String accountType, SQLiteDatabase db) {
        String sql = SQL_GET_SOCIAL_MEDIA_ACCOUNT;
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(sql, new String[] { accountId, accountType });
            if (cursor.moveToNext()) {
                return socialMediaAccountFromCursor(null, cursor);
            }
        } finally {
            Helper.closeCloseable(cursor);
        }

        return null;
    }

    public static EmailAccount emailAccountFromCursor(String encryptionKey, Cursor cursor) {
        EmailAccount account = new EmailAccount();
        account.setId(cursor.getLong(0));
        account.setEmailAddress(cursor.getString(1));
        account.setUsername(cursor.getString(2));
        account.setPasswordFromEncryptedString(cursor.getString(3), encryptionKey);
        account.setHostName(cursor.getString(4));
        account.setPort(cursor.getInt(5));
        account.setConnectionType(cursor.getInt(6));
        account.setLastSyncedOn(getDateFromString(cursor.getString(7)));

        return account;
    }

    public static SocialMediaAccount socialMediaAccountFromCursor(String encryptionKey, Cursor cursor) {
        SocialMediaAccount account = new SocialMediaAccount();
        account.setId(cursor.getLong(0));
        account.setAccountId(cursor.getString(1));
        account.setSocialAccountType(cursor.getString(2));
        account.setDisplayName(cursor.getString(3));
        if (!Helper.isEmpty(encryptionKey)) {
            account.setOauthDataFromEncryptedString(cursor.getString(4), encryptionKey);
        } else {
            account.setOauthData(cursor.getString(4));
        }

        account.setLastSyncedOn(getDateFromString(cursor.getString(5)));

        return account;
    }

    public static RssFeed rssFeedFromCursor(Cursor cursor) {
        RssFeed rssFeed = new RssFeed();
        rssFeed.setId(cursor.getLong(0));
        rssFeed.setTitle(cursor.getString(1));
        rssFeed.setUrl(cursor.getString(2));
        rssFeed.setLastSyncedOn(getDateFromString(cursor.getString(3)));

        return rssFeed;
    }

    public static List<OauthData> getOauthDataList(String accountType, SQLiteDatabase db) {
        List<OauthData> results = new ArrayList<OauthData>();
        String sql = SQL_GET_OAUTH_DATA_LIST_FOR_TYPE;
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(sql, new String[] { accountType });
            while (cursor.moveToNext()) {
                long localAccountId = cursor.getLong(0);
                String socialAccountId = cursor.getString(1);
                String oauthDataString = cursor.getString(2);
                Map<String, String> params = Helper.splitQuery(oauthDataString);
                if (!params.containsKey(Helper.OAUTH_TOKEN_KEY) || Helper.isEmpty(params.get(Helper.OAUTH_TOKEN_KEY))) {
                    continue;
                }

                OauthData oauthData = new OauthData(localAccountId, socialAccountId,
                    params.get(Helper.OAUTH_TOKEN_KEY),
                    params.containsKey(Helper.OAUTH_TOKEN_SECRET_KEY) ? params.get(Helper.OAUTH_TOKEN_SECRET_KEY) : null);
                results.add(oauthData);
            }
        } finally {
            Helper.closeCloseable(cursor);
        }

        return results;
    }

    private static final String SQL_DELETE_OWNED_FTS_INDEXED_ITEMS =
        "DELETE FROM ItemSearchIndex WHERE Id IN (SELECT Id FROM IndexedItems WHERE FeedItemId IN (SELECT Id FROM FeedItems WHERE OwnerId = ? AND Type = ?))";

    private static final String SQL_DELETE_OWNED_INDEXED_ITEMS =
        "DELETE FROM IndexedItems WHERE FeedItemId IN (SELECT Id FROM FeedItems WHERE OwnerId = ? AND Type = ?)";

    private static final String SQL_DELETE_FEED_ITEMS_FOR_OBJECT =
        "DELETE FROM FeedItems WHERE OwnerId = ? AND Type = ?";

    private static final String SQL_DELETE_SOCIAL_MEDIA_ACCOUNT = "DELETE FROM SocialMediaAccounts WHERE Id = ?";

    private static final String SQL_DELETE_EMAIL_ACCOUNT = "DELETE FROM EmailAccounts WHERE Id = ?";

    private static final String SQL_DELETE_RSS_FEED = "DELETE FROM RssFeeds WHERE Id = ?";

    public static void delete(SocialMediaAccount account, SQLiteDatabase db) {
        long ownerId = account.getId();
        int itemType = 0;
        String accountType = account.getSocialAccountType();
        if (SocialMediaAccount.TYPE_FACEBOOK.equals(accountType)) {
            itemType = BaseItem.ITEM_TYPE_FACEBOOK_POST;
        } else if (SocialMediaAccount.TYPE_INSTAGRAM.equals(accountType)) {
            itemType = BaseItem.ITEM_TYPE_INSTAGRAM_POST;
        } else if (SocialMediaAccount.TYPE_TWITTER.equals(accountType)) {
            itemType = BaseItem.ITEM_TYPE_TWITTER_POST;
        }

        db.beginTransaction();
        try {
            String[] commonParams = { String.valueOf(ownerId), String.valueOf(itemType) };
            db.execSQL(SQL_DELETE_OWNED_FTS_INDEXED_ITEMS, commonParams);
            db.execSQL(SQL_DELETE_OWNED_INDEXED_ITEMS, commonParams);
            db.execSQL(SQL_DELETE_FEED_ITEMS_FOR_OBJECT, commonParams);
            db.execSQL(SQL_DELETE_SOCIAL_MEDIA_ACCOUNT, new String[]{String.valueOf(account.getId())});
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    public static void delete(EmailAccount account, SQLiteDatabase db) {
        long ownerId = account.getId();
        int itemType = BaseItem.ITEM_TYPE_EMAIL_MESSAGE;

        db.beginTransaction();
        try {
            String[] commonParams = { String.valueOf(ownerId), String.valueOf(itemType) };
            db.execSQL(SQL_DELETE_OWNED_FTS_INDEXED_ITEMS, commonParams);
            db.execSQL(SQL_DELETE_OWNED_INDEXED_ITEMS, commonParams);
            db.execSQL(SQL_DELETE_FEED_ITEMS_FOR_OBJECT, commonParams);
            db.execSQL(SQL_DELETE_EMAIL_ACCOUNT, new String[]{String.valueOf(account.getId())});
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    public static void delete(RssFeed account, SQLiteDatabase db) {
        long ownerId = account.getId();
        int itemType = BaseItem.ITEM_TYPE_RSS_FEED_ITEM;

        db.beginTransaction();
        try {
            String[] commonParams = { String.valueOf(ownerId), String.valueOf(itemType) };
            db.execSQL(SQL_DELETE_OWNED_FTS_INDEXED_ITEMS, commonParams);
            db.execSQL(SQL_DELETE_OWNED_INDEXED_ITEMS, commonParams);
            db.execSQL(SQL_DELETE_FEED_ITEMS_FOR_OBJECT, commonParams);
            db.execSQL(SQL_DELETE_RSS_FEED, new String[]{String.valueOf(account.getId())});
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    public static String getDateString(Date dt) {
        if (dt != null) {
            return Helper.GLOBAL_DATE_FORMAT.format(dt);
        }
        return null;
    }

    public static Date getDateFromString(String str) {
        if (Helper.isEmpty(str)) {
            return null;
        }

        try {
            return Helper.GLOBAL_DATE_FORMAT.parse(str);
        } catch (ParseException e) {
            return new Date(0);
        }
    }
}
