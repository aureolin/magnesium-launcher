package co.aureolin.labs.magnesium.util;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by akinwale on 17/02/2016.
 */
public interface NativeAdControlInterface {
    void loadNativeAd(int position, ImageView iconView, TextView titleView, TextView summaryView, View parentView);

    boolean isAdLoaded();
}
