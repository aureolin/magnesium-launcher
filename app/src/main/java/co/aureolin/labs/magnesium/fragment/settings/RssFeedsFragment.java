package co.aureolin.labs.magnesium.fragment.settings;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.view.ActionMode;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import co.aureolin.labs.magnesium.R;
import co.aureolin.labs.magnesium.SettingsActivity;
import co.aureolin.labs.magnesium.adapter.RssFeedAdapter;
import co.aureolin.labs.magnesium.dialog.RssFeedDialog;
import co.aureolin.labs.magnesium.model.RssFeed;
import co.aureolin.labs.magnesium.util.Helper;
import co.aureolin.labs.magnesium.util.RssFeedSavedHandler;
import co.aureolin.labs.magnesium.util.db.MagnesiumDbContext;

/**
 * Created by akinwale on 16/02/2016.
 */
public class RssFeedsFragment extends Fragment implements ActionMode.Callback {

    private android.support.v7.view.ActionMode actionMode;

    private boolean deleteInProgress;

    private int numRssFeeds;

    private List<String> rssFeedUrls;

    private ListView rssFeedsList;

    private RssFeedAdapter adapter;

    private FloatingActionButton addButton;

    private View noRssFeeds;

    public static RssFeedsFragment newInstance() {
        return new RssFeedsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_rss_feeds, container, false);

        rssFeedsList = (ListView) rootView.findViewById(R.id.rss_feeds_list);
        noRssFeeds = rootView.findViewById(R.id.no_rss_feeds_view);

        rssFeedsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
            if (actionMode != null) {
                if (rssFeedsList.getCheckedItemCount() == 0) {
                    actionMode.finish();
                } else {
                    actionMode.setTitle(String.valueOf(rssFeedsList.getCheckedItemCount()));
                }
            } else {
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        rssFeedsList.setItemChecked(position, false);
                    }
                });
            }
            }
        });
        rssFeedsList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, final View view, final int position, long id) {
            SettingsActivity activity = (SettingsActivity) getContext();
            if (activity != null) {
                if (actionMode != null) {
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            rssFeedsList.setItemChecked(position, !rssFeedsList.isItemChecked(position));
                            if (rssFeedsList.getCheckedItemCount() == 0) {
                                actionMode.finish();
                            } else {
                                actionMode.setTitle(String.valueOf(rssFeedsList.getCheckedItemCount()));
                            }
                        }
                    });
                } else {
                    //firstSelectedItemPosition = position;
                    actionMode = activity.startSupportActionMode(RssFeedsFragment.this);
                    rssFeedsList.setItemChecked(position, true);
                    actionMode.setTitle(String.valueOf(rssFeedsList.getCheckedItemCount()));
                }
            }
            return true;
            }
        });

        addButton = (FloatingActionButton) rootView.findViewById(R.id.rss_feed_add_button);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            RssFeedDialog dialog = new RssFeedDialog();
            dialog.setNumRssFeeds(numRssFeeds);
            dialog.setExistingRssFeeds(rssFeedUrls);
            dialog.setRssFeedSavedHandler(new RssFeedSavedHandler() {
                @Override
                public void onRssFeedSaved(RssFeed rssFeed) {
                    loadRssFeeds();
                }
            });
            dialog.show(getFragmentManager(), "RssFeedDialog");

            }
        });

        return rootView;
    }

    @Override
    public boolean onCreateActionMode(android.support.v7.view.ActionMode mode, Menu menu) {
        MenuInflater inflater = mode.getMenuInflater();
        inflater.inflate(R.menu.menu_generic_list_select_mode, menu);

        MenuItem editMenuItem = menu.findItem(R.id.action_item_edit);
        editMenuItem.setVisible(false);

        return true;
    }

    @Override
    public boolean onPrepareActionMode(android.support.v7.view.ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public void onDestroyActionMode(android.support.v7.view.ActionMode mode) {
        if (rssFeedsList != null) {
            for (int i = 0; i < adapter.getCount(); i++) {
                rssFeedsList.setItemChecked(i, false);
            }
        }
        actionMode = null;
    }

    @Override
    public boolean onActionItemClicked(android.support.v7.view.ActionMode mode, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_item_delete:
                if (!deleteInProgress) {
                    final List<RssFeed> feedsToDelete = new ArrayList<RssFeed>();
                    if (adapter != null && rssFeedsList != null) {
                        List<Integer> positions = new ArrayList<Integer>();
                        SparseBooleanArray checkedPositions = rssFeedsList.getCheckedItemPositions();
                        for (int i = 0; i < adapter.getCount(); i++) {
                            RssFeed rssFeed = adapter.getItem(i);
                            if (checkedPositions.get(i)) {
                                feedsToDelete.add(rssFeed);
                            }
                        }
                    }

                    (new AsyncTask<Void, Void, Boolean>() {
                        protected void onPreExecute() {
                            deleteInProgress = true;
                        }
                        protected Boolean doInBackground(Void... params) {
                            SettingsActivity activity = (SettingsActivity) getContext();
                            if (activity != null) {
                                SQLiteDatabase db = activity.getDbContext().getWritableDatabase();
                                try {
                                    List<String> urlsToDelete = new ArrayList<String>();
                                    for (int i = 0; i < feedsToDelete.size(); i++) {
                                        MagnesiumDbContext.delete(feedsToDelete.get(i), db);
                                        urlsToDelete.add(feedsToDelete.get(i).getUrl().toLowerCase());
                                    }

                                    for (int i = 0; i < urlsToDelete.size(); i++) {
                                        rssFeedUrls.remove(urlsToDelete.get(i));
                                    }

                                    return true;
                                } catch (Exception ex) {
                                    return false;
                                }
                            }

                            return false;
                        }
                        protected void onPostExecute(Boolean result) {
                            deleteInProgress = false;
                            Context context = getContext();
                            if (!result) {
                                Helper.showToast("The selection could not be deleted.", context);
                                return;
                            }
                            loadRssFeeds();
                            Helper.showToast("The selection was successfully deleted.", context);
                            if (actionMode != null) {
                                actionMode.finish();
                            }
                        }
                    }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
                return true;
        }
        return false;
    };

    @Override
    public void onStart() {
        super.onStart();
        setupAdapter();
        loadRssFeeds();
    }

    private void loadRssFeeds() {
        (new AsyncTask<Void, Void, List<RssFeed>>() {
            protected List<RssFeed> doInBackground(Void... params) {
                SettingsActivity activity = (SettingsActivity) getContext();
                if (activity != null) {
                    SQLiteDatabase db = activity.getDbContext().getReadableDatabase();
                    return MagnesiumDbContext.getRssFeeds(db);
                }

                return new ArrayList<RssFeed>();
            }

            @Override
            protected void onPostExecute(List<RssFeed> feeds) {
                setupAdapter(feeds, false);
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void setupAdapter() {
        setupAdapter(null, true);
    }

    private void setupAdapter(List<RssFeed> feeds, boolean initialSetup) {
        boolean isNewAdapter = false;
        if (adapter == null) {
            adapter = new RssFeedAdapter(getContext());
            rssFeedsList.setAdapter(adapter);
            isNewAdapter = true;
        }
        if (!isNewAdapter) {
            adapter.clear();
        }

        if (feeds != null) {
            if (rssFeedUrls == null) {
                rssFeedUrls = new ArrayList<String>();
            }

            numRssFeeds = 0;
            for (int i = 0; i < feeds.size(); i++) {
                rssFeedUrls.add(feeds.get(i).getUrl().toLowerCase());
                numRssFeeds++;
            }
            adapter.addAll(feeds);
        }

        if (!initialSetup) {
            boolean hasAccounts = (adapter != null && adapter.getCount() > 0);
            noRssFeeds.setVisibility(hasAccounts ? View.INVISIBLE : View.VISIBLE);
        }

        if (!isNewAdapter) {
            adapter.notifyDataSetChanged();
        }
    }
}
