package co.aureolin.labs.magnesium.util;

import android.os.Bundle;

/**
 * Created by akinwale on 18/02/2016.
 */
public interface IabPurchaseStartedCallback {
    public void onIabPurchaseStarted(Bundle result);
}
