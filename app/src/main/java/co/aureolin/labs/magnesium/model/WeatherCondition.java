package co.aureolin.labs.magnesium.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;

import java.util.Date;

import co.aureolin.labs.magnesium.util.Helper;

/**
 * Created by akinwale on 10/02/2016.
 */
public class WeatherCondition {

    private String fullLocationName;

    private String temperature;

    private String conditionText;

    private Date lastUpdated;

    private String woeId;

    public WeatherCondition() {

    }

    public String getFullLocationName() {
        return fullLocationName;
    }

    public void setFullLocationName(String fullLocationName) {
        this.fullLocationName = fullLocationName;
    }

    public String getConditionText() {
        return conditionText;
    }

    public void setConditionText(String conditionText) {
        this.conditionText = conditionText;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    @JsonIgnore
    public String getCelsiusTemperature() {
        if (!Helper.isEmpty(temperature)) {
            return String.valueOf(Helper.fahrenheitToCelsius(Helper.parseInt(temperature)));
        }
        return "";
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getWoeId() {
        return woeId;
    }

    public void setWoeId(String woeId) {
        this.woeId = woeId;
    }

    public static WeatherCondition fromYqlJsonNode(JsonNode node, String fullLocationName, String woeId) {
        WeatherCondition condition = new WeatherCondition();
        condition.setTemperature(Helper.getNodeValue("temp", node));
        condition.setConditionText(Helper.getNodeValue("text", node));
        condition.setFullLocationName(fullLocationName);
        condition.setWoeId(woeId);
        condition.setLastUpdated(new Date());

        return condition;
    }
}
