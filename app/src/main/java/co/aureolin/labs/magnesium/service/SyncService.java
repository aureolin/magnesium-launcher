package co.aureolin.labs.magnesium.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.IBinder;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rometools.rome.feed.module.DCModule;
import com.rometools.rome.feed.module.Module;
import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.feed.synd.SyndPerson;
import com.rometools.rome.io.FeedException;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;
import com.rometools.rome.io.impl.DCModuleParser;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.UIDFolder;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import co.aureolin.labs.magnesium.HomeActivity;
import co.aureolin.labs.magnesium.fragment.settings.GeneralSettingsFragment;
import co.aureolin.labs.magnesium.model.Attachment;
import co.aureolin.labs.magnesium.model.BaseItem;
import co.aureolin.labs.magnesium.model.EmailAccount;
import co.aureolin.labs.magnesium.model.FeedItem;
import co.aureolin.labs.magnesium.model.RssFeed;
import co.aureolin.labs.magnesium.util.Helper;
import co.aureolin.labs.magnesium.util.Http;
import co.aureolin.labs.magnesium.util.db.MagnesiumDbContext;

/**
 * Created by akinwale on 12/02/2016.
 */
public class SyncService extends Service {
    public static final String ACTION_EMAIL_ACCOUNT_SYNCHED = "co.aureolin.labs.magnesium.intent.action.EMAIL_ACCOUNT_SYNCHED";

    public static final String ACTION_FEED_SYNCHED = "co.aureolin.labs.magnesium.intent.action.FEED_SYNCHED";

    private static final String TAG = SyncService.class.getCanonicalName();

    // Sync email 15 minutes?
    private static final int EMAIL_SYNC_INTERVAL = 60000 * 15; // 1 minute * 15

    private static final int FEED_SYNC_INTERVAL = 60000 * 30; // 1 minute * 30

    private static final int EMAIL_CONTENT_SIZE_LIMIT = 128 * 1024;

    private Timer emailSyncTimer;

    private TimerTask emailSyncTask;

    private Timer feedSyncTimer;

    private TimerTask feedSyncTask;

    private MagnesiumDbContext dbContext;

    private Context context;

    private SharedPreferences preferences;

    private String encryptionKey;

    public SyncService() {

    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        context = getApplicationContext();
        preferences = getSharedPreferences(HomeActivity.COMMON_SHARED_PREF_NAME, Context.MODE_PRIVATE);
        initialiseEncryptionKey();

        feedSyncTask = new TimerTask() {
            @Override
            public void run() {
                syncRssFeeds();
            }
        };
        feedSyncTimer = new Timer();
        feedSyncTimer.scheduleAtFixedRate(feedSyncTask, 0, FEED_SYNC_INTERVAL);

        emailSyncTask = new TimerTask() {
            @Override
            public void run() {
                syncEmail();
            }
        };
        emailSyncTimer = new Timer();
        emailSyncTimer.scheduleAtFixedRate(emailSyncTask, 0, EMAIL_SYNC_INTERVAL);

        return START_STICKY;
    }

    private void syncEmail() {
        SQLiteDatabase db = null;
        try {
            dbContext = new MagnesiumDbContext(context);
            db = dbContext.getWritableDatabase();

            Properties properties = System.getProperties();
            List<EmailAccount> accounts = dbContext.getEmailAccounts(encryptionKey, db);
            for (int i = 0; i < accounts.size(); i++) {
                EmailAccount account = accounts.get(i);

                if (account.getConnectionType() == EmailAccount.CONNECTION_TYPE_OAUTH) {
                    ObjectMapper objectMapper = new ObjectMapper();
                    String refreshToken = account.getUsername();
                    // Get a new access token using the refresh token
                    String postData = String.format("refresh_token=%s&client_id=%s&client_secret=%s&grant_type=refresh_token",
                            Helper.getUrlEncodedString(refreshToken),
                            Helper.getUrlEncodedString(Helper.GOOGLE_OAUTH_CLIENT_ID),
                            Helper.getUrlEncodedString(Helper.GOOGLE_OAUTH_CLIENT_SECRET));
                    String json = Http.post(Helper.GOOGLE_OAUTH_TOKEN_URL, postData);
                    JsonNode tokenNode = objectMapper.readTree(json);
                    String accessToken = Helper.getNodeValue("access_token", tokenNode);
                    if (Helper.isEmpty(accessToken)) {
                        continue;
                    }

                    Map<String, String> headers = new HashMap<String, String>();
                    headers.put("Authorization", String.format("Bearer %s", accessToken));

                    String messageListJson = Http.get(Helper.GMAIL_MESSAGE_LIST_URL, headers);
                    JsonNode listNode = objectMapper.readTree(messageListJson);

                    if (!listNode.has("messages")) {
                        continue;
                    }

                    JsonNode messagesNode = listNode.get("messages");
                    if (messagesNode != null && messagesNode.isArray()) {
                        for (int j = 0; j < messagesNode.size(); j++) {
                            JsonNode messageNode = messagesNode.get(j);
                            String messageId = Helper.getNodeValue("id", messageNode);
                            String threadId = Helper.getNodeValue("threadId", messageNode);

                            // Get the message contents
                            String messageUrl = String.format(Helper.GMAIL_MESSAGE_URL_FORMAT, messageId);
                            String messageJson = Http.get(messageUrl, headers);
                            JsonNode messageResultNode = objectMapper.readTree(messageJson);

                            FeedItem feedItem = feedItemFromGmailMessage(messageResultNode);
                            if (feedItem == null) {
                                continue;
                            }
                            feedItem.setOwnerId(account.getId());
                            feedItem.setExternalRefId(messageId);
                            feedItem.setThreadId(threadId);


                            if (!MagnesiumDbContext.externalRefIdForOwnerExists(feedItem, db)) {
                                MagnesiumDbContext.insertFeedItemWithAttachments(feedItem, db);
                                // TODO: Insert attachments for multipart
                            }
                        }
                    }
                } else {
                    String hostName = account.getHostName();
                    boolean secure = (account.getConnectionType() == EmailAccount.CONNECTION_TYPE_SECURE_TLS);
                    String protocol = secure ? "imaps" : "imap";

                    Session mailSession = Session.getInstance(properties, null);
                    Store store = null;
                    Folder mailFolder = null;
                    try {
                        store = mailSession.getStore(protocol);
                        store.connect(hostName, account.getPort(), account.getUsername(), account.getPassword());

                        mailFolder = store.getFolder("INBOX");
                        mailFolder.open(Folder.READ_ONLY);

                        int totalMessageCount = mailFolder.getMessageCount();
                        int start = totalMessageCount - 50;
                        Message[] messages = mailFolder.getMessages(start, totalMessageCount);
                        for (int j = 0; j < messages.length; j++) {
                            Message message = messages[j];

                            Flags.Flag[] systemFlags = message.getFlags().getSystemFlags();
                            boolean seen = false;
                            for (int k = 0; k < systemFlags.length; k++) {
                                if (systemFlags[k] == Flags.Flag.SEEN) {
                                    seen = true;
                                    break;
                                }
                            }
                            if (!seen) {
                                FeedItem feedItem = feedItemFromMailMessage(message, mailFolder, account);
                                if (!MagnesiumDbContext.externalRefIdForOwnerExists(feedItem, db)) {
                                    MagnesiumDbContext.insertFeedItemWithAttachments(feedItem, db);
                                    // TODO: Insert attachments for multipart
                                }
                            }
                        }

                        mailFolder.close(false);

                        account.setLastSyncedOn(new Date()); // TODO: Update last synced on
                        //MagnesiumDbContext.update(account, db);

                        Helper.sendBroadcast(ACTION_EMAIL_ACCOUNT_SYNCHED, this);
                    } catch (MessagingException ex) {
                        Log.e(TAG, "Unable to sync email account: " + account.getEmailAddress(), ex);
                    } finally {
                        if (mailFolder != null && mailFolder.isOpen()) {
                            mailFolder.close(false);
                        }
                        if (store != null) {
                            store.close();
                        }
                    }
                }
            }
        } catch (Exception ex) {
            Log.e(TAG, "Unable to sync email.", ex);
        } finally {
            Helper.closeDb(db);
        }
    }

    private void syncRssFeeds() {
        SQLiteDatabase db = null;
        try {
            dbContext = new MagnesiumDbContext(context);
            db = dbContext.getWritableDatabase();

            List<RssFeed> feeds = dbContext.getRssFeeds(db);
            for (int i = 0; i < feeds.size(); i++) {
                RssFeed feed = feeds.get(i);
                try {

                    String feedXml = Http.get(feed.getUrl());
                    if (!Helper.isEmpty(feedXml)) {
                        if (feedXml.indexOf("dc:creator") > -1) {
                            feedXml = feedXml.replaceAll("<dc:creator>", "<author>");
                            feedXml = feedXml.replaceAll("</dc:creator>", "</author>");
                        }
                    }

                    //URL url = new URL(feed.getUrl());
                    SyndFeedInput feedInput = new SyndFeedInput();
                    SyndFeed syndFeed = feedInput.build(new XmlReader(new ByteArrayInputStream(feedXml.getBytes("UTF-8"))));

                    DCModule dcModule = null;
                    Module module = syndFeed.getModule(DCModule.URI);
                    if (module instanceof DCModule) {
                        dcModule = (DCModule) module;
                    }

                    List entries = syndFeed.getEntries();
                    Iterator it = entries.iterator();
                    while (it.hasNext()) {
                        SyndEntry entry = (SyndEntry) it.next();
                        FeedItem feedItem = feedItemFromSyndEntry(entry, dcModule);
                        feedItem.setSource(syndFeed.getTitle());
                        feedItem.setOwnerId(feed.getId());

                        if (!MagnesiumDbContext.externalRefIdExists(feedItem, db)) {
                            MagnesiumDbContext.insert(feedItem, db);
                        }
                    }

                    Helper.sendBroadcast(ACTION_FEED_SYNCHED, this);
                } catch (FeedException | IOException ex) {
                    Log.e(TAG, "Unable to sync feed: " + feed.getUrl(), ex);
                } catch (Exception ex) {
                    Log.e(TAG, "Unknown exception occurred synching feed: " + feed.getUrl(), ex);
                }
            }
        } catch (Exception ex) {
            Log.e(TAG, "Unable to sync RSS / Atom feeds.", ex);
        } finally {
            Helper.closeDb(db);
        }
    }

    private static FeedItem feedItemFromSyndEntry(SyndEntry entry, DCModule dcModule) {
        FeedItem feedItem = new FeedItem();
        feedItem.setType(BaseItem.ITEM_TYPE_RSS_FEED_ITEM);
        feedItem.setExternalRefId(entry.getLink());
        feedItem.setTitle(entry.getTitle());

        if (!Helper.isEmpty(entry.getAuthor())) {
            feedItem.setAuthor(entry.getAuthor());
        } else  {
            if (dcModule != null) {
                feedItem.setAuthor(dcModule.getCreator());
            }
        }

        feedItem.setContent(entry.getDescription().getValue());
        feedItem.setEntryTime(entry.getPublishedDate());
        feedItem.setUrl(entry.getLink());

        return feedItem;
    }

    private static FeedItem feedItemFromGmailMessage(JsonNode jsonNode) {
        if (!jsonNode.has("payload")) {
            return null;
        }

        FeedItem feedItem = new FeedItem();
        feedItem.setType(BaseItem.ITEM_TYPE_EMAIL_MESSAGE);

        JsonNode payloadNode = jsonNode.get("payload");
        if (!payloadNode.has("headers") || !payloadNode.has("parts")) {
            return null;
        }

        JsonNode headersNode = payloadNode.get("headers");
        if (headersNode != null && headersNode.isArray()) {
            for (int i = 0; i < headersNode.size(); i++) {
                JsonNode node = headersNode.get(i);
                String name = Helper.getNodeValue("name", node);
                String value = Helper.getNodeValue("value", node);
                if (Helper.isEmpty(name)) {
                    continue;
                }
                if ("from".equalsIgnoreCase(name)) {
                    String fullFrom = value;
                    try {
                        InternetAddress[] addresses = InternetAddress.parse(fullFrom);
                        if (addresses.length > 0) {
                            InternetAddress fromAddress = addresses[0];
                            feedItem.setAuthor(fromAddress.getPersonal());
                            if (Helper.isEmpty(feedItem.getAuthor())) {
                                feedItem.setAuthor(fromAddress.getAddress());
                            }
                            feedItem.setPosterId(fromAddress.getAddress());
                        }
                    } catch (AddressException ex) {
                        feedItem.setAuthor(fullFrom);
                        feedItem.setPosterId(fullFrom);
                    }
                } else if ("to".equalsIgnoreCase(name)) {
                    feedItem.setRecipients(value);
                } else if ("cc".equalsIgnoreCase(name)) {
                    feedItem.setCcRecipients(value);
                } else if ("bcc".equalsIgnoreCase(name)) {
                    feedItem.setBccRecipients(value);
                } else if ("reply-to".equalsIgnoreCase(name)) {
                    feedItem.setReplyTo(value);
                } else if ("subject".equalsIgnoreCase(name)) {
                    feedItem.setTitle(value);
                } else if ("date".equalsIgnoreCase(name)) {
                    try {
                        Date dt = Helper.GMAIL_DATE_FORMAT.parse(value);
                        feedItem.setEntryTime(dt);
                    } catch (ParseException ex) {
                        try {
                            Date dt = Helper.GMAIL_DATE_FORMAT_2.parse(value); // Try the alternate date format
                            feedItem.setEntryTime(dt);
                        } catch (ParseException e) {
                            try {
                                Date dt = Helper.GMAIL_DATE_FORMAT_3.parse(value); // Try the 3rd alternate date format (wtf?)
                                feedItem.setEntryTime(dt);
                            } catch (ParseException iex) {
                                // Cannot parse the email date
                                Log.e(TAG, "Could not parse Gmail date format.", iex);
                            }
                        }
                    }
                }
            }
        }

        String plainText = null;
        String htmlMessage = null;
        List<Attachment> attachments = null;
        JsonNode partsNode = payloadNode.get("parts");
        if (partsNode != null && partsNode.isArray()) {
            for (int i = 0; i < partsNode.size(); i++) {
                JsonNode partNode = partsNode.get(i);
                if (!partNode.has("body")) {
                    continue;
                }

                JsonNode bodyNode = partNode.get("body");
                String filename = Helper.getNodeValue("filename", partNode);
                String mimeType = Helper.getNodeValue("mimeType", partNode);
                if (Helper.isEmpty(filename) && !bodyNode.has("data")) {
                    continue;
                }

                if (Helper.isEmpty(filename)) {
                    // message body content
                    String base64Data = Helper.getNodeValue("data", bodyNode);
                    if ("text/plain".equals(mimeType)) {
                        plainText = Helper.base64Decode(base64Data);
                    } else if ("text/html".equals(mimeType)) {
                        htmlMessage = Helper.base64Decode(base64Data);
                    }
                } else {
                    // attachment
                    if (attachments == null) {
                        attachments = new ArrayList<Attachment>();
                    }

                    String externalId = Helper.getNodeValue("attachmentId", bodyNode);
                    long size = Helper.getLongNodeValue("size", bodyNode);
                    Attachment attachment = new Attachment();
                    attachment.setFilename(filename);
                    attachment.setContentType(mimeType);
                    attachment.setExternalId(externalId);
                    attachment.setContentLength(size);
                    attachments.add(attachment);
                }
            }
        }

        if (htmlMessage == null && plainText == null) {
            return null;
        }

        if (attachments != null) {
            feedItem.setAttachments(attachments);
        }

        if (htmlMessage != null) {
            feedItem.setContent(htmlMessage);
        } else if (plainText != null) {
            feedItem.setContent(plainText);
        }

        return feedItem;
    }

    private static FeedItem feedItemFromMailMessage(Message message, Folder folder, EmailAccount account) {
        FeedItem feedItem = new FeedItem();
        try {
            feedItem.setType(BaseItem.ITEM_TYPE_EMAIL_MESSAGE);
            feedItem.setTitle(message.getSubject());
            feedItem.setEntryTime(message.getReceivedDate());
            feedItem.setOwnerId(account.getId());

            UIDFolder uidFolder = (UIDFolder) folder;
            feedItem.setExternalRefId(String.valueOf(uidFolder.getUID(message)));

            Address[] fromAddresses = message.getFrom();
            Address[] toAddresses = message.getRecipients(Message.RecipientType.TO);
            Address[] ccAddresses = message.getRecipients(Message.RecipientType.CC);
            Address[] bccAddresses = message.getRecipients(Message.RecipientType.BCC);
            Address[] replyToAddresses = message.getReplyTo();

            parseAddressesIntoFeedItem(ADDRESS_TYPE_TO, toAddresses, feedItem);
            parseAddressesIntoFeedItem(ADDRESS_TYPE_CC, ccAddresses, feedItem);
            parseAddressesIntoFeedItem(ADDRESS_TYPE_BCC, bccAddresses, feedItem);
            if (fromAddresses != null && fromAddresses.length > 0) {
                InternetAddress fromAddress = (InternetAddress) fromAddresses[0];
                feedItem.setAuthor(fromAddress.getPersonal());
                if (Helper.isEmpty(feedItem.getAuthor())) {
                    feedItem.setAuthor(fromAddress.getAddress());
                }
                feedItem.setPosterId(fromAddress.getAddress());
            }
            if (replyToAddresses != null && replyToAddresses.length > 0) {
                InternetAddress replyToAddress = (InternetAddress) replyToAddresses[0];
                feedItem.setReplyTo(replyToAddress.getAddress());
            }

            List<Attachment> attachments = null;
            Object content = message.getContent();
            if (content instanceof String) {
                String emailMessage = content.toString();
                feedItem.setContent(emailMessage);
            } else if (content instanceof Multipart) {
                Multipart mpContent = (Multipart) content;
                for (int i = 0; i < mpContent.getCount(); i++) {
                    BodyPart part = mpContent.getBodyPart(i);
                    String disposition = part.getDisposition();
                    if (Part.ATTACHMENT.equalsIgnoreCase(disposition)) {
                        // Handle attachment
                        String filename = part.getFileName();
                        String mimeType = part.getContentType();
                        long size = part.getSize();

                        if (attachments == null) {
                            attachments = new ArrayList<Attachment>();
                        }
                        Attachment attachment = new Attachment();
                        attachment.setFilename(filename);
                        attachment.setContentType(mimeType);
                        attachment.setContentLength(size);
                        attachment.setExternalId(String.valueOf(i)); // Use part index as the unique identifier
                        attachments.add(attachment);

                    } else if (part.isMimeType("text/plain") || part.isMimeType("text/html")) {
                        String contentString = (String) part.getContent();
                        feedItem.setContent(contentString);
                    }
                }
            } else if (content instanceof Message) {
                // handle message
            }

            if (attachments != null) {
                feedItem.setAttachments(attachments);
            }
        } catch (MessagingException ex) {
            return null;
        } catch (IOException ex) {
            return null;
        }

        return feedItem;
    }

    private static final int ADDRESS_TYPE_FROM = 1;

    private static final int ADDRESS_TYPE_TO = 2;

    private static final int ADDRESS_TYPE_CC = 3;

    private static final int ADDRESS_TYPE_BCC = 4;

    private static void parseAddressesIntoFeedItem(int addressType, Address[] addresses, FeedItem feedItem) {
        StringBuilder sb = new StringBuilder();
        if (addresses != null) {
            String delim = "";
            for (int i = 0; i < addresses.length; i++) {
                InternetAddress address = (InternetAddress) addresses[i];
                sb.append(delim).append(address.toUnicodeString());
                delim = "; ";
            }

            String value = sb.toString();
            switch (addressType) {
                case ADDRESS_TYPE_TO:
                    feedItem.setRecipients(value);
                    break;
                case ADDRESS_TYPE_CC:
                    feedItem.setCcRecipients(value);
                    break;
                case ADDRESS_TYPE_BCC:
                    feedItem.setBccRecipients(value);
                    break;
            }
        }
    }

    private void initialiseEncryptionKey() {
        encryptionKey = preferences.getString(HomeActivity.SP_ENC_KEY, null);
        if (encryptionKey == null) {
            // Get a default encryption key
            TelephonyManager manager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            if (manager != null) {
                try {
                    encryptionKey = manager.getSubscriberId();
                    if (Helper.isEmpty(encryptionKey)) {
                        encryptionKey = manager.getLine1Number();
                    }
                    if (Helper.isEmpty(encryptionKey)) {
                        encryptionKey = manager.getDeviceId();
                    }
                } catch (SecurityException ex) {
                    // Ignore and try the rest of the options
                }
            }
            if (Helper.isEmpty(encryptionKey)) {
                encryptionKey = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
            }

            // Final fallback
            if (Helper.isEmpty(encryptionKey)) {
                encryptionKey = HomeActivity.SP_DEFAULT_KEY;
            }

            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(HomeActivity.SP_ENC_KEY, encryptionKey);
            editor.commit();
        }
    }
}
