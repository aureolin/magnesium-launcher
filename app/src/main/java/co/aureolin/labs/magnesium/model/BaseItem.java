package co.aureolin.labs.magnesium.model;

import java.util.Comparator;
import java.util.Date;

/**
 * Created by akinwale on 08/02/2016.
 */
public abstract class BaseItem implements Comparable<BaseItem> {
    /**** CONSTANTS ****/
    // General
    public static final int ITEM_TYPE_NOTIFICATION = 1; // Unnecessary???
    public static final int ITEM_TYPE_CONTACT = 2;
    public static final int ITEM_TYPE_CONTACT_NUMBER = 3;
    public static final int ITEM_TYPE_CONTACT_EMAIL = 4;
    public static final int ITEM_TYPE_CALL_LOG = 5;
    public static final int ITEM_TYPE_SMS_MESSAGE = 6;
    public static final int ITEM_TYPE_MMS_MESSAGE = 7;
    public static final int ITEM_TYPE_EMAIL_MESSAGE = 8;
    public static final int ITEM_TYPE_RSS_FEED_ITEM = 9;

    // Social media
    public static final int ITEM_TYPE_FACEBOOK_POST = 21;
    public static final int ITEM_TYPE_INSTAGRAM_POST = 22;
    public static final int ITEM_TYPE_TWITTER_POST = 23;

    // Other
    public static final int ITEM_TYPE_APP = 51;
    public static final int ITEM_TYPE_FILE = 61;
    public static final int ITEM_TYPE_WEB_SEARCH_RESULT = 98;
    public static final int ITEM_TYPE_AD = 99;

    // Phone Call Directions
    public static final int DIRECTION_INCOMING = 101;
    public static final int DIRECTION_OUTGOING = 102;
    /**** END CONSTANTS ****/

    private long id;

    private String url;

    private int type;

    private int direction;

    private int contentLength;

    private String title;

    // for RSS posts/social media posts/SMS/email?
    // Also sender
    private String author;

    private String replyTo;

    private String recipients; // for SMS/email?

    private String ccRecipients;

    private String bccRecipients;

    private Date createdOn; // Created date in local DB (for identifying new items)

    private Date modifiedOn;

    private Date lastSyncedOn;

    private boolean archived;

    private boolean placeholder;

    private boolean ad;

    public BaseItem() {

    }

    public String getRecipients() {
        return recipients;
    }

    public void setRecipients(String recipients) {
        this.recipients = recipients;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public String getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(String replyTo) {
        this.replyTo = replyTo;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getContentLength() {
        return contentLength;
    }

    public void setContentLength(int contentLength) {
        this.contentLength = contentLength;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public abstract String getReferenceId();

    public abstract Date getTimestamp();

    public abstract String getValue();

    public boolean isPlaceholder() {
        return placeholder;
    }

    public void setPlaceholder(boolean placeholder) {
        this.placeholder = placeholder;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public Date getLastSyncedOn() {
        return lastSyncedOn;
    }

    public void setLastSyncedOn(Date lastSyncedOn) {
        this.lastSyncedOn = lastSyncedOn;
    }

    public String getCcRecipients() {
        return ccRecipients;
    }

    public void setCcRecipients(String ccRecipients) {
        this.ccRecipients = ccRecipients;
    }

    public String getBccRecipients() {
        return bccRecipients;
    }

    public void setBccRecipients(String bccRecipients) {
        this.bccRecipients = bccRecipients;
    }

    public boolean isAd() {
        return ad;
    }

    public void setAd(boolean ad) {
        this.ad = ad;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public int compareTo(BaseItem item) {
        if (item.isPlaceholder()) {
            return -1;
        }
        if (this.isPlaceholder()) {
            return 1;
        }

        Date date1 = getTimestamp();
        Date date2 = item.getTimestamp();
        //boolean compareTitles = false;
        /*if (date1 == null && date2 == null) {
            String title1 = getTitle();
            String title2 = item.getTitle();
            compareTitles = (title1 != null && title2 != null);
        }*/

        /*if (compareTitles) {
            return getTitle().compareTo(item.getTitle());
        }*/

        if (date1 == null) {
            date1 = new Date(0);
        }
        if (date2 == null) {
            date2 = new Date(0);
        }

        return date1.compareTo(date2);
    }
}
