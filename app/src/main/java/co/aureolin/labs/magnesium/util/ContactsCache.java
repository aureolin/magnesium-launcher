package co.aureolin.labs.magnesium.util;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.aureolin.labs.magnesium.model.IndexedItem;

/**
 * Created by akinwale on 18/02/2016.
 */
public class ContactsCache {
    private static final String TAG = ContactsCache.class.getCanonicalName();

    private static final String[] countries = {
            "AD",
            "AE",
            "AF",
            "AL",
            "AM",
            "AN",
            "AO",
            "AQ",
            "AR",
            "AT",
            "AU",
            "AW",
            "AZ",
            "BA",
            "BD",
            "BE",
            "BF",
            "BG",
            "BH",
            "BI",
            "BJ",
            "BL",
            "BN",
            "BO",
            "BR",
            "BT",
            "BW",
            "BY",
            "BZ",
            "CA",
            "CC",
            "CD",
            "CF",
            "CG",
            "CH",
            "CI",
            "CK",
            "CL",
            "CM",
            "CN",
            "CO",
            "CR",
            "CU",
            "CV",
            "CX",
            "CY",
            "CZ",
            "DE",
            "DJ",
            "DK",
            "DZ",
            "EC",
            "EE",
            "EG",
            "ER",
            "ES",
            "ET",
            "FI",
            "FJ",
            "FK",
            "FM",
            "FO",
            "FR",
            "GA",
            "GB",
            "GE",
            "GH",
            "GI",
            "GL",
            "GM",
            "GN",
            "GQ",
            "GR",
            "GT",
            "GW",
            "GY",
            "HK",
            "HN",
            "HR",
            "HT",
            "HU",
            "ID",
            "IE",
            "IL",
            "IM",
            "IN",
            "IQ",
            "IR",
            "IT",
            "JO",
            "JP",
            "KE",
            "KG",
            "KH",
            "KI",
            "KM",
            "KP",
            "KR",
            "KW",
            "KZ",
            "LA",
            "LB",
            "LI",
            "LK",
            "LR",
            "LS",
            "LT",
            "LU",
            "LV",
            "LY",
            "MA",
            "MC",
            "MD",
            "ME",
            "MG",
            "MH",
            "MK",
            "ML",
            "MM",
            "MN",
            "MO",
            "MR",
            "MT",
            "MU",
            "MV",
            "MW",
            "MX",
            "MY",
            "MZ",
            "NA",
            "NC",
            "NE",
            "NG",
            "NI",
            "NL",
            "NO",
            "NP",
            "NR",
            "NU",
            "NZ",
            "OM",
            "PA",
            "PE",
            "PF",
            "PG",
            "PH",
            "PK",
            "PL",
            "PM",
            "PN",
            "PR",
            "PT",
            "PW",
            "PY",
            "QA",
            "RO",
            "RS",
            "RU",
            "RW",
            "SA",
            "SB",
            "SC",
            "SD",
            "SE",
            "SG",
            "SH",
            "SI",
            "SK",
            "SL",
            "SM",
            "SN",
            "SO",
            "SR",
            "ST",
            "SV",
            "SY",
            "SZ",
            "TD",
            "TG",
            "TH",
            "TJ",
            "TK",
            "TL",
            "TM",
            "TN",
            "TO",
            "TR",
            "TV",
            "TW",
            "TZ",
            "UA",
            "UG",
            "US",
            "UY",
            "UZ",
            "VA",
            "VE",
            "VN",
            "VU",
            "WF",
            "WS",
            "YE",
            "YT",
            "ZA",
            "ZM",
            "ZW"
    };

    private static final String[] codes = {
            "376",
            "971",
            "93",
            "355",
            "374",
            "599",
            "244",
            "672",
            "54",
            "43",
            "61",
            "297",
            "994",
            "387",
            "880",
            "32",
            "226",
            "359",
            "973",
            "257",
            "229",
            "590",
            "673",
            "591",
            "55",
            "975",
            "267",
            "375",
            "501",
            "1",
            "61",
            "243",
            "236",
            "242",
            "41",
            "225",
            "682",
            "56",
            "237",
            "86",
            "57",
            "506",
            "53",
            "238",
            "61",
            "357",
            "420",
            "49",
            "253",
            "45",
            "213",
            "593",
            "372",
            "20",
            "291",
            "34",
            "251",
            "358",
            "679",
            "500",
            "691",
            "298",
            "33",
            "241",
            "44",
            "995",
            "233",
            "350",
            "299",
            "220",
            "224",
            "240",
            "30",
            "502",
            "245",
            "592",
            "852",
            "504",
            "385",
            "509",
            "36",
            "62",
            "353",
            "972",
            "44",
            "91",
            "964",
            "98",
            "39",
            "962",
            "81",
            "254",
            "996",
            "855",
            "686",
            "269",
            "850",
            "82",
            "965",
            "7",
            "856",
            "961",
            "423",
            "94",
            "231",
            "266",
            "370",
            "352",
            "371",
            "218",
            "212",
            "377",
            "373",
            "382",
            "261",
            "692",
            "389",
            "223",
            "95",
            "976",
            "853",
            "222",
            "356",
            "230",
            "960",
            "265",
            "52",
            "60",
            "258",
            "264",
            "687",
            "227",
            "234",
            "505",
            "31",
            "47",
            "977",
            "674",
            "683",
            "64",
            "968",
            "507",
            "51",
            "689",
            "675",
            "63",
            "92",
            "48",
            "508",
            "870",
            "1",
            "351",
            "680",
            "595",
            "974",
            "40",
            "381",
            "7",
            "250",
            "966",
            "677",
            "248",
            "249",
            "46",
            "65",
            "290",
            "386",
            "421",
            "232",
            "378",
            "221",
            "252",
            "597",
            "239",
            "503",
            "963",
            "268",
            "235",
            "228",
            "66",
            "992",
            "690",
            "670",
            "993",
            "216",
            "676",
            "90",
            "688",
            "886",
            "255",
            "380",
            "256",
            "1",
            "598",
            "998",
            "39",
            "58",
            "84",
            "678",
            "681",
            "685",
            "967",
            "262",
            "27",
            "260",
            "263"
    };

    public static Map<String, String> ContactsMap = new HashMap<String, String>();

    public static Map<String, Boolean> ContactsNotFoundMap = new HashMap<String, Boolean>();

    public static String getCountryDialingCode(String countryCode) {
        for (int i = 0; i < countries.length; i++) {
            if (countryCode.equals(countries[i])) {
                return String.format("+%s", codes[i]);
            }
        }
        return "";
    }

    public static void preInitCacheMap(final String countryCode, final Context context) {
        (new AsyncTask<Void, Void, String>() {
            protected String doInBackground(Void... params) {
                if (context != null) {
                    ContentResolver cr = context.getContentResolver();
                    Cursor cursor = null;
                    Cursor phonesCursor = null;
                    try {
                        phonesCursor = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
                        while (phonesCursor.moveToNext()) {
                            String contactId = phonesCursor.getString(phonesCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
                            String phoneNumber = phonesCursor.getString(phonesCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            if (!Helper.isEmpty(phoneNumber) && phoneNumber.indexOf(" ") > -1) {
                                phoneNumber = phoneNumber.replaceAll(" ", "");
                            }

                            if (!Helper.isEmpty(contactId)) {
                                cursor = cr.query(ContactsContract.Contacts.CONTENT_URI,
                                        new String[] { ContactsContract.Contacts.DISPLAY_NAME },
                                        String.format("%s = ?", ContactsContract.Contacts._ID),
                                        new String[] { contactId }, null);
                                if (cursor.moveToNext()) {
                                    String displayName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                                    String alternateNumber = getAlternateNumber(phoneNumber, countryCode);
                                    ContactsMap.put(phoneNumber, displayName);
                                    if (phoneNumber.indexOf("-") > -1) {
                                        String alternateNumber2 = phoneNumber.replaceAll("-", "");
                                        ContactsMap.put(alternateNumber2, displayName);
                                    }
                                    if (!Helper.isEmpty(alternateNumber)) {
                                        ContactsMap.put(alternateNumber, displayName);
                                        if (alternateNumber.indexOf("-") > -1) {
                                            String alternateNumber2 = alternateNumber.replaceAll("-", "");
                                            ContactsMap.put(alternateNumber2, displayName);
                                        }
                                    }
                                }
                                Helper.closeCloseable(cursor);
                            }
                        }
                    } catch (Exception ex) {
                        Log.e(TAG, "Exception pre initialising contact cache", ex);
                    } finally {
                        Helper.closeCloseable(cursor);
                        Helper.closeCloseable(phonesCursor);
                    }
                }
                return null;
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private static String getAlternateNumber(String number, String countryCode) {
        String alternateNumber = null;
        String countryDialingCode = getCountryDialingCode(countryCode);
        if (number.startsWith(countryDialingCode)) {
            alternateNumber = String.format("0%s", number.substring(countryDialingCode.length()));
        } else if (number.startsWith("0")) {
            alternateNumber = String.format("%s%s", countryDialingCode, number.substring(1));
        }

        return alternateNumber;
    }

    public static void updateViewWithContactName(final String number, String countryCode, final TextView textView, final Context context, boolean forceUpdate) {
        if (Helper.isEmpty(number)) {
            return;
        }

        String alternateNumber = getAlternateNumber(number, countryCode);
        if (ContactsNotFoundMap.containsKey(number) && ContactsNotFoundMap.containsKey(alternateNumber)) {
            if (textView != null && forceUpdate) {
                textView.setText(number);
                textView.setVisibility(View.VISIBLE);
            }
            return;
        }

        if (ContactsMap.containsKey(number)) {
            if (textView != null) {
                textView.setText(ContactsMap.get(number));
                textView.setVisibility(View.VISIBLE);
            }
            return;
        }
        if (ContactsMap.containsKey(alternateNumber)) {
            if (textView != null) {
                textView.setText(ContactsMap.get(alternateNumber));
                textView.setVisibility(View.VISIBLE);
            }
            return;
        }

        final String altNumber = alternateNumber;
        (new AsyncTask<Void, Void, String>() {
            protected String doInBackground(Void... params) {
                if (context != null) {
                    ContentResolver cr = context.getContentResolver();
                    Cursor cursor = null;
                    Cursor phonesCursor = null;
                    try {
                        phonesCursor = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
                        while (phonesCursor.moveToNext()) {
                            String contactId = phonesCursor.getString(phonesCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
                            String phoneNumber = phonesCursor.getString(phonesCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            if (!Helper.isEmpty(phoneNumber) && phoneNumber.indexOf(" ") > -1) {
                                phoneNumber = phoneNumber.replaceAll(" ", "");
                            }

                            if (!Helper.isEmpty(contactId) && (number.equals(phoneNumber) || (altNumber != null && altNumber.equals(phoneNumber)))) {
                                cursor = cr.query(ContactsContract.Contacts.CONTENT_URI,
                                        new String[] { ContactsContract.Contacts.DISPLAY_NAME },
                                        String.format("%s = ?", ContactsContract.Contacts._ID),
                                        new String[] { contactId }, null);
                                if (cursor.moveToNext()) {
                                    String displayName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                                    return displayName;
                                }
                                Helper.closeCloseable(cursor);
                            }
                        }
                    } catch (Exception ex) {
                        Log.e(TAG, "Exception retrieving contact cache", ex);
                    } finally {
                        Helper.closeCloseable(cursor);
                        Helper.closeCloseable(phonesCursor);
                    }
                }
                return null;
            }
            protected void onPostExecute(String name) {
                if (Helper.isEmpty(name)) {
                    ContactsNotFoundMap.put(number, true);
                    if (!Helper.isEmpty(altNumber)) {
                        ContactsNotFoundMap.put(altNumber, true);
                    }
                    return;
                }

                ContactsMap.put(number, name);
                if (!Helper.isEmpty(altNumber)) {
                    ContactsMap.put(altNumber, name);
                    if (number.indexOf("-") > -1) {
                        String altNumber2 = number.replaceAll("-", "");
                        ContactsMap.put(altNumber2, name);
                    }
                    if (altNumber.indexOf("-") > -1) {
                        String altNumber2 = altNumber.replaceAll("-", "");
                        ContactsMap.put(altNumber2, name);
                    }
                }
                if (textView != null) {
                    textView.setText(name);
                    textView.setVisibility(View.VISIBLE);
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
}
