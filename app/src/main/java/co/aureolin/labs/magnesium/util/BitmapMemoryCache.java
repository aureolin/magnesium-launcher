package co.aureolin.labs.magnesium.util;

import android.graphics.Bitmap;
import android.util.LruCache;

/**
 * Created by Akinwale on 21/09/2015.
 */
public class BitmapMemoryCache {
    private static final int CACHE_SIZE = (int) (Runtime.getRuntime().maxMemory() / 1024) / 2;

    private static LruCache<String, Bitmap> memoryCache = new LruCache<String, Bitmap>(CACHE_SIZE) {
        @Override
        protected int sizeOf(String key, Bitmap bitmap) {
            return  bitmap.getByteCount() / 1024;
        }

    };


    public static void putBitmap(String key, Bitmap bitmap) {
        memoryCache.put(key, bitmap);
    }

    public static Bitmap getBitmap(String key) {
        return memoryCache.get(key);
    }
}
