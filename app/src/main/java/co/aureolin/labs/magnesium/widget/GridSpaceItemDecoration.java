package co.aureolin.labs.magnesium.widget;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by akinwale on 11/09/2016.
 */
public class GridSpaceItemDecoration extends RecyclerView.ItemDecoration {
    private int horizontalSpace;

    private int verticalSpace;

    private int columnCount;

    public GridSpaceItemDecoration(int horizontalSpace, int verticalSpace, int columnCount) {
        this.horizontalSpace = horizontalSpace;
        this.verticalSpace = verticalSpace;
        this.columnCount = columnCount;
    }

    public GridSpaceItemDecoration(int space) {
        this(space, space, 4);
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,  RecyclerView parent, RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view); // item position
        int column = position % columnCount; // item column

        outRect.left = horizontalSpace - column * horizontalSpace / columnCount; // spacing - column * ((1f / spanCount) * spacing)
        outRect.right = (column + 1) * horizontalSpace / columnCount; // (column + 1) * ((1f / spanCount) * spacing)

        if (position < columnCount) { // top edge
            outRect.top = verticalSpace;
        }
        outRect.bottom = verticalSpace; // item bottom
    }
}
