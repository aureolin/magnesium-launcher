package co.aureolin.labs.magnesium.fragment.settings;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.view.ActionMode;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import co.aureolin.labs.magnesium.R;
import co.aureolin.labs.magnesium.SettingsActivity;
import co.aureolin.labs.magnesium.adapter.EmailAccountAdapter;
import co.aureolin.labs.magnesium.dialog.EmailAccountDialog;
import co.aureolin.labs.magnesium.model.EmailAccount;
import co.aureolin.labs.magnesium.util.handler.EmailAccountSavedHandler;
import co.aureolin.labs.magnesium.util.Helper;
import co.aureolin.labs.magnesium.util.db.MagnesiumDbContext;

/**
 * Created by akinwale on 14/02/2016.
 */
public class EmailAccountsFragment extends Fragment implements ActionMode.Callback {

    private ActionMode actionMode;

    private boolean deleteInProgress;

    private int numEmailAccounts;

    private List<String> emailAddresses;

    private ListView emailAccountsList;

    private EmailAccountAdapter adapter;

    private FloatingActionButton addButton;

    private View noEmailAccounts;

    public static EmailAccountsFragment newInstance() {
        return new EmailAccountsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_email_accounts, container, false);

        emailAccountsList = (ListView) rootView.findViewById(R.id.email_accounts_list);
        noEmailAccounts = rootView.findViewById(R.id.no_email_accounts_view);

        emailAccountsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                if (actionMode != null) {
                    if (emailAccountsList.getCheckedItemCount() == 0) {
                        actionMode.finish();
                    } else {
                        actionMode.setTitle(String.valueOf(emailAccountsList.getCheckedItemCount()));
                    }
                } else {
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            emailAccountsList.setItemChecked(position, false);
                        }
                    });
                }
            }
        });
        emailAccountsList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, final View view, final int position, long id) {
                SettingsActivity activity = (SettingsActivity) getContext();
                if (activity != null) {
                    if (actionMode != null) {
                        new Handler().post(new Runnable() {
                            @Override
                            public void run() {
                                emailAccountsList.setItemChecked(position, !emailAccountsList.isItemChecked(position));
                                if (emailAccountsList.getCheckedItemCount() == 0) {
                                    actionMode.finish();
                                } else {
                                    actionMode.setTitle(String.valueOf(emailAccountsList.getCheckedItemCount()));
                                }
                            }
                        });
                    } else {
                        //firstSelectedItemPosition = position;
                        actionMode = activity.startSupportActionMode(EmailAccountsFragment.this);
                        emailAccountsList.setItemChecked(position, true);
                        actionMode.setTitle(String.valueOf(emailAccountsList.getCheckedItemCount()));
                    }
                }
                return true;
            }
        });

        addButton = (FloatingActionButton) rootView.findViewById(R.id.email_account_add_button);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EmailAccountDialog dialog = new EmailAccountDialog();
                dialog.setNumEmailAccounts(numEmailAccounts);
                dialog.setExistingEmailAddresses(emailAddresses);
                dialog.setEmailAccountSavedHandler(new EmailAccountSavedHandler() {
                    @Override
                    public void onEmailAccountSaved(EmailAccount account) {
                    loadEmailAccounts();
                    }
                });
                dialog.show(getFragmentManager(), "EmailAccountDialog");

            }
        });

        return rootView;
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        MenuInflater inflater = mode.getMenuInflater();
        inflater.inflate(R.menu.menu_generic_list_select_mode, menu);

        MenuItem editMenuItem = menu.findItem(R.id.action_item_edit);
        editMenuItem.setVisible(false);

        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        if (emailAccountsList != null) {
            for (int i = 0; i < adapter.getCount(); i++) {
                emailAccountsList.setItemChecked(i, false);
            }
        }
        actionMode = null;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_item_delete:
                if (!deleteInProgress) {
                    final List<EmailAccount> accountsToDelete = new ArrayList<EmailAccount>();
                    if (adapter != null && emailAccountsList != null) {
                        List<Integer> positions = new ArrayList<Integer>();
                        SparseBooleanArray checkedPositions = emailAccountsList.getCheckedItemPositions();
                        for (int i = 0; i < adapter.getCount(); i++) {
                            EmailAccount account = adapter.getItem(i);
                            if (checkedPositions.get(i)) {
                                accountsToDelete.add(account);
                            }
                        }
                    }

                    (new AsyncTask<Void, Void, Boolean>() {
                        protected void onPreExecute() {
                            deleteInProgress = true;
                        }
                        protected Boolean doInBackground(Void... params) {
                            SettingsActivity activity = (SettingsActivity) getContext();
                            if (activity != null) {
                                SQLiteDatabase db = activity.getDbContext().getWritableDatabase();
                                try {
                                    for (int i = 0; i < accountsToDelete.size(); i++) {
                                        MagnesiumDbContext.delete(accountsToDelete.get(i), db);
                                    }
                                    return true;
                                } catch (Exception ex) {
                                    return false;
                                }
                            }

                            return false;
                        }
                        protected void onPostExecute(Boolean result) {
                            deleteInProgress = false;
                            Context context = getContext();
                            if (!result) {
                                Helper.showToast("The selection could not be deleted.", context);
                                return;
                            }
                            loadEmailAccounts();
                            Helper.showToast("The selection was successfully deleted.", context);
                            if (actionMode != null) {
                                actionMode.finish();
                            }
                        }
                    }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
                return true;
        }
        return false;
    };

    @Override
    public void onStart() {
        super.onStart();
        setupAdapter();
        loadEmailAccounts();
    }

    private void loadEmailAccounts() {
        (new AsyncTask<Void, Void, List<EmailAccount>>() {
            protected List<EmailAccount> doInBackground(Void... params) {
                SettingsActivity activity = (SettingsActivity) getContext();
                if (activity != null) {
                    SQLiteDatabase db = activity.getDbContext().getReadableDatabase();
                    return MagnesiumDbContext.getEmailAccounts(activity.getEncryptionKey(), db);
                }

                return new ArrayList<EmailAccount>();
            }

            @Override
            protected void onPostExecute(List<EmailAccount> accounts) {
                setupAdapter(accounts, false);
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void setupAdapter() {
        setupAdapter(null, true);
    }

    private void setupAdapter(List<EmailAccount> accounts, boolean initialSetup) {
        boolean isNewAdapter = false;
        if (adapter == null) {
            adapter = new EmailAccountAdapter(getContext());
            emailAccountsList.setAdapter(adapter);
            isNewAdapter = true;
        }
        if (!isNewAdapter) {
            adapter.clear();
        }

        if (accounts != null) {
            if (emailAddresses == null) {
                emailAddresses = new ArrayList<String>();
            }

            numEmailAccounts = 0;
            for (int i = 0; i < accounts.size(); i++) {
                emailAddresses.add(accounts.get(i).getEmailAddress().toLowerCase());
                numEmailAccounts++;
            }
            adapter.addAll(accounts);
        }

        if (!initialSetup) {
            boolean hasAccounts = (adapter != null && adapter.getCount() > 0);
            noEmailAccounts.setVisibility(hasAccounts ? View.INVISIBLE : View.VISIBLE);
        }

        if (!isNewAdapter) {
            adapter.notifyDataSetChanged();
        }
    }
}
