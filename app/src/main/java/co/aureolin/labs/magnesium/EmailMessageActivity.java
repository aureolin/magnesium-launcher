package co.aureolin.labs.magnesium;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LevelListDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import co.aureolin.labs.magnesium.model.FeedItem;
import co.aureolin.labs.magnesium.util.BitmapDiskCache;
import co.aureolin.labs.magnesium.util.Helper;
import co.aureolin.labs.magnesium.util.Http;
import co.aureolin.labs.magnesium.util.db.MagnesiumDbContext;

public class EmailMessageActivity extends AppCompatActivity {

    private static final String URI_PREFIX = "mgemail:";

    private static final String[] supportedHtmlTags = {
        "a", "b", "big", "blockquote", "br", "cite", "dfn", "div", "em",
        "font", "h1", "h2", "h3", "h4", "h5", "h6", "i", "img", "p",
        "small", "strike", "strong", "sub", "sup", "tt", "u"
    };

    private Html.ImageGetter imageGetter;

    private View scrollView;

    private TextView messageNotFoundView;

    private ProgressBar loadProgress;

    private long messageId;

    private boolean messageLoaded;

    private MagnesiumDbContext dbContext;

    private LoadEmailMessageTask loadEmailMessageTask;

    private TextView dateLabelView;

    private TextView dateValueView;

    private TextView titleView;

    private TextView fromLabelView;

    private TextView toLabelView;

    private View ccContainer;

    private TextView ccLabelView;

    private WebView messageView;

    private TextView fromValueView;

    private TextView toValueView;

    private TextView ccValueView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_message);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        dbContext = new MagnesiumDbContext(this);

        scrollView = findViewById(R.id.email_message_scrollview);
        loadProgress = (ProgressBar) findViewById(R.id.email_message_view_load_progress);

        messageNotFoundView = (TextView) findViewById(R.id.email_message_view_not_found);

        ccContainer = findViewById(R.id.email_message_view_cc_meta);
        titleView = (TextView) findViewById(R.id.email_message_view_title);

        dateLabelView = (TextView) findViewById(R.id.email_message_view_date_label);
        fromLabelView = (TextView) findViewById(R.id.email_message_view_from_label);
        toLabelView = (TextView) findViewById(R.id.email_message_view_to_label);
        ccLabelView = (TextView) findViewById(R.id.email_message_view_cc_label);

        dateValueView = (TextView) findViewById(R.id.email_message_view_date_value);
        fromValueView = (TextView) findViewById(R.id.email_message_view_from_value);
        toValueView = (TextView) findViewById(R.id.email_message_view_to_value);
        ccValueView = (TextView) findViewById(R.id.email_message_view_cc_value);

        messageView = (WebView) findViewById(R.id.email_message_view_body);
        messageView.getSettings().setLoadWithOverviewMode(true);
        messageView.getSettings().setUseWideViewPort(true);
        messageView.getSettings().setBuiltInZoomControls(true);
        messageView.getSettings().setDisplayZoomControls(false);
        messageView.setBackgroundColor(Color.TRANSPARENT);
        messageView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                view.setBackgroundColor(Color.TRANSPARENT);
            }
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
                return true;
            }
        });

        TextView[] boldTextViews = {
            dateLabelView, fromLabelView, toLabelView, ccLabelView
        };
        TextView[] normalTextViews = {
            messageNotFoundView, titleView, dateValueView, fromValueView, toValueView, ccValueView
        };
        for (int i = 0; i < boldTextViews.length; i++) {
            boldTextViews[i].setTypeface(Helper.getTypeface("bold", this));
        }
        for (int i = 0; i < normalTextViews.length; i++) {
            normalTextViews[i].setTypeface(Helper.getTypeface("normal", this));
        }
        //messageView.
    }

    public void onDestroy() {
        if (dbContext != null) {
            dbContext.close();
        }
        super.onDestroy();
    }

    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        Uri intentUri = intent.getData();
        if (intentUri == null) {
            finish();
            return;
        }

        String uriString = intentUri.toString();
        messageId = Helper.parseLong(uriString.substring(URI_PREFIX.length()));
        if (messageId <= 0) {
            finish();
            return;
        }
    }

    public void onResume() {
        super.onResume();
        if (!messageLoaded && (loadEmailMessageTask == null || loadEmailMessageTask.getStatus() != AsyncTask.Status.FINISHED)) {
            loadEmailMessageTask = new LoadEmailMessageTask();
            loadEmailMessageTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
        //final ImageView tempImageView = (ImageView) findViewById(R.id.temp_image_view);

        /*if (imageGetter == null) {
            imageGetter = new Html.ImageGetter() {
                @Override
                public Drawable getDrawable(final String source) {
                    LevelListDrawable drawable = new LevelListDrawable();

                    final Context context = HomeActivity.getHomeActivity();
                    if (source != null && source.trim().length() > 0) {
                        // Check if the source key exists in the bitmap cache
                        final String sourceKey = source.replaceAll(":", "_").replaceAll("/", "_").
                                replaceAll("\\?", "_").
                                replaceAll("=", "_").
                                replaceAll("\\.", "-");

                        Bitmap bitmap = BitmapDiskCache.getBitmap(sourceKey);
                        if (bitmap == null) {
                            (new AsyncTask<LevelListDrawable, Void, Bitmap>() {
                                private LevelListDrawable updateDrawable;

                                protected Bitmap doInBackground(LevelListDrawable... params) {
                                    if (params[0] != null) {
                                        updateDrawable = params[0];
                                    }

                                    try {
                                        Bitmap image = Http.getImage(source, context);
                                        if (image != null) {
                                            return Helper.trimBitmap(image);
                                        }
                                        return image;
                                    } catch (Exception ex) {
                                        return null;
                                    }
                                }

                                protected void onPostExecute(Bitmap result) {
                                    if (result != null && updateDrawable != null) {
                                        BitmapDiskCache.putBitmap(sourceKey, result);
                                        BitmapDrawable bmpDrawable = new BitmapDrawable(getResources(), result);
                                        updateDrawable.setBounds(0, 0, bmpDrawable.getMinimumWidth(), bmpDrawable.getMinimumHeight());
                                        updateDrawable.addLevel(0, 0, bmpDrawable);
                                    }
                                    if (messageView != null) {
                                        messageView.invalidate();
                                        messageView.setText(messageView.getText());
                                    }
                                }
                            }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, drawable);
                        } else {
                            BitmapDrawable bmpDrawable = new BitmapDrawable(getResources(), bitmap);
                            drawable.setBounds(0, 0, bmpDrawable.getMinimumWidth(), bmpDrawable.getMinimumHeight());
                            drawable.addLevel(0, 0, drawable);
                            //tempImageView.setImageDrawable(drawable);
                        }
                    }

                    return drawable;
                }
            };
        }*/
    }

    @Override
    public void onBackPressed() {
        handleBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                handleBackPressed();
                return true;
        }
        return false;
    }

    private void handleBackPressed() {
        finish();
    }

    private void displayEmailWithAttachments(FeedItem feedItem) {
        if (feedItem.getEntryTime() == null && feedItem.getCreatedOn() == null) {
            dateValueView.setVisibility(View.GONE);
        } else {
            dateValueView.setVisibility(View.VISIBLE);
            if (feedItem.getEntryTime() != null) {
                dateValueView.setText(Helper.GMAIL_DATE_FORMAT.format(feedItem.getEntryTime()));
            } else if (feedItem.getCreatedOn() != null) {
                dateValueView.setText(Helper.GMAIL_DATE_FORMAT.format(feedItem.getCreatedOn()));
            }
        }


        String author = feedItem.getAuthor();
        fromValueView.setText(!Helper.isEmpty(author) ? author : feedItem.getPosterId());
        toValueView.setText(feedItem.getRecipients());

        String ccRecipients = feedItem.getCcRecipients();
        ccValueView.setText(ccRecipients);
        ccContainer.setVisibility(Helper.isEmpty(ccRecipients) ? View.GONE : View.VISIBLE);

        titleView.setText(feedItem.getTitle());

        String html = feedItem.getContent();
        /*Whitelist whitelist = Whitelist.simpleText();
        whitelist.addTags(supportedHtmlTags);
        //String messageHtml = Jsoup.clean(html, whitelist.basicWithImages().preserveRelativeLinks(true));
        //Spanned spanned = Html.fromHtml(messageHtml, imageGetter, null);*/
        html = String.format("<style type=\"text/css\">html, body { background: transparent }</style>%s", html);
        messageView.loadData(html, "text/html", "utf-8");
    }

    private class LoadEmailMessageTask extends AsyncTask<Void, Void, FeedItem> {
        protected FeedItem doInBackground(Void... params) {
            SQLiteDatabase db = dbContext.getReadableDatabase();
            try {
                return MagnesiumDbContext.getEmailWithAttachments(messageId, db);
            } catch (Exception ex) {
                return null;
            }
        }

        protected void onPostExecute(FeedItem result) {
            loadProgress.setVisibility(View.GONE);

            if (result == null) {
                messageNotFoundView.setVisibility(View.VISIBLE);
                scrollView.setVisibility(View.GONE);
                return;
            }

            messageNotFoundView.setVisibility(View.GONE);
            scrollView.setVisibility(View.VISIBLE);

            messageLoaded = true;
            loadEmailMessageTask = null;

            displayEmailWithAttachments(result);
        }
    }
}
