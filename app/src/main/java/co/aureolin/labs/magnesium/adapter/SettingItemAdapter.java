package co.aureolin.labs.magnesium.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import co.aureolin.labs.magnesium.R;
import co.aureolin.labs.magnesium.model.SettingItem;
import co.aureolin.labs.magnesium.util.Helper;

/**
 * Created by akinwale on 18/02/2016.
 */
public class SettingItemAdapter extends ArrayAdapter<SettingItem> {
    public SettingItemAdapter(Context context) {
        super(context, 0);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_general_setting, null);
        }

        SettingItem item = getItem(position);

        TextView nameView = (TextView) convertView.findViewById(R.id.general_setting_title);
        TextView valueView = (TextView) convertView.findViewById(R.id.general_setting_value);
        TextView annotationView = (TextView) convertView.findViewById(R.id.general_setting_annotation);

        annotationView.setVisibility(item.isOwned() && !item.shouldShowAnnotation() ? View.GONE : View.VISIBLE);

        nameView.setText(item.getName());
        valueView.setText(item.getDisplayValue());
        annotationView.setText(item.getAnnotation());

        return convertView;
    }
}