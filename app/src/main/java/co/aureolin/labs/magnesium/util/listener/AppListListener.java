package co.aureolin.labs.magnesium.util.listener;

/**
 * Created by akinwale on 04/06/2016.
 */
public interface AppListListener {
    public void onAppListInitialised();
}
