package co.aureolin.labs.magnesium.util.handler;

import co.aureolin.labs.magnesium.model.AppItem;

/**
 * Created by akinwale on 21/02/2016.
 */
public interface AppListUninstallClickHandler {
    public void onUninstallClicked(AppItem appItem);

    public void onUninstallLongClicked(AppItem appItem);
}
